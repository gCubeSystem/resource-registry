This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for Resource Registry Service

## [v5.0.0-SNAPSHOT]

- Migrated to Smartgears 4


## [v4.4.0-SNAPSHOT]

- Added support for generic Json Object and Json Array
- Added query parameters to paginate result of queries [#24648]
- Additional fields (schema mixed) can be added to Context [#26102]
- Added state to Context
- Added roles checks to manage Contexts
- Added roles check to add instances to non active Contexts
- Completely refactored JSON Query management [#24163]
- Moved to maven-parent 1.2.0


## [v4.3.0]

- Migrated code to reorganized E/R format [#24992]
- Metadata are added only if requested by the client [#25040]
- Restored Encrypted Property Type and removed Vault [#25041]
- Cleaned RequestInfo in RequestFilter class [#25211]
- Solved bug which allow to create two query template with the same name if a null id was provided [#25650] 


## [v4.2.0]

- Fixed bug on JSONQuery for Facets which does not have any properties to match [#24237]
- Fixed bug on JSONQuery for IsRelatedTo relations indicating both source and target resources [#24264]
- Fixed bug on returned boolean values as string [#24240]
- Enabled array properties [#24225] 
- Using delete in propagation contraint as action indication for delete operation [#24301]
- Fixed default value of propagation constraint of remove action for ConsistsOf to 'cascade' [#24223]
- Removed Encrypted Property Type and added Vault instead [#24655]
- Enhanced gcube-smartgears-bom version


## [v4.1.0] 

- Relation source-target instance types are validated against Relation schema [#7355]
- The instances are validated with the defined schema [#18216]
- Added Types Cache [#18496]
- Superclasses in instances are ordered and does not include internal basic types [#20517]
- Redesigned Sharing REST collection [#20530][#20555] 
- Added the possibility to have a dry run Add/Remove to/from Context [#20530]
- Add/Remove to/from Context return the list of affected instances and not just success or failure code [#20555]
- Generalised the definition of the type of a property [#20516]
- Added type update but the API is not exposed via REST [#20316]
- Prepared Query has been fixed to properly consider polymorphic parameter and provided constraint [#20298]
- Add tests to check the proper behaviour of operation involving Propagation Constraints [#20923]
- The contexts property is not included in header is not requested [#21953]
- When serialising a resource the ConsistsOf does not include source [#22001]
- Getting all the relations of a type it is returned a list of relations and not the list of Resources sources of such relation [#22003]
- Included context names and not only UUIDs in header [#22090]
- Added support for Json Query [#22047]
- Added support for Query Templates [#22091]
- Migrated service to SecretManagerProvider [#22871]
- Fixed bug on results of raw queries [#23662]


## [v4.0.0] [r5.0.0] - 2021-02-04

- Used ContextCache to make the server more efficient
- Added REST APIs to get instance contexts both in Access and Sharing Port Type [#20013]
- Added support to add contexts in instances header for instances safe methods only [#20012]
- Switched JSON management to gcube-jackson [#19116]


## [v3.0.0] [r4.21.0] - 2020-03-30 

- Refactored code to support IS Model reorganization (e.g naming, packages)
- Migrated Code from OrientDB® 2.2.X APIs OrientDB® 3.0.X APIs [#13180]
- Using gxREST defined Methods in place of the ones defined in resource-registry APIs [#11455]
- Added support for Encrypted property [#12812]
- Refactored code to support renaming of Embedded class to Property [#13274]
- Refactored code to avoid to use Jettison library


## [v2.0.0] [r4.13.0] - 2018-11-20

- Removed unneeded dependency (common-smartgears-app)
- Added utility function in ContextUtility class
- Closing Graph Factory before opening a new one [#11672]
- Redesigned REST interface [#11288]


## [v1.7.0] [r4.9.0] - 2017-12-20

- Exposed API to read Context definition in Context port type [#10241]
- Exposed API to read Context definition in Access port type [#10245]
- Exposed API to get the Type Schema in Access port type [#10244]
- Added modifiedBy property in Header [#9999]
- Added reload directive to solve the bug which return the old version after update [#10390]
- Refactored Context API to use HTTP body containing Serialized version of Context instead of URL get parameters [#10216]
- Removed OrientDB® internal properties from Context serialization [#10219]
- Fixed HTTP return content type in case of DELETE Context which caused an error on client even the action success [#10224]
- Added feature to reopen the connection over OrientDB® server if closed [#10221]
- Added API to enumerate all Context in Access port type [#10610]
- Exposed API to enumerate all Context in Context port-type [#10217]
- Added support for hierarchical roles in Contexts to support child Context overview [#10435]
- Using new multiple inheritance support of ISModel to create types when creating new database [#5706]


## [v1.6.0] [r4.7.0] - 2017-10-09

- Added an API to retrieve Resource instances filtering them [#9772]


## [v1.5.0] [r4.6.0] - 2017-07-25

- Improved logging for REST methods
- REST methods override default ServiceUsageRecord 'calledMethod' field to improve accounting performances [#9020]
- Removed List and Set support as workaround for OrientDB® bug [#9021]


## [v1.4.0] [r4.5.0] - 2017-06-07

- Removed unneeded dependencies declaration (Thinkerpop™ Frames)
- Added Context UUID cache
- Added Source Resource in relation including only header. This reflect the behaviour used on Jackson marshalling.
- Implemented possibility to create a relation and target entity with one call.
- Defined custom pattern (defined in is-model) for DateTimeFormat to support timezone. The value is also changed on DB configuration when the database is created.
- Creating an admin with specific username (get from configuration file) instead of the default one 'admin'
- Deprecated properties are not get configuration file anymore
- Defined Connection Strategy to server as global constant of the registry
- Refactored some code for ER creation/update
- Extended Exception management to throw ERAvailableInAnotherContext which is useful for clients


## [v1.3.0] [r4.3.0] - 2017-03-16

- Added API Remove From Context
- Added API to support Entity-Relation navigation
- Added API to get the list of ER types and their own schema
- Added support for Propagation Constraint
- Added ResourceRegistryException serialization
- Full Code Redesign


## [v1.2.0] [r4.2.0] - 2016-12-16

- Added API AddToContext
- Added support for referential Integrity Directive
- Added API to support update Resource (with all facets)


## [v1.1.0] [r4.1.0] - 2016-11-07

- Improved code quality


## [v1.0.0] [r4.0.0] - 2016-07-27

- First Release

