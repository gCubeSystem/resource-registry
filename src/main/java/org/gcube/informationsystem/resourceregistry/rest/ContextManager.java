package org.gcube.informationsystem.resourceregistry.rest;

import java.util.UUID;

import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.PATCH;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;

import org.gcube.informationsystem.contexts.reference.entities.Context;
import org.gcube.informationsystem.resourceregistry.ResourceInitializer;
import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.contexts.ContextNotFoundException;
import org.gcube.informationsystem.resourceregistry.api.request.BaseRequestInfo;
import org.gcube.informationsystem.resourceregistry.api.rest.ContextPath;
import org.gcube.informationsystem.resourceregistry.contexts.ContextUtility;
import org.gcube.informationsystem.resourceregistry.contexts.entities.ContextManagement;
import org.gcube.informationsystem.resourceregistry.rest.requests.ServerRequestInfo;
import org.gcube.smartgears.utils.InnerMethodName;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@Path(ContextPath.CONTEXTS_PATH_PART)
public class ContextManager extends BaseRest {
	
	public static final String CONTEXT_UUID_PATH_PARAMETER = "CONTEXT_UUID";

	public ContextManager() {
		super();
	}
	
	/**
	 * 
	 * GET /contexts
	 * 
	 * It returns the list of contexts as Json Array.
	 * Depending on the role and the requested parameter
	 * the json defining the context could contain
	 * minimal information.
	 * 
	 * Only IS-Manager and Infrastructure-Manager are
	 * allowed to get all the information about contexts such as:
	 * 
	 * - not obfuscated metadata (i.e. createdBy and lastUpdateBy) 
	 * - context state
	 * 
	 * IS-Manager and Infrastructure-Manager get the metadata by default
	 * even not requested.
	 * 
	 * Other users requesting must explicitly request the metadata
	 * to get them but they will always receive obfuscated 
	 * createdBy and lastUpdateBy properties.
	 * 
	 */
	@GET
	@Consumes({MediaType.TEXT_PLAIN, ResourceInitializer.APPLICATION_JSON_CHARSET_UTF_8})
	@Produces(ResourceInitializer.APPLICATION_JSON_CHARSET_UTF_8)
	public String all() throws ContextNotFoundException, ResourceRegistryException {
		logger.info("Requested to read all {}s", Context.NAME);
		setAccountingMethod(Method.LIST, Context.NAME);
		
		ServerRequestInfo serverRequestInfo = initRequestInfo(BaseRequestInfo.DEFAULT_OFFSET, BaseRequestInfo.UNBOUNDED_LIMIT);
		serverRequestInfo.setIncludeMeta(true);
		serverRequestInfo.setAllMeta(true);
		serverRequestInfo.checkLimitOffset();
		
		ContextManagement contextManagement = new ContextManagement();
		return contextManagement.all(false);
	}
	
	/**
	 * GET /contexts/{UUID}
	 * e.g. GET /contexts/c0f314e7-2807-4241-a792-2a6c79ed4fd0
	 * 
	 */
	@GET
	@Path("{" + ContextManager.CONTEXT_UUID_PATH_PARAMETER + "}")
	@Consumes({MediaType.TEXT_PLAIN, ResourceInitializer.APPLICATION_JSON_CHARSET_UTF_8})
	@Produces(ResourceInitializer.APPLICATION_JSON_CHARSET_UTF_8)
	public String read(@PathParam(ContextManager.CONTEXT_UUID_PATH_PARAMETER) String uuid)
			throws ContextNotFoundException, ResourceRegistryException {
		if(uuid.compareTo(ContextPath.CURRENT_CONTEXT_PATH_PART)==0){
			uuid = ContextUtility.getCurrentRequestEnvironment().getUUID().toString();
		}
		logger.info("Requested to read {} with id {} ", Context.NAME, uuid);
		setAccountingMethod(Method.READ, Context.NAME);
		
		ServerRequestInfo serverRequestInfo = initRequestInfo();
		serverRequestInfo.setIncludeMeta(true);
		serverRequestInfo.setAllMeta(true);
		
		ContextManagement contextManagement = new ContextManagement();
		contextManagement.setUUID(UUID.fromString(uuid));
		return contextManagement.readAsString();
	}
	
	/**
	 * PUT /contexts/{UUID}
	 * e.g. PUT /contexts/c0f314e7-2807-4241-a792-2a6c79ed4fd0
	 * 
	 * BODY: {...}
	 * 
	 */
	@PUT
	@Path("{" + ContextManager.CONTEXT_UUID_PATH_PARAMETER + "}")
	@Consumes({MediaType.TEXT_PLAIN, ResourceInitializer.APPLICATION_JSON_CHARSET_UTF_8})
	@Produces(ResourceInitializer.APPLICATION_JSON_CHARSET_UTF_8)
	public String updateCreate(@PathParam(ContextManager.CONTEXT_UUID_PATH_PARAMETER) String uuid, String json)
			throws ResourceRegistryException {
		logger.info("Requested to update/create {} with json {} ", Context.NAME, json);
		setAccountingMethod(Method.UPDATE, Context.NAME);
		
		ServerRequestInfo serverRequestInfo = initRequestInfo();
		serverRequestInfo.setIncludeMeta(true);
		serverRequestInfo.setAllMeta(true);
		
		ContextManagement contextManagement = new ContextManagement();
		contextManagement.setUUID(UUID.fromString(uuid));
		contextManagement.setJson(json);
		return contextManagement.createOrUpdate();
	}
	
	/**
	 * PATCH /contexts/{UUID}
	 * e.g. PATCH /contexts/c0f314e7-2807-4241-a792-2a6c79ed4fd0
	 * 
	 * BODY: {...}
	 * 
	 * The body contains the Contexts JSon representation
	 * only the state property will be evaluated in this 
	 * method
	 * 
	 */
	@PATCH
	@Path("{" + ContextManager.CONTEXT_UUID_PATH_PARAMETER + "}")
	@Consumes({MediaType.TEXT_PLAIN, ResourceInitializer.APPLICATION_JSON_CHARSET_UTF_8})
	public String changeState(@PathParam(ContextManager.CONTEXT_UUID_PATH_PARAMETER) String uuid, String json)
			throws ResourceRegistryException {
		logger.info("Requested to activate {} with UUID {}", Context.NAME, uuid);
		setAccountingMethod(Method.UPDATE, Context.NAME);
		
		ServerRequestInfo serverRequestInfo = initRequestInfo();
		serverRequestInfo.setIncludeMeta(true);
		serverRequestInfo.setAllMeta(true);
		
		ContextManagement contextManagement = new ContextManagement();
		contextManagement.setJson(json);
		contextManagement.setUUID(UUID.fromString(uuid));
		return contextManagement.changeState();
	}
	
	/*
	 * DELETE /contexts/{UUID}
	 * e.g. DELETE /contexts/c0f314e7-2807-4241-a792-2a6c79ed4fd0
	 */
	@DELETE
	@Consumes({MediaType.TEXT_PLAIN, ResourceInitializer.APPLICATION_JSON_CHARSET_UTF_8})
	@Path("{" + ContextManager.CONTEXT_UUID_PATH_PARAMETER + "}")
	public Response delete(@PathParam(ContextManager.CONTEXT_UUID_PATH_PARAMETER) String uuid)
			throws ContextNotFoundException, ResourceRegistryException {
		logger.info("Requested to delete {} with id {} ", Context.NAME, uuid);
		InnerMethodName.set("deleteContext");
		
		ContextManagement contextManagement = new ContextManagement();
		contextManagement.setUUID(UUID.fromString(uuid));
		contextManagement.delete();
		
		return Response.status(Status.NO_CONTENT).build();
	}
	
}
