package org.gcube.informationsystem.resourceregistry.rest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DefaultValue;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.HEAD;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;
import jakarta.ws.rs.core.UriInfo;

import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.queries.templates.reference.entities.QueryTemplate;
import org.gcube.informationsystem.resourceregistry.ResourceInitializer;
import org.gcube.informationsystem.resourceregistry.api.exceptions.AvailableInAnotherContextException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.NotFoundException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.contexts.ContextNotFoundException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.queries.InvalidQueryException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.types.SchemaNotFoundException;
import org.gcube.informationsystem.resourceregistry.api.request.BaseRequestInfo;
import org.gcube.informationsystem.resourceregistry.api.rest.AccessPath;
import org.gcube.informationsystem.resourceregistry.api.rest.ContextPath;
import org.gcube.informationsystem.resourceregistry.api.rest.InstancePath;
import org.gcube.informationsystem.resourceregistry.api.rest.TypePath;
import org.gcube.informationsystem.resourceregistry.base.ElementManagement;
import org.gcube.informationsystem.resourceregistry.contexts.ContextUtility;
import org.gcube.informationsystem.resourceregistry.contexts.entities.ContextManagement;
import org.gcube.informationsystem.resourceregistry.instances.model.ERManagementUtility;
import org.gcube.informationsystem.resourceregistry.instances.model.entities.ResourceManagement;
import org.gcube.informationsystem.resourceregistry.queries.Query;
import org.gcube.informationsystem.resourceregistry.queries.QueryImpl;
import org.gcube.informationsystem.resourceregistry.queries.json.JsonQuery;
import org.gcube.informationsystem.resourceregistry.queries.templates.QueryTemplateManagement;
import org.gcube.informationsystem.resourceregistry.rest.requests.ServerRequestInfo;
import org.gcube.informationsystem.resourceregistry.types.TypeManagement;
import org.gcube.informationsystem.types.TypeMapper;
import org.gcube.informationsystem.types.reference.Type;

import com.orientechnologies.orient.core.record.ODirection;

/**
 * <p>Access class provides RESTful endpoints for accessing, i.e. reading, listing and verifying the existence of:</p>
 * <ul>
 * 	<li>
 * 		<p>contexts</p>
 * 		<ul>
 *   		<li>List Contexts: GET /access/contexts</li>
 *   		<li>Read a Context: GET /access/contexts/{CONTEXT_UUID}</li>
 * 		</ul>
 * 	</li>
 * 	<li>
 * 		<p>types</p>
 * 		<ul>
 * 			<li>Read a Type: GET /access/types/{TYPE_NAME}[?polymorphic=false]</li>
 * 		</ul>
 * 	</li>
 * 	<li>
 * 		<p>query templates</p>
 * 		<ul>
 * 			<li>List Query Templates: GET /access/query-templates</li>
 * 			<li>Read a Query Template: GET /access/query-templates/{QUERY_TEMPLATE_NAME}</li>
 * 		</ul>
 * 	</li>
 * 	<li>
 * 		<p>instances</p>
 * 		<ul>
 * 			<li>List Instances: GET /access/instances/{TYPE_NAME}[?polymorphic=true]</li>
 * 			<li>Check if an Instance Exists: HEAD /access/instances/{TYPE_NAME}/{UUID}</li>
 * 			<li>Read an Instance: GET /access/instances/{TYPE_NAME}/{UUID}</li>
 * 			<li>Get Contexts of an Instance: GET /access/instances/{TYPE_NAME}/{UUID}/contexts</li>
 * 		</ul>
 * 	</li> 
 * </ul>
 * 
 * <p>Moreover, it provides endpoints for:</p> 
 * <ul>
 * 	<li>Run a Prepared Query: GET /access/query/{RESOURCE_TYPE_NAME}/{RELATION_TYPE_NAME}/{ENTITY_TYPE_NAME}[?_reference={REFERENCE_ENTITY_UUID}&_polymorphic=true&_direction=out]</li>
 * 	<li>Run a JSON Query: POST /access/query</li>
 * 	<li>Run a Query Template: POST /access/query-templates/{QUERY_TEMPLATE_NAME}</li>
 * 	<li>
 * 		<p>Run a Graph Query: GET /access/query?q=SELECT FROM V</p>
 * 	</li>
 * </ul>
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@Path(AccessPath.ACCESS_PATH_PART)
public class Access extends BaseRest {
	
	/**
	 * Constant representing the resource type name path parameter.
	 */
	public static final String RESOURCE_TYPE_PATH_PARAMETER = "RESOURCE_TYPE_NAME";
	/**
	 * Constant representing the relation type name path parameter.
	 */
	public static final String RELATION_TYPE_PATH_PARAMETER = "RELATION_TYPE_NAME";
	/**
	 * Constant representing the reference type name path parameter.
	 */
	public static final String REFERENCE_TYPE_PATH_PARAMETER = "REFERENCE_TYPE_NAME";
	
	/**
	 * Constant representing the method name for executing raw queries.
	 */
	public static final String RAW_QUERY_METHOD = "raw";
	/**
	 * Constant representing the method name for executing graph queries.
	 */
	public static final String GRAPH_QUERY_METHOD = "graph";
	/**
	 * Constant representing the method name for executing json queries.
	 */
	public static final String JSON_QUERY_METHOD = "json";
	/**
	 * Constant representing the method name for executing prepared queries.
	 */
	public static final String PREPARED_QUERY_METHOD = "prepared";
	
	/**
	 * Default constructor for the Access class.
	 * Initializes a new instance of the Access class.
	 */
	public Access() {
		super();
	}
	
	/**
	 * <p>Retrieves all contexts.</p>
	 * 
	 * <p>This method handles HTTP GET requests to the specified path and returns a JSON 
	 * representation of all contexts.</p>
	 * 
	 * @return A JSON string representing all contexts.
	 * @throws ResourceRegistryException if an error occurs while retrieving the contexts.
	 */
	// GET /access/contexts
	@GET
	@Path(AccessPath.CONTEXTS_PATH_PART)
	@Produces(ResourceInitializer.APPLICATION_JSON_CHARSET_UTF_8)
	public String getAllContexts() throws ResourceRegistryException {
		logger.info("Requested to read all {}s", org.gcube.informationsystem.contexts.reference.entities.Context.NAME);
		setAccountingMethod(Method.LIST, org.gcube.informationsystem.contexts.reference.entities.Context.NAME);
		
		ServerRequestInfo serverRequestInfo = initRequestInfo(BaseRequestInfo.DEFAULT_OFFSET, BaseRequestInfo.UNBOUNDED_LIMIT);
		/*
		 * We never provide meta information 
		 * for context via this REST API.
		 * Contexts collection should be used instead.
		 */
		serverRequestInfo.setIncludeMeta(false);
		serverRequestInfo.setAllMeta(false);
		/*
		 * serverRequestInfo.checkBooleanQueryParameter(ContextPath.INCLUDE_META_QUERY_PARAMETER);
		 * serverRequestInfo.setAllMeta(true);
		 */
		
		serverRequestInfo.checkLimitOffset();
		
		ContextManagement contextManagement = new ContextManagement();
		return contextManagement.all(false);
	}
	
	/**
	 * <p>Retrieves the context information for the context with the specified UUID.</p>
	 *
	 * @pathExample GET /access/contexts/c0f314e7-2807-4241-a792-2a6c79ed4fd0
	 * 
	 * @param uuid the UUID of the context to retrieve. If the UUID matches the current context path part, 
	 *             it will be replaced with the UUID of the current request environment.
	 * @return a JSON string representation of the context.
	 * @throws ContextNotFoundException if the context with the specified UUID is not found.
	 * @throws ResourceRegistryException if there is an error accessing the resource registry.
	 */
	// GET /access/contexts/{CONTEXT_UUID}
	@GET
	@Path(AccessPath.CONTEXTS_PATH_PART + "/{" + InstancesManager.UUID_PATH_PARAMETER + "}")
	@Produces(ResourceInitializer.APPLICATION_JSON_CHARSET_UTF_8)
	public String getContext(@PathParam(InstancesManager.UUID_PATH_PARAMETER) String uuid)
			throws ContextNotFoundException, ResourceRegistryException {
		if(uuid.compareTo(ContextPath.CURRENT_CONTEXT_PATH_PART)==0){
			uuid = ContextUtility.getCurrentRequestEnvironment().getUUID().toString();
		}
		logger.info("Requested to read {} with id {} ", org.gcube.informationsystem.contexts.reference.entities.Context.NAME, uuid);
		setAccountingMethod(Method.READ, org.gcube.informationsystem.contexts.reference.entities.Context.NAME);
		
		ServerRequestInfo serverRequestInfo = initRequestInfo();
		/*
		 * We have to think if never provide meta information 
		 * for context via this REST API.
		 * Contexts collection should be used instead.
		 */
		serverRequestInfo.setAllMeta(true);
		serverRequestInfo.checkBooleanQueryParameter(ContextPath.INCLUDE_META_QUERY_PARAMETER);
		
		ContextManagement contextManagement = new ContextManagement();
		contextManagement.setUUID(UUID.fromString(uuid));
		return contextManagement.readAsString();
	}
	
	/**
	 * <p>Retrieves the schema for a specified type.</p>
	 *
	 * @pathExample /access/types/Facet?polymorphic=true
	 * 
	 * @param type the type for which the schema is requested
	 * @param polymorphic a boolean indicating whether to include polymorphic types
	 * @return a JSON string representing the type definitions
	 * @throws SchemaNotFoundException if the schema for the specified type is not found
	 * @throws ResourceRegistryException if there is an error retrieving the type definitions
	 * 
	 */
	// GET /access/types/{TYPE_NAME}[?polymorphic=false]
	@GET
	@Path(AccessPath.TYPES_PATH_PART + "/{" + TypeManager.TYPE_PATH_PARAMETER + "}")
	@Produces(ResourceInitializer.APPLICATION_JSON_CHARSET_UTF_8)
	public String getType(@PathParam(TypeManager.TYPE_PATH_PARAMETER) String type,
			@QueryParam(TypePath.POLYMORPHIC_QUERY_PARAMETER) @DefaultValue("false") Boolean polymorphic)
			throws SchemaNotFoundException, ResourceRegistryException {
		logger.info("Requested Schema for type {}", type);
		setAccountingMethod(Method.READ, Type.NAME);
		
		ServerRequestInfo serverRequestInfo = initRequestInfo();
		serverRequestInfo.setAllMeta(true);
		serverRequestInfo.checkBooleanQueryParameter(TypePath.INCLUDE_META_QUERY_PARAMETER);
		
		TypeManagement typeManagement = new TypeManagement();
		typeManagement.setTypeName(type);
		List<Type> types = typeManagement.read(polymorphic);
		try {
			return TypeMapper.serializeTypeDefinitions(types);
		}catch (Exception e) {
			throw new ResourceRegistryException(e);
		}
	}
		
	/*
	 * GET /access/query-templates
	 */
	@GET
	@Path(AccessPath.QUERY_TEMPLATES_PATH_PART)
	@Consumes({MediaType.TEXT_PLAIN, ResourceInitializer.APPLICATION_JSON_CHARSET_UTF_8})
	@Produces(ResourceInitializer.APPLICATION_JSON_CHARSET_UTF_8)
	public String allQueryTemplates() throws NotFoundException, ResourceRegistryException {
		logger.info("Requested to read all {}s", QueryTemplate.NAME);
		setAccountingMethod(Method.LIST, QueryTemplate.NAME);
		
		ServerRequestInfo serverRequestInfo = initRequestInfo();
		serverRequestInfo.setAllMeta(true);
		serverRequestInfo.checkBooleanQueryParameter(ContextPath.INCLUDE_META_QUERY_PARAMETER);
		serverRequestInfo.checkLimitOffset();
		
		QueryTemplateManagement queryTemplateManagement = new QueryTemplateManagement();
		return queryTemplateManagement.all(false);
	}
	
	/*
	 * GET /access/query-templates/{QUERY_TEMPLATE_NAME}
	 * e.g. GET /access/query-templates/GetAllEServiceWithState
	 */
	@GET
	@Path(AccessPath.QUERY_TEMPLATES_PATH_PART + "/{" + QueryTemplateManager.QUERY_TEMPLATE_NAME_PATH_PARAMETER + "}")
	@Produces(ResourceInitializer.APPLICATION_JSON_CHARSET_UTF_8)
	public String readQueryTemplate(@PathParam(QueryTemplateManager.QUERY_TEMPLATE_NAME_PATH_PARAMETER) String queryTemplateName)
			throws NotFoundException, ResourceRegistryException {
		logger.info("Requested {} with name", QueryTemplate.NAME, queryTemplateName);
		setAccountingMethod(Method.READ, QueryTemplate.NAME);
		
		ServerRequestInfo serverRequestInfo = initRequestInfo();
		serverRequestInfo.setAllMeta(true);
		serverRequestInfo.checkBooleanQueryParameter(ContextPath.INCLUDE_META_QUERY_PARAMETER);
		
		QueryTemplateManagement queryTemplateManagement = new QueryTemplateManagement();
		queryTemplateManagement.setName(queryTemplateName);
		return queryTemplateManagement.read();
	}
	
	/*
	 * POST /access/query-templates/{QUERY_TEMPLATE_NAME}
	 * e.g. POST /access/query-templates/GetAllEServiceWithState
	 * 
	 * params = { "$state" : "ready" }
	 * 
	 */
	@POST
	@Path(AccessPath.QUERY_TEMPLATES_PATH_PART + "/{" + QueryTemplateManager.QUERY_TEMPLATE_NAME_PATH_PARAMETER + "}")
	@Produces(ResourceInitializer.APPLICATION_JSON_CHARSET_UTF_8)
	public String runQueryTemplate(@PathParam(QueryTemplateManager.QUERY_TEMPLATE_NAME_PATH_PARAMETER) String queryTemplateName, String params)
			throws NotFoundException, InvalidQueryException, ResourceRegistryException {
		logger.info("Requested {} with name", QueryTemplate.NAME, queryTemplateName);
		setAccountingMethod(Method.RUN, QueryTemplate.NAME);
		
		ServerRequestInfo serverRequestInfo = initRequestInfo();
		serverRequestInfo.checkAllBooleanQueryParameters();
		
		QueryTemplateManagement queryTemplateManagement = new QueryTemplateManagement();
		queryTemplateManagement.setName(queryTemplateName);
		queryTemplateManagement.setParams(params);
		return queryTemplateManagement.run();
	}
	
	
	/*
	 * GET /access/instances/{TYPE_NAME}[?polymorphic=true]
	 * e.g. GET /access/instances/ContactFacet?polymorphic=true
	 * 
	 */
	@GET
	@Path(AccessPath.INSTANCES_PATH_PART + "/{" + TypeManager.TYPE_PATH_PARAMETER + "}")
	@Produces(ResourceInitializer.APPLICATION_JSON_CHARSET_UTF_8)
	public String getAllInstances(@PathParam(TypeManager.TYPE_PATH_PARAMETER) String type,
			@QueryParam(InstancePath.POLYMORPHIC_QUERY_PARAMETER) @DefaultValue("true") Boolean polymorphic)
			throws NotFoundException, ResourceRegistryException {
		logger.info("Requested all {}instances of {}", polymorphic ? InstancePath.POLYMORPHIC_QUERY_PARAMETER + " " : "", type);
		setAccountingMethod(Method.LIST, InstancesManager.INSTANCE);
		
		ServerRequestInfo serverRequestInfo = initRequestInfo();
		serverRequestInfo.checkAllBooleanQueryParameters();
		serverRequestInfo.checkLimitOffset();
		
		ElementManagement<?,?> erManagement = ERManagementUtility.getERManagement(type);
		return erManagement.all(polymorphic);
	}
	
	/*
	 * HEAD /access/instances/{TYPE_NAME}/{UUID}
	 * e.g. HEAD /access/instances/ContactFacet/4023d5b2-8601-47a5-83ef-49ffcbfc7d86
	 * 
	 */
	@HEAD
	@Path(AccessPath.INSTANCES_PATH_PART + "/{" + TypeManager.TYPE_PATH_PARAMETER + "}" + "/{" + InstancesManager.UUID_PATH_PARAMETER + "}")
	@Produces(ResourceInitializer.APPLICATION_JSON_CHARSET_UTF_8)
	public Response instanceExists(@PathParam(TypeManager.TYPE_PATH_PARAMETER) String type,
			@PathParam(InstancesManager.UUID_PATH_PARAMETER) String uuid) throws NotFoundException, ResourceRegistryException {
		logger.info("Requested to check if {} with id {} exists", type, uuid);
		setAccountingMethod(Method.EXIST, InstancesManager.INSTANCE);
		
		ServerRequestInfo serverRequestInfo = initRequestInfo();
		serverRequestInfo.checkBooleanQueryParameter(InstancePath.HIERARCHICAL_MODE_QUERY_PARAMETER);
		
		ElementManagement<?,?> erManagement = ERManagementUtility.getERManagement(type);
		
		try {
			erManagement.setUUID(UUID.fromString(uuid));
			boolean found = erManagement.exists();
			if(found) {
				return Response.status(Status.NO_CONTENT).build();
			} else {
				// This code should never be reached due to exception management
				// anyway adding it for safety reason
				return Response.status(Status.NOT_FOUND).build();
			}
		} catch(NotFoundException e) {
			return Response.status(Status.NOT_FOUND).build();
		} catch(AvailableInAnotherContextException e) {
			return Response.status(Status.FORBIDDEN).build();
		} catch(ResourceRegistryException e) {
			throw e;
		}
	}
	
	/*
	 * GET /access/instances/{TYPE_NAME}/{UUID}
	 * e.g. GET /access/instances/ContactFacet/4023d5b2-8601-47a5-83ef-49ffcbfc7d86
	 * 
	 */
	@GET
	@Path(AccessPath.INSTANCES_PATH_PART + "/{" + TypeManager.TYPE_PATH_PARAMETER + "}" + "/{" + InstancesManager.UUID_PATH_PARAMETER + "}")
	@Produces(ResourceInitializer.APPLICATION_JSON_CHARSET_UTF_8)
	public String getInstance(@PathParam(TypeManager.TYPE_PATH_PARAMETER) String type,
			@PathParam(InstancesManager.UUID_PATH_PARAMETER) String uuid) throws NotFoundException, ResourceRegistryException {
		logger.info("Requested to read {} with id {}", type, uuid);
		setAccountingMethod(Method.READ, InstancesManager.INSTANCE);
		
		ServerRequestInfo serverRequestInfo = initRequestInfo();
		serverRequestInfo.checkAllBooleanQueryParameters();
		
		@SuppressWarnings("rawtypes")
		ElementManagement erManagement = ERManagementUtility.getERManagement(type);
		
		erManagement.setElementType(type);
		erManagement.setUUID(UUID.fromString(uuid));
		return erManagement.read().toString();
	}

	/*
	 * GET /access/instances/{TYPE_NAME}/{UUID}/contexts
	 * e.g. GET /access/instances/ContactFacet/4023d5b2-8601-47a5-83ef-49ffcbfc7d86/contexts
	 * 
	 */
	@GET
	@Path(AccessPath.INSTANCES_PATH_PART + "/{" + TypeManager.TYPE_PATH_PARAMETER + "}" + "/{" + InstancesManager.UUID_PATH_PARAMETER + "}/" + AccessPath.CONTEXTS_PATH_PART)
	@Produces(ResourceInitializer.APPLICATION_JSON_CHARSET_UTF_8)
	public String getInstanceContexts(@PathParam(TypeManager.TYPE_PATH_PARAMETER) String type,
			@PathParam(InstancesManager.UUID_PATH_PARAMETER) String instanceId) throws NotFoundException, ResourceRegistryException {
		logger.info("Requested to get contexts of {} with UUID {}", type, instanceId);
		setAccountingMethod(InstancesManager.GET_INSTANCE_CONTEXTS_METHOD);
		
		ElementManagement<?,?> erManagement = ERManagementUtility.getERManagement(type);
		erManagement.setUUID(UUID.fromString(instanceId));
		return erManagement.getContexts();
	}
	
	
	/**
	 * It includeSubtypes to query Entities and Relations in the current Context.<br />
	 * It accepts idempotent query only.. <br />
	 * <br />
	 * For query syntax please refer to<br />
	 * 
	 * <a href="https://orientdb.com/docs/last/SQL-Syntax.html" target="_blank">
	 * https://orientdb.com/docs/last/SQL-Syntax.html </a> <br />
	 * <br />
	 * 
	 * e.g. GET /access/query?q=SELECT FROM V
	 * 
	 * It is responsibility of the client impose manage paginated results
	 * according the SQL syntax (see SKIP and LIMIT parameters in the documentation indicated above)
	 * 
	 * @param query  Defines the query to send to the backend.
	 * @param raw request a raw response (not a Element based response)
	 * @return The JSON representation of the result
	 * @throws InvalidQueryException if the query is invalid or not idempotent
	 */
	@GET
	@Path(AccessPath.QUERY_PATH_PART)
	@Produces(ResourceInitializer.APPLICATION_JSON_CHARSET_UTF_8)
	public String graphQuery(@QueryParam(AccessPath.Q_QUERY_PARAMETER) String query,
			@QueryParam(AccessPath.RAW_QUERY_PARAMETER) @DefaultValue(AccessPath.RAW_QUERY_PARAMETER_DEFAULT_VALUE) Boolean raw)
			throws InvalidQueryException {
		logger.info("Requested query (Raw {}):\n{}", raw, query);
		if(raw) {
			setAccountingMethod(Method.QUERY, RAW_QUERY_METHOD);
		}else {
			setAccountingMethod(Method.QUERY, GRAPH_QUERY_METHOD);
		}
		
		ServerRequestInfo serverRequestInfo = initRequestInfo();
		if(raw) {
			// TODO Check if the role allow to request raw data 
			serverRequestInfo.checkBooleanQueryParameter(InstancePath.HIERARCHICAL_MODE_QUERY_PARAMETER);
		}else {
			serverRequestInfo.checkAllBooleanQueryParameters();
		}
		
		Query queryManager = new QueryImpl();
		return queryManager.query(query, raw);
	}
	
	/**
	 * Handles a POST request to perform a JSON query.

	 * POST /access/query
	 * 
	 * Content Body example:
	 * 
	 * {
	 * 	"": "EService",
	 * 	"consistsOf": [
	 *		{
	 * 			"": "ConsistsOf",
	 * 			"propagationConstraint" : {
	 * 				"add": "propagate"
	 * 			},
	 * 			"target": {
	 * 				"": "StateFacet",
	 * 				"value": "down"
	 * 			}
	 * 		},
	 * 		{
	 * 			"": "IsIdentifiedBy",
	 * 			"target": {
	 * 				"": "SoftwareFacet",
	 * 				"name": "data-transfer-service",
	 * 				"group": "DataTransfer"
	 * 			}
	 * 		},
	 * 		{
	 * 			"": "ConsistsOf",
	 * 			"target": {
	 * 				"": "AccessPointFacet",
	 * 				"endpoint": "http://pc-frosini.isti.cnr.it:8080/data-transfer-service/gcube/service"
	 * 			}
	 * 		}
	 * 	]
	 * }
	 * 
	 * 
	 * @param jsonQuery the JSON query string to be processed
	 * @return the result of the JSON query as a string
	 * @throws InvalidQueryException if the provided query is invalid
	 * @throws ResourceRegistryException if there is an error in the resource registry
	 */
	@POST
	@Path(AccessPath.QUERY_PATH_PART)
	public String jsonQuery(String jsonQuery) throws InvalidQueryException, ResourceRegistryException {
		logger.info("Requested json query \n{}", jsonQuery);
		setAccountingMethod(Method.QUERY, JSON_QUERY_METHOD);
		
		ServerRequestInfo serverRequestInfo = initRequestInfo();
		serverRequestInfo.checkAllBooleanQueryParameters();
		serverRequestInfo.checkLimitOffset();
		
		JsonQuery jsonQueryManager = new JsonQuery();
		jsonQueryManager.setJsonQuery(jsonQuery);
		return jsonQueryManager.query();
	}
	
	/*
	 * /access/query/{RESOURCE_TYPE_NAME}/{RELATION_TYPE_NAME}/{ENTITY_TYPE_NAME}[?_reference={REFERENCE_ENTITY_UUID}&_polymorphic=true&_direction=out]
	 * 
	 * e.g.
	 * All the EService identified By a SoftwareFacet : 
	 * GET /access/query/EService/isIdentifiedBy/SoftwareFacet?_polymorphic=true&_direction=out
	 * 
	 * All the EService identified By the SoftwareFacet with UUID 7bc997c3-d005-40ff-b9ed-c4b6a35851f1 :
	 * GET /access/query/EService/isIdentifiedBy/SoftwareFacet?_reference=7bc997c3-d005-40ff-b9ed-c4b6a35851f1&_polymorphic=true&_direction=out
	 * 
	 * All the Resources identified By a ContactFacet : 
	 * GET /access/query/Resource/isIdentifiedBy/ContactFacet?_polymorphic=true&_direction=out
	 * 
	 * All the Resources with a ContactFacet : 
	 * /access/query/Resource/ConsistsOf/ContactFacet?_polymorphic=true&_direction=out
	 * 
	 * All the EService having an incoming (IN) Hosts relation with an HostingNode (i.e. all smartgears services)
	 * GET /access/query/EService/Hosts/HostingNode?_polymorphic=true&_direction=in
	 * 
	 * All the EService having an incoming (IN) Hosts relation (i.e. hosted by) the HostingNode with UUID 
	 * 16032d09-3823-444e-a1ff-a67de4f350a 
	 * 	 * GET /access/query/EService/Hosts/HostingNode?_reference=16032d09-3823-444e-a1ff-a67de4f350a8&_polymorphic=true&_direction=in
	 * 
	 */
	@SuppressWarnings({"rawtypes"})
	@GET
	@Path(AccessPath.QUERY_PATH_PART + "/" + "{" + Access.RESOURCE_TYPE_PATH_PARAMETER + "}" + "/" + "{"
			+ Access.RELATION_TYPE_PATH_PARAMETER + "}" + "/" + "{" + Access.REFERENCE_TYPE_PATH_PARAMETER + "}")
	@Produces(ResourceInitializer.APPLICATION_JSON_CHARSET_UTF_8)
	public String getAllResourcesHavingFacet(@PathParam(Access.RESOURCE_TYPE_PATH_PARAMETER) String resourcetype,
			@PathParam(Access.RELATION_TYPE_PATH_PARAMETER) String relationType,
			@PathParam(Access.REFERENCE_TYPE_PATH_PARAMETER) String referenceType,
			@QueryParam(AccessPath._REFERENCE_QUERY_PARAMETER) String reference,
			@QueryParam(AccessPath._POLYMORPHIC_QUERY_PARAMETER) @DefaultValue("false") Boolean polymorphic,
			@QueryParam(AccessPath._DIRECTION_QUERY_PARAMETER) @DefaultValue("out") String direction,
			/*@QueryParam(AccessPath._INCLUDE_RELATION_PARAM) @DefaultValue("false") Boolean includeRelation,*/
			@Context UriInfo uriInfo) throws ResourceRegistryException {
		
		logger.info("Requested {} instances having a(n) {} ({}={}} with {} ({}={}). Request URI is {})", resourcetype, relationType,
				AccessPath._DIRECTION_QUERY_PARAMETER, direction, referenceType, AccessPath._POLYMORPHIC_QUERY_PARAMETER, polymorphic, uriInfo.getRequestUri());
		
		setAccountingMethod(Method.QUERY, PREPARED_QUERY_METHOD);
		
		ServerRequestInfo serverRequestInfo = initRequestInfo();
		serverRequestInfo.checkAllBooleanQueryParameters();
		serverRequestInfo.checkLimitOffset();
		
		ElementManagement erManagement = ERManagementUtility.getERManagement(resourcetype);
		
		if(erManagement instanceof ResourceManagement management) {
			UUID refereceUUID = null;
			ODirection directionEnum = ODirection.OUT;
			
			Map<String,String> constraint = new HashMap<>();
			
			MultivaluedMap<String,String> multivaluedMap = uriInfo.getQueryParameters();
			for(String key : multivaluedMap.keySet()) {
				switch (key) {
					case AccessPath._POLYMORPHIC_QUERY_PARAMETER:
						break;
						
					case AccessPath._DIRECTION_QUERY_PARAMETER:
						break;
					
					case AccessPath._REFERENCE_QUERY_PARAMETER:
						break;
					
					/*
					case AccessPath._INCLUDE_RELATION_PARAM:
						break;
					*/
					
					case "gcube-token":
						break;
						
					case "gcube-scope":
						break;
	
					default:
						constraint.put(key, multivaluedMap.getFirst(key));
						break;
				}
			}
			
			if(reference != null) {
				try {
					refereceUUID = UUID.fromString(reference);
				} catch(Exception e) {
					String error = "%s is not a valid %s".formatted(reference, UUID.class.getSimpleName());
					throw new InvalidQueryException(error);
				}
			}
			try {
				directionEnum = ODirection.valueOf(direction.toUpperCase());
			} catch(Exception e) {
				String error = "%s is not a valid. Allowed values are %s".formatted(direction, ODirection.values());
				throw new InvalidQueryException(error);
			}
			
			return management.query(relationType, referenceType, refereceUUID, directionEnum,
					polymorphic, constraint); /*, includeRelation*/
		}
		
		String error = "%s is not a %s type".formatted(resourcetype, Resource.NAME);
		throw new InvalidQueryException(error);
	}
	
}