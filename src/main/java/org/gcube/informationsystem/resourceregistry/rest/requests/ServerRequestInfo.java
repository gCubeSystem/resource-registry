package org.gcube.informationsystem.resourceregistry.rest.requests;

import java.util.List;

import jakarta.ws.rs.core.UriInfo;

import org.gcube.informationsystem.resourceregistry.api.request.BaseRequestInfo;
import org.gcube.informationsystem.resourceregistry.api.rest.InstancePath;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class ServerRequestInfo extends BaseRequestInfo {
	
	protected static Logger logger = LoggerFactory.getLogger(ServerRequestInfo.class);
	
	protected UriInfo uriInfo;
	
	public ServerRequestInfo() {
		super();
		this.uriInfo = null;
	}
	
	public ServerRequestInfo(int offset, int limit) {
		super(offset, limit);
		this.uriInfo = null;
	}
	
	public UriInfo getUriInfo() {
		return uriInfo;
	}

	/**
	 * -check if the user is allowed to set such a request
	 * @param queryParameterKey requested query parameter
	 * @return true is the user is allowed to set such a request
	 */
	public boolean isAllowed(String queryParameterKey) {
		switch (queryParameterKey) {
			case InstancePath.INCLUDE_META_QUERY_PARAMETER:
				// TODO check is the user has the role to request such parameter
				return true;
			
			case InstancePath.INCLUDE_META_IN_ALL_INSTANCES_QUERY_PARAMETER:
				// TODO check is the user has the role to request such parameter
				return true;
				
			case InstancePath.INCLUDE_CONTEXTS_QUERY_PARAMETER:
				// TODO check is the user has the role to request such parameter
				return true;
	
			case InstancePath.HIERARCHICAL_MODE_QUERY_PARAMETER:
				// TODO check is the user has the role to request such parameter
				return true;
			
			case InstancePath.LIMIT_QUERY_PARAMETER:
				// TODO check is the user has the role to request such parameter
				return true;
	
			case InstancePath.OFFSET_QUERY_PARAMETER:
				// TODO check is the user has the role to request such parameter
				return true;
				
			default:
				break;
		}
		
		return true;
	}
	
	/**
	 * Set the parameter if the user is allowed otherwise the default is maintained 
	 * @param queryParameterKey requested query parameter
	 * @param bool the value to set
	 * @param forceAllowed force the value and skip the isAllowed check
	 * @return the value of variable corresponding the request parameter 
	 * independently if the value has been set.
	 */
	public boolean setIfAllowed(String queryParameterKey, boolean bool, boolean forceAllowed) {

		switch (queryParameterKey) {
			case InstancePath.INCLUDE_META_QUERY_PARAMETER:
				if(forceAllowed || isAllowed(queryParameterKey)) {
					includeMeta = bool;
				}
				return includeMeta;
			
			case InstancePath.INCLUDE_META_IN_ALL_INSTANCES_QUERY_PARAMETER:
				if(forceAllowed || isAllowed(queryParameterKey)) {
					allMeta =  bool;
				}
				return allMeta;
				
			case InstancePath.INCLUDE_CONTEXTS_QUERY_PARAMETER:
				if(forceAllowed || isAllowed(queryParameterKey)) {
					includeContexts = bool;
				}
				return includeContexts;
	
			case InstancePath.HIERARCHICAL_MODE_QUERY_PARAMETER:
				if(forceAllowed || isAllowed(queryParameterKey)) {
					hierarchicalMode = bool;
				}
				return hierarchicalMode;
				
			default:
				break;
		}
		
		return false;
	}
	
	/**
	 * Set the parameter if the user is allowed otherwise the default is maintained 
	 * @param queryParameterKey requested query parameter
	 * @param bool the value to set
	 * @return the value of variable corresponding the request parameter 
	 * independently if the value has been set.
	 */
	public boolean setIfAllowed(String queryParameterKey, boolean bool) {
		return setIfAllowed(queryParameterKey, bool, false);
	}
	
	/**
	 * Set the parameter if the user is allowed otherwise the default is maintained 
	 * @param queryParameterKey requested query parameter
	 * @param integer the int value to set
	 * @param forceAllowed force the value and skip the isAllowed check
	 * @return the value of variable corresponding the request parameter independently if
	 * the value has been set.
	 */
	public int setIfAllowed(String queryParameterKey, int integer, boolean forceAllowed) {

		switch (queryParameterKey) {
			case InstancePath.LIMIT_QUERY_PARAMETER:
				if(forceAllowed || isAllowed(queryParameterKey)) {
					limit = integer;
				}
				return limit;
			
			case InstancePath.OFFSET_QUERY_PARAMETER:
				if(forceAllowed || isAllowed(queryParameterKey)) {
					offset =  integer;
				}
				return offset;
				
			default:
				break;
		}
		
		return -1;
	}
	
	/**
	 * Set the parameter if the user is allowed otherwise the default is maintained 
	 * @param queryParameterKey requested query parameter
	 * @param integer the int value to set
	 * @return the value of variable corresponding the request parameter independently if
	 * the value has been set.
	 */
	public int setIfAllowed(String queryParameterKey, int integer) {
		return setIfAllowed(queryParameterKey, integer, false);
	}
	
	public void checkBooleanQueryParameter(String queryParameterKey) {
		try {
			List<String> queryParameterList = uriInfo.getQueryParameters().get(queryParameterKey);
			if(queryParameterList!=null && queryParameterList.size()>0) {
				String booleanString = queryParameterList.get(0);
				boolean bool = Boolean.valueOf(booleanString);
				setIfAllowed(queryParameterKey, bool);
			}
		}catch (Throwable t) {
			logger.warn("Unable to properly set " + queryParameterKey, t.getMessage());
		}
	}
	
	public void checkIntegerQueryParameter(String queryParameterKey) {
		checkIntegerQueryParameter(queryParameterKey, null);
	}
	
	public void checkIntegerQueryParameter(String queryParameterKey, Integer defaultValue) {
		try {
			List<String> queryParameterList = uriInfo.getQueryParameters().get(queryParameterKey);
			if(queryParameterList!=null && queryParameterList.size()>0) {
				String intString = queryParameterList.get(0);
				int integer = Integer.valueOf(intString);
				setIfAllowed(queryParameterKey, integer);
			}else if(defaultValue!=null) {
				setIfAllowed(queryParameterKey, defaultValue, true);
			}
		}catch (Throwable t) {
			logger.warn("Unable to properly set " + queryParameterKey, t.getMessage());
		}
	}
	
	public void setUriInfo(UriInfo uriInfo) {
		this.uriInfo = uriInfo;
	}
	
	public void checkAllBooleanQueryParameters() {
		checkAllIncludeQueryParameters();
		checkBooleanQueryParameter(InstancePath.HIERARCHICAL_MODE_QUERY_PARAMETER);
	}
	
	public void checkAllIncludeQueryParameters() {
		checkIncludeAllMetaQueryParameters();
		checkBooleanQueryParameter(InstancePath.INCLUDE_CONTEXTS_QUERY_PARAMETER);
	}

	public void checkIncludeAllMetaQueryParameters() {
		checkBooleanQueryParameter(InstancePath.INCLUDE_META_QUERY_PARAMETER);
		checkBooleanQueryParameter(InstancePath.INCLUDE_META_IN_ALL_INSTANCES_QUERY_PARAMETER);
	}
	
	public void checkLimitOffset(int offset, int limit) {
		checkIntegerQueryParameter(InstancePath.OFFSET_QUERY_PARAMETER, offset);
		checkIntegerQueryParameter(InstancePath.LIMIT_QUERY_PARAMETER, limit);
	}
	
	public void checkLimitOffset() {
		checkLimitOffset(DEFAULT_OFFSET, DEFAULT_LIMIT);
	}
	
}
