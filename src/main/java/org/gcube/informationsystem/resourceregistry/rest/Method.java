package org.gcube.informationsystem.resourceregistry.rest;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public enum Method {
	
	LIST("list","s"),
	CREATE("create",""),
	EXIST("exist",""),
	READ("read",""),
	UPDATE("update",""),
	RUN("run",""),
	DELETE("delete",""),
	QUERY("","Query");
	
	protected final String prefix;
	protected final String suffix;
	
	Method(String prefix, String suffix) {
		this.prefix = prefix;
		this.suffix = suffix;
	}
	
	public String getPrefix() {
		return prefix;
	}
	
	public String getSuffix() {
		return suffix;
	}
	
}