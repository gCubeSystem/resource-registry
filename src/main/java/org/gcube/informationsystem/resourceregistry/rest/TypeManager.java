package org.gcube.informationsystem.resourceregistry.rest;

import java.util.List;

import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DefaultValue;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.InternalServerErrorException;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;

import org.gcube.informationsystem.resourceregistry.ResourceInitializer;
import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.types.SchemaException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.types.SchemaNotFoundException;
import org.gcube.informationsystem.resourceregistry.api.rest.TypePath;
import org.gcube.informationsystem.resourceregistry.rest.requests.ServerRequestInfo;
import org.gcube.informationsystem.resourceregistry.types.TypeManagement;
import org.gcube.informationsystem.types.TypeMapper;
import org.gcube.informationsystem.types.reference.Type;


/**
 * @author Luca Frosini (ISTI - CNR)
 */
@Path(TypePath.TYPES_PATH_PART)
public class TypeManager extends BaseRest {

	public static final String TYPE_PATH_PARAMETER = "TYPE_NAME";
	
	public TypeManager() {
		super();
	}
	
	/**
	 * Clean the types cache
	 * @return
	 */
	// @DELETE
	public Response cleanCache() {
		try {
			// TODO
			return Response.status(Status.NO_CONTENT).build();
		}catch (WebApplicationException e) {
			throw e;
		}catch (Exception e) {
			throw new InternalServerErrorException(e);
		}
	}
	
	
	/*
	 * PUT /types/{TYPE_NAME}
	 * e.g. PUT /types/ContactFacet
	 * 
	 * BODY: {...}
	 * 
	 */
	@PUT
	@Path("{" + TypeManager.TYPE_PATH_PARAMETER + "}")
	@Consumes({MediaType.TEXT_PLAIN, ResourceInitializer.APPLICATION_JSON_CHARSET_UTF_8})
	@Produces(ResourceInitializer.APPLICATION_JSON_CHARSET_UTF_8)
	public Response create(@PathParam(TypeManager.TYPE_PATH_PARAMETER) String typeName, String json)
			throws SchemaException, ResourceRegistryException {
		logger.info("Requested {} creation with schema {}", typeName, json);
		setAccountingMethod(Method.CREATE, Type.NAME);
		
		ServerRequestInfo serverRequestInfo = initRequestInfo();
		serverRequestInfo.setAllMeta(true);
		serverRequestInfo.checkBooleanQueryParameter(TypePath.INCLUDE_META_QUERY_PARAMETER);
		
		TypeManagement schemaManagement = new TypeManagement();
		schemaManagement.setTypeName(typeName);
		schemaManagement.setJson(json);
		String ret = schemaManagement.create();
		return Response.status(Status.CREATED).entity(ret).type(ResourceInitializer.APPLICATION_JSON_CHARSET_UTF_8)
				.build();
	}
	
	/*
	 * GET /types/{TYPE_NAME}
	 * e.g. GET /types/ContactFacet?polymorphic=false
	 * 
	 */
	@GET
	@Path("{" + TypeManager.TYPE_PATH_PARAMETER + "}")
	@Produces(ResourceInitializer.APPLICATION_JSON_CHARSET_UTF_8)
	public String read(@PathParam(TypeManager.TYPE_PATH_PARAMETER) String type,
			@QueryParam(TypePath.POLYMORPHIC_QUERY_PARAMETER) @DefaultValue("false") Boolean polymorphic)
			throws SchemaNotFoundException, ResourceRegistryException {
		logger.info("Requested Schema for type {}", type);
		setAccountingMethod(Method.READ, Type.NAME);
		
		ServerRequestInfo serverRequestInfo = initRequestInfo();
		serverRequestInfo.setAllMeta(true);
		serverRequestInfo.checkBooleanQueryParameter(TypePath.INCLUDE_META_QUERY_PARAMETER);
		
		TypeManagement schemaManagement = new TypeManagement();
		schemaManagement.setTypeName(type);
		List<Type> types = schemaManagement.read(polymorphic);
		try {
			return TypeMapper.serializeTypeDefinitions(types);
		}catch (Exception e) {
			throw new ResourceRegistryException(e);
		}
	}
	
}
