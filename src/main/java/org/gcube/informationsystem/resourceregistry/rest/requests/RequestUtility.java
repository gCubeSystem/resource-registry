package org.gcube.informationsystem.resourceregistry.rest.requests;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class RequestUtility {

	private static final InheritableThreadLocal<ServerRequestInfo> requestInfo = new InheritableThreadLocal<ServerRequestInfo>() {
		
		@Override
		protected ServerRequestInfo initialValue() {
			return new ServerRequestInfo();
		}
		
	};
	
	public static InheritableThreadLocal<ServerRequestInfo> getRequestInfo() {
		return requestInfo;
	}
	
}
