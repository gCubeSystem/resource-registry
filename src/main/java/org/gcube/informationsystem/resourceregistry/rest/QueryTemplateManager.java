package org.gcube.informationsystem.resourceregistry.rest;

import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;

import org.gcube.informationsystem.queries.templates.reference.entities.QueryTemplate;
import org.gcube.informationsystem.resourceregistry.ResourceInitializer;
import org.gcube.informationsystem.resourceregistry.api.exceptions.NotFoundException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.queries.InvalidQueryException;
import org.gcube.informationsystem.resourceregistry.api.rest.ContextPath;
import org.gcube.informationsystem.resourceregistry.api.rest.QueryTemplatePath;
import org.gcube.informationsystem.resourceregistry.queries.templates.QueryTemplateManagement;
import org.gcube.informationsystem.resourceregistry.rest.requests.ServerRequestInfo;


/**
 * @author Luca Frosini (ISTI - CNR)
 */
@Path(QueryTemplatePath.QUERY_TEMPLATES_PATH_PART)
public class QueryTemplateManager extends BaseRest {

	public static final String QUERY_TEMPLATE_NAME_PATH_PARAMETER = "QUERY_TEMPLATE_NAME";
	
	public QueryTemplateManager() {
		super();
	}
	
	/**
	 ** GET /query-templates
	 */
	@GET
	@Consumes({MediaType.TEXT_PLAIN, ResourceInitializer.APPLICATION_JSON_CHARSET_UTF_8})
	@Produces(ResourceInitializer.APPLICATION_JSON_CHARSET_UTF_8)
	public String all() throws NotFoundException, ResourceRegistryException {
		logger.info("Requested to read all {}s", QueryTemplate.NAME);
		setAccountingMethod(Method.LIST, QueryTemplate.NAME);
		
		ServerRequestInfo serverRequestInfo = initRequestInfo();
		serverRequestInfo.setAllMeta(true);
		serverRequestInfo.checkBooleanQueryParameter(ContextPath.INCLUDE_META_QUERY_PARAMETER);
		serverRequestInfo.checkLimitOffset();
		
		QueryTemplateManagement queryTemplateManagement = new QueryTemplateManagement();
		return queryTemplateManagement.all(false);
	}
	
	/*
	 * PUT /query-templates/{QUERY_TEMPLATE_NAME}
	 * e.g. PUT /query-templates/GetAllEServiceWithState
	 * 
	 * BODY: 
	 * 
	 * {
	 * 		"name" : "GetAllEServiceWithState",
	 * 		"description" : "The following query return all the EService having the state provided as parameters, e.g. down, ready. 
	 * 					The content of the request to run this query template will be something like {\"$state\": "ready"}",
	 * 		"template": {
	 * 			"type": "EService",
	 * 			"consistsOf": [{
	 * 				"type": "ConsistsOf",
	 * 				"target": {
	 * 					"type": "StateFacet",
	 * 					"value": "$state"
	 * 				}
	 * 			}]
	 * 		},
	 * 		"templateVariables" = {
	 * 			"$state" : {
	 * 				"name": "$state",
	 * 				"description": "The state of the EService, e.g. down, ready.",
	 * 				"defaultValue": "ready"
	 * 			}
	 * 		}
	 * }
	 * 
	 */
	@PUT
	@Path("{" + QueryTemplateManager.QUERY_TEMPLATE_NAME_PATH_PARAMETER + "}")
	@Consumes({MediaType.TEXT_PLAIN, ResourceInitializer.APPLICATION_JSON_CHARSET_UTF_8})
	@Produces(ResourceInitializer.APPLICATION_JSON_CHARSET_UTF_8)
	public String updateCreate(@PathParam(QueryTemplateManager.QUERY_TEMPLATE_NAME_PATH_PARAMETER) String queryTemplateName, String json)
			throws InvalidQueryException, ResourceRegistryException {
		logger.info("Requested {} creation with name {} and content {}", QueryTemplate.NAME, queryTemplateName, json);
		setAccountingMethod(Method.UPDATE, QueryTemplate.NAME);
		
		ServerRequestInfo serverRequestInfo = initRequestInfo();
		serverRequestInfo.setAllMeta(true);
		serverRequestInfo.checkBooleanQueryParameter(ContextPath.INCLUDE_META_QUERY_PARAMETER);
		
		QueryTemplateManagement queryTemplateManagement = new QueryTemplateManagement();
		queryTemplateManagement.setName(queryTemplateName);
		queryTemplateManagement.setJson(json);
		return queryTemplateManagement.createOrUpdate();
	}
	
	/*
	 * GET /query-templates/{QUERY_TEMPLATE_NAME}
	 * e.g. GET /query-templates/GetAllEServiceWithState
	 */
	@GET
	@Path("{" + QueryTemplateManager.QUERY_TEMPLATE_NAME_PATH_PARAMETER + "}")
	@Produces(ResourceInitializer.APPLICATION_JSON_CHARSET_UTF_8)
	public String read(@PathParam(QueryTemplateManager.QUERY_TEMPLATE_NAME_PATH_PARAMETER) String queryTemplateName)
			throws NotFoundException, ResourceRegistryException {
		logger.info("Requested {} with name", QueryTemplate.NAME, queryTemplateName);
		setAccountingMethod(Method.READ, QueryTemplate.NAME);
		
		ServerRequestInfo serverRequestInfo = initRequestInfo();
		serverRequestInfo.setAllMeta(true);
		serverRequestInfo.checkBooleanQueryParameter(ContextPath.INCLUDE_META_QUERY_PARAMETER);
		
		QueryTemplateManagement queryTemplateManagement = new QueryTemplateManagement();
		queryTemplateManagement.setName(queryTemplateName);
		return queryTemplateManagement.read();
	}
	
	
	/*
	 * POST /query-templates/{QUERY_TEMPLATE_NAME}
	 * e.g. POST /query-templates/GetAllEServiceWithState
	 * 
	 * params = { "$state" : "ready" }
	 * 
	 */
	@POST
	@Path("{" + QueryTemplateManager.QUERY_TEMPLATE_NAME_PATH_PARAMETER + "}")
	@Produces(ResourceInitializer.APPLICATION_JSON_CHARSET_UTF_8)
	public String run(@PathParam(QueryTemplateManager.QUERY_TEMPLATE_NAME_PATH_PARAMETER) String queryTemplateName, String params)
			throws NotFoundException, InvalidQueryException, ResourceRegistryException {
		logger.info("Requested {} with name", QueryTemplate.NAME, queryTemplateName);
		setAccountingMethod(Method.RUN, QueryTemplate.NAME);
		
		ServerRequestInfo serverRequestInfo = initRequestInfo();
		serverRequestInfo.checkAllBooleanQueryParameters();
		
		QueryTemplateManagement queryTemplateManagement = new QueryTemplateManagement();
		queryTemplateManagement.setName(queryTemplateName);
		queryTemplateManagement.setParams(params);
		return queryTemplateManagement.run();
	}
	
	
	/*
	 * DELETE /query-templates/{QUERY_TEMPLATE_NAME}
	 * e.g. DELETE /query-templates/GetAllEServiceWithState
	 */
	@DELETE
	@Consumes({MediaType.TEXT_PLAIN, ResourceInitializer.APPLICATION_JSON_CHARSET_UTF_8})
	@Path("{" + QueryTemplateManager.QUERY_TEMPLATE_NAME_PATH_PARAMETER + "}")
	public Response delete(@PathParam(QueryTemplateManager.QUERY_TEMPLATE_NAME_PATH_PARAMETER) String queryTemplateName)
			throws NotFoundException, ResourceRegistryException {
		logger.info("Requested to delete {} with name {} ", QueryTemplate.NAME, queryTemplateName);
		setAccountingMethod(Method.DELETE, QueryTemplate.NAME);
		
		QueryTemplateManagement queryTemplateManagement = new QueryTemplateManagement();
		queryTemplateManagement.setName(queryTemplateName);
		queryTemplateManagement.delete();
		
		return Response.status(Status.NO_CONTENT).build();
	}
	
}
