package org.gcube.informationsystem.resourceregistry.rest;

import java.io.IOException;

import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ContainerRequestFilter;
import jakarta.ws.rs.container.ContainerResponseContext;
import jakarta.ws.rs.container.ContainerResponseFilter;
import jakarta.ws.rs.container.PreMatching;
import jakarta.ws.rs.ext.Provider;

import org.gcube.informationsystem.resourceregistry.rest.requests.RequestUtility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @author Luca Frosini (ISTI - CNR)
 */
@Provider
@PreMatching
public class RequestFilter implements ContainerRequestFilter, ContainerResponseFilter {
	
	private final static Logger logger = LoggerFactory.getLogger(RequestFilter.class);
	
	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {
		logger.trace("PreMatching RequestFilter");
		RequestUtility.getRequestInfo().remove();
	}

	@Override
	public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext)
			throws IOException {
		logger.trace("ResponseFilter");
		RequestUtility.getRequestInfo().remove();
	}

}
