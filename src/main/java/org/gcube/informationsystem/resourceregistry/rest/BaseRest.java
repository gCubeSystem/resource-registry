package org.gcube.informationsystem.resourceregistry.rest;

import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.UriInfo;

import org.gcube.informationsystem.resourceregistry.rest.requests.RequestUtility;
import org.gcube.informationsystem.resourceregistry.rest.requests.ServerRequestInfo;
import org.gcube.smartgears.utils.InnerMethodName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class BaseRest {
	
	protected Logger logger = LoggerFactory.getLogger(this.getClass());

	@Context
	protected UriInfo uriInfo;
	
	public BaseRest() {

	}

	protected void setAccountingMethod(String method) {
		InnerMethodName.set(method);
	}
	
	protected void setAccountingMethod(Method method, String type) {
		StringBuffer accountingMethod = new StringBuffer();
		accountingMethod.append(method.getPrefix());
		accountingMethod.append(type);
		accountingMethod.append(method.getSuffix());
		setAccountingMethod(accountingMethod.toString());
	}
	
	private ServerRequestInfo initRequestInfo(ServerRequestInfo requestInfo) {
		requestInfo.setUriInfo(uriInfo);
		RequestUtility.getRequestInfo().set(requestInfo);
		return requestInfo;
	}
	
	protected ServerRequestInfo initRequestInfo(int offset, int limit) {
		ServerRequestInfo requestInfo = new ServerRequestInfo(offset, limit);
		return initRequestInfo(requestInfo);
	}
	
	protected ServerRequestInfo initRequestInfo() {
		ServerRequestInfo requestInfo = new ServerRequestInfo();
		return initRequestInfo(requestInfo);
	}
	
}
