package org.gcube.informationsystem.resourceregistry.rest;

import java.util.UUID;

import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.DefaultValue;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.HEAD;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;

import org.gcube.informationsystem.resourceregistry.ResourceInitializer;
import org.gcube.informationsystem.resourceregistry.api.exceptions.AvailableInAnotherContextException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.NotFoundException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.contexts.ContextNotFoundException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.entities.resource.ResourceNotFoundException;
import org.gcube.informationsystem.resourceregistry.api.rest.InstancePath;
import org.gcube.informationsystem.resourceregistry.api.rest.SharingPath;
import org.gcube.informationsystem.resourceregistry.base.ElementManagement;
import org.gcube.informationsystem.resourceregistry.instances.model.ERManagementUtility;
import org.gcube.informationsystem.resourceregistry.rest.requests.ServerRequestInfo;


/**
 * @author Luca Frosini (ISTI - CNR)
 */
@Path(InstancePath.INSTANCES_PATH_PART)
public class InstancesManager extends BaseRest {
	
	public static final String UUID_PATH_PARAMETER = "UUID";
	
	public static final String INSTANCE = "Instance";
	public static final String GET_INSTANCE_CONTEXTS_METHOD = "getInstanceContexts";
	
	public InstancesManager() {
		super();
	}
	
	/*
	 * GET /instances/{TYPE_NAME}[?polymorphic=true]
	 * e.g. GET /instances/ContactFacet?polymorphic=true
	 * 
	 */
	@GET
	@Path("/{" + TypeManager.TYPE_PATH_PARAMETER + "}")
	@Consumes({MediaType.TEXT_PLAIN, ResourceInitializer.APPLICATION_JSON_CHARSET_UTF_8})
	@Produces(ResourceInitializer.APPLICATION_JSON_CHARSET_UTF_8)
	public String readAll(@PathParam(TypeManager.TYPE_PATH_PARAMETER) String type,
			@QueryParam(InstancePath.POLYMORPHIC_QUERY_PARAMETER) @DefaultValue("true") Boolean polymorphic)
			throws NotFoundException, ResourceRegistryException {
		logger.info("Requested all {}instances of {}", polymorphic ? InstancePath.POLYMORPHIC_QUERY_PARAMETER + " " : "", type);
		setAccountingMethod(Method.LIST, InstancesManager.INSTANCE);
		
		ServerRequestInfo serverRequestInfo = initRequestInfo();
		serverRequestInfo.checkAllBooleanQueryParameters();
		serverRequestInfo.checkLimitOffset();
		
		ElementManagement<?,?> erManagement = ERManagementUtility.getERManagement(type);
		return erManagement.all(polymorphic);
	}
	
	/*
	 * HEAD /instances/{TYPE_NAME}/{UUID}
	 * e.g. HEAD /instances/ContactFacet/4023d5b2-8601-47a5-83ef-49ffcbfc7d86
	 * 
	 */
	@HEAD
	@Path("/{" + TypeManager.TYPE_PATH_PARAMETER + "}" + "/{" + InstancesManager.UUID_PATH_PARAMETER + "}")
	@Consumes({MediaType.TEXT_PLAIN, ResourceInitializer.APPLICATION_JSON_CHARSET_UTF_8})
	@Produces(ResourceInitializer.APPLICATION_JSON_CHARSET_UTF_8)
	public Response exists(@PathParam(TypeManager.TYPE_PATH_PARAMETER) String type,
			@PathParam(InstancesManager.UUID_PATH_PARAMETER) String uuid) throws NotFoundException, ResourceRegistryException {
		logger.info("Requested to check if {} with id {} exists", type, uuid);
		setAccountingMethod(Method.EXIST, InstancesManager.INSTANCE);
		
		ServerRequestInfo serverRequestInfo = initRequestInfo();
		serverRequestInfo.checkBooleanQueryParameter(InstancePath.HIERARCHICAL_MODE_QUERY_PARAMETER);
		
		@SuppressWarnings("rawtypes")
		ElementManagement erManagement = ERManagementUtility.getERManagement(type);
		try {
			erManagement.setUUID(UUID.fromString(uuid));
			boolean found = erManagement.exists();
			if(found) {
				return Response.status(Status.NO_CONTENT).build();
			} else {
				// This code should never be reached due to exception management
				// anyway adding it for safety reason
				return Response.status(Status.NOT_FOUND).build();
			}
		} catch(NotFoundException e) {
			return Response.status(Status.NOT_FOUND).build();
		} catch(AvailableInAnotherContextException e) {
			return Response.status(Status.FORBIDDEN).build();
		} catch(ResourceRegistryException e) {
			throw e;
		}
	}
	
	/*
	 * GET /instances/{TYPE_NAME}/{UUID}
	 * e.g. GET /instances/ContactFacet/4023d5b2-8601-47a5-83ef-49ffcbfc7d86
	 * 
	 */
	@GET
	@Path("/{" + TypeManager.TYPE_PATH_PARAMETER + "}" + "/{" + InstancesManager.UUID_PATH_PARAMETER + "}")
	@Consumes({MediaType.TEXT_PLAIN, ResourceInitializer.APPLICATION_JSON_CHARSET_UTF_8})
	@Produces(ResourceInitializer.APPLICATION_JSON_CHARSET_UTF_8)
	public String read(@PathParam(TypeManager.TYPE_PATH_PARAMETER) String type,
			@PathParam(InstancesManager.UUID_PATH_PARAMETER) String uuid) throws NotFoundException, ResourceRegistryException {
		logger.info("Requested to read {} with id {}", type, uuid);
		setAccountingMethod(Method.READ, InstancesManager.INSTANCE);
		
		ServerRequestInfo serverRequestInfo = initRequestInfo();
		serverRequestInfo.checkAllBooleanQueryParameters();
		
		ElementManagement<?,?> erManagement = ERManagementUtility.getERManagement(type);
		erManagement.setElementType(type);
		erManagement.setUUID(UUID.fromString(uuid));
		return erManagement.read().toString();
	}
	
	/*
	 * PUT /instances/{TYPE_NAME}/{UUID}
	 * e.g. PUT /instances/ContactFacet/4023d5b2-8601-47a5-83ef-49ffcbfc7d86
	 * 
	 * BODY: {...}
	 * 
	 */
	@PUT
	@Path("/{" + TypeManager.TYPE_PATH_PARAMETER + "}" + "/{" + InstancesManager.UUID_PATH_PARAMETER + "}")
	@Consumes({MediaType.TEXT_PLAIN, ResourceInitializer.APPLICATION_JSON_CHARSET_UTF_8})
	@Produces(ResourceInitializer.APPLICATION_JSON_CHARSET_UTF_8)
	public String updateOrCreate(@PathParam(TypeManager.TYPE_PATH_PARAMETER) String type,
			@PathParam(InstancesManager.UUID_PATH_PARAMETER) String uuid, String json) throws ResourceRegistryException {
		logger.info("Requested to update/create {} with id {}", type, uuid);
		logger.trace("Requested to update/create {} with id {} with json {}", type, uuid, json);
		setAccountingMethod(Method.UPDATE, InstancesManager.INSTANCE);
		
		ServerRequestInfo serverRequestInfo = initRequestInfo();
		serverRequestInfo.checkAllIncludeQueryParameters();
		
		@SuppressWarnings("rawtypes")
		ElementManagement erManagement = ERManagementUtility.getERManagement(type);
		erManagement.setUUID(UUID.fromString(uuid));
		erManagement.setElementType(type);
		erManagement.setJson(json);
		return erManagement.createOrUpdate();
	}
	
	/*
	 * DELETE /instances/{TYPE_NAME}/{UUID}
	 * e.g. DELETE /instances/ContactFacet/4023d5b2-8601-47a5-83ef-49ffcbfc7d86
	 * 
	 */
	@DELETE
	@Path("/{" + TypeManager.TYPE_PATH_PARAMETER + "}" + "/{" + InstancesManager.UUID_PATH_PARAMETER + "}")
	public Response delete(@PathParam(TypeManager.TYPE_PATH_PARAMETER) String type,
			@PathParam(InstancesManager.UUID_PATH_PARAMETER) String uuid) throws ResourceRegistryException {
		logger.info("Requested to delete {} with id {}", type, uuid);
		setAccountingMethod(Method.DELETE, InstancesManager.INSTANCE);
		
		ElementManagement<?,?> erManagement = ERManagementUtility.getERManagement(type);
		erManagement.setUUID(UUID.fromString(uuid));
		erManagement.delete();
		
		return Response.status(Status.NO_CONTENT).build();
	}
	
	
	/*
	 * GET /instances/{TYPE_NAME}/{UUID}/contexts/ e.g GET
	 * /resource-registry/instances/HostingNode/16032d09-3823-444e-a1ff-a67de4f350a8/contexts 
	 * 
	 * Where 16032d09-3823-444e-a1ff-a67de4f350a8 is the HostingNode UUID
	 * 
	 * Return a list of UUID identifying the context the instance belongs to.
	 */
	@GET
	@Path("{" + TypeManager.TYPE_PATH_PARAMETER + "}" + "/{" + InstancesManager.UUID_PATH_PARAMETER + "}/"
			+ SharingPath.CONTEXTS_PATH_PART)
	@Produces(ResourceInitializer.APPLICATION_JSON_CHARSET_UTF_8)
	public String getInstanceContexts(@PathParam(TypeManager.TYPE_PATH_PARAMETER) String type,
			@PathParam(InstancesManager.UUID_PATH_PARAMETER) String instanceId,
			@PathParam(ContextManager.CONTEXT_UUID_PATH_PARAMETER) String contextId)
			throws ResourceNotFoundException, ContextNotFoundException, ResourceRegistryException {
		logger.info("Requested to get contexts of {} with UUID {}", type, instanceId);
		setAccountingMethod(InstancesManager.GET_INSTANCE_CONTEXTS_METHOD);

		@SuppressWarnings("rawtypes")
		ElementManagement erManagement = ERManagementUtility.getERManagement(type);
		erManagement.setUUID(UUID.fromString(instanceId));
		return erManagement.getContexts();
	}
	
}
