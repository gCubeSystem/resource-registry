package org.gcube.informationsystem.resourceregistry.rest;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import jakarta.ws.rs.DefaultValue;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;

import org.gcube.com.fasterxml.jackson.core.JsonProcessingException;
import org.gcube.com.fasterxml.jackson.databind.JsonNode;
import org.gcube.com.fasterxml.jackson.databind.ObjectMapper;
import org.gcube.com.fasterxml.jackson.databind.node.ArrayNode;
import org.gcube.informationsystem.contexts.reference.entities.Context;
import org.gcube.informationsystem.resourceregistry.ResourceInitializer;
import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.contexts.ContextNotFoundException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.entities.resource.ResourceNotFoundException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.types.SchemaViolationException;
import org.gcube.informationsystem.resourceregistry.api.rest.SharingPath;
import org.gcube.informationsystem.resourceregistry.api.rest.SharingPath.SharingOperation;
import org.gcube.informationsystem.resourceregistry.base.ElementManagement;
import org.gcube.informationsystem.resourceregistry.instances.model.ERManagement;
import org.gcube.informationsystem.resourceregistry.instances.model.ERManagementUtility;
import org.gcube.informationsystem.resourceregistry.rest.requests.ServerRequestInfo;
import org.gcube.informationsystem.resourceregistry.utils.UUIDUtility;
import org.gcube.informationsystem.utils.TypeUtility;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@Path(SharingPath.SHARING_PATH_PART)
public class SharingManager extends BaseRest {

	public SharingManager() {
		super();
	}
	
	protected String serializeAffectedInstaces(ObjectMapper objectMapper, Map<UUID,JsonNode> affectedInstances) throws ResourceRegistryException {
		ArrayNode arrayNode = objectMapper.createArrayNode();
		for(JsonNode jsonNode : affectedInstances.values()) {
			arrayNode.add(jsonNode);
		}
		
		try {
			return objectMapper.writeValueAsString(arrayNode);
		} catch (JsonProcessingException e) {
			throw new ResourceRegistryException(e);
		}
	}
	
	/**
	 * Add/Remove to/from the context identified by CONTEXT_UUID path parameter the list of instances contained in the body of the request.
	 * The the body is the following 
	 * 
	 * [
	 *  {"type" : "HostingNode", "id" : "16032d09-3823-444e-a1ff-a67de4f350a8" },
	 *  {"type" : "Hosts", "id" : "97ab8a6b-6b1b-4868-b8fc-ba48d0439ba9"},
	 *  {"type" : "EService", "id" : "d3b1a29b-aa70-4a5a-be83-361a4209dd3e"}
	 * ]
	 * 
	 * 
	 * Each instance is managed without considering the propagation constraint of relations. 
	 * 
	 * POST /sharing/contexts/{CONTEXT_UUID}?operation=PUT(ADD|REMOVE)&[dryRun=true]
	 * 
	 * e.g 
	 * POST /resource-registry/contexts/67062c11-9c3a-4906-870d-7df6a43408b0?operation=ADD&dryRun=true
	 * POST /resource-registry/contexts/67062c11-9c3a-4906-870d-7df6a43408b0?operation=REMOVE
	 * 
	 * where
	 * 67062c11-9c3a-4906-870d-7df6a43408b0/ is the Context UUID
	 * 
	 * The body contains the list of instances to add/remove to/from the context identified by CONTEXT_UUID
	 * 
	 */
	// @POST
	// @Path("/{" + SharingPath.CONTEXTS_PATH_PART + "/{" + AccessPath.CONTEXT_UUID_PATH_PARAM + "}")
	public String addRemoveNoPropagationConstraint(
			@PathParam(ContextManager.CONTEXT_UUID_PATH_PARAMETER) String contextId,  
			@QueryParam(SharingPath.OPERATION_QUERY_PARAMETER) SharingOperation operation,
			@QueryParam(SharingPath.DRY_RUN_QUERY_QUERY_PARAMETER) @DefaultValue("false") Boolean dryRun,
			@QueryParam(SharingPath.FORCE_ADD_TO_CONTEXT_QUERY_PARAMETER) @DefaultValue("false") Boolean forceAddToContext,
			String body)
			throws SchemaViolationException, ResourceNotFoundException, ContextNotFoundException, ResourceRegistryException {

		ServerRequestInfo serverRequestInfo = initRequestInfo();
		serverRequestInfo.checkAllIncludeQueryParameters();
		
		try {
			StringBuffer calledMethod = new StringBuffer(); 
			if(dryRun==null) {
				dryRun = false;
			}
			
			if(dryRun) {
				calledMethod.append("dryRun");
			}
			
			if(operation == SharingOperation.ADD) {
				logger.info("Requested {} {} to {} with UUID {}", dryRun? "a dry run for adding": "to add", body, Context.NAME, contextId);
				calledMethod.append("AddToContext");
			}else {
				logger.info("Requested {} {} from {} with UUID {}", dryRun? "a dry run for removing": "to remove", body, Context.NAME, contextId);
				calledMethod.append("RemoveFromContext");
			}
			calledMethod.append("NoPropagationConstraint");
			setAccountingMethod(calledMethod.toString());
	
			ObjectMapper objectMapper = new ObjectMapper();
			ArrayNode arrayNode = (ArrayNode) objectMapper.readTree(body);
			
			Map<UUID,JsonNode> expectedInstances = new HashMap<>();
			
			for(JsonNode node : arrayNode) {
				@SuppressWarnings("unused")
				String type = TypeUtility.getTypeName(node);
				UUID uuid = UUIDUtility.getUUID(node);
				expectedInstances.put(uuid, node);
			}
			
			@SuppressWarnings("unused")
			UUID contextUUID = UUID.fromString(contextId);
			Map<UUID,JsonNode> affectedInstances = null;
			if(operation == SharingOperation.ADD) {
				// affectedInstances = ERManagementUtility.addToContextNoPropagationConstraint(expectedInstances, contextUUID, dryRun);
			}else {
				// affectedInstances = ERManagementUtility.removeFromContextNoPropagationConstraint(expectedInstances, contextUUID, dryRun);
			}
			
			return serializeAffectedInstaces(objectMapper, affectedInstances);
		}catch (ResourceRegistryException e) {
			throw e;
		}catch (Exception e) {
			throw new ResourceRegistryException(e);
		}
		
	}
	
	/** 
	 * Add/Remove an instance from context. The operation can have a cascade effect due to propagation constraint.
	 * 
	 * Return the list of instances affected by an add/remove to/from context the target instance identified by UUID path parameter  
	 *  
	 * POST /sharing/contexts/{CONTEXT_UUID}/{TYPE_NAME}/{UUID}?operation=(ADD|REMOVE)&dryRun=true]
	 * 
	 * e.g 
	 * POST /resource-registry/sharing/contexts/67062c11-9c3a-4906-870d-7df6a43408b0/HostingNode/16032d09-3823-444e-a1ff-a67de4f350a8?operation=ADD&dryRun=true
	 * POST /resource-registry/sharing/contexts/67062c11-9c3a-4906-870d-7df6a43408b0/HostingNode/16032d09-3823-444e-a1ff-a67de4f350a8?operation=REMOVE&dryRun=true
	 * 
	 * where
	 * 67062c11-9c3a-4906-870d-7df6a43408b0 is the Context UUID and
	 * 16032d09-3823-444e-a1ff-a67de4f350a8 is the HostingNode UUID
	 * 
	 */
	@POST
	@Path("/" + SharingPath.CONTEXTS_PATH_PART + "/{" + ContextManager.CONTEXT_UUID_PATH_PARAMETER + "}/"
		+ "{" + TypeManager.TYPE_PATH_PARAMETER + "}" + "/{" + InstancesManager.UUID_PATH_PARAMETER + "}")
	@Produces(ResourceInitializer.APPLICATION_JSON_CHARSET_UTF_8)
	public String addRemove(
			@PathParam(ContextManager.CONTEXT_UUID_PATH_PARAMETER) String contextId,
			@PathParam(TypeManager.TYPE_PATH_PARAMETER) String type,
			@PathParam(InstancesManager.UUID_PATH_PARAMETER) String instanceId,
			@QueryParam(SharingPath.OPERATION_QUERY_PARAMETER) SharingOperation operation,
			@QueryParam(SharingPath.DRY_RUN_QUERY_QUERY_PARAMETER) @DefaultValue("false") Boolean dryRun,
			@QueryParam(SharingPath.FORCE_ADD_TO_CONTEXT_QUERY_PARAMETER) @DefaultValue("false") Boolean forceAddToContext)
			throws SchemaViolationException, ResourceNotFoundException, ContextNotFoundException, ResourceRegistryException {
		
		ServerRequestInfo serverRequestInfo = initRequestInfo();
		serverRequestInfo.checkAllIncludeQueryParameters();
		
		StringBuffer calledMethod = new StringBuffer(); 
		if(dryRun==null) {
			dryRun = false;
		}
		
		if(dryRun) {
			calledMethod.append("dryRun");
		}
		
		if(operation == SharingOperation.ADD) {
			logger.info("Requested {} {} with UUID {} to {} with UUID {}", dryRun? "a dry run for adding": "to add", type, instanceId, Context.NAME, contextId);
			calledMethod.append("AddToContext");
		}else {
			logger.info("Requested {} {} with UUID {} from {} with UUID {}", dryRun? "a dry run for removing": "to remove", type, instanceId, Context.NAME, contextId);
			calledMethod.append("RemoveFromContext");
		}
		setAccountingMethod(calledMethod.toString());
		

		ElementManagement<?,?> erManagement = ERManagementUtility.getERManagement(type);
		erManagement.setUUID(UUID.fromString(instanceId));
		erManagement.setDryRun(dryRun);
		UUID contextUUID = UUID.fromString(contextId);
		
		
		if(operation == SharingOperation.ADD) {
			((ERManagement) erManagement).setForceAddToContext(forceAddToContext);
			((ERManagement) erManagement).addToContext(contextUUID);
		}else {
			((ERManagement) erManagement).removeFromContext(contextUUID);
		}
		
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			return serializeAffectedInstaces(objectMapper, erManagement.getAffectedInstances());
		} catch (Exception e) {
			throw new ResourceRegistryException(e);
		}
	}

}
