package org.gcube.informationsystem.resourceregistry;

import jakarta.ws.rs.ApplicationPath;
import jakarta.ws.rs.core.MediaType;

import org.gcube.informationsystem.resourceregistry.dbinitialization.DatabaseEnvironment;
import org.gcube.informationsystem.resourceregistry.rest.Access;
import org.gcube.smartgears.annotations.ManagedBy;
import org.glassfish.jersey.server.ResourceConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@ApplicationPath("/")
@ManagedBy(ResourceRegistryManager.class)
public class ResourceInitializer extends ResourceConfig {
	
	private static Logger logger = LoggerFactory.getLogger(ResourceInitializer.class);
	
	public static final String APPLICATION_JSON_CHARSET_UTF_8 = MediaType.APPLICATION_JSON + ";charset=UTF-8";
	
	public ResourceInitializer() {
		packages(Access.class.getPackage().toString());
		logger.info("The server is going to use OrientDB at {}", DatabaseEnvironment.DB_URI);
	}
	
}
