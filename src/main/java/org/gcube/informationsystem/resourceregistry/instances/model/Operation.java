package org.gcube.informationsystem.resourceregistry.instances.model;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public enum Operation {

	CREATE,
	EXISTS(true),
	READ(true),
	UPDATE,
	DELETE,
	ADD_TO_CONTEXT,
	REMOVE_FROM_CONTEXT,
	QUERY(true),
	// GET_METADATA e.g. getInstanceContexts
	GET_METADATA(true);
	
	private final boolean safe; 
	
	private Operation() {
		this.safe = false;
	}
	
	private Operation(boolean safe) {
		this.safe = safe;
	}
	
	public boolean isSafe() {
		return safe;
	}
}
