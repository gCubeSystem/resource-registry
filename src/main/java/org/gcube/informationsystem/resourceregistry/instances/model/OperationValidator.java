package org.gcube.informationsystem.resourceregistry.instances.model;

import java.util.Map;
import java.util.UUID;

import org.gcube.com.fasterxml.jackson.databind.JsonNode;
import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface OperationValidator {
	
	public boolean isValidOperation(Map<UUID, JsonNode> affectedInstances) throws ResourceRegistryException;
	
}
