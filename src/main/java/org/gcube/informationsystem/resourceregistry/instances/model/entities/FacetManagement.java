package org.gcube.informationsystem.resourceregistry.instances.model.entities;

import org.gcube.com.fasterxml.jackson.databind.JsonNode;
import org.gcube.informationsystem.base.reference.AccessType;
import org.gcube.informationsystem.model.reference.entities.Facet;
import org.gcube.informationsystem.resourceregistry.api.exceptions.AlreadyPresentException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.NotFoundException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.contexts.ContextException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.entities.facet.FacetAlreadyPresentException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.entities.facet.FacetAvailableInAnotherContextException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.entities.facet.FacetNotFoundException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.types.SchemaViolationException;
import org.gcube.informationsystem.resourceregistry.environments.Environment.PermissionMode;
import org.gcube.informationsystem.resourceregistry.instances.model.Operation;
import org.gcube.informationsystem.resourceregistry.instances.model.relations.ConsistsOfManagement;
import org.gcube.informationsystem.types.reference.entities.FacetType;

import com.orientechnologies.orient.core.db.document.ODatabaseDocument;
import com.orientechnologies.orient.core.record.ODirection;
import com.orientechnologies.orient.core.record.OEdge;
import com.orientechnologies.orient.core.record.OVertex;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class FacetManagement extends EntityManagement<Facet, FacetType> {
	
	protected ConsistsOfManagement consistsOfManagement;
	protected ResourceManagement resourceManagement;
	
	public FacetManagement() {
		super(AccessType.FACET);
	}
	
	@Override
	protected FacetNotFoundException getSpecificNotFoundException(NotFoundException e) {
		return new FacetNotFoundException(e.getMessage(), e.getCause());
	}
	
	@Override
	public FacetAvailableInAnotherContextException getSpecificAvailableInAnotherContextException(String message) {
		return new FacetAvailableInAnotherContextException(message);
	}
	
	@Override
	protected FacetAlreadyPresentException getSpecificAlreadyPresentException(String message) {
		return new FacetAlreadyPresentException(message);
	}
	
	
	@Override
	public JsonNode createCompleteJsonNode() throws ResourceRegistryException {
		return serializeSelfAsJsonNode();
	}
	
	@Override
	protected OVertex reallyCreate() throws FacetAlreadyPresentException, ResourceRegistryException {
		return createVertex();
	}
	
	@Override
	protected OVertex reallyUpdate() throws FacetNotFoundException, ResourceRegistryException {
		OVertex facet = getElement();
		facet = (OVertex) updateProperties(oClass, facet, jsonNode, ignoreKeys, ignoreStartWithKeys);
		return facet;
	}
	
	@Override
	protected void reallyRemoveFromContext()
			throws ContextException, ResourceRegistryException {
		if(entryPoint) {
			OEdge oEdge = getElement().getEdges(ODirection.IN).iterator().next();
			consistsOfManagement = new ConsistsOfManagement();
			consistsOfManagement.setElement(oEdge);
			consistsOfManagement.setTargetEntityManagement(this);
			// Not needed consistsOfManagement.setSourceInstanceEnvironment(sourceInstanceEnvironment);
			consistsOfManagement.setTargetInstanceEnvironment(targetInstanceEnvironment);
			
			
			affectedInstances.put(uuid, consistsOfManagement.serializeAsAffectedInstance());
			
			OVertex oVertex = getElement().getVertices(ODirection.IN).iterator().next();
			resourceManagement = new ResourceManagement();
			resourceManagement.setElement(oVertex);
			// Not needed resourceManagement.setSourceInstanceEnvironment(sourceInstanceEnvironment);
			resourceManagement.setTargetInstanceEnvironment(targetInstanceEnvironment);
			resourceManagement.addToRelationManagements(consistsOfManagement);
		}
		super.reallyRemoveFromContext();
	}
	
	@Override
	protected void reallyDelete() throws FacetNotFoundException, ResourceRegistryException {
		if(entryPoint) {
			OEdge oEdge = getElement().getEdges(ODirection.IN).iterator().next();
			consistsOfManagement = new ConsistsOfManagement();
			consistsOfManagement.setElement(oEdge);
			consistsOfManagement.setTargetEntityManagement(this);
			affectedInstances.put(uuid, consistsOfManagement.serializeAsAffectedInstance());
			
			OVertex oVertex = getElement().getVertices(ODirection.IN).iterator().next();
			resourceManagement = new ResourceManagement();
			resourceManagement.setElement(oVertex);
			resourceManagement.addToRelationManagements(consistsOfManagement);
		}
		
		affectedInstances.put(uuid, serializeAsAffectedInstance());
		getElement().delete();
	}
	
	protected void checkResource() throws SchemaViolationException, ResourceRegistryException {
		if(!entryPoint) {
			return;
		}
		
		if(operation.isSafe()) {
			/* 
			 * The sanity check is not required for a safe operation.
			 */
			return;
		}
		
		if(operation == Operation.UPDATE) {
			// an update to the Facet only modify the Facet properties, no need to check the source resource
			return;
		}
		ODatabaseDocument targetInstanceEnvironmentODatabaseDocument = null;
		try {
			if(resourceManagement==null) {
				OVertex oVertex = getElement().getVertices(ODirection.IN).iterator().next();
				ResourceManagement resourceManagement = new ResourceManagement();
				resourceManagement.setElement(oVertex);
			}
			switch (operation) {
				case CREATE: case DELETE:
					resourceManagement.setWorkingEnvironment(getWorkingEnvironment());
					resourceManagement.setODatabaseDocument(oDatabaseDocument);
					break;
					
				case ADD_TO_CONTEXT:
					resourceManagement.setSourceInstanceEnvironment(sourceInstanceEnvironment);
					resourceManagement.setTargetInstanceEnvironment(targetInstanceEnvironment);
					resourceManagement.setWorkingEnvironment(targetInstanceEnvironment);
					targetInstanceEnvironmentODatabaseDocument = targetInstanceEnvironment.getDatabaseDocument(PermissionMode.READER);
					resourceManagement.setODatabaseDocument(targetInstanceEnvironmentODatabaseDocument);
					break;
				
				case REMOVE_FROM_CONTEXT:
					// Not needed resourceManagement.setSourceInstanceEnvironment(sourceInstanceEnvironment);
					resourceManagement.setTargetInstanceEnvironment(targetInstanceEnvironment);
					resourceManagement.setWorkingEnvironment(targetInstanceEnvironment);
					targetInstanceEnvironmentODatabaseDocument = targetInstanceEnvironment.getDatabaseDocument(PermissionMode.READER);
					resourceManagement.setODatabaseDocument(targetInstanceEnvironmentODatabaseDocument);
					break;
					
				default:
					// You should not be here
					return;
			}
			
			resourceManagement.setOperation(operation);
			
			resourceManagement.sanityCheck();
			
		}catch (ResourceRegistryException e) {
			throw e;
		}catch (Exception e) {
			throw new ResourceRegistryException(e);
		}finally {
			if(targetInstanceEnvironmentODatabaseDocument!=null) {
				targetInstanceEnvironmentODatabaseDocument.close();
				oDatabaseDocument.activateOnCurrentThread();
			}
		}
	}
	
	@Override
	public void sanityCheck() throws SchemaViolationException, ResourceRegistryException {
		super.sanityCheck();
		checkResource();
	}
	
	@Override
	public String create() throws AlreadyPresentException, ResourceRegistryException {
		throw new SchemaViolationException("You cannot create a stand alone Facet");
	}
	
	public OVertex internalCreate() throws AlreadyPresentException, ResourceRegistryException {
		if(entryPoint) {
			throw new SchemaViolationException("You cannot create a stand alone Facet");
		}
		return super.internalCreate();
	}
	
}
