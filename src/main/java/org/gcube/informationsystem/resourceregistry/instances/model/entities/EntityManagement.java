package org.gcube.informationsystem.resourceregistry.instances.model.entities;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import jakarta.ws.rs.ForbiddenException;

import org.gcube.com.fasterxml.jackson.core.JsonProcessingException;
import org.gcube.com.fasterxml.jackson.databind.JsonNode;
import org.gcube.com.fasterxml.jackson.databind.ObjectMapper;
import org.gcube.com.fasterxml.jackson.databind.node.ArrayNode;
import org.gcube.com.fasterxml.jackson.databind.node.ObjectNode;
import org.gcube.informationsystem.base.reference.AccessType;
import org.gcube.informationsystem.contexts.reference.ContextState;
import org.gcube.informationsystem.contexts.reference.entities.Context;
import org.gcube.informationsystem.model.reference.entities.Entity;
import org.gcube.informationsystem.model.reference.entities.Facet;
import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.model.reference.relations.ConsistsOf;
import org.gcube.informationsystem.model.reference.relations.Relation;
import org.gcube.informationsystem.resourceregistry.api.exceptions.AvailableInAnotherContextException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.NotFoundException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.contexts.ContextException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.entities.EntityAlreadyPresentException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.queries.InvalidQueryException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.types.SchemaViolationException;
import org.gcube.informationsystem.resourceregistry.base.ElementManagement;
import org.gcube.informationsystem.resourceregistry.base.ElementManagementUtility;
import org.gcube.informationsystem.resourceregistry.base.entities.EntityElementManagement;
import org.gcube.informationsystem.resourceregistry.contexts.ContextUtility;
import org.gcube.informationsystem.resourceregistry.contexts.ServerContextCache;
import org.gcube.informationsystem.resourceregistry.environments.Environment;
import org.gcube.informationsystem.resourceregistry.environments.Environment.PermissionMode;
import org.gcube.informationsystem.resourceregistry.environments.administration.AdminEnvironment;
import org.gcube.informationsystem.resourceregistry.environments.instances.InstanceEnvironment;
import org.gcube.informationsystem.resourceregistry.instances.model.ERManagement;
import org.gcube.informationsystem.resourceregistry.instances.model.Operation;
import org.gcube.informationsystem.resourceregistry.instances.model.relations.RelationManagement;
import org.gcube.informationsystem.resourceregistry.rest.requests.RequestUtility;
import org.gcube.informationsystem.resourceregistry.rest.requests.ServerRequestInfo;
import org.gcube.informationsystem.resourceregistry.types.TypesCache;
import org.gcube.informationsystem.resourceregistry.utils.MetadataUtility;
import org.gcube.informationsystem.resourceregistry.utils.OrientDBUtility;
import org.gcube.informationsystem.types.reference.entities.EntityType;

import com.orientechnologies.orient.core.db.document.ODatabaseDocument;
import com.orientechnologies.orient.core.record.ODirection;
import com.orientechnologies.orient.core.record.OEdge;
import com.orientechnologies.orient.core.record.OElement;
import com.orientechnologies.orient.core.record.OVertex;
import com.orientechnologies.orient.core.record.impl.ODocument;
import com.orientechnologies.orient.core.sql.executor.OResult;
import com.orientechnologies.orient.core.sql.executor.OResultSet;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public abstract class EntityManagement<E extends Entity, ET extends EntityType> extends EntityElementManagement<E, ET> implements ERManagement {
	
	/**
	 * The source context of an addToContex
	 */
	protected InstanceEnvironment sourceInstanceEnvironment;
	
	/**
	 * The target context of an addToContex/RemoveFromContext
	 */
	protected InstanceEnvironment targetInstanceEnvironment;

	/**
	 * By default the system honour the propagation constraints 
	 * so this variable is initialised as true.
	 * 
	 * To revert a previous operation or for a particular maintenance
	 * we could request to the service do not honour the propagation 
	 * constraints but under certain conditions and with some limitation 
	 * only. 
	 * These limitation are required to keep the system in a consistent
	 * state.
	 * In fact, this directive is only valid for Resources and IsRelatedTo
	 * relations. We need to take in account that to add an
	 * IsRelatedTo to a context always the source and target 
	 * Resources must be in such a Context.
	 * Please also take into account that adding a Resource 
	 * to a context always imply to honour the propagation constraints
	 * of ConsistsOf relations. In fact, a resource must be characterised
	 * least by one facet in any context it belongs. Giving that we
	 * cannot made assumption on which facets must be used. 
	 * A way could be to consider just the facets are mandatory for such a 
	 * Resource Type, but the type could not have mandatory facets 
	 * (even every Resource Type in the gCube Model has one mandatory facet).
	 * As counterpart, when a Resource is removed from a Context all the facets
	 * charactering it must be removed.
	 * 
	 * This option can also be used in conjunction with 
	 * {@link ElementManagement#dryRun}=true. 
	 * This allow to simulate a sharing operation which requires 
	 * do not honour the propagation constraints.
	 */
	protected boolean honourPropagationConstraintsInContextSharing;
	
	@Override
	public void setSourceInstanceEnvironment(InstanceEnvironment source) {
		this.sourceInstanceEnvironment = source;
	}
	
	@Override
	public InstanceEnvironment getSourceInstanceEnvironment() {
		return sourceInstanceEnvironment;
	}
	
	@Override
	public void setTargetInstanceEnvironment(InstanceEnvironment target) {
		this.targetInstanceEnvironment = target;
	}
	
	@Override
	public InstanceEnvironment getTargetInstanceEnvironment() {
		return sourceInstanceEnvironment;
	}

	@Override
	public boolean isHonourPropagationConstraintsInContextSharing() {
		return honourPropagationConstraintsInContextSharing;
	}

	@Override
	public void setHonourPropagationConstraintsInContextSharing(boolean honourPropagationConstraintsInContextSharing) {
		this.honourPropagationConstraintsInContextSharing = honourPropagationConstraintsInContextSharing;
	}
	
	/**
	 * Provide a cache edge-internal-id -> RelationManagement 
	 * this avoid to recreate the relationManagement of already visited edges
	 */
	protected Map<String,RelationManagement<?, ?>> relationManagements;
	
	/* The instance is added to the context even is not in source context */
	protected boolean forceAddToContext;
	
	/* Indicate that AddToContext skipped the instance because it was not the source context */
	protected boolean skipped; 
	
	protected EntityManagement(AccessType accessType) {
		super(accessType);
		
		this.ignoreStartWithKeys.add(IN_PREFIX.toLowerCase());
		this.ignoreStartWithKeys.add(OUT_PREFIX.toLowerCase());
		this.ignoreStartWithKeys.add(IN_PREFIX.toUpperCase());
		this.ignoreStartWithKeys.add(OUT_PREFIX.toUpperCase());
		
		this.relationManagements = new HashMap<>();
		
		/*
		 * By default the system honor the propagation constraints 
		 * so this variable is initialized as true.
		 */
		this.honourPropagationConstraintsInContextSharing = true;
		
		this.skipped = false;
	}
	
	@Override
	protected Environment getWorkingEnvironment() throws ResourceRegistryException {
		if(workingEnvironment == null) {
			workingEnvironment = ContextUtility.getCurrentRequestEnvironment();
			Context context = ServerContextCache.getInstance().getFullInfoContextByUUID(workingEnvironment.getUUID());
			String state = context.getState();
			if(state.compareTo(ContextState.ACTIVE.getState())!=0) {
				Set<String> allowedRoles = workingEnvironment.getAllowedRoles();
				if(!workingEnvironment.isUserAllowed(allowedRoles)) {
					throw new ForbiddenException("You are not allowed to operate in non " + ContextState.ACTIVE.getState() + " Contexts. Allowed roles are " + allowedRoles);
				}
			}
		}
		return workingEnvironment;
	}
	
	@Override
	public OVertex getElement() throws NotFoundException, AvailableInAnotherContextException, ResourceRegistryException {
		try {
			element = super.getElement();
		} catch(NotFoundException e) {
			try {
				retrieveElementFromAnyContext();
				throw getSpecificAvailableInAnotherContextException(typeName == null ? accessType.getName()
						: typeName + " with ID " + uuid + " is available in another "
								+ Context.class.getSimpleName());
			} catch(AvailableInAnotherContextException e1) {
				throw e1;
			} catch(Exception e1) {
				throw e;
			}
		} catch(ResourceRegistryException e) {
			throw e;
		} catch(Exception e) {
			throw new ResourceRegistryException(e);
		}
		return element;
	}
	
	/*
	 * It works perfectly in case of any kind of update. In case of use from create
	 * the cache does not work by using the ID because until commit the edge has a
	 * fake id starting with - (minus) sign. This not imply any collateral effect
	 * but a better solution is a desiderata.
	 */
	protected RelationManagement<?,?> getRelationManagement(OEdge edge) throws ResourceRegistryException {
		String id = edge.getIdentity().toString();
		RelationManagement<?,?> relationManagement = relationManagements.get(id);
		if(relationManagement == null) {
			relationManagement = ElementManagementUtility.getRelationManagement(getWorkingEnvironment(), oDatabaseDocument, edge);
			relationManagements.put(id, relationManagement);
		}
		return relationManagement;
	}
	
	public void addToRelationManagements(RelationManagement<?,?> relationManagement)
			throws ResourceRegistryException {
		OElement elem = relationManagement.getElement();
		String id = elem.getIdentity().toString();
		if(relationManagements.get(id) != null && relationManagements.get(id) != relationManagement) {
			StringBuilder errorMessage = new StringBuilder();
			errorMessage.append("Two different instance of ");
			errorMessage.append(relationManagement.getClass().getSimpleName());
			errorMessage.append(" point to the same ");
			errorMessage.append(elem.getClass().getSimpleName());
			errorMessage.append(". ");
			errorMessage.append(OrientDBUtility.SHOULD_NOT_OCCUR_ERROR_MESSAGE);
			throw new ResourceRegistryException(errorMessage.toString());
		}
		relationManagements.put(id, relationManagement);
	}
	
	protected static JsonNode addRelation(JsonNode sourceResource, JsonNode relation, String arrayKey)
			throws ResourceRegistryException {
		ObjectMapper objectMapper = new ObjectMapper();
		ArrayNode relationArray = objectMapper.createArrayNode();
		try {
			if(sourceResource.has(arrayKey)) {
				relationArray = (ArrayNode) sourceResource.get(arrayKey);
			}
			relationArray.add(relation);
			
			((ObjectNode) sourceResource).replace(arrayKey, relationArray);
		} catch(Exception e) {
			throw new ResourceRegistryException(e);
		}
		return sourceResource;
	}
	
	@Override
	protected OVertex createVertex() throws EntityAlreadyPresentException, ResourceRegistryException {
		
		logger.trace("Going to create {} for {} ({}) using {}", OVertex.class.getSimpleName(), accessType.getName(),
				typeName, jsonNode);
		
		try {
			
			if(oClass.isAbstract()) {
				String error = "Trying to create an instance of %s of type %s which is abstract. The operation will be aborted.".formatted(
						accessType.getName(), typeName);
				throw new SchemaViolationException(error);
			}
			
			OVertex vertexEntity = oDatabaseDocument.newVertex(typeName);
			
			try {
				if(uuid != null) {
					OVertex v = getElement();
					if(v != null) {
						String error = "A %s with UUID %s already exist".formatted(typeName, uuid.toString());
						throw getSpecificAlreadyPresentException(error);
					}
				}
				
			} catch(NotFoundException e) {
				try {
					OElement el = ElementManagementUtility.getAnyElementByUUID(uuid);
					String error = "UUID %s is already used by another %s. This is not allowed.".formatted(
							uuid.toString(), (el instanceof OVertex) ? Entity.NAME : Relation.NAME);
					throw getSpecificAvailableInAnotherContextException(error);
					
				} catch(NotFoundException e1) {
					// OK the UUID is not already used.
				}
			} catch(AvailableInAnotherContextException e) {
				throw e;
			}
			
			this.element = vertexEntity;
			
			if(accessType == AccessType.RESOURCE) {
				// Facet and relation are created in calling method
			} else {
				updateProperties(oClass, element, jsonNode, ignoreKeys, ignoreStartWithKeys);
			}
			
			logger.debug("Created {} is {}", OVertex.class.getSimpleName(),
					OrientDBUtility.getAsStringForLogging((OVertex) element));
			
			return element;
		} catch(ResourceRegistryException e) {
			throw e;
		} catch(Exception e) {
			logger.trace("Error while creating {} for {} ({}) using {}", OVertex.class.getSimpleName(),
					accessType.getName(), typeName, jsonNode, e);
			throw new ResourceRegistryException("Error Creating " + typeName + " with " + jsonNode, e.getCause());
		}
	}
	
	protected void reallyAddToContext()
			throws ContextException, ResourceRegistryException {
		if(!forceAddToContext && !sourceInstanceEnvironment.isElementInContext(getElement())) {
			// The element in not in the source security context. It will be skipped
			skipped = true;
			return;
		}
		
		
		targetInstanceEnvironment.addElement(getElement(), oDatabaseDocument);
		
		/* 
		 * DO NOT UNCOMMENT
		 * // affectedInstances.put(uuid, serializeSelfOnly());
		 * the instance is added in internalAddToContext() function after 
		 * the update of Metadata i.e. modifiedBy, lastUpdateTime 
		 */
		if(honourPropagationConstraintsInContextSharing) {
			Iterable<OEdge> edges = getElement().getEdges(ODirection.OUT);
			
			for(OEdge edge : edges) {
				RelationManagement<?,?> relationManagement = getRelationManagement(edge);
				relationManagement.setDryRun(dryRun);
				relationManagement.setHonourPropagationConstraintsInContextSharing(honourPropagationConstraintsInContextSharing);
				relationManagement.setSourceInstanceEnvironment(sourceInstanceEnvironment);
				relationManagement.setTargetInstanceEnvironment(targetInstanceEnvironment);
				relationManagement.internalAddToContext();
				affectedInstances.putAll(relationManagement.getAffectedInstances());
			}
		}
	}
	
	@Override
	public void internalAddToContext()
			throws ContextException, ResourceRegistryException {
		try {
			setOperation(Operation.ADD_TO_CONTEXT);
			reallyAddToContext();
			if(!skipped) {
				MetadataUtility.updateModifiedByAndLastUpdate(element);
				element.save();
				affectedInstances.put(uuid, serializeAsAffectedInstance());
				sanityCheck();
			}
		} catch(ResourceRegistryException e) {
			throw e;
		} catch(Exception e) {
			throw new ResourceRegistryException(
					"Error Adding " + typeName + " to " + targetInstanceEnvironment.toString(), e.getCause());
		}
	}
	
	@Override
	public void addToContext(UUID contextUUID) throws SchemaViolationException, NotFoundException, ContextException, ResourceRegistryException {
		String contextFullName = ServerContextCache.getInstance().getContextFullNameByUUID(contextUUID);
		logger.info("Going to add {} with UUID {} to Context with UUID {} (i.e. {})", accessType.getName(), uuid, contextUUID, contextFullName);
		ODatabaseDocument current = ContextUtility.getCurrentODatabaseDocumentFromThreadLocal();
		try {
			workingEnvironment = AdminEnvironment.getInstance();
			oDatabaseDocument = workingEnvironment.getDatabaseDocument(PermissionMode.WRITER);
			oDatabaseDocument.begin();
			setAsEntryPoint();
			
			sourceInstanceEnvironment = ContextUtility.getCurrentRequestEnvironment();
			targetInstanceEnvironment = ContextUtility.getInstance().getEnvironmentByUUID(contextUUID);
			
			internalAddToContext();
			
			if(!dryRun) {
				oDatabaseDocument.commit();
			}else {
				oDatabaseDocument.rollback();
			}
			logger.info("{} with UUID {} successfully added to Context with UUID {} (i.e. {})", typeName, uuid, contextUUID, contextFullName);
		} catch(ResourceRegistryException e) {
			logger.error("Unable to add {} with UUID {} to Context with UUID {} (i.e. {}) - Reason is {}", typeName, uuid, contextUUID, contextFullName, e.getMessage());
			if(oDatabaseDocument != null) {
				oDatabaseDocument.rollback();
			}
			throw e;
		} catch(Exception e) {
			logger.error("Unable to add {} with UUID {} to Context with UUID {} (i.e. {})", typeName, uuid, contextUUID, contextFullName, e);
			if(oDatabaseDocument != null) {
				oDatabaseDocument.rollback();
			}
			throw new ContextException(e);
		} finally {
			if(oDatabaseDocument != null) {
				oDatabaseDocument.close();
			}
			
			if(current!=null) {
				current.activateOnCurrentThread();
			}
		}
	}
	
	@Override
	public void internalRemoveFromContext()
			throws ContextException, ResourceRegistryException {
		try {
			setOperation(Operation.REMOVE_FROM_CONTEXT);
			reallyRemoveFromContext();
			MetadataUtility.updateModifiedByAndLastUpdate(element);
			element.save();
			affectedInstances.put(uuid, serializeAsAffectedInstance());
			sanityCheck();
		} catch(ResourceRegistryException e) {
			throw e;
		} catch(Exception e) {
			throw new ResourceRegistryException(
					"Error Removing " + typeName + " from " + targetInstanceEnvironment.toString(), e.getCause());
		}
	}
	
	protected void reallyRemoveFromContext()
			throws ContextException, ResourceRegistryException {
		
		if(!targetInstanceEnvironment.isElementInContext(getElement())) {
			// The element in not in the source security context. It will be skipped
			return;
		}
		
		if(honourPropagationConstraintsInContextSharing) {
			Iterable<OEdge> edges = getElement().getEdges(ODirection.OUT);
			
			for(OEdge edge : edges) {
				RelationManagement<?,?> relationManagement = getRelationManagement(edge);
				relationManagement.setDryRun(dryRun);
				relationManagement.setHonourPropagationConstraintsInContextSharing(honourPropagationConstraintsInContextSharing);
				// Not needed relationManagement.setSourceInstanceEnvironment(sourceInstanceEnvironment);
				relationManagement.setTargetInstanceEnvironment(targetInstanceEnvironment);
				relationManagement.internalRemoveFromContext();
				addToRelationManagements(relationManagement);
				affectedInstances.putAll(relationManagement.getAffectedInstances());
			}
		}
		
		targetInstanceEnvironment.removeElement(getElement(), oDatabaseDocument);
		
		/* 
		 * DO NOT UNCOMMENT
		 * affectedInstances.put(uuid, serializeSelfOnly());
		 * the instance is added in internalAddToContext() function after 
		 * the update of Metadata i.e. modifiedBy, lastUpdateTime
		 */
	}
	
	@Override
	public void removeFromContext(UUID contextUUID)
			throws SchemaViolationException, NotFoundException, ContextException, ResourceRegistryException {
		
		logger.debug("Going to remove {} with UUID {} from Context with UUID {}", typeName, uuid, contextUUID);
		ODatabaseDocument current = ContextUtility.getCurrentODatabaseDocumentFromThreadLocal();
		try {
			workingEnvironment = AdminEnvironment.getInstance();
			oDatabaseDocument = workingEnvironment.getDatabaseDocument(PermissionMode.WRITER);
			oDatabaseDocument.begin();
			setAsEntryPoint();
			
			// Not needed sourcesourceInstanceEnvironment = ContextUtility.getCurrentRequestEnvironment();
			targetInstanceEnvironment = ContextUtility.getInstance().getEnvironmentByUUID(contextUUID);
			
			internalRemoveFromContext();
			
			if(!dryRun) {
				oDatabaseDocument.commit();
			}else {
				oDatabaseDocument.rollback();
			}
			logger.info("{} with UUID {} successfully removed from Context with UUID {}", typeName, uuid, contextUUID);
		} catch(ResourceRegistryException e) {
			logger.error("Unable to remove {} with UUID {} from Context with UUID {}", typeName, uuid, contextUUID);
			if(oDatabaseDocument != null) {
				oDatabaseDocument.rollback();
			}
			throw e;
		} catch(Exception e) {
			logger.error("Unable to remove {} with UUID {} from Context with UUID {}", typeName, uuid, contextUUID,
					e);
			if(oDatabaseDocument != null) {
				oDatabaseDocument.rollback();
			}
			throw new ContextException(e);
		} finally {
			if(oDatabaseDocument != null) {
				oDatabaseDocument.close();
			}
			
			if(current!=null) {
				current.activateOnCurrentThread();
			}
		}
	}
	
	@Override
	public String reallyGetAll(boolean polymorphic) throws ResourceRegistryException {
		ObjectMapper objectMapper = new ObjectMapper();
		ArrayNode arrayNode = objectMapper.createArrayNode();
		
		ServerRequestInfo requestInfo = RequestUtility.getRequestInfo().get();
		int limit = requestInfo.getLimit();
		int offset = requestInfo.getOffset();
		
		int position = -1;
		int count = 0;
		
		Iterable<ODocument> iterable = oDatabaseDocument.browseClass(typeName, polymorphic);
		for(ODocument vertex : iterable) {
			if(++position < offset) {
				continue;
			}
			
			EntityManagement<?,?> entityManagement = ElementManagementUtility.getEntityManagement(getWorkingEnvironment(),
					oDatabaseDocument, (OVertex) vertex);
			try {
				entityManagement.setAsEntryPoint();
				JsonNode jsonNode = entityManagement.serializeAsJsonNode();
				arrayNode.add(jsonNode);
				if(limit > 0 && ++count >= limit) {
					break;
				}
			} catch(ResourceRegistryException e) {
				logger.error("Unable to correctly serialize {}. It will be excluded from results. {}",
						vertex.toString(), OrientDBUtility.SHOULD_NOT_OCCUR_ERROR_MESSAGE);
			}
		}
		
		try {
			return objectMapper.writeValueAsString(arrayNode);
		} catch(JsonProcessingException e) {
			throw new ResourceRegistryException(e);
		}
	}
	
	public String reallyQuery(String relationType, String referenceType, UUID referenceUUID,
			ODirection direction, boolean polymorphic, Map<String,String> constraint) throws ResourceRegistryException {
		ObjectMapper objectMapper = new ObjectMapper();
		ArrayNode arrayNode = objectMapper.createArrayNode();
		
		if(referenceUUID != null) {
			constraint.put(Entity.ID_PROPERTY, referenceUUID.toString());
		}
		
		/*
		 * SELECT FROM (
		 * 	TRAVERSE outV('EService'), inE('isIdentifiedBy') FROM (
		 * 		SELECT FROM SoftwareFacet WHERE group='VREManagement' AND name='SmartExecutor'
		 * 	)
		 * )
		 * 
		 * WHERE @class INSTANCEOF 'EService' // if polymorphic is true
		 * 		 
		 * WHERE @class='EService' // if polymorphic is false
		 */
		
		StringBuffer selectStringBuffer = new StringBuffer();
		selectStringBuffer.append("SELECT FROM (TRAVERSE ");
		selectStringBuffer.append(direction.name().toLowerCase());
		selectStringBuffer.append("V('");
		selectStringBuffer.append(typeName);
		selectStringBuffer.append("')");
		
		selectStringBuffer.append(", ");
		
		selectStringBuffer.append(direction.opposite().name().toLowerCase());
		selectStringBuffer.append("E('");
		selectStringBuffer.append(relationType);
		selectStringBuffer.append("')");
		
		selectStringBuffer.append(" FROM (SELECT FROM ");
		selectStringBuffer.append(referenceType);
		boolean first = true;
		for(String key : constraint.keySet()) {
			if(first) {
				selectStringBuffer.append(" WHERE ");
				first = false;
			} else {
				selectStringBuffer.append(" AND ");
			}
			selectStringBuffer.append(key);
			selectStringBuffer.append("=");
			String value = constraint.get(key).trim();
			selectStringBuffer.append("'");
			selectStringBuffer.append(value);
			selectStringBuffer.append("'");
		}
		selectStringBuffer.append(" ))");
		
		
		selectStringBuffer.append(" WHERE @class");
		if(polymorphic) {
			selectStringBuffer.append(" INSTANCEOF '");
		} else {	
			selectStringBuffer.append("='");
		}
		selectStringBuffer.append(typeName);
		selectStringBuffer.append("'");
		
		
		ServerRequestInfo requestInfo = RequestUtility.getRequestInfo().get();
		Integer limit = requestInfo.getLimit();
		if(limit==null) {
			limit = -1;
		}
		Integer offset = requestInfo.getOffset();
		if(offset == null) {
			offset = 0;
		}
		
		selectStringBuffer.append(" SKIP :offset");
		selectStringBuffer.append(" LIMIT :limit");
		
		Map<String, Object> map = new HashMap<>();
		map.put("offset", offset);
		map.put("limit", limit);
		
		
		String select = selectStringBuffer.toString();
		logger.trace(select);
		
		OResultSet resultSet = oDatabaseDocument.command(select, map);
		
		while(resultSet.hasNext()) {
			OResult oResult = resultSet.next();
			OElement element = ElementManagementUtility.getElementFromOptional(oResult.getElement()); 
			
			OVertex vertex = (OVertex) element;
			
			EntityManagement<?,?> entityManagement = ElementManagementUtility.getEntityManagement(getWorkingEnvironment(),
					oDatabaseDocument, vertex);
			try {
				if(constraint.containsKey(Entity.ID_PROPERTY)) {
					String uuid = constraint.get(Entity.ID_PROPERTY);
					if(entityManagement.getUUID().compareTo(UUID.fromString(uuid)) == 0) {
						continue;
					}
				}
				entityManagement.setAsEntryPoint();
				JsonNode jsonNode = entityManagement.serializeAsJsonNode();
				arrayNode.add(jsonNode);
			} catch(ResourceRegistryException e) {
				logger.error("Unable to correctly serialize {}. It will be excluded from results. {}",
						vertex.toString(), OrientDBUtility.SHOULD_NOT_OCCUR_ERROR_MESSAGE);
			}
		}
		
		try {
			return objectMapper.writeValueAsString(arrayNode);
		} catch(JsonProcessingException e) {
			throw new ResourceRegistryException(e);
		}
	}
	
	public String query(String relationType, String referenceType, UUID referenceUUID, ODirection direction,
			boolean polymorphic, Map<String,String> constraint) throws ResourceRegistryException {
		
		ODatabaseDocument current = ContextUtility.getCurrentODatabaseDocumentFromThreadLocal();
		try {
			workingEnvironment = AdminEnvironment.getInstance();
			oDatabaseDocument = workingEnvironment.getDatabaseDocument(PermissionMode.READER);
			
			setAsEntryPoint();
			setOperation(Operation.QUERY);
			
			TypesCache typesCache = TypesCache.getInstance();
			AccessType relationAccessType = typesCache.getCachedType(relationType).getAccessType();
			if(relationAccessType != AccessType.IS_RELATED_TO && relationAccessType != AccessType.CONSISTS_OF) {
				String error = "%s must be a relation type".formatted(relationType);
				throw new ResourceRegistryException(error);
			}
			
			AccessType referenceAccessType = typesCache.getCachedType(referenceType).getAccessType();
			if(referenceAccessType != AccessType.RESOURCE && referenceAccessType != AccessType.FACET) {
				String error = "%s must be a en entity type".formatted(referenceType);
				throw new ResourceRegistryException(error);
			}
			
			if(constraint == null) {
				constraint = new HashMap<>();
			}
			
			switch(accessType) {
				case RESOURCE:
					
					if(relationAccessType == AccessType.CONSISTS_OF) {
						
						if(direction != ODirection.OUT) {
							String error = "%s can only goes %s from %s.".formatted(relationType,
									ODirection.OUT.name(), typeName);
							throw new InvalidQueryException(error);
						} else {
							if(referenceAccessType != AccessType.FACET) {
								String error = "%s can only has as target a %s. Provided instead %s : %s".formatted(
										relationType, Facet.NAME, referenceAccessType, referenceType);
								throw new InvalidQueryException(error);
							}
						}
					}
					
					break;
				
				case FACET:
					if(relationAccessType != AccessType.CONSISTS_OF || direction != ODirection.IN
							|| referenceAccessType != AccessType.RESOURCE) {
						String error = "%s can only has %s %s from a %s.".formatted(typeName,
								ODirection.IN.name(), ConsistsOf.NAME, Resource.NAME);
						throw new InvalidQueryException(error);
					}
					
					break;
				
				default:
					break;
			}
			
			return reallyQuery(relationType, referenceType, referenceUUID, direction, polymorphic, constraint);
			
		} catch(ResourceRegistryException e) {
			throw e;
		} catch(Exception e) {
			throw new ResourceRegistryException(e);
		} finally {
			if(oDatabaseDocument != null) {
				oDatabaseDocument.close();
			}
			if(current!=null) {
				current.activateOnCurrentThread();
			}
		}
	}

	@Override
	public void setForceAddToContext(Boolean forceAddToContext) {
		this.forceAddToContext = forceAddToContext;
	}

}
