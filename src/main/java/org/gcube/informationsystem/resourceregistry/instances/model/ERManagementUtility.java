package org.gcube.informationsystem.resourceregistry.instances.model;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.gcube.com.fasterxml.jackson.databind.JsonNode;
import org.gcube.informationsystem.base.reference.AccessType;
import org.gcube.informationsystem.model.reference.entities.Entity;
import org.gcube.informationsystem.model.reference.relations.Relation;
import org.gcube.informationsystem.resourceregistry.api.exceptions.NotFoundException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.contexts.ContextException;
import org.gcube.informationsystem.resourceregistry.base.ElementManagement;
import org.gcube.informationsystem.resourceregistry.base.ElementManagementUtility;
import org.gcube.informationsystem.resourceregistry.contexts.ContextUtility;
import org.gcube.informationsystem.resourceregistry.environments.Environment;
import org.gcube.informationsystem.resourceregistry.environments.Environment.PermissionMode;
import org.gcube.informationsystem.resourceregistry.environments.administration.AdminEnvironment;
import org.gcube.informationsystem.resourceregistry.environments.instances.InstanceEnvironment;
import org.gcube.informationsystem.resourceregistry.instances.model.entities.FacetManagement;
import org.gcube.informationsystem.resourceregistry.instances.model.entities.ResourceManagement;
import org.gcube.informationsystem.resourceregistry.instances.model.relations.ConsistsOfManagement;
import org.gcube.informationsystem.resourceregistry.instances.model.relations.IsRelatedToManagement;
import org.gcube.informationsystem.resourceregistry.types.TypesCache;
import org.gcube.informationsystem.utils.TypeUtility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.orientechnologies.orient.core.db.document.ODatabaseDocument;
import com.orientechnologies.orient.core.record.OEdge;
import com.orientechnologies.orient.core.record.OElement;
import com.orientechnologies.orient.core.record.OVertex;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class ERManagementUtility {
	
	private static Logger staticLogger = LoggerFactory.getLogger(ERManagementUtility.class);
	
	public static ElementManagement<?,?> getERManagement(String type) throws ResourceRegistryException {
		
		AccessType accessType = TypesCache.getInstance().getCachedType(type).getAccessType();
		
		ElementManagement<?,?> erManagement = null;
		
		switch (accessType) {
			case RESOURCE:
				erManagement = new ResourceManagement();
				break;
	
			case FACET:
				erManagement = new FacetManagement();
				break;
			
			case IS_RELATED_TO:
				erManagement = new IsRelatedToManagement();
				break;
				
			case CONSISTS_OF:
				erManagement = new ConsistsOfManagement();
				break;
			
			default:
				throw new ResourceRegistryException("%s is not querable".formatted(type.toString()));
		}
		erManagement.setElementType(type);
		
		return erManagement;
	}

	public static ElementManagement<?, ?> getERManagementFromUUID(Environment workingContext, ODatabaseDocument orientGraph,
			UUID uuid) throws ResourceRegistryException {
		OElement element;
		try {
			element = ElementManagementUtility.getAnyElementByUUID(orientGraph, uuid);
			return getERManagement(workingContext, orientGraph, element);
		} catch(Exception e) {
			throw new ResourceRegistryException("%s does not belong to an %s nor to a %s".formatted(
					uuid.toString(), Entity.NAME, Relation.NAME));
		}
	}
	
	public static ElementManagement<?,?> getERManagement(Environment workingContext, ODatabaseDocument orientGraph,
			OElement element) throws ResourceRegistryException {
		if(element instanceof OVertex vertex) {
			return ElementManagementUtility.getEntityManagement(workingContext, orientGraph, vertex);
		} else if(element instanceof OEdge edge) {
			return ElementManagementUtility.getRelationManagement(workingContext, orientGraph, edge);
		}
		throw new ResourceRegistryException("%s is not a %s nor a %s".formatted(element.getClass().getSimpleName(),
				Entity.NAME, Relation.NAME));
	}
	
	public static Map<UUID,JsonNode> addToContextNoPropagationConstraint(Map<UUID, JsonNode> expectedInstances, UUID contextUUID, boolean dryRun)
			throws NotFoundException, ContextException, ResourceRegistryException {
		Set<UUID> instances = expectedInstances.keySet();
		staticLogger.info("Going to add {} to Context with UUID {} not following Propagation Constraints", instances, contextUUID);
		ODatabaseDocument current = ContextUtility.getCurrentODatabaseDocumentFromThreadLocal();
		ODatabaseDocument oDatabaseDocument = null;
		try {
			AdminEnvironment adminEnvironment = AdminEnvironment.getInstance();
			oDatabaseDocument = adminEnvironment.getDatabaseDocument(PermissionMode.WRITER);
			oDatabaseDocument.begin();
			
			InstanceEnvironment targetInstanceEnvironment = ContextUtility.getInstance().getEnvironmentByUUID(contextUUID);
			
			// Map<UUID, JsonNode> enforcedInstances = new HashMap<>();
			
			Map<UUID, ElementManagement<?,?>> instancesManagement = new HashMap<>();
			Set<UUID> uuids = expectedInstances.keySet();
			for(UUID uuid : uuids) {
				String type = TypeUtility.getTypeName(expectedInstances.get(uuid));
				ElementManagement<?,?> elementManagement = ERManagementUtility.getERManagement(type);
				elementManagement.setWorkingEnvironment(adminEnvironment);
				elementManagement.setODatabaseDocument(oDatabaseDocument);
				elementManagement.setUUID(uuid);
				elementManagement.setElementType(type);
				elementManagement.setDryRun(dryRun);
				
				((ERManagement) elementManagement).setHonourPropagationConstraintsInContextSharing(false);
				// enforcedInstances.putAll(((ERManagement) elementManagement).internalAddToContext(targetInstanceEnvironment));
				((ERManagement) elementManagement).setTargetInstanceEnvironment(targetInstanceEnvironment);
				((ERManagement) elementManagement).internalAddToContext();
				instancesManagement.put(uuid, elementManagement);
			}
			
			/*
			for(UUID uuid : uuids) {
				ElementManagement<?,?> elementManagement = instancesManagement.get(uuid);
				((ERManagement) elementManagement).contextSanityCheck(targetInstanceEnvironment, expectedInstances);
			}
			*/
			
			/*
			SharingOperationValidator operationValidator = new SharingOperationValidator(expectedInstances, SharingOperation.ADD);
			if(operationValidator.isValidOperation(enforcedInstances)) {
				 oDatabaseDocument.commit();
			}
			*/
			
			oDatabaseDocument.activateOnCurrentThread();
			oDatabaseDocument.commit();
			staticLogger.info("{} successfully added to Context with UUID {} not following Propagation Constraints", instances, contextUUID);
			
			/*
			 * Without following the propagation constraint no instances are enforced to be added
			 * to the context. The sanity check ensure that the graph is consistent otherwise a
			 * an exception is raised.
			 * So no need to collect affected instances and expected instance can be returned.
			 */
			return expectedInstances;
		} catch(ResourceRegistryException e) {
			staticLogger.error("Unable to add {} to Context with UUID {} not following Propagation Constraints - Reason is {}", instances, contextUUID, e.getMessage());
			if(oDatabaseDocument != null) {
				oDatabaseDocument.rollback();
			}
			throw e;
		} catch(Exception e) {
			staticLogger.error("Unable to add {} to Context with UUID {} not following Propagation Constraints.", instances, contextUUID, e.getMessage());
			if(oDatabaseDocument != null) {
				oDatabaseDocument.rollback();
			}
			throw new ContextException(e);
		} finally {
			if(oDatabaseDocument != null) {
				oDatabaseDocument.close();
			}
			
			if(current!=null) {
				current.activateOnCurrentThread();
			}
		}
	}
	
	public static Map<UUID,JsonNode> removeFromContextNoPropagationConstraint(Map<UUID, JsonNode> expectedInstances, UUID contextUUID, boolean dryRun)
			throws NotFoundException, ContextException, ResourceRegistryException {
		Set<UUID> instances = expectedInstances.keySet();
		staticLogger.info("Going to remove {} from Context with UUID {} not following Propagation Constraints", instances, contextUUID);
		ODatabaseDocument current = ContextUtility.getCurrentODatabaseDocumentFromThreadLocal();
		ODatabaseDocument oDatabaseDocument = null;
		try {
			AdminEnvironment adminEnvironment = AdminEnvironment.getInstance();
			oDatabaseDocument = adminEnvironment.getDatabaseDocument(PermissionMode.WRITER);
			oDatabaseDocument.begin();
			
			InstanceEnvironment targetInstanceEnvironment = ContextUtility.getInstance().getEnvironmentByUUID(contextUUID);
			
			//Map<UUID, JsonNode> enforcedInstances = new HashMap<>();
			
			Map<UUID, ElementManagement<?,?>> instancesManagement = new HashMap<>();
			for(UUID uuid : expectedInstances.keySet()) {
				String type = TypeUtility.getTypeName(expectedInstances.get(uuid));
				ElementManagement<?,?> elementManagement = ERManagementUtility.getERManagement(type);
				elementManagement.setWorkingEnvironment(adminEnvironment);
				elementManagement.setODatabaseDocument(oDatabaseDocument);
				elementManagement.setUUID(uuid);
				((ERManagement) elementManagement).setHonourPropagationConstraintsInContextSharing(false);
				elementManagement.setDryRun(dryRun);
				//enforcedInstances.putAll(((ERManagement) elementManagement).internalRemoveFromContext(targetInstanceEnvironment));
				((ERManagement) elementManagement).setTargetInstanceEnvironment(targetInstanceEnvironment);
				((ERManagement) elementManagement).internalRemoveFromContext();
				instancesManagement.put(uuid, elementManagement);
			}
			
			for(UUID uuid : expectedInstances.keySet()) {
				ElementManagement<?,?> elementManagement = instancesManagement.get(uuid);
				// TODO
				elementManagement.sanityCheck();
			}
			
			/*
			SharingOperationValidator operationValidator = new SharingOperationValidator(expectedInstances, SharingOperation.REMOVE);
			
			if(operationValidator.isValidOperation(enforcedInstances)) {
				 oDatabaseDocument.commit();
			}
			*/
			
			oDatabaseDocument.commit();
			staticLogger.info("{} successfully removed from Context with UUID {} not following Propagation Constraints", instances, contextUUID);
			
			/*
			 * Without following the propagation constraint no instances are enforced to be added
			 * to the context. The sanity check ensure that the graph is consistent otherwise a
			 * an exception is raised.
			 * So no need to collect affected instances and expected instance can be returned.
			 */
			return expectedInstances;
		} catch(ResourceRegistryException e) {
			staticLogger.error("Unable to remove {} from Context with UUID {} not following Propagation Constraints - Reason is {}", instances, contextUUID, e.getMessage());
			if(oDatabaseDocument != null) {
				oDatabaseDocument.rollback();
			}
			throw e;
		} catch(Exception e) {
			staticLogger.error("Unable to remove {} from Context with UUID {} not following Propagation Constraints.", instances, contextUUID, e.getMessage());
			if(oDatabaseDocument != null) {
				oDatabaseDocument.rollback();
			}
			throw new ContextException(e);
		} finally {
			if(oDatabaseDocument != null) {
				oDatabaseDocument.close();
			}
			
			if(current!=null) {
				current.activateOnCurrentThread();
			}
		}
	}
	
}
