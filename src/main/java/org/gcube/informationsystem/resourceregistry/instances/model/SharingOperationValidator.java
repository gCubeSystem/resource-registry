package org.gcube.informationsystem.resourceregistry.instances.model;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.gcube.com.fasterxml.jackson.databind.JsonNode;
import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;
import org.gcube.informationsystem.resourceregistry.api.rest.SharingPath.SharingOperation;
import org.gcube.informationsystem.utils.TypeUtility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class SharingOperationValidator implements OperationValidator {

	protected static Logger logger = LoggerFactory.getLogger(SharingOperationValidator.class);
	
	protected final Map<UUID, JsonNode> expectedInstances;
	protected final SharingOperation sharingOperation;
	
	public SharingOperationValidator(Map<UUID, JsonNode> expectedInstances, SharingOperation sharingOperation) {
		this.expectedInstances = expectedInstances;
		this.sharingOperation = sharingOperation;
	}
	
	@Override
	public boolean isValidOperation(Map<UUID, JsonNode> affectedInstances) throws ResourceRegistryException {
		Set<UUID> expectedUUIDSet = expectedInstances.keySet();
		Set<UUID> affectedUUIDSet = new HashSet<>(affectedInstances.keySet());
		
		affectedUUIDSet.removeAll(expectedUUIDSet);
		if(affectedUUIDSet.size()>0) {
			StringBuffer stringBuffer = new StringBuffer();
			stringBuffer.append("To keep the integrity of the IS the operation ");
			stringBuffer.append(sharingOperation.toString());
			stringBuffer.append(" affects more instances than the requested by the client. The operation will be aborted. ");
			stringBuffer.append("Additional affected instances are ");
			stringBuffer.append(affectedUUIDSet.toString());
			String errorMessage = stringBuffer.toString();
			logger.error(errorMessage);
			throw new ResourceRegistryException(errorMessage);
		}
				
		for(UUID expectedUUID : expectedUUIDSet) {
			JsonNode expectedJsonNode = expectedInstances.get(expectedUUID);
			JsonNode affectedJsonNode = affectedInstances.get(expectedUUID);
			if(affectedJsonNode==null) {
				StringBuffer stringBuffer = new StringBuffer();
				stringBuffer.append("The operation ");
				stringBuffer.append(sharingOperation.toString());
				stringBuffer.append(" did not affected the instance with UUID ");
				stringBuffer.append(expectedUUID.toString());
				stringBuffer.append(" ");
				stringBuffer.append(expectedJsonNode);
				stringBuffer.append(". This should not occur. Please contact the administrator.");
				String errorMessage = stringBuffer.toString();
				logger.error(errorMessage);
				throw new ResourceRegistryException(errorMessage);
			}
			
			String expectedType = TypeUtility.getTypeName(expectedJsonNode);
			String affectedType = TypeUtility.getTypeName(affectedJsonNode);
			if(expectedType.compareTo(affectedType)!=0) {
				StringBuffer stringBuffer = new StringBuffer();
				stringBuffer.append("The expected type of the instance with UUID ");
				stringBuffer.append(expectedUUID);
				stringBuffer.append(" is ");
				stringBuffer.append(expectedType);
				stringBuffer.append(". Instead the effective instance with such UUID is");
				stringBuffer.append(affectedType);
				stringBuffer.append(". The operation has been aborted.");
				String errorMessage = stringBuffer.toString();
				logger.error(errorMessage);
				throw new ResourceRegistryException(errorMessage);
			}
		}
		
		return true;
	}

}
