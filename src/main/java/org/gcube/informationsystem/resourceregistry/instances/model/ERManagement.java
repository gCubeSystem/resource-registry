package org.gcube.informationsystem.resourceregistry.instances.model;

import java.util.Map;
import java.util.UUID;

import org.gcube.com.fasterxml.jackson.databind.JsonNode;
import org.gcube.informationsystem.resourceregistry.api.exceptions.AvailableInAnotherContextException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.NotFoundException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.contexts.ContextException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.types.SchemaViolationException;
import org.gcube.informationsystem.resourceregistry.environments.instances.InstanceEnvironment;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface ERManagement {

	public boolean isHonourPropagationConstraintsInContextSharing();

	public void setHonourPropagationConstraintsInContextSharing(boolean honourPropagationConstraintsInContextSharing);

	public Map<UUID,JsonNode> getAffectedInstances();
	
	/**
	 * Set source InstanceEnvironment to evaluate addToContext
	 * @param source the source security context
	 */
	public void setSourceInstanceEnvironment(InstanceEnvironment source);
	
	public InstanceEnvironment getSourceInstanceEnvironment();
	
	public void internalAddToContext()
			throws ContextException, ResourceRegistryException;

	public void addToContext(UUID contextUUID)
			throws SchemaViolationException, NotFoundException, ContextException, ResourceRegistryException;
	
	/**
	 * Set target security context of addToContext/removeFromContext
	 * @param target
	 */
	public void setTargetInstanceEnvironment(InstanceEnvironment target);
	
	public InstanceEnvironment getTargetInstanceEnvironment();
	
	public void internalRemoveFromContext()
			throws ContextException, ResourceRegistryException;

	public void removeFromContext(UUID contextUUID)
			throws SchemaViolationException, NotFoundException, ContextException, ResourceRegistryException;

	// public void sanityCheck() throws SchemaViolationException, ResourceRegistryException;

	public AvailableInAnotherContextException getSpecificAvailableInAnotherContextException(String message);

	public void setForceAddToContext(Boolean forceAddToContext);

}