package org.gcube.informationsystem.resourceregistry.instances.model.relations;

import org.gcube.com.fasterxml.jackson.databind.JsonNode;
import org.gcube.com.fasterxml.jackson.databind.node.ObjectNode;
import org.gcube.informationsystem.base.reference.AccessType;
import org.gcube.informationsystem.base.reference.entities.EntityElement;
import org.gcube.informationsystem.model.impl.properties.PropagationConstraintImpl;
import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint.AddConstraint;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint.DeleteConstraint;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint.RemoveConstraint;
import org.gcube.informationsystem.model.reference.relations.Relation;
import org.gcube.informationsystem.resourceregistry.api.exceptions.NotFoundException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.relations.isrelatedto.IsRelatedToAlreadyPresentException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.relations.isrelatedto.IsRelatedToAvailableInAnotherContextException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.relations.isrelatedto.IsRelatedToNotFoundException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.types.SchemaViolationException;
import org.gcube.informationsystem.resourceregistry.base.entities.EntityElementManagement;
import org.gcube.informationsystem.resourceregistry.instances.model.entities.ResourceManagement;
import org.gcube.informationsystem.resourceregistry.utils.OrientDBUtility;
import org.gcube.informationsystem.types.reference.entities.EntityType;
import org.gcube.informationsystem.types.reference.entities.ResourceType;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class IsRelatedToManagement extends RelationManagement<ResourceManagement, ResourceType> {
	
	public static final PropagationConstraint DEFAULT_IS_RELATED_TO_PC;

	static {
		DEFAULT_IS_RELATED_TO_PC = new PropagationConstraintImpl();
		DEFAULT_IS_RELATED_TO_PC.setDeleteConstraint(DeleteConstraint.keep);
		DEFAULT_IS_RELATED_TO_PC.setRemoveConstraint(RemoveConstraint.keep);
		DEFAULT_IS_RELATED_TO_PC.setAddConstraint(AddConstraint.unpropagate);
	}
	
	public IsRelatedToManagement() {
		super(AccessType.IS_RELATED_TO,  Resource.class, DEFAULT_IS_RELATED_TO_PC);
	}
	
	@Override
	protected IsRelatedToNotFoundException getSpecificNotFoundException(NotFoundException e) {
		return new IsRelatedToNotFoundException(e.getMessage(), e.getCause());
	}
	
	@Override
	public IsRelatedToAvailableInAnotherContextException getSpecificAvailableInAnotherContextException(
			String message) {
		return new IsRelatedToAvailableInAnotherContextException(message);
	}
	
	@Override
	protected IsRelatedToAlreadyPresentException getSpecificAlreadyPresentException(String message) {
		return new IsRelatedToAlreadyPresentException(message);
	}
	
	@Override
	protected ResourceManagement newTargetEntityManagement() throws ResourceRegistryException {
		ResourceManagement resourceManagement = new ResourceManagement();
		resourceManagement.setWorkingEnvironment(getWorkingEnvironment());
		resourceManagement.setODatabaseDocument(oDatabaseDocument);
		return resourceManagement;
	}

	@Override
	protected JsonNode createCompleteJsonNode() throws ResourceRegistryException {
		JsonNode relation = serializeSelfAsJsonNode();
		
		try {
			if(includeSource) {
				EntityElementManagement<? extends EntityElement, ? extends EntityType> sourceEntityManagement = getSourceEntityManagement();
				((ObjectNode) relation).replace(Relation.SOURCE_PROPERTY, sourceEntityManagement.serializeSelfAsJsonNode());
			}
			
			if(includeTarget) {
				EntityElementManagement<? extends EntityElement, ? extends EntityType> targetEntityManagement = getTargetEntityManagement();
				
				/* 
				 * This function differ from the super implementation in RelationElementManagement from the following line of code
				 * An IsRelatedTo must not include the whole target Resource (with all the facets) instead only the Resource entity.
				 * 
				 * ((ObjectNode) relation).replace(Relation.TARGET_PROPERTY, targetEntityManagement.serializeAsJsonNode()); 
				 */
				((ObjectNode) relation).replace(Relation.TARGET_PROPERTY, targetEntityManagement.serializeSelfAsJsonNode());
			}
			
		} catch(ResourceRegistryException e) {
			logger.error("Unable to correctly serialize {}. {}", element, OrientDBUtility.SHOULD_NOT_OCCUR_ERROR_MESSAGE, e);
			throw e;
		} catch(Exception e) {
			logger.error("Unable to correctly serialize {}. {}", element, OrientDBUtility.SHOULD_NOT_OCCUR_ERROR_MESSAGE, e);
			throw new ResourceRegistryException(e);
		}
		
		return relation;
	}
	
	
	@Override
	public void sanityCheck() throws SchemaViolationException, ResourceRegistryException {
		super.sanityCheck();
	}
	
}
