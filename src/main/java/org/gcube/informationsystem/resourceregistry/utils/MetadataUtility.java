package org.gcube.informationsystem.resourceregistry.utils;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.gcube.com.fasterxml.jackson.core.JsonParseException;
import org.gcube.com.fasterxml.jackson.databind.JsonMappingException;
import org.gcube.com.fasterxml.jackson.databind.JsonNode;
import org.gcube.com.fasterxml.jackson.databind.node.ObjectNode;
import org.gcube.common.security.Owner;
import org.gcube.common.security.providers.SecretManagerProvider;
import org.gcube.informationsystem.base.reference.Element;
import org.gcube.informationsystem.base.reference.IdentifiableElement;
import org.gcube.informationsystem.model.reference.properties.Metadata;
import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;
import org.gcube.informationsystem.serialization.ElementMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.orientechnologies.orient.core.record.OElement;
import com.orientechnologies.orient.core.record.impl.ODocument;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class MetadataUtility {
	
	private static final Logger logger = LoggerFactory.getLogger(MetadataUtility.class);
	
	public static String getUser() {
		String username = Metadata.UNKNOWN_USER;
		try {
			Owner owner = SecretManagerProvider.get().getOwner();
			username = owner.getId();
		} catch(Exception e) {
			logger.error("Unable to retrieve user. {} will be used", username, e);
		}
		return username;
	}
	
	public static Metadata createMetadata() {
		MetadataOrient metadata = new MetadataOrient();
		
		String creator = getUser();
		metadata.setCreatedBy(creator);
		metadata.setLastUpdateBy(creator);
		
		Date date = Calendar.getInstance().getTime();
		SimpleDateFormat ft = new SimpleDateFormat("E yyyy.MM.dd 'at' hh:mm:ss a zzz");
		logger.trace("Setting Last Update and Creation Time to " + ft.format(date));
		
		metadata.setCreationTime(date);
		metadata.setLastUpdateTime(date);
		
		return metadata;
	}
	
	public static Metadata getMetadata(JsonNode jsonNode)
			throws JsonParseException, JsonMappingException, IOException, ResourceRegistryException {
		if(jsonNode.has(IdentifiableElement.METADATA_PROPERTY)) {
			ObjectNode metadataNode = jsonNode.get(IdentifiableElement.METADATA_PROPERTY).deepCopy();
			if(metadataNode.isNull()) {
				return null;
			}
			MetadataOrient metadata = new MetadataOrient();
			metadataNode.set(OrientDBUtility.ORIENTDB_CLASS_PROPERTY, metadataNode.get(Element.TYPE_PROPERTY));
			metadataNode.remove(Element.TYPE_PROPERTY);
			metadata.fromJSON(metadataNode.toString());
			return metadata;
		}
		return null;
	}
	
	public static MetadataOrient getMetadataOrient(ODocument oDocument) throws ResourceRegistryException {
		if(oDocument instanceof MetadataOrient orient) {
			return orient;
		} else {
			try {
				MetadataOrient metadataOrient = new MetadataOrient();
				String json = OrientDBUtility.toJsonString(oDocument);
				Metadata metadata = ElementMapper.unmarshal(Metadata.class, json);
				metadataOrient.setCreatedBy(metadata.getCreatedBy());
				metadataOrient.setCreationTime(metadata.getCreationTime());
				metadataOrient.setLastUpdateBy(metadata.getLastUpdateBy());
				metadataOrient.setLastUpdateTime(metadata.getLastUpdateTime());
				return metadataOrient;
			} catch(Exception e) {
				throw new ResourceRegistryException(
						"Unable to recreate Metadata. " + OrientDBUtility.SHOULD_NOT_OCCUR_ERROR_MESSAGE);
			}
		}
	}
	
	public static Metadata addMetadata(OElement element) {
		Metadata metadata = createMetadata();
		element.setProperty(IdentifiableElement.METADATA_PROPERTY, metadata);
		return metadata;
	}
	
	public static Metadata getMetadata(OElement element) throws ResourceRegistryException {
		return OrientDBUtility.getPropertyDocument(Metadata.class, element, IdentifiableElement.METADATA_PROPERTY);
	}
	
	public static void updateModifiedByAndLastUpdate(OElement element) throws ResourceRegistryException {
		ODocument oDocument = element.getProperty(IdentifiableElement.METADATA_PROPERTY);
		String lastUpdateBy = getUser();
		oDocument.field(Metadata.LAST_UPDATE_BY_PROPERTY, lastUpdateBy);
		Date lastUpdateTime = Calendar.getInstance().getTime();
		oDocument.field(Metadata.LAST_UPDATE_TIME_PROPERTY, lastUpdateTime);
		element.setProperty(IdentifiableElement.METADATA_PROPERTY, oDocument);
	}
	
}
