 package org.gcube.informationsystem.resourceregistry.utils;

import java.util.HashMap;
import java.util.UUID;

import org.gcube.com.fasterxml.jackson.databind.JsonNode;
import org.gcube.com.fasterxml.jackson.databind.ObjectMapper;
import org.gcube.com.fasterxml.jackson.databind.node.ObjectNode;
import org.gcube.informationsystem.base.reference.Element;
import org.gcube.informationsystem.base.reference.IdentifiableElement;
import org.gcube.informationsystem.base.reference.properties.PropertyElement;
import org.gcube.informationsystem.model.reference.entities.Entity;
import org.gcube.informationsystem.model.reference.relations.Relation;
import org.gcube.informationsystem.resourceregistry.api.exceptions.NotFoundException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;
import org.gcube.informationsystem.resourceregistry.base.ElementManagementUtility;
import org.gcube.informationsystem.resourceregistry.contexts.ContextUtility;
import org.gcube.informationsystem.resourceregistry.environments.Environment.PermissionMode;
import org.gcube.informationsystem.resourceregistry.environments.administration.AdminEnvironment;
import org.gcube.informationsystem.serialization.ElementMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.orientechnologies.orient.core.db.document.ODatabaseDocument;
import com.orientechnologies.orient.core.record.OEdge;
import com.orientechnologies.orient.core.record.OElement;
import com.orientechnologies.orient.core.record.ORecord;
import com.orientechnologies.orient.core.record.OVertex;
import com.orientechnologies.orient.core.record.impl.ODocument;
import com.orientechnologies.orient.core.record.impl.ODocumentHelper;
import com.orientechnologies.orient.core.sql.executor.OResult;
import com.orientechnologies.orient.core.sql.executor.OResultSet;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class OrientDBUtility {
	
	private static final Logger logger = LoggerFactory.getLogger(OrientDBUtility.class);
	
	public static final String ORIENTDB_CLASS_PROPERTY = ODocumentHelper.ATTRIBUTE_CLASS;
	
	public static final String SHOULD_NOT_OCCUR_ERROR_MESSAGE = "This is really strange and should not occur. Please contact the system administrator.";
	
	public static JsonNode toJsonNode(OElement element) throws ResourceRegistryException {
		ORecord oRecord = element.getRecord();
		return OrientDBUtility.toJsonNode(oRecord);
	}
	
	public static JsonNode toJsonNode(ORecord oRecord) throws ResourceRegistryException {
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			String string = toJsonString(oRecord);
			ObjectNode objectNode = (ObjectNode) objectMapper.readTree(string);
			return objectNode;
		} catch(Exception e) {
			throw new ResourceRegistryException(e);
		}
	}
	
	public static String replaceType(String s) {
		s = s.replace("\"" + ORIENTDB_CLASS_PROPERTY + "\"", "\"" + Element.TYPE_PROPERTY + "\"");
		return s;
	}
	
	public static String toJsonString(OResult oResult) {
		String ret = oResult.toJSON();
		// The above method set the type in @class property
		// We want to use the property set in Element.TYPE_PROPERTY
		ret = replaceType(ret);
		return ret;
	}
	
	public static String toJsonString(ORecord oRecord) {
		String ret = oRecord.toJSON("class");
		// The above method set the type in @class property
		// We want to use the property set in Element.TYPE_PROPERTY
		ret = replaceType(ret);
		return ret;
	}
	
	public static String getAsStringForLogging(ORecord oRecord) {
		return oRecord.toJSON();
	}
	
	public static String getAsStringForException(ORecord oRecord) {
		return toJsonString(oRecord);
	}
	
	public static <El extends OElement> El getElementByUUIDAsAdmin(String elementType, UUID uuid,
			Class<? extends El> clz) throws NotFoundException, ResourceRegistryException {
		ODatabaseDocument adminDatabaseDocument = null;
		ODatabaseDocument current = ContextUtility.getCurrentODatabaseDocumentFromThreadLocal();
		try {
			current = ContextUtility.getCurrentODatabaseDocumentFromThreadLocal();
			AdminEnvironment adminEnvironment = AdminEnvironment.getInstance();
			adminDatabaseDocument = adminEnvironment.getDatabaseDocument(PermissionMode.READER);
			return OrientDBUtility.getElementByUUID(adminDatabaseDocument, elementType, uuid, clz);
		} finally {
			if(adminDatabaseDocument != null) {
				adminDatabaseDocument.close();
			}
			if(current!=null) {
				current.activateOnCurrentThread();
			}
		}
	}
	
	public static <El extends OElement> El getElementByUUID(ODatabaseDocument oDatabaseDocument, String elementType, UUID uuid,
			Class<? extends El> clz) throws NotFoundException, ResourceRegistryException {
		
		if(elementType == null || elementType.compareTo("") == 0) {
			if(OVertex.class.isAssignableFrom(clz)) {
				elementType = Entity.NAME;
			}
			if(OEdge.class.isAssignableFrom(clz)) {
				elementType = Relation.NAME;
			}
		}
		
		// TODO Rewrite using Gremlin
		String select = "SELECT FROM " + elementType + " WHERE " + IdentifiableElement.ID_PROPERTY + " = \"" + uuid.toString() + "\"";
		
		OResultSet resultSet = oDatabaseDocument.query(select, new HashMap<>());
		
		
		if(resultSet == null || !resultSet.hasNext()) {
			String error = "No %s with UUID %s was found".formatted(elementType, uuid.toString());
			logger.debug(error);
			throw new NotFoundException(error);
		}
		
		OResult oResult = resultSet.next();
		@SuppressWarnings("unchecked")
		El element = (El) ElementManagementUtility.getElementFromOptional(oResult.getElement());
		
		logger.trace("{} with id {} is : {}", elementType, uuid.toString(), OrientDBUtility.getAsStringForLogging(element));
		
		if(resultSet.hasNext()) {
			throw new ResourceRegistryException("Found more than one " + elementType + " with uuid " + uuid.toString()
					+ ". This is a fatal error please contact Admnistrator");
		}
		
		return element;
	}
	
	public static <P extends PropertyElement> P getPropertyDocument(Class<P> clz, OElement element, String property)
			throws ResourceRegistryException {
		try {
			ODocument oDocument = element.getProperty(property);
			P e = ElementMapper.unmarshal(clz, OrientDBUtility.toJsonString(oDocument));
			return e;
		} catch(Exception ex) {
			String error = "Error while getting %s from %s".formatted(property, getAsStringForException(element));
			throw new ResourceRegistryException(error, ex);
		}
	}
	
}