package org.gcube.informationsystem.resourceregistry.utils;

import java.util.UUID;

import org.gcube.informationsystem.base.reference.IdentifiableElement;
import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;

import com.orientechnologies.orient.core.record.OElement;

/**
 * Utility class for handling UUID operations specific to the resource registry.
 * Extends the base UUIDUtility class from the gcube information system utilities.
 * @author Luca Frosini (ISTI - CNR)
 */
public class UUIDUtility extends org.gcube.informationsystem.utils.UUIDUtility {
	
	public static UUID getUUID(OElement element) throws ResourceRegistryException {
		String uuidString = element.getProperty(IdentifiableElement.ID_PROPERTY);
		UUID uuid = UUID.fromString(uuidString);
		return uuid;
	}
	
}
