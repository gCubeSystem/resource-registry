package org.gcube.informationsystem.resourceregistry.utils;

import java.security.Key;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.gcube.common.encryption.encrypter.StringEncrypter;
import org.gcube.informationsystem.model.reference.properties.Encrypted;
import org.gcube.informationsystem.model.reference.properties.Property;
import org.gcube.informationsystem.resourceregistry.dbinitialization.DatabaseEnvironment;
import org.gcube.informationsystem.resourceregistry.types.CachedType;
import org.gcube.informationsystem.resourceregistry.types.TypesCache;
import org.gcube.informationsystem.types.reference.properties.PropertyType;
import org.gcube.informationsystem.utils.TypeUtility;

import com.orientechnologies.orient.core.record.impl.ODocument;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
/**
 * The EncryptedOrient class extends ODocument and implements the Encrypted interface.
 * It provides functionality for handling encrypted values within an OrientDB document.
 * 
 * <p>This class includes methods for setting and getting encrypted and decrypted values,
 * as well as methods for encrypting values using database and context keys.</p>
 * 
 * <p>Fields:</p>
 * <ul>
 *   <li>{@code NAME} - The name of the encrypted document type.</li>
 *   <li>{@code VALUE} - The field name for the encrypted value.</li>
 *   <li>{@code decryptedValue} - The decrypted value of the document.</li>
 *   <li>{@code dbEncryptedValue} - The value encrypted with the database key.</li>
 *   <li>{@code contextEncryptedValue} - The value encrypted with the context key.</li>
 * </ul>
 * 
 * <p>Constructors:</p>
 * <ul>
 *   <li>{@code EncryptedOrient()} - Default constructor initializing the document with the encrypted type name.</li>
 *   <li>{@code EncryptedOrient(String iClassName)} - Protected constructor initializing the document with a specified class name.</li>
 * </ul>
 * 
 * <p>Methods:</p>
 * <ul>
 *   <li>{@code getTypeName()} - Returns the type name of the class.</li>
 *   <li>{@code getSupertypes()} - Returns a list of super types for the class.</li>
 *   <li>{@code getExpectedtype()} - Returns the expected type (currently returns null).</li>
 *   <li>{@code getEncryptedValue()} - Returns the encrypted value from the document field.</li>
 *   <li>{@code setEncryptedValue(String encryptedValue)} - Sets the encrypted value in the document field.</li>
 *   <li>{@code getDecryptedValue()} - Returns the decrypted value.</li>
 *   <li>{@code getDbEncryptedValue()} - Returns the value encrypted with the database key.</li>
 *   <li>{@code getContextEncryptedValue()} - Returns the value encrypted with the context key.</li>
 *   <li>{@code setDecryptedValue(String decryptedValue, boolean setEncryptedForContext)} - Sets the decrypted value and encrypts it using both database and context keys. Optionally sets the encrypted value for the context.</li>
 *   <li>{@code getAdditionalProperties()} - Returns additional properties (currently returns null).</li>
 *   <li>{@code setAdditionalProperties(Map<String, Object> additionalProperties)} - Sets additional properties (currently does nothing).</li>
 *   <li>{@code getAdditionalProperty(String key)} - Returns an additional property by key (currently returns null).</li>
 *   <li>{@code setAdditionalProperty(String key, Object value)} - Sets an additional property by key (currently does nothing).</li>
 *   <li>{@code getValue()} - Returns the encrypted value.</li>
 *   <li>{@code setValue(String value)} - Sets the encrypted value.</li>
 *   <li>{@code toJSON(String iFormat)} - Converts the document to a JSON string and replaces the type using OrientDBUtility.</li>
 * </ul>
 */
public class EncryptedOrient extends ODocument implements Encrypted {
	
	public static final String NAME = "Encrypted";
	public static final String VALUE = "value";
	
	protected String decryptedValue;
	protected String dbEncryptedValue;
	protected String contextEncryptedValue;
	
	public EncryptedOrient() {
		super(EncryptedOrient.NAME);
	}
	
	protected EncryptedOrient(String iClassName) {
		super(iClassName);
	}
	
	@Override
	public String getTypeName() {
		return TypeUtility.getTypeName(this.getClass());
	}
	
	@Override
	public List<String> getSupertypes() {
		TypesCache typesCache = TypesCache.getInstance();
		@SuppressWarnings("unchecked")
		CachedType<PropertyType> cachedType = (CachedType<PropertyType>) typesCache.getCachedType(getTypeName());
		try {
			return cachedType.getSuperTypes();
		} catch (Exception e) {
			List<String> list = new ArrayList<>();
			list.add(TypeUtility.getTypeName(Property.class));
			return list;
		}
	}
	
	@Override
	public String getExpectedtype() {
		return null;
	}

	public String getEncryptedValue() {
		return this.field(EncryptedOrient.VALUE);
	}

	public void setEncryptedValue(String encryptedValue) {
		this.field(EncryptedOrient.VALUE, encryptedValue);
	}
	
	public String getDecryptedValue() {
		return decryptedValue;
	}

	public String getDbEncryptedValue() {
		return dbEncryptedValue;
	}

	public String getContextEncryptedValue() {
		return contextEncryptedValue;
	}
	
	/**
	 * Sets the decrypted value and encrypts it using both the database key and the context key.
	 * Depending on the value of the setEncryptedForContext parameter, it sets the encrypted value
	 * to either the context-encrypted value or the database-encrypted value.
	 *
	 * @param decryptedValue the decrypted value to be set and encrypted
	 * @param setEncryptedForContext if true, sets the encrypted value to the context-encrypted value;
	 *                                otherwise, sets it to the database-encrypted value
	 * @throws Exception if an error occurs during encryption
	 */
	public void setDecryptedValue(String decryptedValue, boolean setEncryptedForContext) throws Exception {
		this.decryptedValue = decryptedValue;
		
		// Encrypting with DB Key
		Key databaseKey = DatabaseEnvironment.getDatabaseKey();
		this.dbEncryptedValue = StringEncrypter.getEncrypter().encrypt(decryptedValue, databaseKey);
		
		// Encrypting with Context Key (default key)
		this.contextEncryptedValue = StringEncrypter.getEncrypter().encrypt(decryptedValue);
		
		
		if(setEncryptedForContext) {
			setEncryptedValue(contextEncryptedValue);
		}else {
			setEncryptedValue(dbEncryptedValue);
		}
		
	}

	@Override
	public Map<String, Object> getAdditionalProperties() {
		return null;
	}

	@Override
	public void setAdditionalProperties(Map<String, Object> additionalProperties) {
		
	}

	@Override
	public Object getAdditionalProperty(String key) {
		return null;
	}

	@Override
	public void setAdditionalProperty(String key, Object value) {
			
	}
	
	@Override
	public String getValue() {
		return getEncryptedValue();
	}

	@Override
	public void setValue(String value) {
		setEncryptedValue(value);
	}
	
	@Override
	public String toJSON(String iFormat) {
		String ret =  super.toJSON(iFormat);
		ret = OrientDBUtility.replaceType(ret);
		return ret;
	}
	
}
