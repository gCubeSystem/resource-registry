package org.gcube.informationsystem.resourceregistry.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.gcube.informationsystem.model.reference.properties.PropagationConstraint;
import org.gcube.informationsystem.model.reference.properties.Property;
import org.gcube.informationsystem.resourceregistry.types.CachedType;
import org.gcube.informationsystem.resourceregistry.types.TypesCache;
import org.gcube.informationsystem.types.reference.properties.PropertyType;
import org.gcube.informationsystem.utils.TypeUtility;

import com.orientechnologies.orient.core.record.impl.ODocument;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class PropagationConstraintOrient extends ODocument implements PropagationConstraint {
	
	public PropagationConstraintOrient() {
		super(PropagationConstraint.NAME);
	}
	
	protected PropagationConstraintOrient(String iClassName) {
		super(iClassName);
	}
	
	@Override
	public String getTypeName() {
		return TypeUtility.getTypeName(this.getClass());
	}
	
	@Override
	public List<String> getSupertypes() {
		TypesCache typesCache = TypesCache.getInstance();
		@SuppressWarnings("unchecked")
		CachedType<PropertyType> cachedType = (CachedType<PropertyType>) typesCache.getCachedType(getTypeName());
		try {
			return cachedType.getSuperTypes();
		} catch (Exception e) {
			List<String> list = new ArrayList<>();
			list.add(TypeUtility.getTypeName(Property.class));
			return list;
		}
	}
	
	@Override
	public String getExpectedtype() {
		return null;
	}

	@Override
	public AddConstraint getAddConstraint() {
		return AddConstraint.valueOf((String) this.field(PropagationConstraint.ADD_PROPERTY));
	}

	@Override
	public void setAddConstraint(AddConstraint addConstraint) {
		this.field(PropagationConstraint.ADD_PROPERTY, addConstraint.name());
	}
	
	@Override
	public DeleteConstraint getDeleteConstraint() {
		return DeleteConstraint.valueOf((String) this.field(PropagationConstraint.DELETE_PROPERTY));
	}

	@Override
	public void setDeleteConstraint(DeleteConstraint deleteConstraint) {
		this.field(PropagationConstraint.DELETE_PROPERTY, deleteConstraint.name());
	}
	
	@Override
	public RemoveConstraint getRemoveConstraint() {
		return RemoveConstraint.valueOf((String) this.field(PropagationConstraint.REMOVE_PROPERTY));
	}

	@Override
	public void setRemoveConstraint(RemoveConstraint removeConstraint) {
		this.field(PropagationConstraint.REMOVE_PROPERTY, removeConstraint.name());
	}

	@Override
	public Map<String, Object> getAdditionalProperties() {
		return null;
	}

	@Override
	public void setAdditionalProperties(Map<String, Object> additionalProperties) {
		
	}

	@Override
	public Object getAdditionalProperty(String key) {
		return null;
	}

	@Override
	public void setAdditionalProperty(String key, Object value) {
			
	}
	
	@Override
	public String toJSON(String iFormat) {
		String ret =  super.toJSON(iFormat);
		ret = OrientDBUtility.replaceType(ret);
		return ret;
	}
	
}
