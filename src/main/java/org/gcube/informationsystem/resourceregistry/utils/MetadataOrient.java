package org.gcube.informationsystem.resourceregistry.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.gcube.informationsystem.model.reference.properties.Metadata;
import org.gcube.informationsystem.model.reference.properties.Property;
import org.gcube.informationsystem.resourceregistry.types.CachedType;
import org.gcube.informationsystem.resourceregistry.types.TypesCache;
import org.gcube.informationsystem.types.reference.properties.PropertyType;
import org.gcube.informationsystem.utils.TypeUtility;

import com.orientechnologies.orient.core.record.impl.ODocument;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class MetadataOrient extends ODocument implements Metadata {
	
	public MetadataOrient() {
		super(Metadata.NAME);
	}
	
	protected MetadataOrient(String iClassName) {
		super(iClassName);
	}
	
	@Override
	public String getTypeName() {
		return TypeUtility.getTypeName(this.getClass());
	}

	@Override
	public List<String> getSupertypes() {
		TypesCache typesCache = TypesCache.getInstance();
		@SuppressWarnings("unchecked")
		CachedType<PropertyType> cachedType = (CachedType<PropertyType>) typesCache.getCachedType(getTypeName());
		try {
			return cachedType.getSuperTypes();
		} catch (Exception e) {
			List<String> list = new ArrayList<>();
			list.add(TypeUtility.getTypeName(Property.class));
			return list;
		}
	}
	
	@Override
	public String getExpectedtype() {
		return null;
	}
	
	@Override
	public String getCreatedBy() {
		return this.field(Metadata.CREATED_BY_PROPERTY);
	}
	
	public void setCreatedBy(String createdBy) {
		this.field(Metadata.CREATED_BY_PROPERTY, createdBy);
	}
	
	@Override
	public Date getCreationTime() {
		return this.field(Metadata.CREATION_TIME_PROPERTY);
	}
	
	public void setCreationTime(Date creationTime) {
		this.field(Metadata.CREATION_TIME_PROPERTY, creationTime);
	}
	
	@Override
	public String getLastUpdateBy() {
		return this.field(Metadata.LAST_UPDATE_BY_PROPERTY);
	}
	
	public void setLastUpdateBy(String lastUpdateBy) {
		this.field(Metadata.LAST_UPDATE_BY_PROPERTY, lastUpdateBy);
	}
	
	@Override
	public Date getLastUpdateTime() {
		return this.field(Metadata.LAST_UPDATE_TIME_PROPERTY);
	}
	
	public void setLastUpdateTime(Date lastUpdateTime) {
		this.field(Metadata.LAST_UPDATE_TIME_PROPERTY, lastUpdateTime);
	}

	@Override
	public Map<String, Object> getAdditionalProperties() {
		return null;
	}

	@Override
	public void setAdditionalProperties(Map<String, Object> additionalProperties) {
		
	}

	@Override
	public Object getAdditionalProperty(String key) {
		return null;
	}

	@Override
	public void setAdditionalProperty(String key, Object value) {
		
	}
	
	@Override
	public String toJSON(String iFormat) {
		String ret =  super.toJSON(iFormat);
		ret = OrientDBUtility.replaceType(ret);
		return ret;
	}
	
}
