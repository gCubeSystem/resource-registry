package org.gcube.informationsystem.resourceregistry.base;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import jakarta.activation.UnsupportedDataTypeException;

import org.gcube.com.fasterxml.jackson.databind.JsonNode;
import org.gcube.com.fasterxml.jackson.databind.node.ArrayNode;
import org.gcube.com.fasterxml.jackson.databind.node.JsonNodeType;
import org.gcube.informationsystem.model.reference.entities.Facet;
import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.model.reference.relations.ConsistsOf;
import org.gcube.informationsystem.model.reference.relations.IsRelatedTo;
import org.gcube.informationsystem.resourceregistry.api.exceptions.NotFoundException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;
import org.gcube.informationsystem.resourceregistry.base.properties.PropertyElementManagement;
import org.gcube.informationsystem.resourceregistry.environments.Environment;
import org.gcube.informationsystem.resourceregistry.instances.model.entities.EntityManagement;
import org.gcube.informationsystem.resourceregistry.instances.model.entities.FacetManagement;
import org.gcube.informationsystem.resourceregistry.instances.model.entities.ResourceManagement;
import org.gcube.informationsystem.resourceregistry.instances.model.relations.ConsistsOfManagement;
import org.gcube.informationsystem.resourceregistry.instances.model.relations.IsRelatedToManagement;
import org.gcube.informationsystem.resourceregistry.instances.model.relations.RelationManagement;
import org.gcube.informationsystem.resourceregistry.utils.OrientDBUtility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.orientechnologies.orient.core.db.document.ODatabaseDocument;
import com.orientechnologies.orient.core.metadata.schema.OClass;
import com.orientechnologies.orient.core.record.OEdge;
import com.orientechnologies.orient.core.record.OElement;
import com.orientechnologies.orient.core.record.OVertex;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class ElementManagementUtility {
	
	private static Logger logger = LoggerFactory.getLogger(ElementManagementUtility.class);
	
	public static OElement getAnyElementByUUID(UUID uuid) throws NotFoundException, ResourceRegistryException {
		try {
			return OrientDBUtility.getElementByUUIDAsAdmin(null, uuid, OVertex.class);
		} catch(NotFoundException e) {
			return OrientDBUtility.getElementByUUIDAsAdmin(null, uuid, OEdge.class);
		} catch(ResourceRegistryException e) {
			throw e;
		} catch(Exception e) {
			throw new ResourceRegistryException(e);
		}
	}
	
	public static OElement getAnyElementByUUID(ODatabaseDocument oDatabaseDocument, UUID uuid)
			throws NotFoundException, ResourceRegistryException {
		try {
			return OrientDBUtility.getElementByUUID(oDatabaseDocument, null, uuid, OVertex.class);
		} catch(NotFoundException e) {
			return OrientDBUtility.getElementByUUID(oDatabaseDocument, null, uuid, OEdge.class);
		} catch(ResourceRegistryException e) {
			throw e;
		} catch(Exception e) {
			throw new ResourceRegistryException(e);
		}
	}
	
	
	
	public static EntityManagement<?, ?> getEntityManagement(Environment workingContext, ODatabaseDocument oDatabaseDocument,
			OVertex vertex) throws ResourceRegistryException {
		
		if(oDatabaseDocument == null) {
			throw new ResourceRegistryException(
					ODatabaseDocument.class.getSimpleName() + "instance is null. " + OrientDBUtility.SHOULD_NOT_OCCUR_ERROR_MESSAGE);
		}
		
		if(vertex == null) {
			throw new ResourceRegistryException(
					OVertex.class.getSimpleName() + "instance is null. " + OrientDBUtility.SHOULD_NOT_OCCUR_ERROR_MESSAGE);
		}
		
		OClass oClass = null;
		try {
			oClass = ElementManagementUtility.getOClass(vertex);
		} catch(Exception e) {
			String error = "Unable to detect type of %s. %s".formatted(vertex.toString(),
					OrientDBUtility.SHOULD_NOT_OCCUR_ERROR_MESSAGE);
			logger.error(error, e);
			throw new ResourceRegistryException(error);
		}
		
		EntityManagement<?, ?> entityManagement = null;
		if(oClass.isSubClassOf(Resource.NAME)) {
			entityManagement = new ResourceManagement();
		} else if(oClass.isSubClassOf(Facet.NAME)) {
			entityManagement = new FacetManagement();
		} else {
			String error = "{%s is not a %s nor a %s. %s".formatted(vertex, Resource.NAME, Facet.NAME,
					OrientDBUtility.SHOULD_NOT_OCCUR_ERROR_MESSAGE);
			throw new ResourceRegistryException(error);
		}
		entityManagement.setODatabaseDocument(oDatabaseDocument);
		entityManagement.setWorkingEnvironment(workingContext);
		entityManagement.setElement(vertex);
		return entityManagement;
	}
	
	public static RelationManagement<?,?> getRelationManagement(Environment workingContext, ODatabaseDocument oDatabaseDocument,
			OEdge edge) throws ResourceRegistryException {
		
		if(oDatabaseDocument == null) {
			throw new ResourceRegistryException(
					ODatabaseDocument.class.getSimpleName() + "instance is null. " + OrientDBUtility.SHOULD_NOT_OCCUR_ERROR_MESSAGE);
		}
		
		if(edge == null) {
			throw new ResourceRegistryException(
					OEdge.class.getSimpleName() + "instance is null. " + OrientDBUtility.SHOULD_NOT_OCCUR_ERROR_MESSAGE);
		}
		
		OClass oClass = ElementManagementUtility.getOClass(edge);
		
		RelationManagement<?,?> relationManagement = null;
		if(oClass.isSubClassOf(ConsistsOf.NAME)) {
			relationManagement = new ConsistsOfManagement();
		} else if(oClass.isSubClassOf(IsRelatedTo.NAME)) {
			relationManagement = new IsRelatedToManagement();
		} else {
			String error = "{%s is not a %s nor a %s. %s".formatted(edge, ConsistsOf.NAME, IsRelatedTo.NAME,
					OrientDBUtility.SHOULD_NOT_OCCUR_ERROR_MESSAGE);
			throw new ResourceRegistryException(error);
		}
		
		
		relationManagement.setODatabaseDocument(oDatabaseDocument);
		relationManagement.setWorkingEnvironment(workingContext);
		
		relationManagement.setElement(edge);
		return relationManagement;
	}
	
	public static <E extends OElement> E getElementFromOptional(Optional<E> optional) throws ResourceRegistryException {
		if(optional.isPresent()) {
			return optional.get();
		}else {
			throw new ResourceRegistryException("An element not belonging to any defined type should not exists. Please contact the administrator.");
		}
	}
	
	public static OClass getOClass(OElement oElement) throws ResourceRegistryException {
		Optional<OClass> optional = oElement.getSchemaType();
		if(optional.isPresent()) {
			return optional.get();
		}else {
			throw new ResourceRegistryException("An element not belonging to any defined type should not exists. Please contact the administrator.");
		}
	}

	public static Object getObjectFromJsonNode(JsonNode value)
			throws UnsupportedDataTypeException, ResourceRegistryException {
		JsonNodeType jsonNodeType = value.getNodeType();
		
		switch(jsonNodeType) {
			case OBJECT:
				return PropertyElementManagement.getPropertyDocument(value);
			
			case ARRAY:
				/*
				 * Due to bug https://github.com/orientechnologies/orientdb/issues/7354
				 * we should not support ArrayList
				 */
				List<Object> list = new ArrayList<>();
				ArrayNode arrayNode = (ArrayNode) value;
				for(JsonNode node : arrayNode) {
					list.add(getObjectFromJsonNode(node));
				}
				return list;
				
			case BINARY:
				break;
			
			case BOOLEAN:
				return value.asBoolean();
			
			case NULL:
				break;
			
			case NUMBER:
				if(value.isDouble() || value.isFloat()) {
					return value.asDouble();
				}
				if(value.isBigInteger() || value.isShort() || value.isInt()) {
					return value.asInt();
				}
				
				if(value.isLong()) {
					return value.asLong();
				}
				break;
			
			case STRING:
				return value.asText();
			
			case MISSING:
				break;
			
			case POJO:
				break;
			
			default:
				break;
		}
		
		return null;
	}
	
}
