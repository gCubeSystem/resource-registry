package org.gcube.informationsystem.resourceregistry.base;

import java.io.IOException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;

import jakarta.ws.rs.WebApplicationException;

import org.gcube.com.fasterxml.jackson.core.JsonProcessingException;
import org.gcube.com.fasterxml.jackson.databind.JsonNode;
import org.gcube.com.fasterxml.jackson.databind.ObjectMapper;
import org.gcube.com.fasterxml.jackson.databind.node.ArrayNode;
import org.gcube.com.fasterxml.jackson.databind.node.BooleanNode;
import org.gcube.com.fasterxml.jackson.databind.node.JsonNodeType;
import org.gcube.com.fasterxml.jackson.databind.node.ObjectNode;
import org.gcube.com.fasterxml.jackson.databind.node.TextNode;
import org.gcube.common.security.Owner;
import org.gcube.common.security.providers.SecretManagerProvider;
import org.gcube.common.security.secrets.Secret;
import org.gcube.informationsystem.base.reference.AccessType;
import org.gcube.informationsystem.base.reference.Element;
import org.gcube.informationsystem.base.reference.IdentifiableElement;
import org.gcube.informationsystem.model.reference.ERElement;
import org.gcube.informationsystem.model.reference.ModelElement;
import org.gcube.informationsystem.model.reference.properties.Metadata;
import org.gcube.informationsystem.model.reference.relations.Relation;
import org.gcube.informationsystem.resourceregistry.api.exceptions.AlreadyPresentException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.AvailableInAnotherContextException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.NotFoundException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.contexts.ContextException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.types.SchemaException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.types.SchemaViolationException;
import org.gcube.informationsystem.resourceregistry.base.properties.PropertyElementManagement;
import org.gcube.informationsystem.resourceregistry.contexts.ContextUtility;
import org.gcube.informationsystem.resourceregistry.contexts.ServerContextCache;
import org.gcube.informationsystem.resourceregistry.environments.Environment;
import org.gcube.informationsystem.resourceregistry.environments.Environment.PermissionMode;
import org.gcube.informationsystem.resourceregistry.environments.administration.AdminEnvironment;
import org.gcube.informationsystem.resourceregistry.environments.instances.InstanceEnvironment;
import org.gcube.informationsystem.resourceregistry.instances.model.Operation;
import org.gcube.informationsystem.resourceregistry.rest.requests.RequestUtility;
import org.gcube.informationsystem.resourceregistry.rest.requests.ServerRequestInfo;
import org.gcube.informationsystem.resourceregistry.types.CachedType;
import org.gcube.informationsystem.resourceregistry.types.TypesCache;
import org.gcube.informationsystem.resourceregistry.utils.MetadataOrient;
import org.gcube.informationsystem.resourceregistry.utils.MetadataUtility;
import org.gcube.informationsystem.resourceregistry.utils.OrientDBUtility;
import org.gcube.informationsystem.resourceregistry.utils.UUIDUtility;
import org.gcube.informationsystem.types.reference.Type;
import org.gcube.informationsystem.types.reference.entities.ResourceType;
import org.gcube.informationsystem.types.reference.properties.PropertyDefinition;
import org.gcube.informationsystem.types.reference.properties.PropertyType;
import org.gcube.informationsystem.utils.TypeUtility;
import org.gcube.informationsystem.utils.UUIDManager;
import org.gcube.smartgears.utils.InnerMethodName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.orientechnologies.orient.core.db.document.ODatabaseDocument;
import com.orientechnologies.orient.core.metadata.schema.OClass;
import com.orientechnologies.orient.core.metadata.schema.OProperty;
import com.orientechnologies.orient.core.metadata.schema.OType;
import com.orientechnologies.orient.core.record.OElement;
import com.orientechnologies.orient.core.record.impl.ODocument;
import com.orientechnologies.orient.core.util.ODateHelper;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public abstract class ElementManagement<El extends OElement, T extends Type> {
	
	protected Logger logger = LoggerFactory.getLogger(this.getClass());
	
	public final static String DELETED = "deleted"; 
	
	public final static String AT = "@";
	public final static String UNDERSCORE = "_";
	public final static String DOLLAR = "$";
	
	protected final Set<String> ignoreKeys;
	protected final Set<String> ignoreStartWithKeys;
	
	protected Class<El> elementClass;
	protected final AccessType accessType;
	
	protected ODatabaseDocument oDatabaseDocument;
	
	protected UUID uuid;
	protected JsonNode jsonNode;
	protected OClass oClass;
	protected String typeName;
	
	protected JsonNode self;
	protected JsonNode complete;
	
	protected CachedType<T> cachedType;
	
	protected El element;
	protected boolean reload;
	
	/**
	 * Used to force Meta inclusion.
	 * It is use for example to request to include meta
	 * by the context cache manager.
	 */
	protected boolean forceIncludeMeta;
	/**
	 * Used to force Meta inclusion in all instances.
	 * It is use for example to request to include meta
	 * by the context cache manager.
	 */
	protected boolean forceIncludeAllMeta;
	
	/**
	 * Some operation, e.g. delete has a cascade impact which is not know a priori by the client.
	 * Setting this variable to false the system just simulate the operation so an interested client
	 * could know the impact in advance. 
	 * 
	 * By the default the system execute effectively the requested operation.
	 * So this variable is initialised as false.
	 * 
	 */
	protected boolean dryRun;
	
	/**
	 * An operation can affects multiple instances (e.g. create, update)
	 * We need to know if the instance is the entry point of the operation 
	 * or if it is just a subordinated operation. 
	 * This is required for example in delete to trigger sanity check 
	 * in the Resource when the action is performed directly on the Facet.
	 * Resource sanity check is not required to be triggered by the Facet
	 * because the entry point of the action already take care 
	 * of triggering this action (e.g. the delete was invoked on the Resource and
	 * each describing Facets must not trigger multiple time the sanityCheck).
	 */
	protected boolean entryPoint;
	
	/**
	 * It is assigned only in the entry point
	 */
	protected Operation operation;
	
	/**
	 * A Delete an addToContext and a RemoveFromContext operation has a cascade impact 
	 * we want to know the impact, i.e. instances involved
	 */
	protected final Map<UUID,JsonNode> affectedInstances;
	
	
	
	protected ElementManagement(AccessType accessType) {
		this.accessType = accessType;
		
		this.ignoreKeys = new HashSet<String>();
		this.ignoreKeys.add(Element.TYPE_PROPERTY);
		this.ignoreKeys.add(ModelElement.SUPERTYPES_PROPERTY);
		this.ignoreKeys.add(ModelElement.EXPECTED_TYPE_PROPERTY);
		this.ignoreKeys.add(IdentifiableElement.ID_PROPERTY);
		this.ignoreKeys.add(IdentifiableElement.METADATA_PROPERTY);
		
		this.ignoreStartWithKeys = new HashSet<String>();
		this.ignoreStartWithKeys.add(ElementManagement.AT);
		this.ignoreStartWithKeys.add(ElementManagement.UNDERSCORE);
		this.ignoreStartWithKeys.add(ElementManagement.DOLLAR);
		
		this.reload = false;
		
		this.entryPoint = false;
		this.operation = null;
		
		/*
		 * By the default the system execute the operation 
		 * which has a cascade impact so this variable is initialised as false.
		 */
		this.dryRun = false;
		
		this.affectedInstances = new HashMap<>();
		
		this.forceIncludeMeta = false;
		this.forceIncludeAllMeta = false;
		
	}

	public boolean isForceIncludeMeta() {
		return forceIncludeMeta;
	}


	public void setForceIncludeMeta(boolean forceIncludeMeta) {
		this.forceIncludeMeta = forceIncludeMeta;
	}


	public boolean isForceIncludeAllMeta() {
		return forceIncludeAllMeta;
	}


	public void setForceIncludeAllMeta(boolean forceIncludeAllMeta) {
		this.forceIncludeAllMeta = forceIncludeAllMeta;
	}

	public Map<UUID,JsonNode> getAffectedInstances() {
		return affectedInstances;
	}
	
	public boolean isDryRun() {
		return dryRun;
	}
	
	public void setDryRun(boolean dryRun) {
		this.dryRun = dryRun;
	}

	public void setAsEntryPoint() {
		this.entryPoint = true;
	}

	public void setOperation(Operation operation) {
		this.operation = operation;
	}
	
	protected void cleanCachedSerialization() {
		this.self = null;
		this.complete = null;
	}
	
	public UUID getUUID() {
		return uuid;
	}

	public boolean isReload() {
		return reload;
	}
	
	public void setReload(boolean reload) {
		this.reload = reload;
	}
	
	public AccessType getAccessType() {
		return accessType;
	}
	
	protected Environment workingEnvironment;
	
	protected abstract Environment getWorkingEnvironment() throws ResourceRegistryException;
	
	public void setWorkingEnvironment(Environment workingContext) {
		this.workingEnvironment = workingContext;
	}
	
	public void setUUID(UUID uuid) throws ResourceRegistryException {
		this.uuid = uuid;
		if(jsonNode != null) {
			checkUUIDMatch();
		}
	}
	
	public void setJsonNode(JsonNode jsonNode) throws ResourceRegistryException {
		this.jsonNode = jsonNode;
		checkJsonNode();
	}
	
	public void setJson(String json) throws ResourceRegistryException {
		ObjectMapper mapper = new ObjectMapper();
		try {
			this.jsonNode = mapper.readTree(json);
		} catch(IOException e) {
			throw new ResourceRegistryException(e);
		}
		checkJsonNode();
	}
	
	public void setODatabaseDocument(ODatabaseDocument oDatabaseDocument) {
		this.oDatabaseDocument = oDatabaseDocument;
	}
	
	public void setOClass(OClass oClass) {
		this.oClass = oClass;
	}
	
	protected OClass getOClass() throws SchemaException, ResourceRegistryException {
		if(oClass == null) {
			if(element != null) {
				try {
					oClass = ElementManagementUtility.getOClass(element);
					if(typeName==null) {
						typeName = oClass.getName();
					}
					getCachedType().setOClass(oClass);
				}catch (ResourceRegistryException e) {
					try {
						oClass = getCachedType().getOClass();
						if(typeName==null) {
							typeName = oClass.getName();
						}
					}catch (Exception e1) {
						throw e;
					}
				}
			} else {
				if(typeName==null) {
					throw new SchemaException("Unknown type name. Please set it first.");
				}
				oClass = getCachedType().getOClass();
				AccessType gotAccessType = cachedType.getAccessType();
				if(accessType!=gotAccessType) {
					throw new SchemaException(typeName + " is not a " + accessType.getName());
				}
			}
		}
		return oClass;
	}
	
	@SuppressWarnings("unchecked")
	protected CachedType<T> getCachedType(){
		if(cachedType==null) {
			TypesCache typesCache = TypesCache.getInstance();
			cachedType = (CachedType<T>) typesCache.getCachedType(typeName);
		}
		return cachedType;
	}
	
	public void setElementType(String elementType) throws ResourceRegistryException {
		if(this.typeName == null) {
			if(elementType == null || elementType.compareTo("") == 0) {
				elementType = accessType.getName();
			}
			this.typeName = elementType;
		} else {
			if(elementType.compareTo(elementType) != 0) {
				throw new ResourceRegistryException(
						"Provided type " + elementType + " does not match with the one already known " + this.accessType);
			}
		}
		
		if(jsonNode != null) {
			checkERMatch();
		}
	}
	
	public String getTypeName() throws NotFoundException, AvailableInAnotherContextException, ResourceRegistryException {
		if(typeName==null) {
			if(element!=null) {
				typeName = getOClass().getName();
			}
			
			if(typeName==null && jsonNode!=null) {
				this.typeName = TypeUtility.getTypeName(jsonNode);
			}
			
		}
		return typeName;
	}
	
	protected void checkJsonNode() throws ResourceRegistryException {
		if(uuid == null) {
			try {
				uuid = UUIDUtility.getUUID(jsonNode);
			} catch(Exception e) {
			}
		} else {
			checkUUIDMatch();
		}
		
		if(uuid!=null) {
			try {
				UUIDManager.getInstance().validateUUID(uuid);
			} catch (Exception e) {
				throw new ResourceRegistryException(e.getMessage());
			}
		}
		
		if(this.typeName == null) {
			this.typeName = TypeUtility.getTypeName(jsonNode);
			getOClass();
		} else {
			checkERMatch();
		}
	}
	
	protected void checkERMatch() throws ResourceRegistryException {
		if(jsonNode != null) {
			String type = TypeUtility.getTypeName(jsonNode);
			if(type != null && type.compareTo(typeName) != 0) {
				String error = "Requested type does not match with json representation %s!=%s".formatted(
						typeName, type);
				logger.trace(error);
				throw new ResourceRegistryException(error);
			}
		}
		getOClass();
	}
	
	
	
	protected void checkUUIDMatch() throws ResourceRegistryException {
		UUID resourceUUID = UUIDUtility.getUUID(jsonNode); 
		if(resourceUUID!=null) {
			if(resourceUUID.compareTo(uuid) != 0) {
				String error = "UUID provided in the instance (%s) differs from UUID (%s) used to identify the %s instance".formatted(
						resourceUUID.toString(), uuid.toString(), typeName);
				throw new ResourceRegistryException(error);
				
			}
		}
	}
	
	private void analizeProperty(OElement element, String key, ObjectNode objectNode) throws ResourceRegistryException {
		Object object = element.getProperty(key);
		if(object == null) {
			objectNode.replace(key, null);
			return;
		}
		JsonNode jsonNode  = getPropertyForJson(key, object);
		if(jsonNode != null) {
			objectNode.replace(key, jsonNode);
		}
	}

	private JsonNode createSelfJsonNode() throws ResourceRegistryException {
		try {
			
			ObjectMapper objectMapper = new ObjectMapper();
			ObjectNode objectNode = objectMapper.createObjectNode();
			
			OElement element = getElement();
			Set<String> keys = element.getPropertyNames();
			
			/* Add first these key to provide an order in Json */
			
			ServerRequestInfo requestInfo = RequestUtility.getRequestInfo().get();
			
			List<String> keysToAddFirst = new ArrayList<>();
			keysToAddFirst.add(Element.TYPE_PROPERTY);
			keysToAddFirst.add(ModelElement.SUPERTYPES_PROPERTY);
			keysToAddFirst.add(IdentifiableElement.ID_PROPERTY);
			keysToAddFirst.add(IdentifiableElement.METADATA_PROPERTY);
			keysToAddFirst.add(ERElement.CONTEXTS_PROPERTY);
			keysToAddFirst.add(Relation.PROPAGATION_CONSTRAINT_PROPERTY);
			
			for(String key : keysToAddFirst) {
				switch (key) {
					case Element.TYPE_PROPERTY:
						objectNode.put(Element.TYPE_PROPERTY, getTypeName());
						break;
					
					case ModelElement.SUPERTYPES_PROPERTY:
						Collection<String> supertypes = getCachedType().getSuperTypes();
						ArrayNode arrayNode = objectMapper.valueToTree(supertypes);
						if(arrayNode==null || arrayNode.size()==0) {
							objectNode.remove(ModelElement.SUPERTYPES_PROPERTY);
						}else {
							objectNode.replace(ModelElement.SUPERTYPES_PROPERTY, arrayNode);
						}
						break;
					
					case IdentifiableElement.METADATA_PROPERTY:
						if(requestInfo.includeMeta() || forceIncludeMeta) {
							if(requestInfo.allMeta() || entryPoint || forceIncludeAllMeta) {
								analizeProperty(element, key, objectNode);
							}
						}
						break;
						
					case ERElement.CONTEXTS_PROPERTY:
						if(requestInfo.includeContexts()) {
							objectNode.replace(ERElement.CONTEXTS_PROPERTY, getContextsAsObjectNode());
						}
						break;
	
					default:
						if(keys.contains(key)) {
							analizeProperty(element, key, objectNode);
						}
						break;
				}
			}
			
			for(String key : keys) {
				if(keysToAddFirst.contains(key)) {
					// the property has been already added
					continue;
				}
				analizeProperty(element, key, objectNode);
			}
			
			return objectNode;
		} catch(ResourceRegistryException e) {
			throw e;
		} catch(Exception e) {
			throw new ResourceRegistryException("Error while serializing " + getElement().toString(), e);
		}
	}
	
	public JsonNode serializeAsAffectedInstance() throws ResourceRegistryException {
		return serializeSelfAsJsonNode();
	}
	
	public JsonNode serializeSelfAsJsonNode() throws ResourceRegistryException {
		try {
			if(self==null || reload) {
				self = createSelfJsonNode();
			}
			return self.deepCopy();
		} catch(Exception e) {
			throw new ResourceRegistryException(e);
		}
	}
	
	protected abstract JsonNode createCompleteJsonNode() throws ResourceRegistryException;
	
	public JsonNode serializeAsJsonNode() throws ResourceRegistryException {
		try {
			if(complete==null || reload) {
				complete = createCompleteJsonNode();
			}
			return complete;
		} catch(Exception e) {
			throw new ResourceRegistryException(e);
		}
	}
	
	protected abstract El reallyCreate() throws AlreadyPresentException, ResourceRegistryException;
	
	public El internalCreate() throws AlreadyPresentException, ResourceRegistryException {
		try {
			setOperation(Operation.CREATE);
			
			UUIDManager uuidManager = UUIDManager.getInstance();
			
			if(uuid == null) {
				uuid = uuidManager.generateValidUUID();
			}
			
			element = reallyCreate();
			
			element.setProperty(IdentifiableElement.ID_PROPERTY, uuid.toString());
			
			MetadataUtility.addMetadata(element);
			
			getWorkingEnvironment().addElement(element, oDatabaseDocument);
			
			element.save();
			
			sanityCheck();
			
			return element;
		} catch(ResourceRegistryException e) {
			throw e;
		} catch(Exception e) {
			throw new ResourceRegistryException("Error Creating " + typeName + " with " + jsonNode, e);
		}
	}
	
	protected abstract El reallyUpdate() throws NotFoundException, ResourceRegistryException;
	
	public El internalUpdate() throws NotFoundException, ResourceRegistryException {
		try {
			setOperation(Operation.UPDATE);
			
			reallyUpdate();
			
			MetadataUtility.updateModifiedByAndLastUpdate(element);
			
			element.save();
			
			sanityCheck();
			
			return element;
		} catch(WebApplicationException e) {
			throw e;
		} catch(ResourceRegistryException e) {
			throw e;
		} catch(Exception e) {
			throw new ResourceRegistryException("Error Updating " + typeName + " with " + jsonNode, e);
		}
	}
	
	public El internalCreateOrUdate() throws ResourceRegistryException {
		try {
			return internalUpdate();
		} catch(NotFoundException e) {
			return internalCreate();
		}
	}
	
	protected abstract void reallyDelete() throws NotFoundException, ResourceRegistryException;
	
	public void internalDelete() throws NotFoundException, ResourceRegistryException {
		setOperation(Operation.DELETE);
		reallyDelete();
		sanityCheck();
	}
	
	public void setElement(El element) throws ResourceRegistryException {
		if(element == null) {
			throw new ResourceRegistryException("Trying to set null " + elementClass.getSimpleName() + " in " + this);
		}
		this.element = element;
		this.uuid = UUIDUtility.getUUID(element);
		OClass oClass = getOClass();
		this.typeName = oClass.getName();
	}
	
	protected abstract NotFoundException getSpecificNotFoundException(NotFoundException e);
	
	protected abstract AlreadyPresentException getSpecificAlreadyPresentException(String message);
	
	public El getElement() throws NotFoundException, AvailableInAnotherContextException, ResourceRegistryException {
		if(element == null) {
			try {
				element = retrieveElement();
			} catch(NotFoundException e) {
				throw e;
			} catch(ResourceRegistryException e) {
				throw e;
			} catch(Exception e) {
				throw new ResourceRegistryException(e);
			}
			
		} else {
			if(reload) {
				element.reload();
			}
		}
		return element;
	}
	
	public El retrieveElement() throws NotFoundException, ResourceRegistryException {
		try {
			if(uuid == null) {
				throw new NotFoundException("null UUID does not allow to retrieve the Element");
			}
			return OrientDBUtility.getElementByUUID(oDatabaseDocument, typeName == null ? accessType.getName() : typeName, uuid,
					elementClass);
		} catch(NotFoundException e) {
			throw getSpecificNotFoundException(e);
		} catch(ResourceRegistryException e) {
			throw e;
		} catch(Exception e) {
			throw new ResourceRegistryException(e);
		}
	}
	
	public El retrieveElementFromAnyContext() throws NotFoundException, ResourceRegistryException {
		try {
			return OrientDBUtility.getElementByUUIDAsAdmin(typeName == null ? accessType.getName() : typeName, uuid,
					elementClass);
		} catch(NotFoundException e) {
			throw getSpecificNotFoundException(e);
		} catch(ResourceRegistryException e) {
			throw e;
		} catch(Exception e) {
			throw new ResourceRegistryException(e);
		}
	}
	
	public abstract String reallyGetAll(boolean polymorphic) throws ResourceRegistryException;
	
	public String all(boolean polymorphic) throws ResourceRegistryException {
		ODatabaseDocument current = ContextUtility.getCurrentODatabaseDocumentFromThreadLocal();
		try {
			oDatabaseDocument = getWorkingEnvironment().getDatabaseDocument(PermissionMode.READER);
			setAsEntryPoint();
			setOperation(Operation.QUERY);
			return reallyGetAll(polymorphic);
		} catch (WebApplicationException e) {
			throw e;
		} catch(ResourceRegistryException e) {
			throw e;
		} catch(Exception e) {
			throw new ResourceRegistryException(e);
		} finally {
			if(oDatabaseDocument != null) {
				oDatabaseDocument.close();
			}
			
			if(current!=null) {
				current.activateOnCurrentThread();
			}
		}
	}
	
	public boolean exists() throws NotFoundException, AvailableInAnotherContextException, ResourceRegistryException {
		ODatabaseDocument current = ContextUtility.getCurrentODatabaseDocumentFromThreadLocal();
		try {
			oDatabaseDocument = getWorkingEnvironment().getDatabaseDocument(PermissionMode.READER);
			setAsEntryPoint();
			setOperation(Operation.EXISTS);
			
			getElement();
			
			return true;
		} catch (WebApplicationException e) {
			logger.error("Unable to find {} with UUID {}. Reason {}", accessType.getName(), uuid, e.getMessage());
			throw e;
		} catch(ResourceRegistryException e) {
			logger.error("Unable to find {} with UUID {}. Reason {}", accessType.getName(), uuid, e.getMessage());
			throw e;
		} catch(Exception e) {
			logger.error("Unable to find {} with UUID {}", accessType.getName(), uuid, e);
			throw new ResourceRegistryException(e);
		} finally {
			if(oDatabaseDocument != null) {
				oDatabaseDocument.close();
			}
			
			if(current!=null) {
				current.activateOnCurrentThread();
			}
		}
	}
	
	public String createOrUpdate()
			throws NotFoundException, AvailableInAnotherContextException, ResourceRegistryException {
		ODatabaseDocument current = ContextUtility.getCurrentODatabaseDocumentFromThreadLocal();
		try {
			Environment environment = getWorkingEnvironment();
			oDatabaseDocument = environment.getDatabaseDocument(PermissionMode.WRITER);
			oDatabaseDocument.begin();
			boolean update = false;
			setAsEntryPoint();
			setOperation(Operation.UPDATE);
			try {
				getElement();
				// TODO environment.isUserAllowed(operation);
				update = true;
				internalUpdate();
			} catch(NotFoundException e) {
				setOperation(Operation.CREATE);
				// TODO environment.isUserAllowed(operation);
				String calledMethod = InnerMethodName.get();
				calledMethod = calledMethod.replace("update", "create");
				InnerMethodName.set(calledMethod);
				internalCreate();
			}
			
			oDatabaseDocument.commit();
			
			if(update) {
				setReload(true);
			}
			
			// TODO Notify to subscriptionNotification
			
			return serializeAsJsonNode().toString();
			
		} catch (WebApplicationException e) {
			logger.error("Unable to update {} with UUID {}. Reason {}", accessType.getName(), uuid, e.getMessage());
			if(oDatabaseDocument != null) {
				oDatabaseDocument.rollback();
			}
			throw e;
		} catch(ResourceRegistryException e) {
			logger.error("Unable to update {} with UUID {}. Reason {}", accessType.getName(), uuid, e.getMessage());
			if(oDatabaseDocument != null) {
				oDatabaseDocument.rollback();
			}
			throw e;
		} catch(Exception e) {
			logger.error("Unable to update {} with UUID {}", accessType.getName(), uuid, e);
			if(oDatabaseDocument != null) {
				oDatabaseDocument.rollback();
			}
			throw new ResourceRegistryException(e);
		} finally {
			if(oDatabaseDocument != null) {
				oDatabaseDocument.close();
			}
			
			if(current!=null) {
				current.activateOnCurrentThread();
			}
		}
	}
	
	public String create() throws AlreadyPresentException, ResourceRegistryException {
		
		ODatabaseDocument current = ContextUtility.getCurrentODatabaseDocumentFromThreadLocal();
		try {
			Environment environment = getWorkingEnvironment();
			// TODO environment.isUserAllowed(Operation.CREATE);
			oDatabaseDocument = environment.getDatabaseDocument(PermissionMode.WRITER);
			oDatabaseDocument.begin();
			setAsEntryPoint();
			
			internalCreate();
			
			oDatabaseDocument.commit();
			
			// TODO Notify to subscriptionNotification
			
			return serializeAsJsonNode().toString();
			
		} catch (WebApplicationException e) {
			logger.error("Unable to create {}. Reason {}", accessType.getName(), e.getMessage());
			if(oDatabaseDocument != null) {
				oDatabaseDocument.rollback();
			}
			throw e;
		} catch(ResourceRegistryException e) {
			logger.error("Unable to create {}. Reason {}", accessType.getName(), e.getMessage());
			if(oDatabaseDocument != null) {
				oDatabaseDocument.rollback();
			}
			throw e;
		} catch(Exception e) {
			logger.error("Unable to create {}", accessType.getName(), e);
			if(oDatabaseDocument != null) {
				oDatabaseDocument.rollback();
			}
			throw new ResourceRegistryException(e);
		} finally {
			if(oDatabaseDocument != null) {
				oDatabaseDocument.close();
			}
			
			if(current!=null) {
				current.activateOnCurrentThread();
			}
		}
	}
	
	public String read() throws NotFoundException, AvailableInAnotherContextException, ResourceRegistryException {
		
		ODatabaseDocument current = ContextUtility.getCurrentODatabaseDocumentFromThreadLocal();
		try {
			Environment environment = getWorkingEnvironment();
			setOperation(Operation.READ);
			// TODO environment.isUserAllowed(operation);
			oDatabaseDocument = environment.getDatabaseDocument(PermissionMode.READER);
			
			setAsEntryPoint();
			
			getElement();
			
			return serializeAsJsonNode().toString();
		} catch (WebApplicationException e) {
			logger.error("Unable to read {} with UUID {}. Reason {}", accessType.getName(), uuid, e.getMessage());
			throw e;
		} catch(ResourceRegistryException e) {
			logger.error("Unable to read {} with UUID {}. Reason {}", accessType.getName(), uuid, e.getMessage());
			throw e;
		} catch(Exception e) {
			logger.error("Unable to read {} with UUID {}", accessType.getName(), uuid, e);
			throw new ResourceRegistryException(e);
		} finally {
			if(oDatabaseDocument != null) {
				oDatabaseDocument.close();
			}
			
			if(current!=null) {
				current.activateOnCurrentThread();
			}
		}
	}
	
	public String update() throws NotFoundException, AvailableInAnotherContextException, ResourceRegistryException {
		ODatabaseDocument current = ContextUtility.getCurrentODatabaseDocumentFromThreadLocal();
		try {
			Environment environment = getWorkingEnvironment();
			// TODO environment.isUserAllowed(Operation.UPDATE);
			oDatabaseDocument = environment.getDatabaseDocument(PermissionMode.WRITER);
			oDatabaseDocument.begin();
			
			setAsEntryPoint();
			internalUpdate();
			
			oDatabaseDocument.commit();
			
			setReload(true);
			
			// TODO Notify to subscriptionNotification
			
			return serializeAsJsonNode().toString();
			
		} catch (WebApplicationException e) {
			logger.error("Unable to update {} with UUID {}. Reason {}", accessType.getName(), uuid, e.getMessage());
			if(oDatabaseDocument != null) {
				oDatabaseDocument.rollback();
			}
			throw e;
		} catch(ResourceRegistryException e) {
			logger.error("Unable to update {} with UUID {}. Reason {}", accessType.getName(), uuid, e.getMessage());
			if(oDatabaseDocument != null) {
				oDatabaseDocument.rollback();
			}
			throw e;
		} catch(Exception e) {
			logger.error("Unable to update {} with UUID {}", accessType.getName(), uuid, e);
			if(oDatabaseDocument != null) {
				oDatabaseDocument.rollback();
			}
			throw new ResourceRegistryException(e);
		} finally {
			if(oDatabaseDocument != null) {
				oDatabaseDocument.close();
			}
			
			if(current!=null) {
				current.activateOnCurrentThread();
			}
		}
	}
	
	public void delete() throws NotFoundException, AvailableInAnotherContextException, SchemaViolationException, ResourceRegistryException {
		logger.trace("Going to delete {} instance with UUID {}", accessType.getName(), uuid);
		ODatabaseDocument current = ContextUtility.getCurrentODatabaseDocumentFromThreadLocal();
		try {
			Environment environment = getWorkingEnvironment();
			setOperation(Operation.DELETE);
			// TODO environment.isUserAllowed(operation);
			oDatabaseDocument = environment.getDatabaseDocument(PermissionMode.WRITER);
			oDatabaseDocument.begin();
			setAsEntryPoint();
			
			internalDelete();
			
			if(!dryRun) {
				oDatabaseDocument.commit();
				logger.info("{} with UUID {} was successfully deleted.", accessType.getName(), uuid);
			}else {
				oDatabaseDocument.rollback();
			}
		} catch (WebApplicationException e) {
			logger.error("Unable to delete {} with UUID {}. Reason {}", accessType.getName(), uuid, e.getMessage());
			if(oDatabaseDocument != null) {
				oDatabaseDocument.rollback();
			}
			throw e;
		} catch(ResourceRegistryException e) {
			logger.error("Unable to delete {} with UUID {}. Reason {}", accessType.getName(), uuid, e.getMessage());
			if(oDatabaseDocument != null) {
				oDatabaseDocument.rollback();
			}
			throw e;
		} catch(Exception e) {
			logger.error("Unable to delete {} with UUID {}", accessType.getName(), uuid, e);
			if(oDatabaseDocument != null) {
				oDatabaseDocument.rollback();
			}
			throw new ResourceRegistryException(e);
		} finally {
			if(oDatabaseDocument != null) {
				oDatabaseDocument.close();
			}
			
			if(current!=null) {
				current.activateOnCurrentThread();
			}
		}
	}
	
	public Set<String> getContextsSet() throws NotFoundException, ContextException, ResourceRegistryException {
		logger.trace("Going to get contexts for {} instance with UUID {}", typeName, uuid);
		ODatabaseDocument current = ContextUtility.getCurrentODatabaseDocumentFromThreadLocal();
		ODatabaseDocument instanceDB = this.oDatabaseDocument;
		try {
			AdminEnvironment adminEnvironment = AdminEnvironment.getInstance();
			setOperation(Operation.GET_METADATA);
			// TODO adminEnvironment.isUserAllowed(operation);
			oDatabaseDocument = adminEnvironment.getDatabaseDocument(PermissionMode.READER);
			
			setAsEntryPoint();
			
			Set<String> contexts = InstanceEnvironment.getContexts(getElement());
			return contexts;
		} catch (WebApplicationException e) {
			logger.error("Unable to get contexts for {} with UUID {}. Reason {}", typeName, uuid, e.getMessage());
			throw e;
		} catch(ResourceRegistryException e) {
			logger.error("Unable to get contexts for {} with UUID {}. Reason {}", typeName, uuid, e.getMessage());
			throw e;
		} catch(Exception e) {
			logger.error("Unable to get contexts for {} with UUID {}", typeName, uuid, e);
			throw new ContextException(e);
		} finally {
			if(oDatabaseDocument != null) {
				oDatabaseDocument.close();
			}
			
			if(current!=null) {
				current.activateOnCurrentThread();
			}
			
			if(instanceDB!=null) {
				this.oDatabaseDocument = instanceDB;
			}
		}
	}
	
	public String getContexts() throws NotFoundException, ContextException, ResourceRegistryException {
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			ObjectNode objectNode = getContextsAsObjectNode(objectMapper);
			return objectMapper.writeValueAsString(objectNode);
		} catch(ResourceRegistryException e) {
			throw e;
		} catch (Exception e) {
			throw new ContextException(e);
		}
	}
	
	private ObjectNode getContextsAsObjectNode(ObjectMapper objectMapper) throws NotFoundException, ContextException, ResourceRegistryException {
		try {
			Set<String> contexts = getContextsSet();
			ServerContextCache contextCache = ServerContextCache.getInstance();
			ObjectNode objectNode = objectMapper.createObjectNode();
			for(String contextUUID : contexts) {
				String contextFullName = contextCache.getContextFullNameByUUID(contextUUID);
				objectNode.put(contextUUID, contextFullName);
			}
			return objectNode;
		} catch(ResourceRegistryException e) {
			throw e;
		} catch (Exception e) {
			throw new ContextException(e);
		}
	}
	
	public ObjectNode getContextsAsObjectNode() throws NotFoundException, ContextException, ResourceRegistryException {
		ObjectMapper objectMapper = new ObjectMapper();
		return getContextsAsObjectNode(objectMapper);
	}
	
	public Map<String,JsonNode> getPropertyMap(JsonNode jsonNode, Set<String> ignoreKeys,
			Set<String> ignoreStartWith) throws JsonProcessingException, IOException {
		
		Map<String, JsonNode> map = new HashMap<>();
		
		if(ignoreKeys == null) {
			ignoreKeys = new HashSet<>();
		}
		
		if(ignoreStartWith == null) {
			ignoreStartWith = new HashSet<>();
		}
		
		Iterator<Entry<String,JsonNode>> fields = jsonNode.fields();
		
		OUTER_WHILE: while(fields.hasNext()) {
			Entry<String,JsonNode> entry = fields.next();
			
			String key = entry.getKey();
			
			if(ignoreKeys.contains(key)) {
				continue;
			}
			
			for(String prefix : ignoreStartWith) {
				if(key.startsWith(prefix)) {
					continue OUTER_WHILE;
				}
			}
			
			JsonNode value = entry.getValue();
			map.put(key, value);
			
		}
		
		return map;
	}
	
	public void setProperty(OProperty oProperty, String key, JsonNode value) throws Exception {
		switch (oProperty.getType()) {
		
			case EMBEDDED:
				ODocument oDocument = PropertyElementManagement.getPropertyDocument(value);
				element.setProperty(key, oDocument, OType.EMBEDDED);
				break;
			
			case EMBEDDEDLIST:
				List<Object> list = new ArrayList<Object>();
				Iterator<JsonNode> arrayElement = value.elements();
				while(arrayElement.hasNext()) {
					JsonNode elementOfArray = arrayElement.next();
					Object object = null;
					
					if(oProperty.getLinkedType()!=null) {
						object = ElementManagementUtility.getObjectFromJsonNode(elementOfArray);
					}else {
						object = PropertyElementManagement.getPropertyDocument(elementOfArray);
					}
					list.add(object);
				}
				element.setProperty(key, list, OType.EMBEDDEDLIST);
				break;
			
			case EMBEDDEDSET:
				Set<Object> set = new HashSet<Object>();
				Iterator<JsonNode> setElement = value.elements();
				while(setElement.hasNext()) {
					JsonNode elementOfSet = setElement.next();
					Object object = null;
					
					if(oProperty.getLinkedType()!=null) {
						object = ElementManagementUtility.getObjectFromJsonNode(elementOfSet);
					}else {
						object = PropertyElementManagement.getPropertyDocument(elementOfSet);
					}
					set.add(object);
				}
				element.setProperty(key, set, OType.EMBEDDEDSET);
				break;
			
			case EMBEDDEDMAP:
				Map<String, Object> map = new HashMap<>();
				Iterator<String> fieldNames = value.fieldNames();
				while(fieldNames.hasNext()) {
					String fieldKey = fieldNames.next();
					Object object = null;
					if(oProperty.getLinkedType()!=null) {
						object = ElementManagementUtility.getObjectFromJsonNode(value.get(fieldKey));
					}else {
						object = PropertyElementManagement.getPropertyDocument(value.get(fieldKey));
					}
					map.put(fieldKey, object);
				}
				element.setProperty(key, map, OType.EMBEDDEDMAP);
				break;
			
			case STRING:
				
				if(value.getNodeType() == JsonNodeType.OBJECT) {
					element.setProperty(key, value.toString());
				}else {
					element.setProperty(key, ElementManagementUtility.getObjectFromJsonNode(value));
				}
				break;
				
			default:
				Object obj = ElementManagementUtility.getObjectFromJsonNode(value);
				if(obj != null) {
					element.setProperty(key, obj);
				}
				break;
		}
	}
	
	public OElement updateProperties(OClass oClass, OElement element, JsonNode jsonNode, Set<String> ignoreKeys,
			Set<String> ignoreStartWithKeys) throws ResourceRegistryException {
		
		Set<String> oldKeys = element.getPropertyNames();
		
		Map<String,JsonNode> properties;
		try {
			properties = getPropertyMap(jsonNode, ignoreKeys, ignoreStartWithKeys);
		} catch(IOException e) {
			throw new ResourceRegistryException(e);
		}
		
		oldKeys.removeAll(properties.keySet());
		
		getOClass();
		
		for(String key : properties.keySet()) {
			try {
				JsonNode value = properties.get(key);
				
				OProperty oProperty = oClass.getProperty(key);
				
				if(oProperty==null) {
					Object object = ElementManagementUtility.getObjectFromJsonNode(value);
					if(object != null) {
						if(object instanceof ODocument) {
							element.setProperty(key, object, OType.EMBEDDED);
						/*
						 * Due to bug https://github.com/orientechnologies/orientdb/issues/7354
						 * we should not support ArrayList
						 */
						} else if(object instanceof List){
							element.setProperty(key, object, OType.EMBEDDEDLIST);
						} else {
							element.setProperty(key, object);
						}
					}
				}else {
					setProperty(oProperty, key, value);
				}

			} catch(Exception e) {
				String error = "Error while setting property %s : %s (%s)".formatted(key,
						properties.get(key).toString(), e.getMessage());
				logger.error(error);
				throw new ResourceRegistryException(error, e);
			}
			
		}
		
		OUTER_FOR: for(String key : oldKeys) {
			
			if(ignoreKeys.contains(key)) {
				continue;
			}
			
			for(String prefix : ignoreStartWithKeys) {
				if(key.startsWith(prefix)) {
					continue OUTER_FOR;
				}
			}
			
			element.removeProperty(key);
		}
		
		return element;
	}
	
	public boolean isUserAllowedToGetPrivacyMeta() {
		boolean allowed = false;
		Secret secret = SecretManagerProvider.get();
		Owner owner = secret.getOwner();
		Collection<String> roles = new HashSet<>(owner.getRoles());
		if(roles.contains(Environment.CONTEXT_MANAGER)) {
			return true;
		}
		roles.retainAll(Environment.getAllOperationsAllowedRoles());
		if(roles.size()>0) {
			allowed = true;
		}
		return allowed;
	}
	
	protected JsonNode getPropertyForJson(String key, Object object) throws ResourceRegistryException {
		try {
			if(object == null) {
				return null;
			}
			
			if(object instanceof JsonNode node) {
				return node;
			}
			
			if(key.compareTo(IdentifiableElement.METADATA_PROPERTY) == 0) {
				// Keeping the metadata
				MetadataOrient metadataOrient = MetadataUtility.getMetadataOrient((ODocument) object);
				ObjectNode metadataJson = (ObjectNode) OrientDBUtility.toJsonNode(metadataOrient);
				
				if(!isUserAllowedToGetPrivacyMeta()) {
					metadataJson.replace(Metadata.CREATED_BY_PROPERTY, new TextNode(Metadata.HIDDEN_FOR_PRIVACY_USER));
					metadataJson.replace(Metadata.LAST_UPDATE_BY_PROPERTY, new TextNode(Metadata.HIDDEN_FOR_PRIVACY_USER));
				}
				
				// TODO check a solution for supertypes
				TypesCache typesCache = TypesCache.getInstance();
				@SuppressWarnings("unchecked")
				CachedType<PropertyType> metadataType = (CachedType<PropertyType>) typesCache.getCachedType(Metadata.NAME);
				ObjectMapper objectMapper = new ObjectMapper();
				Collection<String> superClasses = metadataType.getSuperTypes();
				ArrayNode arrayNode = objectMapper.valueToTree(superClasses);
				metadataJson.replace(ModelElement.SUPERTYPES_PROPERTY, arrayNode);
				
				return metadataJson;
			}
			
			if(key.compareTo(IdentifiableElement.ID_PROPERTY) == 0 ) {
				return new TextNode(object.toString());
			}
			
			if(ignoreKeys.contains(key)) {
				return null;
			}
			
			for(String prefix : ignoreStartWithKeys) {
				if(key.startsWith(prefix)) {
					return null;
				}
			}
			
			if(object instanceof ODocument oDocument) {
				return PropertyElementManagement.getJsonNode(oDocument);
			}
			
			if(object instanceof Date date) {
				OProperty oProperty = getOClass().getProperty(key);
				OType oType = oProperty.getType();
				DateFormat dateFormat = ODateHelper.getDateTimeFormatInstance();
				switch(oType) {
					case DATE:
						dateFormat = ODateHelper.getDateFormatInstance();
						break;
					
					case DATETIME:
						dateFormat = ODateHelper.getDateTimeFormatInstance();
						break;
					
					default:
						break;
				}
				
				return new TextNode(dateFormat.format(date));
			}
			
			if(object instanceof Collection<?> collection) {
				ObjectMapper objectMapper = new ObjectMapper();
				ArrayNode arrayNode = objectMapper.createArrayNode();
				
				for(Object o : collection) {
					JsonNode obj = getPropertyForJson("PLACEHOLDER", o);
					if(obj!=null) {
						arrayNode.add(obj);
					}
				}
				
				return arrayNode;
			}
			
			if(object instanceof Map) {
				@SuppressWarnings("unchecked")
				Map<String, Object> map = (Map<String, Object>) object;
				ObjectMapper objectMapper = new ObjectMapper();
				ObjectNode objectNode = objectMapper.createObjectNode();
				for(String k : map.keySet()) {
					JsonNode obj = getPropertyForJson("PLACEHOLDER", map.get(k));
					objectNode.set(k, obj);
				}
				return objectNode;
			}
			
			if(object instanceof Boolean boolean1) {
				return BooleanNode.valueOf(boolean1);
			}
			
			return new TextNode(object.toString());
			
		} catch(Exception e) {
			throw new ResourceRegistryException(
					"Error while serializing " + key + "=" + object.toString() + " in " + getElement().toString(), e);
		}
	}
	
	protected String getNotNullErrorMessage(String fieldName) {
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append("The type ");
		stringBuffer.append(typeName);
		stringBuffer.append(" defines the fields ");
		stringBuffer.append(fieldName);
		stringBuffer.append(" as not nullable. Null or no value has been provided instead.");
		return stringBuffer.toString();
	}
	
	protected String getMandatoryErrorMessage(String fieldName) {
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append("The type ");
		stringBuffer.append(typeName);
		stringBuffer.append(" defines the fields ");
		stringBuffer.append(fieldName);
		stringBuffer.append(" as mandatory but no value has been provided.");
		return stringBuffer.toString();
	}
	
	protected boolean typeSatified(TypesCache typesCache, String requiredType, String effectiveType) throws SchemaException, ResourceRegistryException {
		if(requiredType.compareTo(effectiveType)==0) {
			return true;
		}
		
		@SuppressWarnings("unchecked")
		CachedType<ResourceType> cachedType = (CachedType<ResourceType>) typesCache.getCachedType(requiredType);
		if(cachedType.getSubTypes().contains(effectiveType)) {
			return true;
		}
		
		return false;
	}
	
	/*
	 * Get not only the properties defined in the type but also the properties 
	 * defined in the super types 
	 */
	protected Set<PropertyDefinition> getAllProperties() throws SchemaException, ResourceRegistryException{
		TypesCache typesCache = TypesCache.getInstance();
		Set<PropertyDefinition> definedProperties = getCachedType().getType().getProperties();
		
		Set<CachedType<T>> cachedSuperTypes = new HashSet<>();
		List<String> superTypes = cachedType.getSuperTypes();
		for(String superTypeName : superTypes) {
			@SuppressWarnings("unchecked")
			CachedType<T> cachedSuperType = (CachedType<T>) typesCache.getCachedType(superTypeName);
			cachedSuperTypes.add(cachedSuperType);
			
			Type superType = cachedSuperType.getType();
			Set<PropertyDefinition> properties = superType.getProperties();
			if(properties!=null) {
				definedProperties.addAll(properties);
			}
		}
		
		return definedProperties;
	}
	
	public void sanityCheck() throws SchemaViolationException, ResourceRegistryException {
		// OrientDB distributed mode does not support 
		// mandatory and notnull constraints due to technical problem
		// https://www.orientdb.com/docs/last/java/Graph-Schema-Property.html#using-constraints
		// Going to validate them here
		
		if(operation.isSafe()) {
			/* 
			 * The sanity check is not required for a safe operation.
			 */
			return;
		}
		
		
		Set<PropertyDefinition> definedProperties = getAllProperties();
		
		if(definedProperties==null) {
			// The type could define no property
			return;
		}
		
		Set<String> elementPropertyNames = getElement().getPropertyNames();
		
		for(PropertyDefinition propertyDefinition : definedProperties) {
			
			String fieldName = propertyDefinition.getName();
			
			if(propertyDefinition.isMandatory() && !elementPropertyNames.contains(fieldName)) {
				if(propertyDefinition.isNotnull()) {
					// If the field is mandatory but null value is accepted I add the
					// field as null value
					element.setProperty(fieldName, null);
				} else {
					throw new SchemaViolationException(getMandatoryErrorMessage(fieldName));
				}
			}
			
			/*
			JsonNode jsonNode = instances.get(fieldName);
			
			if(!propertyDefinition.isNotnull() && jsonNode==null) {
				throw new SchemaViolationException(getNotNullErrorMessage(fieldName));
			}
			*/
			
			// This validation was required to check if all mandatory fields are presents
			// The validation of the values has been performed at create/update time.
			
			
		}
		
	}
	
}
