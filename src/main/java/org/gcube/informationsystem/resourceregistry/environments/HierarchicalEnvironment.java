/**
 * 
 */
package org.gcube.informationsystem.resourceregistry.environments;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;
import org.gcube.informationsystem.resourceregistry.contexts.ContextUtility;
import org.gcube.informationsystem.resourceregistry.dbinitialization.DatabaseEnvironment;
import org.gcube.informationsystem.resourceregistry.rest.requests.RequestUtility;
import org.gcube.informationsystem.resourceregistry.rest.requests.ServerRequestInfo;
import org.gcube.informationsystem.utils.UUIDManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.orientechnologies.orient.core.db.ODatabasePool;
import com.orientechnologies.orient.core.db.document.ODatabaseDocument;
import com.orientechnologies.orient.core.db.record.OIdentifiable;
import com.orientechnologies.orient.core.db.record.ORecordLazySet;
import com.orientechnologies.orient.core.metadata.security.ORestrictedOperation;
import com.orientechnologies.orient.core.metadata.security.ORole;
import com.orientechnologies.orient.core.metadata.security.OSecurity;
import com.orientechnologies.orient.core.metadata.security.OSecurityRole.ALLOW_MODES;
import com.orientechnologies.orient.core.metadata.security.OUser;
import com.orientechnologies.orient.core.record.OElement;
import com.orientechnologies.orient.core.record.impl.ODocument;

/**
 * @author Luca Frosini (ISTI - CNR)
 * Base class for all environments which have an hierarchy.
 * An example is InstanceEnviroment where the instances 
 * belonging to a context must be visible from parent 
 * context if the requesting client has enough privileges
 */
public abstract class HierarchicalEnvironment extends Environment {
	
	private static Logger logger = LoggerFactory.getLogger(HierarchicalEnvironment.class);
	
	/*
	 * H stand for Hierarchical
	 */
	public static final String H = "H";
	
	protected final boolean hierarchicalMode;
	
	protected final Map<PermissionMode,ODatabasePool> hierarchicPoolMap;
	
	protected HierarchicalEnvironment parentEnvironment;
	
	protected Set<HierarchicalEnvironment> children;
	
	public HierarchicalEnvironment(UUID uuid) throws ResourceRegistryException {
		super(uuid);
		
		this.hierarchicPoolMap = new HashMap<>();
		
		boolean hierarchicalModeRequested = RequestUtility.getRequestInfo().get().isHierarchicalMode();
		logger.trace("HierarchicalMode {}requested", hierarchicalModeRequested ? "" : "not ");
		
		boolean hierarchicalAllowed = isUserAllowed(Environment.getAllOperationsAllowedRoles());
		logger.trace("{} is {}allowed to request the HierarchicalMode", ContextUtility.getCurrentUserUsername(), hierarchicalAllowed ? "" : "not ");
		
		/*
		 * Only the Infrastructure Manager and IS Manager are entitled to use hierarchical mode. 
		 * I decided not to complain if the user does not have such roles and assumed the hierarchical mode was not requested.
		 */
		if(hierarchicalModeRequested && !hierarchicalAllowed) {
			StringBuffer sb = new StringBuffer();
			sb.append("The user ");
			sb.append(ContextUtility.getCurrentUserUsername());
			sb.append(" requested hierarchical mode but he/she does not have one of the following roles ");
			sb.append(allOperationAllowedRoles.toString());
			sb.append(". Instead of complaining, the request will be elaborated not in hierarchical mode.");
			logger.warn(sb.toString());
		}
		
		this.hierarchicalMode = hierarchicalAllowed && hierarchicalModeRequested;
		
		this.children = new HashSet<>();
		
	}
	
	protected boolean isHierarchicalMode() {
		return hierarchicalMode;
	}
	
	public void setParentEnvironment(HierarchicalEnvironment parentEnvironment) {
		if(this.parentEnvironment!=null) {
			this.parentEnvironment.getChildren().remove(this);
		}
		
		this.parentEnvironment = parentEnvironment;
		if(parentEnvironment!=null) {
			this.parentEnvironment.addChild(this);
		}
	}
	
	public HierarchicalEnvironment getParentEnvironment() {
		return parentEnvironment;
	}
	
	private void addChild(HierarchicalEnvironment child) {
		this.children.add(child);
	}
	
	public Set<HierarchicalEnvironment> getChildren(){
		return this.children;
	}
	
	/**
	 * @return a set containing all children and recursively
	 * all children.  
	 */
	private Set<HierarchicalEnvironment> getAllChildren(){
		Set<HierarchicalEnvironment> allChildren = new HashSet<>();
		allChildren.add(this);
		for(HierarchicalEnvironment hierarchicalEnvironment : getChildren()) {
			allChildren.addAll(hierarchicalEnvironment.getAllChildren());
		}
		return allChildren;
	}
	
	/**
	 * @return 
	 */
	private Set<HierarchicalEnvironment> getAllParents(){
		Set<HierarchicalEnvironment> allParents = new HashSet<>();
		HierarchicalEnvironment parent = getParentEnvironment();
		while(parent!=null) {
			allParents.add(parent);
			parent = parent.getParentEnvironment();
		}
		return allParents;
	}
	
	
	/**
	 * Use to change the parent not to set the first time
	 * 
	 * @param newParentEnvironment
	 * @param orientGraph
	 * @throws ResourceRegistryException 
	 */
	public void changeParentEnvironment(HierarchicalEnvironment newParentEnvironment, ODatabaseDocument orientGraph) throws ResourceRegistryException {
		OSecurity oSecurity = getOSecurity(orientGraph);
		
		Set<HierarchicalEnvironment> allChildren = getAllChildren();
		
		Set<HierarchicalEnvironment> oldParents = getAllParents();
		
		Set<HierarchicalEnvironment> newParents = new HashSet<>();
		if(newParentEnvironment!=null) {
			newParents = newParentEnvironment.getAllParents();
		}
		
		/* 
		 * From old parents I remove the new parents so that oldParents
		 * contains only the parents where I have to remove all 
		 * HReaderRole-UUID e HWriterRole-UUID of allChildren by using 
		 * removeHierarchicRoleFromParent() function
		 * 
		 */
		oldParents.removeAll(newParents);
		removeChildrenHRolesFromParents(oSecurity, oldParents, allChildren);
		
		setParentEnvironment(newParentEnvironment);
		
		if(newParentEnvironment!=null){
			for(PermissionMode permissionMode : PermissionMode.values()) {
				List<ORole> roles = new ArrayList<>();
				for(HierarchicalEnvironment child : allChildren) {
					String roleName = child.getSecurityRoleOrUserName(permissionMode, SecurityType.ROLE, true);
					ORole role = oSecurity.getRole(roleName);
					roles.add(role);
				}
				newParentEnvironment.addHierarchicalRoleToParent(oSecurity, permissionMode, roles.toArray(new ORole[allChildren.size()]));
			}
		}
		
	}
	
	@Override
	protected synchronized ODatabasePool getPool(PermissionMode permissionMode, boolean recreate) {
		ODatabasePool pool = null;
		
		Boolean h = hierarchicalMode || RequestUtility.getRequestInfo().get().isHierarchicalMode();
		
		Map<PermissionMode,ODatabasePool> pools =  h ? hierarchicPoolMap : poolMap;
		
		if(recreate) {
			pool = pools.get(permissionMode);
			if(pool!=null) {
				pool.close();
				pools.remove(permissionMode);
			}
		}

		
		pool = pools.get(permissionMode);
		
		if(pool == null) {
			
			String username = getSecurityRoleOrUserName(permissionMode, SecurityType.USER, h);
			String password = DatabaseEnvironment.DEFAULT_PASSWORDS.get(permissionMode);
			
			pool = new ODatabasePool(DatabaseEnvironment.DB_URI, username, password);
			
			pools.put(permissionMode, pool);
		}
		
		return pool;
	}
	
	public static String getRoleOrUserName(PermissionMode permissionMode, SecurityType securityType) {
		return getRoleOrUserName(permissionMode, securityType, false);
	}
	
	public static String getRoleOrUserName(PermissionMode permissionMode, SecurityType securityType,
			boolean hierarchic) {
		StringBuilder stringBuilder = new StringBuilder();
		if(hierarchic) {
			stringBuilder.append(H);
		}
		stringBuilder.append(permissionMode);
		stringBuilder.append(securityType);
		return stringBuilder.toString();
	}
	
	public String getSecurityRoleOrUserName(PermissionMode permissionMode, SecurityType securityType,
			boolean hierarchic) {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(getRoleOrUserName(permissionMode, securityType, hierarchic));
		stringBuilder.append("_");
		stringBuilder.append(environmentUUID.toString());
		return stringBuilder.toString();
	}
	
	private OSecurity getOSecurity(ODatabaseDocument oDatabaseDocument) {
		return oDatabaseDocument.getMetadata().getSecurity();
	}
	
	public static Set<String> getContexts(OElement element) {
		Set<String> contexts = new HashSet<>();
		ORecordLazySet oRecordLazySet = element.getProperty(OSecurity.ALLOW_ALL_FIELD);
		for (OIdentifiable oIdentifiable : oRecordLazySet) {
			ODocument oDocument = (ODocument) oIdentifiable;
			String name = oDocument.getProperty("name");
			if (name.startsWith(getRoleOrUserName(PermissionMode.WRITER, SecurityType.ROLE))
					|| name.startsWith(getRoleOrUserName(PermissionMode.READER, SecurityType.ROLE))) {
				String[] list = name.split("_");
				if (list.length == 2) {
					String contextUUID = list[1];
					if (!UUIDManager.getInstance().isReservedUUID(contextUUID)) {
						contexts.add(contextUUID);
					}
				}
			}
		}
		return contexts;
	}
	
	protected void allow(OSecurity oSecurity, ODocument oDocument, boolean hierarchic) {
		String writerRoleName = getSecurityRoleOrUserName(PermissionMode.WRITER, SecurityType.ROLE, hierarchic);
		oSecurity.allowRole(oDocument, ORestrictedOperation.ALLOW_ALL, writerRoleName);
		String readerRoleName = getSecurityRoleOrUserName(PermissionMode.READER, SecurityType.ROLE, hierarchic);
		oSecurity.allowRole(oDocument, ORestrictedOperation.ALLOW_READ, readerRoleName);
	}
	
	@Override
	public void addElement(OElement element, ODatabaseDocument oDatabaseDocument) {
		ODocument oDocument = element.getRecord();
		OSecurity oSecurity = getOSecurity(oDatabaseDocument);
		allow(oSecurity, oDocument, false);
		allow(oSecurity, oDocument, true);
		oDocument.save();
		element.save();
	}
	
	protected void deny(OSecurity oSecurity, ODocument oDocument, boolean hierarchical) {
		// The element could be created in such a context so the writerUser for the
		// context is allowed by default because it was the creator
		String writerUserName = getSecurityRoleOrUserName(PermissionMode.WRITER, SecurityType.USER, hierarchical);
		oSecurity.denyUser(oDocument, ORestrictedOperation.ALLOW_ALL, writerUserName);
		String readerUserName = getSecurityRoleOrUserName(PermissionMode.WRITER, SecurityType.USER, hierarchical);
		oSecurity.denyUser(oDocument, ORestrictedOperation.ALLOW_READ, readerUserName);
		
		String writerRoleName = getSecurityRoleOrUserName(PermissionMode.WRITER, SecurityType.ROLE, hierarchical);
		oSecurity.denyRole(oDocument, ORestrictedOperation.ALLOW_ALL, writerRoleName);
		String readerRoleName = getSecurityRoleOrUserName(PermissionMode.READER, SecurityType.ROLE, hierarchical);
		oSecurity.denyRole(oDocument, ORestrictedOperation.ALLOW_READ, readerRoleName);
		
	}
	
	@Override
	public void removeElement(OElement element, ODatabaseDocument oDatabaseDocument) {
		ODocument oDocument = element.getRecord();
		OSecurity oSecurity = getOSecurity(oDatabaseDocument);
		deny(oSecurity, oDocument, false);
		deny(oSecurity, oDocument, true);
		oDocument.save();
		element.save();
	}
	
	@Override
	protected boolean allowed(final ORole role, final ODocument oDocument) {
		ServerRequestInfo sri = RequestUtility.getRequestInfo().get();
		Boolean hm = sri.isHierarchicalMode();
		sri.setHierarchicalMode(false);
		
		try {
			return super.allowed(role, oDocument);
		}finally {
			sri.setHierarchicalMode(hm);
		}
	}
	
	protected void addHierarchicalRoleToParent(OSecurity oSecurity, PermissionMode permissionMode, ORole... roles) {
		String userName = getSecurityRoleOrUserName(permissionMode, SecurityType.USER, true);
		OUser user = oSecurity.getUser(userName);
		for(ORole role : roles) {
			user.addRole(role);
		}
		user.save();
		
		if(getParentEnvironment() != null) {
			getParentEnvironment().addHierarchicalRoleToParent(oSecurity, permissionMode, roles);
		}
	}
	
	protected void createRolesAndUsers(OSecurity oSecurity) {
		boolean[] booleanArray = new boolean[] {false, true};
		
		for(boolean hierarchical : booleanArray) {
			for(PermissionMode permissionMode : PermissionMode.values()) {
				ORole superRole = getSuperRole(oSecurity, permissionMode);
				
				String roleName = getSecurityRoleOrUserName(permissionMode, SecurityType.ROLE, hierarchical);
				ORole role = oSecurity.createRole(roleName, superRole, ALLOW_MODES.DENY_ALL_BUT);
				addExtraRules(role, permissionMode);
				role.save();
				logger.trace("{} created", role);
				
				if(hierarchical && getParentEnvironment() != null) {
					getParentEnvironment().addHierarchicalRoleToParent(oSecurity, permissionMode, role);
				}
				
				String userName = getSecurityRoleOrUserName(permissionMode, SecurityType.USER, hierarchical);
				OUser user = oSecurity.createUser(userName, DatabaseEnvironment.DEFAULT_PASSWORDS.get(permissionMode),
						role);
				user.save();
				logger.trace("{} created", user);
			}
		}
		
	}
	
	protected void removeChildrenHRolesFromParents(OSecurity oSecurity) {
		Set<HierarchicalEnvironment> parents = getAllParents();
		Set<HierarchicalEnvironment> allChildren = getAllChildren();
		removeChildrenHRolesFromParents(oSecurity, parents, allChildren);
	}
	
	protected void removeChildrenHRolesFromParents(OSecurity oSecurity, Set<HierarchicalEnvironment> parents, Set<HierarchicalEnvironment> children) {
		for(HierarchicalEnvironment parent : parents) {
			parent.removeChildrenHRolesFromMyHUsers(oSecurity, children);
		}
	}
	
	protected void removeChildrenHRolesFromMyHUsers(OSecurity oSecurity, Set<HierarchicalEnvironment> children) {
		for(PermissionMode permissionMode : PermissionMode.values()) {
			String userName = getSecurityRoleOrUserName(permissionMode, SecurityType.USER, true);
			OUser user = oSecurity.getUser(userName);
			for(HierarchicalEnvironment child : children) {
				String roleName = child.getSecurityRoleOrUserName(permissionMode, SecurityType.ROLE, true);
				logger.debug("Going to remove {} from {}", roleName, userName);
				boolean removed = user.removeRole(roleName);
				logger.trace("{} {} removed from {}", roleName, removed ? "successfully" : "NOT", userName);
			}
			user.save();
		}
		
	}
	
	protected void removeHierarchicRoleFromMyHUser(OSecurity oSecurity, PermissionMode permissionMode, String roleName) {
		String userName = getSecurityRoleOrUserName(permissionMode, SecurityType.USER, true);
		OUser user = oSecurity.getUser(userName);
		logger.debug("Going to remove {} from {}", roleName, userName);
		boolean removed = user.removeRole(roleName);
		logger.trace("{} {} removed from {}", roleName, removed ? "successfully" : "NOT", userName);
		user.save();
	}
	
	protected void deleteRolesAndUsers(OSecurity oSecurity) {
		boolean[] booleanArray = new boolean[] {false, true};
		for(boolean hierarchic : booleanArray) {
			if(hierarchic) {
				removeChildrenHRolesFromParents(oSecurity);
			}
			for(PermissionMode permissionMode : PermissionMode.values()) {
				for(SecurityType securityType : SecurityType.values()) {
					String name = getSecurityRoleOrUserName(permissionMode, securityType, hierarchic);
					drop(oSecurity, name, securityType);
				}
			}
		}
	}
	
}
