package org.gcube.informationsystem.resourceregistry.environments.instances;

import java.util.UUID;

import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;
import org.gcube.informationsystem.resourceregistry.environments.HierarchicalEnvironment;

/**
 * @author Luca Frosini (ISTI - CNR)
 * Contains all the instances belonging to a context.
 */
public class InstanceEnvironment extends HierarchicalEnvironment {

	public InstanceEnvironment(UUID uuid) throws ResourceRegistryException {
		super(uuid);
	}
	
}
