package org.gcube.informationsystem.resourceregistry.environments.administration;

import java.util.UUID;

import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;
import org.gcube.informationsystem.resourceregistry.dbinitialization.DatabaseEnvironment;
import org.gcube.informationsystem.resourceregistry.environments.Environment;
import org.gcube.informationsystem.resourceregistry.environments.SystemEnvironment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.orientechnologies.orient.core.metadata.security.ORole;
import com.orientechnologies.orient.core.metadata.security.ORule;
import com.orientechnologies.orient.core.metadata.security.OSecurity;

/**
 * @author Luca Frosini (ISTI - CNR)
 * This SystemEnvironment is used in all the case the
 * operation must be done by an administrator 
 */
public class AdminEnvironment extends SystemEnvironment {
	
	private static Logger logger = LoggerFactory.getLogger(Environment.class);
	
	private static final String ADMIN_SECURITY_CONTEXT;
	private static final UUID ADMIN_SECURITY_CONTEXT_UUID;
	
	static {
		ADMIN_SECURITY_CONTEXT = "00000000-0000-0000-0000-000000000000";
		ADMIN_SECURITY_CONTEXT_UUID = UUID.fromString(ADMIN_SECURITY_CONTEXT);
	}
	
	private static AdminEnvironment instance;
	
	public static AdminEnvironment getInstance() throws ResourceRegistryException {
		if(instance==null) {
			instance = new AdminEnvironment();
		}
		return instance;
	}
	
	private AdminEnvironment() throws ResourceRegistryException {
		super(ADMIN_SECURITY_CONTEXT_UUID);
	}
	
	@Override
	public void create() {
		throw new RuntimeException("Cannot use this method for Admin Context");
	}
	
	@Override
	protected ORole getSuperRole(OSecurity oSecurity, PermissionMode permissionMode) {
		return oSecurity.getRole(DatabaseEnvironment.DEFAULT_ADMIN_ROLE);
	}
	
	@Override
	protected ORole addExtraRules(ORole role, PermissionMode permissionMode) {
		logger.trace("Adding extra rules for {}", role.getName());
		switch(permissionMode) {
			case WRITER:
				role.addRule(ORule.ResourceGeneric.BYPASS_RESTRICTED, null, ORole.PERMISSION_ALL);
				break;
			
			case READER:
				role.addRule(ORule.ResourceGeneric.BYPASS_RESTRICTED, null, ORole.PERMISSION_READ);
				break;
			
			default:
				break;
		}
		return role;
	}
	
}
