package org.gcube.informationsystem.resourceregistry.environments.contexts;

import java.util.UUID;

import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;
import org.gcube.informationsystem.resourceregistry.environments.Environment;
import org.gcube.informationsystem.resourceregistry.environments.SystemEnvironment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.orientechnologies.orient.core.metadata.security.ORole;
import com.orientechnologies.orient.core.metadata.security.ORule;

/**
 * @author Luca Frosini (ISTI - CNR)
 * This SystemEnvironment is used to pesists
 * Contexts information and their relations among
 * others, e.g. a Context is parent of another.
 */
public class ContextEnvironment extends SystemEnvironment {
	
	private static Logger logger = LoggerFactory.getLogger(Environment.class);
	
	private static final String CONTEXT_SECURITY_CONTEXT;
	private static final UUID CONTEXT_SECURITY_CONTEXT_UUID;
	
	static {
		CONTEXT_SECURITY_CONTEXT = "ffffffff-ffff-ffff-ffff-ffffffffffff";
		CONTEXT_SECURITY_CONTEXT_UUID = UUID.fromString(CONTEXT_SECURITY_CONTEXT);
	}
	
	private static ContextEnvironment instance;
	
	public static ContextEnvironment getInstance() throws ResourceRegistryException {
		if(instance==null) {
			instance = new ContextEnvironment();
		}
		return instance;
	}
	
	private ContextEnvironment() throws ResourceRegistryException {
		super(CONTEXT_SECURITY_CONTEXT_UUID);
	}
	
	
	@Override
	protected ORole addExtraRules(ORole role, PermissionMode permissionMode) {
		logger.trace("Adding extra rules for {}", role.getName());
		switch(permissionMode) {
			case WRITER:
				role.addRule(ORule.ResourceGeneric.CLUSTER, null, ORole.PERMISSION_ALL);
				role.addRule(ORule.ResourceGeneric.SYSTEM_CLUSTERS, null, ORole.PERMISSION_ALL);
				role.addRule(ORule.ResourceGeneric.CLASS, null, ORole.PERMISSION_ALL);
				break;
			
			case READER:
				role.addRule(ORule.ResourceGeneric.CLUSTER, null, ORole.PERMISSION_READ);
				role.addRule(ORule.ResourceGeneric.SYSTEM_CLUSTERS, null, ORole.PERMISSION_READ);
				role.addRule(ORule.ResourceGeneric.CLASS, null, ORole.PERMISSION_READ);
				break;
			
			default:
				break;
		}
		return role;
	}
	
}
