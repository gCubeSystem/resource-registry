package org.gcube.informationsystem.resourceregistry.environments.contexts;

import java.util.UUID;

import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;
import org.gcube.informationsystem.resourceregistry.environments.Environment;
import org.gcube.informationsystem.resourceregistry.environments.SystemEnvironment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.orientechnologies.orient.core.metadata.security.ORole;
import com.orientechnologies.orient.core.metadata.security.ORule;

/**
 * @author Luca Frosini (ISTI - CNR)
 * Added for feature #19428
 * It is used as a sort of cemetery for deleted Contexts. 
 * e.g the information regarding a deleted context are not
 * really deleted instead are moved in this partition to
 * ah historical information and to provide a minimal support for 
 * Context reborn.
 */
public class ShadowContextEnvironment extends SystemEnvironment {
	
	private static Logger logger = LoggerFactory.getLogger(Environment.class);
	
	private static final String SHADOW_CONTEXT_SECURITY_CONTEXT;
	private static final UUID SHADOW_CONTEXT_SECURITY_CONTEXT_UUID;
	
	
	static {
		SHADOW_CONTEXT_SECURITY_CONTEXT = "cccccccc-cccc-cccc-cccc-cccccccccccc";
		SHADOW_CONTEXT_SECURITY_CONTEXT_UUID = UUID.fromString(SHADOW_CONTEXT_SECURITY_CONTEXT);
		
	}
	
	private static ShadowContextEnvironment instance;
	
	public static ShadowContextEnvironment getInstance() throws ResourceRegistryException {
		if(instance==null) {
			instance = new ShadowContextEnvironment();
		}
		return instance;
	}
	
	private ShadowContextEnvironment() throws ResourceRegistryException {
		super(SHADOW_CONTEXT_SECURITY_CONTEXT_UUID);
	}
	
	@Override
	protected ORole addExtraRules(ORole role, PermissionMode permissionMode) {
		logger.trace("Adding extra rules for {}", role.getName());
		switch(permissionMode) {
			case WRITER:
				role.addRule(ORule.ResourceGeneric.CLUSTER, null, ORole.PERMISSION_ALL);
				role.addRule(ORule.ResourceGeneric.SYSTEM_CLUSTERS, null, ORole.PERMISSION_ALL);
				role.addRule(ORule.ResourceGeneric.CLASS, null, ORole.PERMISSION_ALL);
				break;
			
			case READER:
				role.addRule(ORule.ResourceGeneric.CLUSTER, null, ORole.PERMISSION_READ);
				role.addRule(ORule.ResourceGeneric.SYSTEM_CLUSTERS, null, ORole.PERMISSION_READ);
				role.addRule(ORule.ResourceGeneric.CLASS, null, ORole.PERMISSION_READ);
				break;
			
			default:
				break;
		}
		return role;
	}
	
}
