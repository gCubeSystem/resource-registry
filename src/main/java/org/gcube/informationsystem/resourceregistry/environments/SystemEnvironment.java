package org.gcube.informationsystem.resourceregistry.environments;

import java.util.UUID;

import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public abstract class SystemEnvironment extends Environment {

	protected SystemEnvironment(UUID context) throws ResourceRegistryException {
		super(context);
	}

}
