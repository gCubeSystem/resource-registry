/**
 * 
 */
package org.gcube.informationsystem.resourceregistry.environments;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.gcube.common.security.Owner;
import org.gcube.common.security.providers.SecretManagerProvider;
import org.gcube.common.security.secrets.Secret;
import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;
import org.gcube.informationsystem.resourceregistry.contexts.ContextUtility;
import org.gcube.informationsystem.resourceregistry.dbinitialization.DatabaseEnvironment;
import org.gcube.informationsystem.resourceregistry.environments.administration.AdminEnvironment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.orientechnologies.orient.core.db.ODatabasePool;
import com.orientechnologies.orient.core.db.ODatabaseSession;
import com.orientechnologies.orient.core.db.document.ODatabaseDocument;
import com.orientechnologies.orient.core.id.ORID;
import com.orientechnologies.orient.core.metadata.security.ORestrictedOperation;
import com.orientechnologies.orient.core.metadata.security.ORole;
import com.orientechnologies.orient.core.metadata.security.OSecurity;
import com.orientechnologies.orient.core.metadata.security.OSecurityRole.ALLOW_MODES;
import com.orientechnologies.orient.core.metadata.security.OUser;
import com.orientechnologies.orient.core.record.OElement;
import com.orientechnologies.orient.core.record.ORecord;
import com.orientechnologies.orient.core.record.impl.ODocument;

/**
 * @author Luca Frosini (ISTI - CNR)
 * Base class for any environment.
 * In the current implementation it represents a 
 * portion of a graph which in OrientDB 
 * is implemented via security.
 */
public abstract class Environment {
	
	private static Logger logger = LoggerFactory.getLogger(Environment.class);
	
	protected static final String DEFAULT_WRITER_ROLE = "writer";
	protected static final String DEFAULT_READER_ROLE = "reader";
	
	public enum SecurityType {
		ROLE("Role"), USER("User");
		
		private final String name;
		
		private SecurityType(String name) {
			this.name = name;
		}
		
		public String toString() {
			return name;
		}
	}
	
	public enum PermissionMode {
		READER("Reader"), WRITER("Writer");
		
		private final String name;
		
		private PermissionMode(String name) {
			this.name = name;
		}
		
		public String toString() {
			return name;
		}
	}
	
	protected final UUID environmentUUID;
	
	protected final Map<PermissionMode,ODatabasePool> poolMap;
	
	/**
	 * Roles allowed to operate on the security context
	 */
	protected static Set<String> allOperationAllowedRoles;
	protected Set<String> allowedRoles;
	
	public final static String INFRASTRUCTURE_MANAGER = "Infrastructure-Manager";
	public final static String IS_MANAGER = "IS-Manager";
	
	public final static String CONTEXT_MANAGER = "Context-Manager";
	
	static {
		Environment.allOperationAllowedRoles = new HashSet<>();
		Environment.allOperationAllowedRoles.add(INFRASTRUCTURE_MANAGER);
		Environment.allOperationAllowedRoles.add(IS_MANAGER);
	}
	
	public static Set<String> getAllOperationsAllowedRoles() {
		return new HashSet<>(allOperationAllowedRoles);
	}
	
	public Set<String> getAllowedRoles() {
		return new HashSet<>(allowedRoles);
	}
	
	protected Environment(UUID context) throws ResourceRegistryException {
		this.environmentUUID = context;
		this.poolMap = new HashMap<>();
		
		this.allowedRoles = new HashSet<>(Environment.allOperationAllowedRoles);
		this.allowedRoles.add(CONTEXT_MANAGER);
		
	}
	
	protected synchronized ODatabasePool getPool(PermissionMode permissionMode, boolean recreate) {
		ODatabasePool pool = null;
		
		if(recreate) {
			pool = poolMap.get(permissionMode);
			if(pool!=null) {
				pool.close();
				poolMap.remove(permissionMode);
			}
		}

		
		pool = poolMap.get(permissionMode);
		
		if(pool == null) {
			
			String username = getSecurityRoleOrUserName(permissionMode, SecurityType.USER);
			String password = DatabaseEnvironment.DEFAULT_PASSWORDS.get(permissionMode);
			
			pool = new ODatabasePool(DatabaseEnvironment.DB_URI, username, password);
			
			poolMap.put(permissionMode, pool);
		}
		
		return pool;
	}
	
	public UUID getUUID() {
		return environmentUUID;
	}
	
	public static String getRoleOrUserName(PermissionMode permissionMode, SecurityType securityType) {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(permissionMode);
		stringBuilder.append(securityType);
		return stringBuilder.toString();
	}
	
	public String getSecurityRoleOrUserName(PermissionMode permissionMode, SecurityType securityType) {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(getRoleOrUserName(permissionMode, securityType));
		stringBuilder.append("_");
		stringBuilder.append(environmentUUID.toString());
		return stringBuilder.toString();
	}
	
	private OSecurity getOSecurity(ODatabaseDocument oDatabaseDocument) {
		return oDatabaseDocument.getMetadata().getSecurity();
	}
	
	public void addElement(OElement element) throws ResourceRegistryException {
		ODatabaseDocument current = ContextUtility.getCurrentODatabaseDocumentFromThreadLocal();
		ODatabaseDocument adminDatabaseDocument = null;
		try {
			adminDatabaseDocument = AdminEnvironment.getInstance().getDatabaseDocument(PermissionMode.WRITER);
			addElement(element, adminDatabaseDocument);
		}finally {
			if(adminDatabaseDocument!=null) {
				adminDatabaseDocument.close();
			}
			
			if(current!=null) {
				current.activateOnCurrentThread();
			}
		}
	}
	
	protected void allow(OSecurity oSecurity, ODocument oDocument) {
		String writerRoleName = getSecurityRoleOrUserName(PermissionMode.WRITER, SecurityType.ROLE);
		oSecurity.allowRole(oDocument, ORestrictedOperation.ALLOW_ALL, writerRoleName);
		String readerRoleName = getSecurityRoleOrUserName(PermissionMode.READER, SecurityType.ROLE);
		oSecurity.allowRole(oDocument, ORestrictedOperation.ALLOW_READ, readerRoleName);
	}
	
	public boolean isElementInContext(final OElement element) throws ResourceRegistryException {
		ORID orid = element.getIdentity();
		ODatabaseDocument current = ContextUtility.getCurrentODatabaseDocumentFromThreadLocal();
		ODatabaseDocument contextODatabaseDocument = null;
		
		try {
			contextODatabaseDocument = getDatabaseDocument(PermissionMode.READER);
			
			ORecord oRecord = contextODatabaseDocument.getRecord(orid);
			if(oRecord==null) {
				return false;
			}
			return true;
			
		} finally {
			if(contextODatabaseDocument!=null) {
				contextODatabaseDocument.close();
			}
			
			if(current!=null) {
				current.activateOnCurrentThread();
			}
		}
		
	}
	
	public void addElement(OElement element, ODatabaseDocument oDatabaseDocument) {
		ODocument oDocument = element.getRecord();
		OSecurity oSecurity = getOSecurity(oDatabaseDocument);
		allow(oSecurity, oDocument);
		oDocument.save();
		element.save();
	}
	
	public void removeElement(OElement element) throws ResourceRegistryException {
		ODatabaseDocument current = ContextUtility.getCurrentODatabaseDocumentFromThreadLocal();
		ODatabaseDocument adminDatabaseDocument = null;
		try {
			adminDatabaseDocument = AdminEnvironment.getInstance().getDatabaseDocument(PermissionMode.WRITER);
			removeElement(element, adminDatabaseDocument);
		}finally {
			if(adminDatabaseDocument!=null) {
				adminDatabaseDocument.close();
			}
			
			if(current!=null) {
				current.activateOnCurrentThread();
			}
		}
	}
	
	protected void deny(OSecurity oSecurity, ODocument oDocument) {
		// The element could be created in such a context so the writerUser for the
		// context is allowed by default because it was the creator
		String writerUserName = getSecurityRoleOrUserName(PermissionMode.WRITER, SecurityType.USER);
		oSecurity.denyUser(oDocument, ORestrictedOperation.ALLOW_ALL, writerUserName);
		String readerUserName = getSecurityRoleOrUserName(PermissionMode.WRITER, SecurityType.USER);
		oSecurity.denyUser(oDocument, ORestrictedOperation.ALLOW_READ, readerUserName);
		
		String writerRoleName = getSecurityRoleOrUserName(PermissionMode.WRITER, SecurityType.ROLE);
		oSecurity.denyRole(oDocument, ORestrictedOperation.ALLOW_ALL, writerRoleName);
		String readerRoleName = getSecurityRoleOrUserName(PermissionMode.READER, SecurityType.ROLE);
		oSecurity.denyRole(oDocument, ORestrictedOperation.ALLOW_READ, readerRoleName);
		
	}
	
	public void removeElement(OElement element, ODatabaseDocument oDatabaseDocument) {
		ODocument oDocument = element.getRecord();
		OSecurity oSecurity = getOSecurity(oDatabaseDocument);
		deny(oSecurity, oDocument);
		oDocument.save();
		element.save();
	}
	
	protected boolean allowed(final ORole role, final ODocument oDocument) {
		ODatabaseDocument current = ContextUtility.getCurrentODatabaseDocumentFromThreadLocal();
		ODatabaseDocument oDatabaseDocument = null;
		try {
			oDatabaseDocument = getDatabaseDocument(PermissionMode.READER);
			oDatabaseDocument.activateOnCurrentThread();
			ORecord element = oDatabaseDocument.getRecord(oDocument.getIdentity());
			if(element == null) {
				return false;
			}
			return true;
		} catch(Exception e) {
			return false;
		} finally {
			
			if(oDatabaseDocument!=null) {
				oDatabaseDocument.close();
			}
			
			if(current!=null) {
				current.activateOnCurrentThread();
			}
		}
		
	}
	
	public boolean isUserAllowed(Collection<String> allowedRoles) {
		boolean allowed = false;
		Secret secret = SecretManagerProvider.get();
		Owner owner = secret.getOwner();
		Collection<String> roles = new HashSet<>(owner.getRoles());
		roles.addAll(owner.getGlobalRoles());
		roles.retainAll(allowedRoles);
		if(roles.size()>0) {
			allowed = true;
		}
		return allowed;
	}
	
//	public boolean isUserAllowed(Operation operation) {
//		switch (operation) {
//			case CREATE:
//				break;
//				
//			case READ:
//				break;
//				
//			case EXISTS:
//				break;
//				
//			case UPDATE:
//				break;
//				
//			case DELETE:
//				break;
//				
//			case ADD_TO_CONTEXT:
//				break;
//	
//			case REMOVE_FROM_CONTEXT:
//				break;
//		
//			case QUERY:
//				break;
//				
//			case GET_METADATA:
//				return isUserAllowed(allOperationAllowedRoles);
//				
//			default:
//				break;
//		}
//		
//		return true;
//	}
	
	public void create() throws ResourceRegistryException {
		ODatabaseDocument current = ContextUtility.getCurrentODatabaseDocumentFromThreadLocal();
		ODatabaseDocument adminDatabaseDocument = null;
		try {
			adminDatabaseDocument = AdminEnvironment.getInstance().getDatabaseDocument(PermissionMode.WRITER);
			
			create(adminDatabaseDocument);
			
			adminDatabaseDocument.commit();
		} finally {
			if(adminDatabaseDocument!=null) {
				adminDatabaseDocument.close();
			}
			
			if(current!=null) {
				current.activateOnCurrentThread();
			}
		}
	}
	
	protected ORole addExtraRules(ORole role, PermissionMode permissionMode) {
		return role;
	}
	
	protected ORole getSuperRole(OSecurity oSecurity, PermissionMode permissionMode) {
		String superRoleName = permissionMode.name().toLowerCase();
		return oSecurity.getRole(superRoleName);
	}
	
	protected void createRolesAndUsers(OSecurity oSecurity) {
		for(PermissionMode permissionMode : PermissionMode.values()) {
			ORole superRole = getSuperRole(oSecurity, permissionMode);
			
			String roleName = getSecurityRoleOrUserName(permissionMode, SecurityType.ROLE);
			ORole role = oSecurity.createRole(roleName, superRole, ALLOW_MODES.DENY_ALL_BUT);
			addExtraRules(role, permissionMode);
			role.save();
			logger.trace("{} created", role);
			
			String userName = getSecurityRoleOrUserName(permissionMode, SecurityType.USER);
			OUser user = oSecurity.createUser(userName, DatabaseEnvironment.DEFAULT_PASSWORDS.get(permissionMode),
					role);
			user.save();
			logger.trace("{} created", user);
		}
		
	}
	
	public void create(ODatabaseDocument oDatabaseDocument) {
		OSecurity oSecurity = getOSecurity(oDatabaseDocument);
		createRolesAndUsers(oSecurity);
		logger.trace("Security Context (roles and users) with UUID {} successfully created", environmentUUID.toString());
	}
	
	protected void drop(OSecurity oSecurity, String name, SecurityType securityType) {
		boolean dropped = false;
		switch(securityType) {
			case ROLE:
				dropped = oSecurity.dropRole(name);
				break;
			
			case USER:
				dropped = oSecurity.dropUser(name);
				break;
			
			default:
				break;
		}
		if(dropped) {
			logger.trace("{} successfully dropped", name);
		} else {
			logger.error("{} was not dropped successfully", name);
		}
	}
	
	public void delete() throws ResourceRegistryException {
		ODatabaseDocument current = ContextUtility.getCurrentODatabaseDocumentFromThreadLocal();
		ODatabaseDocument adminDatabaseDocument = null;
		try {
			adminDatabaseDocument = AdminEnvironment.getInstance().getDatabaseDocument(PermissionMode.WRITER);
			
			delete(adminDatabaseDocument);
			
			adminDatabaseDocument.commit();
		} finally {
			if(adminDatabaseDocument!=null) {
				adminDatabaseDocument.close();
			}
			
			if(current!=null) {
				current.activateOnCurrentThread();
			}
		}
		
	}
	
	protected void deleteRolesAndUsers(OSecurity oSecurity) {
		for(PermissionMode permissionMode : PermissionMode.values()) {
			for(SecurityType securityType : SecurityType.values()) {
				String name = getSecurityRoleOrUserName(permissionMode, securityType);
				drop(oSecurity, name, securityType);
			}
		}
	}
	
	public void delete(ODatabaseDocument orientGraph) {
		OSecurity oSecurity = getOSecurity(orientGraph);
		delete(oSecurity);
	}
	
	private void delete(OSecurity oSecurity) {
		logger.trace("Going to remove Security Context (roles and users) with UUID {}", environmentUUID.toString());
		deleteRolesAndUsers(oSecurity);
		logger.trace("Security Context (roles and users) with UUID {} successfully removed", environmentUUID.toString());
	}
	
	public ODatabaseDocument getDatabaseDocument(PermissionMode permissionMode) throws ResourceRegistryException {
		try {
			ODatabasePool oDatabasePool = getPool(permissionMode, false);
			ODatabaseSession oDatabaseSession = null;
			try {
				oDatabaseSession = oDatabasePool.acquire();
				if(oDatabaseSession.isClosed()) {
					// Enforcing pool recreation 
					throw new Exception();
				}
			}catch (Exception e) {
				oDatabasePool = getPool(permissionMode, true);
				oDatabaseSession = oDatabasePool.acquire();
			}
			oDatabaseSession.activateOnCurrentThread();
			return oDatabaseSession;
		}catch (Exception e) {
			throw new ResourceRegistryException(e);
		}
	}
	
	@Override
	public String toString() {
		return "%s %s".formatted(this.getClass().getSimpleName(), getUUID().toString());
	}

}
