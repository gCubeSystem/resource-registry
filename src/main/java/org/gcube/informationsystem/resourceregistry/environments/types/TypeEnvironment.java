package org.gcube.informationsystem.resourceregistry.environments.types;

import java.util.UUID;

import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;
import org.gcube.informationsystem.resourceregistry.environments.Environment;
import org.gcube.informationsystem.resourceregistry.environments.SystemEnvironment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.orientechnologies.orient.core.metadata.security.ORole;
import com.orientechnologies.orient.core.metadata.security.ORule;

/**
 * @author Luca Frosini (ISTI - CNR)
 * Contains all the information-system-model types
 * plus all the types defined on top of the 
 * information-system-model (e.g the gcube-model)
 */
public class TypeEnvironment extends SystemEnvironment {
	
	private static Logger logger = LoggerFactory.getLogger(Environment.class);
	
	private static final String SCHEMA_SECURITY_CONTEXT;
	private static final UUID SCHEMA_SECURITY_CONTEXT_UUID;

	static {
		SCHEMA_SECURITY_CONTEXT = "eeeeeeee-eeee-eeee-eeee-eeeeeeeeeeee";
		SCHEMA_SECURITY_CONTEXT_UUID = UUID.fromString(SCHEMA_SECURITY_CONTEXT);
	}
	
	private static TypeEnvironment instance;
	
	public static TypeEnvironment getInstance() throws ResourceRegistryException {
		if(instance==null) {
			instance = new TypeEnvironment();
		}
		return instance;
	}
	
	private TypeEnvironment() throws ResourceRegistryException {
		super(SCHEMA_SECURITY_CONTEXT_UUID);
	}
	
	@Override
	protected ORole addExtraRules(ORole role, PermissionMode permissionMode) {
		logger.trace("Adding extra rules for {}", role.getName());
		switch(permissionMode) {
			case WRITER:
				role.addRule(ORule.ResourceGeneric.CLUSTER, null, ORole.PERMISSION_ALL);
				role.addRule(ORule.ResourceGeneric.SYSTEM_CLUSTERS, null, ORole.PERMISSION_ALL);
				role.addRule(ORule.ResourceGeneric.CLASS, null, ORole.PERMISSION_ALL);
				break;
			
			case READER:
				role.addRule(ORule.ResourceGeneric.CLUSTER, null, ORole.PERMISSION_READ);
				role.addRule(ORule.ResourceGeneric.SYSTEM_CLUSTERS, null, ORole.PERMISSION_READ);
				role.addRule(ORule.ResourceGeneric.CLASS, null, ORole.PERMISSION_READ);
				break;
			
			default:
				break;
		}
		return role;
	}
	
}
