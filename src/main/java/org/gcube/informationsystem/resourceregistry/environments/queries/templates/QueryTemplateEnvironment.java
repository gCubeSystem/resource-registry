package org.gcube.informationsystem.resourceregistry.environments.queries.templates;

import java.util.UUID;

import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;
import org.gcube.informationsystem.resourceregistry.environments.Environment;
import org.gcube.informationsystem.resourceregistry.environments.SystemEnvironment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.orientechnologies.orient.core.metadata.security.ORole;
import com.orientechnologies.orient.core.metadata.security.ORule;

/**
 * @author Luca Frosini (ISTI - CNR)
 * Contains all the query templates
 */
public class QueryTemplateEnvironment extends SystemEnvironment {
	
	private static Logger logger = LoggerFactory.getLogger(Environment.class);
	
	private static final String QUERY_TEMPLATES_SECURITY_CONTEXT;
	private static final UUID QUERY_TEMPLATES_SECURITY_CONTEXT_UUID;
	
	static {
		QUERY_TEMPLATES_SECURITY_CONTEXT = "dddddddd-dddd-dddd-dddd-dddddddddddd";
		QUERY_TEMPLATES_SECURITY_CONTEXT_UUID = UUID.fromString(QUERY_TEMPLATES_SECURITY_CONTEXT);
	}
	
	private static QueryTemplateEnvironment instance;
	
	public static QueryTemplateEnvironment getInstance() throws ResourceRegistryException {
		if(instance==null) {
			instance = new QueryTemplateEnvironment();
		}
		return instance;
	}
	
	private QueryTemplateEnvironment() throws ResourceRegistryException {
		super(QUERY_TEMPLATES_SECURITY_CONTEXT_UUID);
	}
	
	@Override
	protected ORole addExtraRules(ORole role, PermissionMode permissionMode) {
		logger.trace("Adding extra rules for {}", role.getName());
		switch(permissionMode) {
			case WRITER:
				role.addRule(ORule.ResourceGeneric.CLUSTER, null, ORole.PERMISSION_ALL);
				role.addRule(ORule.ResourceGeneric.SYSTEM_CLUSTERS, null, ORole.PERMISSION_ALL);
				role.addRule(ORule.ResourceGeneric.CLASS, null, ORole.PERMISSION_ALL);
				break;
			
			case READER:
				role.addRule(ORule.ResourceGeneric.CLUSTER, null, ORole.PERMISSION_READ);
				role.addRule(ORule.ResourceGeneric.SYSTEM_CLUSTERS, null, ORole.PERMISSION_READ);
				role.addRule(ORule.ResourceGeneric.CLASS, null, ORole.PERMISSION_READ);
				break;
			
			default:
				break;
		}
		return role;
	}
	
}
