package org.gcube.informationsystem.resourceregistry.types.entities;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.gcube.com.fasterxml.jackson.databind.JsonNode;
import org.gcube.informationsystem.base.reference.AccessType;
import org.gcube.informationsystem.resourceregistry.api.exceptions.AlreadyPresentException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.NotFoundException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.entities.EntityAlreadyPresentException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.types.SchemaAlreadyPresentException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.types.SchemaNotFoundException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.types.SchemaViolationException;
import org.gcube.informationsystem.resourceregistry.base.ElementManagementUtility;
import org.gcube.informationsystem.resourceregistry.base.entities.EntityElementManagement;
import org.gcube.informationsystem.resourceregistry.environments.Environment;
import org.gcube.informationsystem.resourceregistry.environments.types.TypeEnvironment;
import org.gcube.informationsystem.resourceregistry.utils.OrientDBUtility;
import org.gcube.informationsystem.types.TypeMapper;
import org.gcube.informationsystem.types.reference.entities.EntityType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.orientechnologies.orient.core.record.OVertex;
import com.orientechnologies.orient.core.sql.executor.OResult;
import com.orientechnologies.orient.core.sql.executor.OResultSet;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public abstract class EntityTypeDefinitionManagement<E extends EntityType> extends EntityElementManagement<E, E> {

	private static Logger logger = LoggerFactory.getLogger(EntityTypeDefinitionManagement.class);

	protected String name;

	protected EntityTypeDefinitionManagement(Class<E> clz) {
		super(AccessType.ENTITY_TYPE);
		this.typeName = TypeMapper.getType(clz);
		this.forceIncludeAllMeta = true;
	}
	
	@Override
	public Map<UUID,JsonNode> getAffectedInstances() {
		throw new UnsupportedOperationException();
	}

	@Override
	protected Environment getWorkingEnvironment() throws ResourceRegistryException {
		if (workingEnvironment == null) {
			workingEnvironment = TypeEnvironment.getInstance();
		}
		return workingEnvironment;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		if (name == null) {
			if (element == null) {
				if (jsonNode != null) {
					name = jsonNode.get(EntityType.NAME_PROPERTY).asText();
				}
			} else {
				name = element.getProperty(EntityType.NAME_PROPERTY);
			}
		}
		return name;
	}

	@Override
	protected JsonNode createCompleteJsonNode() throws ResourceRegistryException {
		return serializeSelfAsJsonNode();
	}

	@Override
	protected OVertex reallyCreate() throws AlreadyPresentException, ResourceRegistryException {
		logger.debug("Going to create {} for {}", this.typeName, getName());
		return createVertex();
	}

	@Override
	protected OVertex reallyUpdate() throws NotFoundException, ResourceRegistryException {
		logger.debug("Going to update {} for {}", this.typeName, getName());
		OVertex entityTypeDefinition = getElement();
		entityTypeDefinition = (OVertex) updateProperties(oClass, entityTypeDefinition, jsonNode, ignoreKeys,
				ignoreStartWithKeys);
		return entityTypeDefinition;
	}

	@Override
	protected void reallyDelete() throws NotFoundException, ResourceRegistryException {
		logger.debug("Going to remove {} for {}", this.typeName, getName());
		getElement().delete();
	}

	@Override
	public OVertex getElement() throws NotFoundException, ResourceRegistryException {
		if (element == null) {
			try {
				element = retrieveElement();
			} catch (NotFoundException e) {
				throw e;
			} catch (ResourceRegistryException e) {
				throw e;
			} catch (Exception e) {
				throw new ResourceRegistryException(e);
			}

		} else {
			if (reload) {
				element.reload();
			}
		}
		return element;
	}

	@Override
	public OVertex retrieveElement() throws NotFoundException, ResourceRegistryException {
		try {
			if (getName() == null) {
				throw new NotFoundException("null name does not allow to retrieve the Element");
			}

			String select = "SELECT FROM " + typeName + " WHERE " + EntityType.NAME_PROPERTY + " = \"" + getName()
					+ "\"";

			OResultSet resultSet = oDatabaseDocument.query(select, new HashMap<>());

			if (resultSet == null || !resultSet.hasNext()) {
				String error = "No %s with name %s was found".formatted(typeName, getName());
				logger.info(error);
				throw new NotFoundException(error);
			}

			OResult oResult = resultSet.next();
			OVertex element = (OVertex) ElementManagementUtility.getElementFromOptional(oResult.getElement());

			logger.trace("{} with id {} is : {}", typeName, getName(), OrientDBUtility.getAsStringForLogging(element));

			if (resultSet.hasNext()) {
				throw new ResourceRegistryException("Found more than one " + typeName + " with name " + getName()
						+ ". This is a fatal error please contact Admnistrator");
			}

			return element;
		} catch (NotFoundException e) {
			throw getSpecificNotFoundException(e);
		} catch (ResourceRegistryException e) {
			throw e;
		} catch (Exception e) {
			throw new ResourceRegistryException(e);
		}
	}

	@Override
	protected OVertex createVertex() throws EntityAlreadyPresentException, ResourceRegistryException {

		logger.trace("Going to create {} for {} ({}) using {}", OVertex.class.getSimpleName(), accessType.getName(),
				typeName, jsonNode);

		try {

			this.element = oDatabaseDocument.newVertex(typeName);

			updateProperties(oClass, element, jsonNode, ignoreKeys, ignoreStartWithKeys);

			logger.debug("Created {} is {}", OVertex.class.getSimpleName(), OrientDBUtility.getAsStringForLogging(element));

			return element;
		} catch (ResourceRegistryException e) {
			throw e;
		} catch (Exception e) {
			logger.trace("Error while creating {} for {} ({}) using {}", OVertex.class.getSimpleName(),
					accessType.getName(), typeName, jsonNode, e);
			throw new ResourceRegistryException("Error Creating " + typeName + " with " + jsonNode, e.getCause());
		}
	}

	@Override
	public String reallyGetAll(boolean polymorphic) throws ResourceRegistryException {
		throw new UnsupportedOperationException();
	}

	@Override
	protected NotFoundException getSpecificNotFoundException(NotFoundException e) {
		return new SchemaNotFoundException(e.getMessage(), e.getCause());
	}

	@Override
	protected AlreadyPresentException getSpecificAlreadyPresentException(String message) {
		return new SchemaAlreadyPresentException(message);
	}

	@Override
	public void sanityCheck() throws SchemaViolationException, ResourceRegistryException {
		// Nothing to do
	}

}
