package org.gcube.informationsystem.resourceregistry.types.relations;

import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.types.SchemaViolationException;
import org.gcube.informationsystem.resourceregistry.environments.types.TypeEnvironment;
import org.gcube.informationsystem.resourceregistry.types.entities.ResourceTypeDefinitionManagement;
import org.gcube.informationsystem.types.reference.entities.ResourceType;
import org.gcube.informationsystem.types.reference.relations.IsRelatedToType;

import com.orientechnologies.orient.core.db.document.ODatabaseDocument;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class IsRelatedToTypeDefinitionManagement
		extends RelationTypeDefinitionManagement<ResourceTypeDefinitionManagement,ResourceType> {
	
	public IsRelatedToTypeDefinitionManagement() {
		super(ResourceType.class);
		this.typeName = IsRelatedToType.NAME;
	}
	
	public IsRelatedToTypeDefinitionManagement(TypeEnvironment typeEnvironment, ODatabaseDocument oDatabaseDocument)
			throws ResourceRegistryException {
		super(typeEnvironment, oDatabaseDocument, ResourceType.class);
		this.typeName = IsRelatedToType.NAME;
	}
	
	@Override
	protected ResourceTypeDefinitionManagement newTargetEntityManagement() throws ResourceRegistryException {
		ResourceTypeDefinitionManagement rtdm = new ResourceTypeDefinitionManagement();
		rtdm.setWorkingEnvironment(getWorkingEnvironment());
		rtdm.setODatabaseDocument(oDatabaseDocument);
		return rtdm;
	}
	
	@Override
	public void sanityCheck() throws SchemaViolationException, ResourceRegistryException {
		// Nothing to do
	}
}
