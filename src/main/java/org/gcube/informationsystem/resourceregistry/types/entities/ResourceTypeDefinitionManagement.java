package org.gcube.informationsystem.resourceregistry.types.entities;

import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.types.SchemaViolationException;
import org.gcube.informationsystem.types.reference.entities.ResourceType;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class ResourceTypeDefinitionManagement extends EntityTypeDefinitionManagement<ResourceType> {
	
	public ResourceTypeDefinitionManagement() {
		super(ResourceType.class);
	}
	
	@Override
	public void sanityCheck() throws SchemaViolationException, ResourceRegistryException {
		// TODO
		// We should check that a constraint defined here is not in contrast with the constraint of supertypes
	}
	
}
