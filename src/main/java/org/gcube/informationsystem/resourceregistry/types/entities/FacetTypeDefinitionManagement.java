package org.gcube.informationsystem.resourceregistry.types.entities;

import org.gcube.informationsystem.types.reference.entities.FacetType;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class FacetTypeDefinitionManagement extends EntityTypeDefinitionManagement<FacetType> {
	
	public FacetTypeDefinitionManagement() {
		super(FacetType.class);
	}
	
}
