package org.gcube.informationsystem.resourceregistry.types;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class TypesCache {

	private static Logger logger = LoggerFactory.getLogger(TypesCache.class);
	
	private static TypesCache typesCache;
	
	public synchronized static TypesCache getInstance() {
		if(typesCache == null) {
			typesCache = new TypesCache();
		}
		return typesCache;
	}
	
	protected final Map<String, CachedType<?>> cachedTypes;
	
	private TypesCache() {
		cachedTypes = new HashMap<>();
	}

	public CachedType<?> getCachedType(String typeName) {
		CachedType<?> cachedType = cachedTypes.get(typeName);
		if(cachedType == null ) {
			logger.trace("{} not in cache. Going to create {} instance", typeName, CachedType.class.getSimpleName());
			cachedType = new CachedType<>(typeName);
			cachedTypes.put(typeName, cachedType);
		}
		return cachedType;
	}

}
