package org.gcube.informationsystem.resourceregistry.types.relations;

import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.types.SchemaViolationException;
import org.gcube.informationsystem.resourceregistry.environments.types.TypeEnvironment;
import org.gcube.informationsystem.resourceregistry.types.entities.FacetTypeDefinitionManagement;
import org.gcube.informationsystem.types.reference.entities.FacetType;
import org.gcube.informationsystem.types.reference.relations.ConsistsOfType;

import com.orientechnologies.orient.core.db.document.ODatabaseDocument;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class ConsistsOfTypeDefinitionManagement
		extends RelationTypeDefinitionManagement<FacetTypeDefinitionManagement,FacetType> {
	
	public ConsistsOfTypeDefinitionManagement() {
		super(FacetType.class);
		this.typeName = ConsistsOfType.NAME;
	}
	
	public ConsistsOfTypeDefinitionManagement(TypeEnvironment typeEnvironment, ODatabaseDocument oDatabaseDocument)
			throws ResourceRegistryException {
		super(typeEnvironment, oDatabaseDocument, FacetType.class);
		this.typeName = ConsistsOfType.NAME;
	}
	
	@Override
	protected FacetTypeDefinitionManagement newTargetEntityManagement() throws ResourceRegistryException {
		FacetTypeDefinitionManagement ftdm = new FacetTypeDefinitionManagement();
		ftdm.setWorkingEnvironment(getWorkingEnvironment());
		ftdm.setODatabaseDocument(oDatabaseDocument);
		return ftdm;
	}
	
	@Override
	public void sanityCheck() throws SchemaViolationException, ResourceRegistryException {
		// Nothing to do
	}
	
}