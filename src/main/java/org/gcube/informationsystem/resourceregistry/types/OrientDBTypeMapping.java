package org.gcube.informationsystem.resourceregistry.types;

import java.util.HashMap;
import java.util.Map;

import org.gcube.informationsystem.types.PropertyTypeName.BaseType;

import com.orientechnologies.orient.core.metadata.schema.OType;

/**
 * @author Luca Frosini (ISTI - CNR)
 * 
 * Create a mapping between OrientDB {@link OType} 
 * https://orientdb.gitbooks.io/orientdb-manual/content/orientdb.wiki/Types.html
 * and {@link BaseType} 
 */
public class OrientDBTypeMapping {
	
	protected static final Map<BaseType,OType> BASE_TYPE_TO_OTYPE;
	
	protected static final Map<OType,BaseType> OTYPE_TO_BASE_TYPE;
	
	
	static {
		BASE_TYPE_TO_OTYPE = new HashMap<>();
		
		BASE_TYPE_TO_OTYPE.put(BaseType.ANY, OType.ANY);
		BASE_TYPE_TO_OTYPE.put(BaseType.BOOLEAN, OType.BOOLEAN);
		BASE_TYPE_TO_OTYPE.put(BaseType.INTEGER, OType.INTEGER);
		BASE_TYPE_TO_OTYPE.put(BaseType.SHORT, OType.SHORT);
		BASE_TYPE_TO_OTYPE.put(BaseType.LONG, OType.LONG);
		BASE_TYPE_TO_OTYPE.put(BaseType.FLOAT, OType.FLOAT);
		BASE_TYPE_TO_OTYPE.put(BaseType.DOUBLE, OType.DOUBLE);
		BASE_TYPE_TO_OTYPE.put(BaseType.DATE, OType.DATETIME);
		BASE_TYPE_TO_OTYPE.put(BaseType.STRING, OType.STRING);
		BASE_TYPE_TO_OTYPE.put(BaseType.BINARY, OType.BINARY);
		BASE_TYPE_TO_OTYPE.put(BaseType.BYTE, OType.BYTE);
		BASE_TYPE_TO_OTYPE.put(BaseType.JSON_OBJECT, OType.EMBEDDED);
		BASE_TYPE_TO_OTYPE.put(BaseType.JSON_ARRAY, OType.EMBEDDEDLIST);
		BASE_TYPE_TO_OTYPE.put(BaseType.PROPERTY, OType.EMBEDDED);
		BASE_TYPE_TO_OTYPE.put(BaseType.LIST, OType.EMBEDDEDLIST);
		BASE_TYPE_TO_OTYPE.put(BaseType.SET, OType.EMBEDDEDSET);
		BASE_TYPE_TO_OTYPE.put(BaseType.MAP, OType.EMBEDDEDMAP);
		
		OTYPE_TO_BASE_TYPE = new HashMap<>();
		for(BaseType baseType : BASE_TYPE_TO_OTYPE.keySet()) {
			OTYPE_TO_BASE_TYPE.put(BASE_TYPE_TO_OTYPE.get(baseType), baseType);
		}
		
	}
	
	public static OType getOTypeByBaseType(final BaseType baseType) {
		return BASE_TYPE_TO_OTYPE.get(baseType);
	}
	
	public static BaseType getBaseTypeByOType(final OType oType) {
		return OTYPE_TO_BASE_TYPE.get(oType);
	}
	
}
