package org.gcube.informationsystem.resourceregistry.types;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import org.gcube.informationsystem.base.reference.AccessType;
import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.types.SchemaException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.types.SchemaNotFoundException;
import org.gcube.informationsystem.resourceregistry.base.ElementManagement;
import org.gcube.informationsystem.resourceregistry.contexts.ContextUtility;
import org.gcube.informationsystem.resourceregistry.dbinitialization.DatabaseEnvironment;
import org.gcube.informationsystem.resourceregistry.environments.Environment.PermissionMode;
import org.gcube.informationsystem.resourceregistry.environments.administration.AdminEnvironment;
import org.gcube.informationsystem.types.TypeMapper;
import org.gcube.informationsystem.types.reference.Type;
import org.gcube.informationsystem.types.reference.properties.LinkedEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.orientechnologies.orient.core.db.document.ODatabaseDocument;
import com.orientechnologies.orient.core.metadata.OMetadata;
import com.orientechnologies.orient.core.metadata.schema.OClass;
import com.orientechnologies.orient.core.metadata.schema.OSchema;
import com.orientechnologies.orient.core.metadata.security.OSecurity;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class CachedType<T extends Type> {
	
	private static Logger logger = LoggerFactory.getLogger(CachedType.class);
	
	private static final Set<String> superClassesToBeExcluded;
	
	static {
		superClassesToBeExcluded = AccessType.names();
		AccessType[] modelTypes = AccessType.getModelTypes();
		for(AccessType accessType : modelTypes) {
			superClassesToBeExcluded.remove(accessType.getName());
		}
	}
	
	protected final String typeName;
	
	protected OClass oClass;
	
	protected AccessType accessType;
	
	protected T type;
	protected List<String> superTypes;
	protected List<String> subTypes;
	
	/* Valid only for resource types */
	protected Set<LinkedEntity> constraints;
	
	
	public CachedType(String typeName) {
		this.typeName = typeName;
	}
	
	private OClass retrieveOClass() throws SchemaException, SchemaNotFoundException, ResourceRegistryException {
		ODatabaseDocument current = ContextUtility.getCurrentODatabaseDocumentFromThreadLocal();
		ODatabaseDocument oDatabaseDocument = null;
		try {
			logger.debug("GettingType {} schema", typeName);
			AdminEnvironment adminEnvironment = AdminEnvironment.getInstance();
			oDatabaseDocument = adminEnvironment.getDatabaseDocument(PermissionMode.READER);
			OMetadata oMetadata = oDatabaseDocument.getMetadata();
			OSchema oSchema = oMetadata.getSchema();
			try {
				OClass oClass = oSchema.getClass(typeName);
				if(oClass == null) {
					throw new SchemaNotFoundException(typeName + " was not registered");
				}
				return oClass;
			} catch(SchemaNotFoundException snfe) {
				throw snfe;
			} catch(Exception e) {
				throw new SchemaException(e.getMessage());
			}
		} catch(ResourceRegistryException e) {
			throw e;
		} catch(Exception e) {
			throw new ResourceRegistryException(e);
		} finally {
			if(oDatabaseDocument != null) {
				oDatabaseDocument.close();
			}
			
			if(current!=null) {
				current.activateOnCurrentThread();
			}
		}
		
		
	}
	
	public synchronized void setOClass(OClass oClass) throws SchemaNotFoundException, SchemaException, ResourceRegistryException {
		if(this.oClass==null) {
			this.oClass = oClass;
		}
	}
	
	private AccessType getAccessTypeFromOClass(OClass oClass) throws ResourceRegistryException {
		AccessType[] accessTypes = AccessType.values();
		for(int i=accessTypes.length-1; i>=0; i--) {
			AccessType accessType = accessTypes[i];
			if(oClass.isSubClassOf(accessType.getName())) {
				return accessType;
			}
		}
		throw new ResourceRegistryException(typeName + " is not a base type");
	}
	
	private List<String> getAllSuperclasses(Collection<String> superClassesToBeExcluded) throws SchemaException, ResourceRegistryException {
		TypesCache typesCache = TypesCache.getInstance();
		
		List<String> allSuperClasses = new ArrayList<>();
		
		// Instead of using  getAllSuperClasses() we get just the first level superclasses and so on.
		// This allow to have an order list where the first superclass is a a first level superclass.
		List<OClass> superClasses = internalGetOClass().getSuperClasses();
		while(superClasses.size()>0) {
			List<OClass> toBeAnalysed = new ArrayList<>(superClasses);
			superClasses = new ArrayList<>();
			for(OClass oSuperClass : toBeAnalysed) {
				String name = oSuperClass.getName();
				CachedType<?> cachedType = typesCache.getCachedType(name);
				cachedType.setOClass(oSuperClass);
				if(name.compareTo(DatabaseEnvironment.VERTEX_CLASS_NAME) == 0 || name.compareTo(DatabaseEnvironment.EDGE_CLASS_NAME) == 0
						|| name.compareTo(OSecurity.RESTRICTED_CLASSNAME) == 0) {
					continue;
				}
				
				if(superClassesToBeExcluded.contains(name)) {
					continue;
				}
				allSuperClasses.add(allSuperClasses.size(), name);
				superClasses.addAll(oSuperClass.getSuperClasses());
			}
		}
		return allSuperClasses;
	}
	
	protected List<String> getAllSubclasses() throws SchemaException, ResourceRegistryException {
		TypesCache typesCache = TypesCache.getInstance();
		
		List<String> allSubClasses = new ArrayList<>();
		Collection<OClass> subclasses = internalGetOClass().getSubclasses();
		while(subclasses.size()>0) {
			List<OClass> toBeAnalysed = new ArrayList<>(subclasses);
			subclasses = new ArrayList<>();
			for(OClass oSubClass : toBeAnalysed) {
				String name = oSubClass.getName();
				CachedType<?> cachedType = typesCache.getCachedType(name);
				cachedType.setOClass(oSubClass);
				allSubClasses.add(allSubClasses.size(), name);
				subclasses.addAll(oSubClass.getSubclasses());
			}
		}
		return allSubClasses;
	}
	
	private OClass internalGetOClass() throws SchemaNotFoundException, SchemaException, ResourceRegistryException {
		if(oClass==null) {
			oClass = retrieveOClass();
		}
		return oClass;
	}
	
	@SuppressWarnings("unchecked")
	private T internalGetType() throws SchemaNotFoundException, SchemaException, ResourceRegistryException {
		if(type==null) {
			ElementManagement<?,?> erManagement = TypeManagement.getTypeManagement(internalGetOClass());
			String typeString = erManagement.read();
			try {
				type = (T) TypeMapper.deserializeTypeDefinition(typeString);
			} catch (Exception e) {
				throw new ResourceRegistryException(e);
			}	
		}
		return type;
	}
	
	private AccessType internalGetAccessType() throws SchemaNotFoundException, SchemaException, ResourceRegistryException {
		if(accessType==null) {
			if(type!=null) {
				accessType = type.getAccessType();
			}else {
				accessType = getAccessTypeFromOClass(internalGetOClass());
			}
		}
		return accessType;
	}
	
	private List<String> internalGetSuperTypes() throws SchemaException, ResourceRegistryException {
		if(superTypes==null) {
			superTypes = getAllSuperclasses(superClassesToBeExcluded);
		}
		return superTypes;
	}
	
	private List<String> internalGetSubTypes() throws SchemaException, ResourceRegistryException {
		if(subTypes==null) {
			subTypes = getAllSubclasses();
		}
		return subTypes;
	}
	
	
	public synchronized OClass getOClass() throws SchemaNotFoundException, SchemaException, ResourceRegistryException {
		return internalGetOClass();
	}
	
	public synchronized T getType() throws SchemaNotFoundException, SchemaException, ResourceRegistryException {
		return internalGetType();
	}
	
	public synchronized AccessType getAccessType() throws SchemaNotFoundException, SchemaException, ResourceRegistryException {
		return internalGetAccessType();
	}
	
	public synchronized List<String> getSuperTypes() throws SchemaException, ResourceRegistryException {
		return internalGetSuperTypes();
	}

	public synchronized List<String> getSubTypes() throws SchemaException, ResourceRegistryException {
		return internalGetSubTypes();
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(typeName);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		@SuppressWarnings("unchecked")
		CachedType<T> other = (CachedType<T>) obj;
		return Objects.equals(typeName, other.typeName);
	}
	
}
