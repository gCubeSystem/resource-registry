package org.gcube.informationsystem.resourceregistry;

import org.gcube.common.security.providers.SecretManagerProvider;
import org.gcube.common.security.secrets.Secret;
import org.gcube.smartgears.ApplicationManager;
import org.gcube.smartgears.ContextProvider;
import org.gcube.smartgears.configuration.Mode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * This class is used to Manage the application initialization and shutdown per
 * context; The init and shutdown methods are called one per context in which
 * the app is running respectively at init and a shutdown time. It is connected
 * to the app declaring it via the @ManagedBy annotation (@see ResourceInitializer)
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
public class ResourceRegistryManager implements ApplicationManager {

	private static Logger logger = LoggerFactory.getLogger(ResourceRegistryManager.class);

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onInit() {
		if (ContextProvider.get().container().configuration().mode() == Mode.offline) {
			logger.debug("Resource Registry init called in offline mode");
		} else {
			Secret secret = SecretManagerProvider.get();
			if (secret != null) {
				logger.debug("Resource Registry init called in context {}", secret.getContext());
			} else {
				logger.debug("Resource Registry init called in null context");
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onShutdown() {
		if (ContextProvider.get().container().configuration().mode() == Mode.offline) {
			logger.debug("Resource Registry shutDown called in offline mode");
		} else {
			Secret secret = SecretManagerProvider.get();
			if (secret != null) {
				logger.debug("Resource Registry shutDown called in context {}", secret.getContext());
			} else {
				logger.debug("Resource Registry shutDown called in null context");
			}
		}
	}

}
