package org.gcube.informationsystem.resourceregistry.dbinitialization;

import org.gcube.informationsystem.base.reference.Element;
import org.gcube.informationsystem.discovery.DiscoveredElementAction;
import org.gcube.informationsystem.resourceregistry.api.exceptions.types.SchemaAlreadyPresentException;
import org.gcube.informationsystem.resourceregistry.types.TypeManagement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class CreateTypeAction implements DiscoveredElementAction<Element> {
	
	private static Logger logger = LoggerFactory.getLogger(CreateTypeAction.class);
	
	@Override
	public void analizeElement(Class<Element> e) throws Exception {
		TypeManagement typeManagement = new TypeManagement();
		typeManagement.setTypeAndTypeName(e);
		try {
			typeManagement.create();
		} catch(SchemaAlreadyPresentException sape) {
			logger.warn("{} already exists. It will be ignored", typeManagement.getTypeName());
		} catch(Exception ex) {
			logger.error("Error creating schema for {} {}: {}", typeManagement.getType().getAccessType(), typeManagement.getTypeName(), ex.getMessage());
			throw ex;
		}
	}

}
