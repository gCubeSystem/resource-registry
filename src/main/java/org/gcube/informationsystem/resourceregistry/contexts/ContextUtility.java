package org.gcube.informationsystem.resourceregistry.contexts;

import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.UUID;

import org.gcube.common.security.ContextBean;
import org.gcube.common.security.providers.SecretManagerProvider;
import org.gcube.common.security.secrets.Secret;
import org.gcube.informationsystem.contexts.reference.entities.Context;
import org.gcube.informationsystem.contexts.reference.relations.IsParentOf;
import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.contexts.ContextException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.contexts.ContextNotFoundException;
import org.gcube.informationsystem.resourceregistry.base.ElementManagementUtility;
import org.gcube.informationsystem.resourceregistry.environments.Environment.PermissionMode;
import org.gcube.informationsystem.resourceregistry.environments.administration.AdminEnvironment;
import org.gcube.informationsystem.resourceregistry.environments.instances.InstanceEnvironment;
import org.gcube.informationsystem.resourceregistry.utils.OrientDBUtility;
import org.gcube.informationsystem.resourceregistry.utils.UUIDUtility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.orientechnologies.orient.core.db.ODatabaseRecordThreadLocal;
import com.orientechnologies.orient.core.db.document.ODatabaseDocument;
import com.orientechnologies.orient.core.record.ODirection;
import com.orientechnologies.orient.core.record.OVertex;
import com.orientechnologies.orient.core.sql.executor.OResult;
import com.orientechnologies.orient.core.sql.executor.OResultSet;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class ContextUtility {
	
	private static final Logger logger = LoggerFactory.getLogger(ContextUtility.class);
	
	private Map<UUID,InstanceEnvironment> contexts;
	
	private static ContextUtility contextUtility;
	
	public static ContextUtility getInstance() {
		if(contextUtility == null) {
			contextUtility = new ContextUtility();
		}
		return contextUtility;
	}
	
	private ContextUtility() {
		contexts = new HashMap<>();
	}
	
	public static String getCurrentContextFullName() {
		return SecretManagerProvider.get().getContext();
	}
	
	public static InstanceEnvironment getCurrentRequestEnvironment() throws ResourceRegistryException {
		String fullName = getCurrentContextFullName();
		if(fullName == null) {
			throw new ContextException("Null Token and Scope. Please set your token first.");
		}
		return ContextUtility.getInstance().getRequestEnvironmentByContextFullName(fullName);
	}
	
	public static String getCurrentUserUsername() {
		Secret secret = SecretManagerProvider.get();
		return secret.getOwner().getId();
	}
	
	public synchronized void addInstanceEnvironment(InstanceEnvironment instanceEnvironment) {
		contexts.put(instanceEnvironment.getUUID(), instanceEnvironment);
	}
	
	public synchronized InstanceEnvironment getRequestEnvironmentByContextFullName(String fullName) throws ContextException {
		ODatabaseDocument current = ContextUtility.getCurrentODatabaseDocumentFromThreadLocal();
		ODatabaseDocument oDatabaseDocument = null;
		try {
			InstanceEnvironment instanceEnvironment = null;
			
			logger.trace("Trying to get {} for {}", InstanceEnvironment.class.getSimpleName(), fullName);
			UUID uuid = ServerContextCache.getInstance().getUUIDByFullName(fullName);
			
			if(uuid != null) {
				instanceEnvironment = contexts.get(uuid);
			}
			
			if(instanceEnvironment==null) {
				logger.trace("{} for {} is not in cache. Going to get it", InstanceEnvironment.class.getSimpleName(),
						fullName);
				oDatabaseDocument = AdminEnvironment.getInstance().getDatabaseDocument(PermissionMode.READER);
				
				OVertex contextVertex = getContextVertexByFullName(oDatabaseDocument, fullName);
				
				uuid = UUIDUtility.getUUID(contextVertex);
				
				instanceEnvironment = getEnvironmentByUUID(uuid, contextVertex);
				
				addInstanceEnvironment(instanceEnvironment);
				
			} 
			
			return instanceEnvironment;
			
		} catch(ContextException e) {
			throw e;
		} catch(Exception e) {
			throw new ContextException("Unable to retrieve Context UUID from current Context", e);
		} finally {
			if(oDatabaseDocument!=null) {
				oDatabaseDocument.close();
			}
			
			if(current!=null) {
				current.activateOnCurrentThread();
			}
		}
	}
	
	public InstanceEnvironment getEnvironmentByUUID(UUID uuid) throws ResourceRegistryException {
		return getEnvironmentByUUID(uuid, null);
	}
	
	public static ODatabaseDocument getCurrentODatabaseDocumentFromThreadLocal() {
		ODatabaseDocument current = null;
		try {
			current =  (ODatabaseDocument) ODatabaseRecordThreadLocal.instance().get();
		}catch (Exception e) {
			// It is possible that there is no current ODatabaseDocument
		}
		return current;
	}
	
	private InstanceEnvironment getEnvironmentByUUID(UUID uuid, OVertex contextVertex) throws ResourceRegistryException {
		InstanceEnvironment instanceEnvironment = contexts.get(uuid);
		if(instanceEnvironment == null) {
			
			instanceEnvironment = new InstanceEnvironment(uuid);
			
			ODatabaseDocument current = ContextUtility.getCurrentODatabaseDocumentFromThreadLocal();
			ODatabaseDocument oDatabaseDocument = null;
			
			try {
				if(contextVertex == null) {
					oDatabaseDocument = AdminEnvironment.getInstance().getDatabaseDocument(PermissionMode.READER);
					contextVertex = OrientDBUtility.getElementByUUID(oDatabaseDocument, Context.NAME, uuid, OVertex.class);
				}
				
				OVertex parentVertex = contextVertex.getVertices(ODirection.IN, IsParentOf.NAME).iterator().next();
				
				if(parentVertex != null) {
					UUID parentUUID = UUIDUtility.getUUID(parentVertex);
					instanceEnvironment.setParentEnvironment(getEnvironmentByUUID(parentUUID, parentVertex));
				}
				
			} catch(NoSuchElementException e) {
				// No parent
			} finally {
				if(oDatabaseDocument!=null) {
					oDatabaseDocument.close();
				}
				
				if(current!=null) {
					current.activateOnCurrentThread();
				}
			}
			
			contexts.put(uuid, instanceEnvironment);
		}
		
		return instanceEnvironment;
	}
	
	/*
	protected UUID getContextUUIDFromFullName(String fullName) throws ResourceRegistryException {
		OVertex contextVertex = getContextVertexByFullName(fullName);
		return Utility.getUUID(contextVertex);
	}
	*/
	
	private OVertex getContextVertexByFullName(ODatabaseDocument oDatabaseDocument, String fullName) throws ResourceRegistryException {
		logger.trace("Going to get {} {} with full name '{}'", Context.NAME, OVertex.class.getSimpleName(), fullName);
			
		ContextBean contexBean = new ContextBean(fullName);
		String name = contexBean.name();
		
		// TODO Rewrite better query. This query works because all the scope parts has a different name
		String select = "SELECT FROM " + Context.class.getSimpleName() + " WHERE " + Context.NAME_PROPERTY + " = :name";
		Map<String, String> map = new HashMap<>();
		map.put("name", name);
		
		OResultSet resultSet = oDatabaseDocument.query(select, map);
		
		if(resultSet == null || !resultSet.hasNext()) {
			throw new ContextNotFoundException("Error retrieving context with name " + fullName);
		}
		
		OResult oResult = resultSet.next();
		OVertex context = ElementManagementUtility.getElementFromOptional(oResult.getVertex());
		
		logger.trace("Context Representing Vertex : {}", OrientDBUtility.getAsStringForLogging(context));
		
		if(resultSet.hasNext()) {
			throw new ContextNotFoundException("Found more than one context with name " + name
					+ "but required the one with path" + fullName + ". Please Reimplement the query");
		}
		
		return context;
	}
	
}
