package org.gcube.informationsystem.resourceregistry.contexts.relations;

import java.util.Map;
import java.util.UUID;

import org.gcube.com.fasterxml.jackson.databind.JsonNode;
import org.gcube.com.fasterxml.jackson.databind.node.ObjectNode;
import org.gcube.informationsystem.base.reference.AccessType;
import org.gcube.informationsystem.contexts.reference.entities.Context;
import org.gcube.informationsystem.model.reference.relations.Relation;
import org.gcube.informationsystem.resourceregistry.api.exceptions.AvailableInAnotherContextException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.NotFoundException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.relations.isparentof.IsParentOfAlreadyPresentException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.relations.isparentof.IsParentOfNotFoundException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.types.SchemaViolationException;
import org.gcube.informationsystem.resourceregistry.base.relations.RelationElementManagement;
import org.gcube.informationsystem.resourceregistry.contexts.entities.ContextManagement;
import org.gcube.informationsystem.resourceregistry.environments.Environment;
import org.gcube.informationsystem.resourceregistry.environments.contexts.ContextEnvironment;
import org.gcube.informationsystem.resourceregistry.utils.OrientDBUtility;
import org.gcube.informationsystem.types.reference.entities.EntityType;

import com.orientechnologies.orient.core.db.document.ODatabaseDocument;
import com.orientechnologies.orient.core.record.ODirection;
import com.orientechnologies.orient.core.record.OVertex;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class IsParentOfManagement extends RelationElementManagement<ContextManagement, ContextManagement, EntityType, EntityType> {

	public IsParentOfManagement() {
		super(AccessType.IS_PARENT_OF, Context.class, Context.class);
	}

	public IsParentOfManagement(ODatabaseDocument oDatabaseDocument) throws ResourceRegistryException {
		this();
		this.oDatabaseDocument = oDatabaseDocument;
		getWorkingEnvironment();
		this.includeSource = false;
		this.includeTarget = true;
		this.forceIncludeMeta = true;
		this.forceIncludeAllMeta = true;
	}
	
	@Override
	public Map<UUID,JsonNode> getAffectedInstances() {
		throw new UnsupportedOperationException();
	}

	@Override
	protected Environment getWorkingEnvironment() throws ResourceRegistryException {
		if (workingEnvironment == null) {
			workingEnvironment = ContextEnvironment.getInstance();
		}
		return workingEnvironment;
	}

	@Override
	protected IsParentOfNotFoundException getSpecificNotFoundException(NotFoundException e) {
		return new IsParentOfNotFoundException(e.getMessage(), e.getCause());
	}

	@Override
	protected IsParentOfAlreadyPresentException getSpecificAlreadyPresentException(String message) {
		return new IsParentOfAlreadyPresentException(message);
	}

	@Override
	public JsonNode createCompleteJsonNode()
			throws ResourceRegistryException {
		JsonNode relation = serializeSelfAsJsonNode();

		try {
			OVertex source = element.getVertex(ODirection.OUT);
			ContextManagement sourceContextManagement = new ContextManagement(oDatabaseDocument);
			sourceContextManagement.setElement(source);
			if (includeSource) {
				((ObjectNode) relation).replace(Relation.SOURCE_PROPERTY,
						sourceContextManagement.serializeSelfAsJsonNode());
			}

			OVertex target = element.getVertex(ODirection.IN);
			ContextManagement targetContextManagement = new ContextManagement(oDatabaseDocument);
			targetContextManagement.setElement(target);
			if (includeTarget) {
				((ObjectNode) relation).replace(Relation.TARGET_PROPERTY,
						targetContextManagement.serializeSelfAsJsonNode());
			}

		} catch (ResourceRegistryException e) {
			logger.error("Unable to correctly serialize {}. {}", element, OrientDBUtility.SHOULD_NOT_OCCUR_ERROR_MESSAGE, e);
			throw e;
		} catch (Exception e) {
			logger.error("Unable to correctly serialize {}. {}", element, OrientDBUtility.SHOULD_NOT_OCCUR_ERROR_MESSAGE, e);
			throw new ResourceRegistryException(e);
		}

		return relation;
	}

	@Override
	protected ContextManagement newSourceEntityManagement() throws ResourceRegistryException {
		return new ContextManagement(oDatabaseDocument);
	}

	@Override
	protected ContextManagement newTargetEntityManagement() throws ResourceRegistryException {
		return new ContextManagement(oDatabaseDocument);
	}

	@Override
	public String reallyGetAll(boolean polymorphic) throws ResourceRegistryException {
		throw new UnsupportedOperationException();
	}

	@Override
	public void sanityCheck() throws SchemaViolationException, ResourceRegistryException {
		// Nothing to do
	}

	@Override
	protected void checksourceAndTargetEntityCompliancy() throws NotFoundException, AvailableInAnotherContextException,
			SchemaViolationException, ResourceRegistryException {
		// The compliancy is ensured by the code
	}

}
