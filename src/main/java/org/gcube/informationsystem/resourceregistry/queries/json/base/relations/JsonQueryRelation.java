package org.gcube.informationsystem.resourceregistry.queries.json.base.relations;

import org.gcube.com.fasterxml.jackson.databind.JsonNode;
import org.gcube.informationsystem.base.reference.AccessType;
import org.gcube.informationsystem.model.reference.relations.Relation;
import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.types.SchemaException;
import org.gcube.informationsystem.resourceregistry.queries.json.base.JsonQueryERElement;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public abstract class JsonQueryRelation extends JsonQueryERElement {

	public JsonQueryRelation(JsonNode jsonQuery, AccessType accessType) throws SchemaException, ResourceRegistryException {
		super(jsonQuery, accessType);
		fieldNamesToRemove.add(Relation.TARGET_PROPERTY);
		fieldNamesToRemove.add(Relation.SOURCE_PROPERTY);
	}
	
}
