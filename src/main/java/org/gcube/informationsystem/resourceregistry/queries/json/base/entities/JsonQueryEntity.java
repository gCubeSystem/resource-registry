package org.gcube.informationsystem.resourceregistry.queries.json.base.entities;

import org.gcube.com.fasterxml.jackson.databind.JsonNode;
import org.gcube.informationsystem.base.reference.AccessType;
import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.types.SchemaException;
import org.gcube.informationsystem.resourceregistry.queries.json.base.JsonQueryERElement;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public abstract class JsonQueryEntity extends JsonQueryERElement {

	public JsonQueryEntity(JsonNode jsonQuery, AccessType accessType) throws SchemaException, ResourceRegistryException {
		super(jsonQuery, accessType);
	}
	
}
