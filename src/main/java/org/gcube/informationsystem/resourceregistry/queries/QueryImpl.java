package org.gcube.informationsystem.resourceregistry.queries;

import org.gcube.com.fasterxml.jackson.databind.JsonNode;
import org.gcube.com.fasterxml.jackson.databind.ObjectMapper;
import org.gcube.com.fasterxml.jackson.databind.node.ArrayNode;
import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.queries.InvalidQueryException;
import org.gcube.informationsystem.resourceregistry.base.ElementManagement;
import org.gcube.informationsystem.resourceregistry.base.ElementManagementUtility;
import org.gcube.informationsystem.resourceregistry.contexts.ContextUtility;
import org.gcube.informationsystem.resourceregistry.environments.Environment.PermissionMode;
import org.gcube.informationsystem.resourceregistry.environments.instances.InstanceEnvironment;
import org.gcube.informationsystem.resourceregistry.instances.model.ERManagementUtility;
import org.gcube.informationsystem.resourceregistry.utils.OrientDBUtility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.orientechnologies.orient.core.db.document.ODatabaseDocument;
import com.orientechnologies.orient.core.record.OElement;
import com.orientechnologies.orient.core.sql.executor.OResult;
import com.orientechnologies.orient.core.sql.executor.OResultSet;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class QueryImpl implements Query {
	
	private static Logger logger = LoggerFactory.getLogger(QueryImpl.class);
	
	@Override
	public String query(String query, boolean raw) throws InvalidQueryException {
		ODatabaseDocument oDatabaseDocument = null;
		ODatabaseDocument current = ContextUtility.getCurrentODatabaseDocumentFromThreadLocal();
		
		try {
			InstanceEnvironment instanceEnvironment = ContextUtility.getCurrentRequestEnvironment();
			
			oDatabaseDocument = instanceEnvironment.getDatabaseDocument(PermissionMode.READER);
			oDatabaseDocument.begin();
			
			logger.debug("Going to execute query '{}'", query);
			
			OResultSet resultSet = oDatabaseDocument.query(query);
			
			ObjectMapper objectMapper = new ObjectMapper();
			ArrayNode arrayNode = objectMapper.createArrayNode();
			
			while(resultSet.hasNext()) {
				OResult oResult = resultSet.next();
				
				try {
					JsonNode jsonNode = null;
					if(raw) {
						if(oResult.isElement()) {
							OElement element = ElementManagementUtility.getElementFromOptional(oResult.getElement());
							jsonNode = OrientDBUtility.toJsonNode(element);
						}else {
							ObjectMapper mapper = new ObjectMapper();
							jsonNode = mapper.readTree(OrientDBUtility.toJsonString(oResult));
						}
					} else {
						OElement element = ElementManagementUtility.getElementFromOptional(oResult.getElement());
						ElementManagement<?,?> erManagement = ERManagementUtility.getERManagement(instanceEnvironment, oDatabaseDocument,
								element);
						erManagement.setAsEntryPoint();
						jsonNode = erManagement.serializeAsJsonNode();
					}
					arrayNode.add(jsonNode);
					
				} catch(ResourceRegistryException e) {
					logger.error("Unable to correctly serialize {}. It will be excluded from results. {}",
							OrientDBUtility.toJsonString(oResult), OrientDBUtility.SHOULD_NOT_OCCUR_ERROR_MESSAGE);
				}
			}
			
			return objectMapper.writeValueAsString(arrayNode);
			
		} catch(Exception e) {
			throw new InvalidQueryException(e.getMessage());
		} finally {
			if(oDatabaseDocument != null) {
				oDatabaseDocument.close();
			}
			if(current!=null) {
				current.activateOnCurrentThread();
			}
		}
		
	}
	
}
