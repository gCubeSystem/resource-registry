package org.gcube.informationsystem.resourceregistry.queries.json.base.relations;

import java.util.List;

import org.gcube.com.fasterxml.jackson.databind.JsonNode;
import org.gcube.informationsystem.base.reference.AccessType;
import org.gcube.informationsystem.base.reference.Direction;
import org.gcube.informationsystem.model.reference.relations.ConsistsOf;
import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.types.SchemaException;
import org.gcube.informationsystem.resourceregistry.queries.json.base.JsonQueryERElement;
import org.gcube.informationsystem.resourceregistry.queries.json.base.entities.JsonQueryFacet;
import org.gcube.informationsystem.resourceregistry.queries.json.base.entities.JsonQueryResource;
import org.gcube.informationsystem.resourceregistry.utils.OrientDBUtility;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class JsonQueryConsistsOf extends JsonQueryRelation {

	protected String requestedResourceType;
	
	public JsonQueryConsistsOf(JsonNode jsonQuery) throws SchemaException, ResourceRegistryException {
		super(jsonQuery, AccessType.CONSISTS_OF);
		this.direction = Direction.IN;
	}
	
	public String getRequestedResourceType() {
		return requestedResourceType;
	}

	public void setRequestedResourceType(String requestedResourceType) {
		this.requestedResourceType = requestedResourceType;
	}

	protected StringBuffer traverseBackToCallerResource(StringBuffer stringBuffer) {
		StringBuffer buffer = new StringBuffer();
		buffer.append("TRAVERSE ");
		buffer.append(direction.opposite().name().toLowerCase());
		buffer.append("V(\"");
		buffer.append(requestedResourceType);
		buffer.append("\") FROM ( "); // Open (
		buffer.append(stringBuffer);
		buffer.append(")"); // Close )
		return buffer;
	}
	
	
	public StringBuffer createSelect(StringBuffer stringBuffer, boolean wrapInnerQuery)  throws SchemaException, ResourceRegistryException {
		StringBuffer buffer = new StringBuffer();
		buffer.append("SELECT FROM ");
		
		if(wrapInnerQuery) {
			buffer.append("( ");
			buffer.append(stringBuffer);
			buffer.append(")");
		}else {
			buffer.append(type);
		}
		
		if(entryPoint || size>1) {
			buffer.append(" WHERE ");
		}
		
		if(size > 1) {
			buffer.append(addConstraints(jsonNode, null, null));
			if(entryPoint) {
				buffer.append(" AND ");
			}	
		}
		
		if(entryPoint) {
			buffer.append(OrientDBUtility.ORIENTDB_CLASS_PROPERTY);
			buffer.append(" INSTANCEOF \"");
			buffer.append(type);
			buffer.append("\"");
		}
		
		return buffer;
	}
	
	@Override
	public StringBuffer createTraversalQuery(StringBuffer stringBuffer) throws SchemaException, ResourceRegistryException {
		
		boolean wrapInnerQuery = false;
		
		if(traverseBack) {
			StringBuffer buffer = new StringBuffer();
			buffer.append("TRAVERSE ");
			buffer.append(direction.opposite().name().toLowerCase());		
			buffer.append("E(\"");
			buffer.append(type);
			buffer.append("\") FROM ( ");
			buffer.append(stringBuffer);
			buffer.append(")");
			stringBuffer = buffer;
			wrapInnerQuery = true;
		}
		
		if(jsonNode.has(ConsistsOf.TARGET_PROPERTY)) {
			--size;
			JsonNode facetJsonNode = jsonNode.get(ConsistsOf.TARGET_PROPERTY);
			JsonQueryFacet jsonQueryFacet = new JsonQueryFacet(facetJsonNode);
			jsonQueryFacet.setTraverseBack(!((!traverseBack) && !wrapInnerQuery));
			stringBuffer = jsonQueryFacet.createTraversalQuery(stringBuffer);
			wrapInnerQuery = true;
		}
	
		if(jsonNode.has(ConsistsOf.SOURCE_PROPERTY)) {
			--size;
			JsonNode resourceJsonNode = jsonNode.get(ConsistsOf.SOURCE_PROPERTY);
			JsonQueryResource jsonQueryResource = new JsonQueryResource(resourceJsonNode);
			jsonQueryResource.setTraverseBack(!((!traverseBack) && !wrapInnerQuery));
			stringBuffer = jsonQueryResource.createTraversalQuery(stringBuffer);
			wrapInnerQuery = true;
		}
		
		if(wrapInnerQuery) {
			StringBuffer buffer = new StringBuffer();
			buffer.append("TRAVERSE ");
			buffer.append(direction.name().toLowerCase());
			buffer.append("E(\"");
			buffer.append(type);
			buffer.append("\") FROM ( ");
			buffer.append(stringBuffer);
			buffer.append(")");
			stringBuffer = buffer;
		}
		
		if(entryPoint || size>1) {
			stringBuffer = createSelect(stringBuffer, wrapInnerQuery);
		}
		
		if(!entryPoint && requestedResourceType!=null) {
			stringBuffer = traverseBackToCallerResource(stringBuffer);
		}
		
		return stringBuffer;
	}

	@Override
	protected StringBuffer getSpecificMatchQuery(List<JsonQueryERElement> childrenBreadcrumb)
			throws SchemaException, ResourceRegistryException {
		int childrenPosition = 0;
		
		boolean traverseBack = this.traverseBack;
		
		StringBuffer newBuffer = new StringBuffer();
		if(jsonNode.has(ConsistsOf.TARGET_PROPERTY)) {
			--size;
			JsonNode facetJsonNode = jsonNode.get(ConsistsOf.TARGET_PROPERTY);
			JsonQueryFacet jsonQueryFacet = new JsonQueryFacet(facetJsonNode);
			jsonQueryFacet.setBreadcrumb(childrenBreadcrumb);
			jsonQueryFacet.setPosition(childrenPosition++);
			jsonQueryFacet.setTraverseBack(true);
			Direction direction = Direction.IN;
			jsonQueryFacet.setDirection(direction);
			newBuffer = jsonQueryFacet.createMatchQuery(newBuffer);
			
			newBuffer.append("\n\t");
			newBuffer.append(".");
			newBuffer.append(direction.name().toLowerCase());		
			newBuffer.append("E('");
			newBuffer.append(type);
			newBuffer.append("') ");
			newBuffer.append(" { where: ($matched.");
			newBuffer.append(getAlias(true));
			newBuffer.append(" == $currentMatch)}");
			
			traverseBack = false;
		}
	
		if(jsonNode.has(ConsistsOf.SOURCE_PROPERTY)) {
			--size;
			JsonNode resourceJsonNode = jsonNode.get(ConsistsOf.SOURCE_PROPERTY);
			JsonQueryResource jsonQueryResource = new JsonQueryResource(resourceJsonNode);
			jsonQueryResource.setBreadcrumb(childrenBreadcrumb);
			jsonQueryResource.setPosition(childrenPosition++);
			jsonQueryResource.setTraverseBack(true);
			Direction direction = Direction.OUT;
			jsonQueryResource.setDirection(direction);
			newBuffer = jsonQueryResource.createMatchQuery(newBuffer);
			
			newBuffer.append("\n\t");
			newBuffer.append(".");
			newBuffer.append(direction.name().toLowerCase());		
			newBuffer.append("E('");
			newBuffer.append(type);
			newBuffer.append("') ");
			newBuffer.append(" { where: ($matched.");
			newBuffer.append(getAlias(true));
			newBuffer.append(" == $currentMatch)}");
			
			traverseBack = false;
		}
		
		StringBuffer buffer = new StringBuffer();
		
		if(!entryPoint) {
			buffer.append("\n\t");
			buffer.append(".");
			buffer.append(direction.name().toLowerCase());		
			buffer.append("E('");
			buffer.append(type);
			buffer.append("')");
			
			
			alias = getAlias(true);
			StringBuffer sb = null;
			if(size > 0) {
				sb = addConstraints(jsonNode, null, null);
			}
			
			buffer.append(" {");
			buffer.append(" as: ");
			buffer.append(alias);
			buffer.append(",");
			buffer.append(" where: ");
			if(sb!=null && sb.length()>0) {
				buffer.append("(");
			}
			buffer.append("($currentMatch['@class'] INSTANCEOF '");
			buffer.append(type);
			buffer.append("')");
			
			if(sb!=null && sb.length()>0) {
				buffer.append(" AND (");
				buffer.append(sb);
				buffer.append(")");
				buffer.append(")");
			}
			
			buffer.append("}");
			
				
		}
		
		buffer.append(newBuffer);
		
		if(traverseBack) {
			buffer.append("\n\t");
			buffer.append(".");
			buffer.append(direction.opposite().name().toLowerCase());		
			buffer.append("E('");
			buffer.append(type);
			buffer.append("') ");
			buffer.append(" { where: ($matched.");
			buffer.append(alias);
			buffer.append(" == $currentMatch)}");
		}

		return buffer;
	}
	
}
