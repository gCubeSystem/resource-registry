package org.gcube.informationsystem.resourceregistry.queries.json;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.gcube.com.fasterxml.jackson.databind.JsonNode;
import org.gcube.com.fasterxml.jackson.databind.ObjectMapper;
import org.gcube.com.fasterxml.jackson.databind.node.ArrayNode;
import org.gcube.com.fasterxml.jackson.databind.node.ObjectNode;
import org.gcube.informationsystem.base.reference.AccessType;
import org.gcube.informationsystem.base.reference.Direction;
import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.queries.InvalidQueryException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.types.SchemaException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.types.SchemaNotFoundException;
import org.gcube.informationsystem.resourceregistry.base.ElementManagement;
import org.gcube.informationsystem.resourceregistry.base.ElementManagementUtility;
import org.gcube.informationsystem.resourceregistry.contexts.ContextUtility;
import org.gcube.informationsystem.resourceregistry.environments.Environment.PermissionMode;
import org.gcube.informationsystem.resourceregistry.environments.instances.InstanceEnvironment;
import org.gcube.informationsystem.resourceregistry.instances.model.ERManagementUtility;
import org.gcube.informationsystem.resourceregistry.queries.json.base.JsonQueryERElement;
import org.gcube.informationsystem.resourceregistry.queries.json.base.entities.JsonQueryFacet;
import org.gcube.informationsystem.resourceregistry.queries.json.base.entities.JsonQueryResource;
import org.gcube.informationsystem.resourceregistry.queries.json.base.relations.JsonQueryConsistsOf;
import org.gcube.informationsystem.resourceregistry.queries.json.base.relations.JsonQueryIsRelatedTo;
import org.gcube.informationsystem.resourceregistry.rest.requests.RequestUtility;
import org.gcube.informationsystem.resourceregistry.rest.requests.ServerRequestInfo;
import org.gcube.informationsystem.resourceregistry.types.TypesCache;
import org.gcube.informationsystem.resourceregistry.utils.OrientDBUtility;
import org.gcube.informationsystem.utils.TypeUtility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.orientechnologies.orient.core.db.document.ODatabaseDocument;
import com.orientechnologies.orient.core.record.OElement;
import com.orientechnologies.orient.core.sql.executor.OResult;
import com.orientechnologies.orient.core.sql.executor.OResultSet;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class JsonQuery {

	private static Logger logger = LoggerFactory.getLogger(JsonQuery.class);
	
	protected ObjectMapper objectMapper;
	protected JsonNode jsonQuery;
	protected JsonQueryERElement entryPoint;
	protected ODatabaseDocument oDatabaseDocument;
	
	public JsonQuery() {
		this.objectMapper = new ObjectMapper();
	}
	
	public void setJsonQuery(JsonNode jsonQuery) {
		this.jsonQuery = jsonQuery;
	}
	
	public void setJsonQuery(String jsonQuery) throws InvalidQueryException {
		try {
			this.jsonQuery = objectMapper.readTree(jsonQuery);
		} catch (IOException e) {
			throw new InvalidQueryException(e);
		}
		
	}
	
	public static JsonQueryERElement getJsonQueryERElement(JsonNode jsonQuery) throws SchemaNotFoundException, SchemaException, ResourceRegistryException {
		String type = TypeUtility.getTypeName(jsonQuery);

		AccessType accessType = TypesCache.getInstance().getCachedType(type).getAccessType();
		
		JsonQueryERElement jsonQueryERElement = null;
		
		switch (accessType) {
			case RESOURCE:
				jsonQueryERElement = new JsonQueryResource(jsonQuery);
				jsonQueryERElement.setDirection(Direction.OUT);
				break;
	
			case FACET:
				jsonQueryERElement = new JsonQueryFacet(jsonQuery);
				break;
			
			case IS_RELATED_TO:
				jsonQueryERElement = new JsonQueryIsRelatedTo(jsonQuery);
				break;
				
			case CONSISTS_OF:
				jsonQueryERElement = new JsonQueryConsistsOf(jsonQuery);
				break;
			
			default:
				throw new InvalidQueryException("%s is not querable".formatted(type.toString()));
		}
		
		return jsonQueryERElement;
	}
	
	public StringBuffer createQuery() throws SchemaException, InvalidQueryException, ResourceRegistryException {
		entryPoint = getJsonQueryERElement(jsonQuery);
		entryPoint.setEntryPoint(true);
		return entryPoint.createQuery(new StringBuffer());
	}
	
	
	public StringBuffer createMatchQuery() throws SchemaException, InvalidQueryException, ResourceRegistryException {
		entryPoint = getJsonQueryERElement(jsonQuery);
		entryPoint.setEntryPoint(true);
		return entryPoint.createMatchQuery(new StringBuffer());
	}
	
	public String query() throws InvalidQueryException, ResourceRegistryException {
		ODatabaseDocument current = ContextUtility.getCurrentODatabaseDocumentFromThreadLocal();
		oDatabaseDocument = null;
		try {
			InstanceEnvironment instanceEnvironment = ContextUtility.getCurrentRequestEnvironment();
			
			oDatabaseDocument = instanceEnvironment.getDatabaseDocument(PermissionMode.READER);
			oDatabaseDocument.begin();
			
			ServerRequestInfo requestInfo = RequestUtility.getRequestInfo().get();
			Integer limit = requestInfo.getLimit();
			if(limit==null) {
				limit = -1;
			}
			Integer offset = requestInfo.getOffset();
			if(offset == null) {
				offset = 0;
			}
			
			StringBuffer stringBuffer = createQuery();
			stringBuffer.append(" SKIP :offset");
			stringBuffer.append(" LIMIT :limit");
			
			Map<String, Object> map = new HashMap<>();
			map.put("offset", offset);
			map.put("limit", limit);
			
			
			String query =  stringBuffer.toString();
			logger.trace("Going to execute the following query:\n{} \n from the JSONQuery\n{}", query, objectMapper.writeValueAsString(jsonQuery));
			
			OResultSet resultSet = oDatabaseDocument.query(query, map);
			
			ArrayNode arrayNode = objectMapper.createArrayNode();
			
			boolean projection = entryPoint.isProjection();
			boolean first = true;
			
			Set<String> keys = new HashSet<>();
			
			while(resultSet.hasNext()) {
				OResult oResult = resultSet.next();
				
				if(projection) {
					if(first) {
						keys = oResult.getPropertyNames();
						first = false;
					}
					ObjectNode objectNode = objectMapper.createObjectNode();
					
					for(String key : keys) {
						Object value = oResult.getProperty(key);
						
						if(value == null) {
							objectNode.put(key, "");
						}else if(value instanceof String string) {
							objectNode.put(key, string);
						}else if(value instanceof Integer integer) {
							objectNode.put(key, integer);
						}else if(value instanceof Long long1) {
							objectNode.put(key, long1);
						}else {
							objectNode.put(key, value.toString());
						}
						
					}
					arrayNode.add(objectNode);
				}else {
					OElement element = ElementManagementUtility.getElementFromOptional(oResult.getElement());
				
					try {
						JsonNode jsonNodeResult = null;
						ElementManagement<?,?> erManagement = ERManagementUtility.getERManagement(instanceEnvironment, oDatabaseDocument,
									element);
						erManagement.setAsEntryPoint();
						jsonNodeResult = erManagement.serializeAsJsonNode();
						arrayNode.add(jsonNodeResult);
					} catch(ResourceRegistryException e) {
						logger.error("Unable to correctly serialize {}. It will be excluded from results. {}",
								element.toString(), OrientDBUtility.SHOULD_NOT_OCCUR_ERROR_MESSAGE);
					}
				}
			}
			
			return objectMapper.writeValueAsString(arrayNode);
			
		} catch(Exception e) {
			throw new InvalidQueryException(e.getMessage());
		} finally {
			if(oDatabaseDocument != null) {
				oDatabaseDocument.close();
			}
			if(current!=null) {
				current.activateOnCurrentThread();
			}
		}
	}

}
