package org.gcube.informationsystem.resourceregistry.queries.operators;

/**
 * @author Luca Frosini (ISTI - CNR)
 * 
 * Methods: also called "Field Operators"
 * 
 * SQL Methods are similar to SQL functions but they apply to values. 
 * In Object Oriented paradigm they are called "methods", as functions related to a class. 
 * So what's the difference between a function and a method?
 * 
 * This is a SQL function:
 * 
 * SELECT sum( salary ) FROM employee
 * 
 * This is a SQL method:
 * 
 * SELECT salary.toJSON() FROM employee
 * 
 * As you can see the method is executed against a field/value. 
 * Methods can receive parameters, like functions. 
 * You can concatenate N operators in sequence.
 * 
 * See https://orientdb.com/docs/3.0.x/sql/SQL-Where.html#methods
 *     http://orientdb.com/docs/3.0.x/sql/SQL-Methods.html
 */
public enum FieldOperator {

}
