package org.gcube.informationsystem.resourceregistry.queries;

import org.gcube.informationsystem.resourceregistry.api.exceptions.queries.InvalidQueryException;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface Query {
	
	public String query(String query, boolean raw) throws InvalidQueryException;

}
