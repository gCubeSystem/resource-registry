package org.gcube.informationsystem.resourceregistry.queries.operators;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author Luca Frosini (ISTI - CNR)
 * See https://www.orientdb.com/docs/3.0.x/sql/SQL-Where.html
 */
public enum ProjectionOperator {

	EMIT("_emit", "");
	
	protected final String operator;
	protected final String description;
	
	private ProjectionOperator(String operator, String description) {
		this.operator = operator;
		this.description = description;
	}
	
	public String getOperator() {
		return operator;
	}
	
	public String getDescription() {
		return description;
	}

	private static Set<String> operators;
	private static Map<String,ProjectionOperator> operatorByKey;
	
	static {
		ProjectionOperator.operators = new HashSet<>();
		ProjectionOperator.operatorByKey = new HashMap<>();
		
		for(ProjectionOperator po : ProjectionOperator.values()) {
			ProjectionOperator.operators.add(po.getOperator());
			ProjectionOperator.operatorByKey.put(po.getOperator(), po);
		}
	}
	
	public static Set<String> getOperators() {
		return ProjectionOperator.operators;
	}
	
	public static ProjectionOperator getQueryLogicalOperator(String key) {
		return operatorByKey.get(key);
	}
	
}