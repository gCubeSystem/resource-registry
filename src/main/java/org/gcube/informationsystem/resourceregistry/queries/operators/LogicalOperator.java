package org.gcube.informationsystem.resourceregistry.queries.operators;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author Luca Frosini (ISTI - CNR)
 * See https://www.orientdb.com/docs/3.0.x/sql/SQL-Where.html
 */
public enum LogicalOperator {

	AND("_and", " AND ", "true if both the conditions are true"),
	OR("_or", " OR ", "true if at least one of the condition is true"),
	NOT("_not", " NOT ", "true if the condition is false.");
	
	protected final String operatorKey;
	protected final String dbOperator;
	protected final String description;
	
	private LogicalOperator(String operatorKey, String dbOperator, String description) {
		this.operatorKey = operatorKey;
		this.dbOperator = dbOperator;
		this.description = description;
	}
	
	public String getOperatorKey() {
		return operatorKey;
	}
	
	public String getDbOperator() {
		return dbOperator;
	}

	public String getDescription() {
		return description;
	}

	private static Set<String> operators;
	private static Map<String,LogicalOperator> operatorByKey;
	
	static {
		LogicalOperator.operators = new HashSet<>();
		LogicalOperator.operatorByKey = new HashMap<>();
		
		for(LogicalOperator logicalOperator : LogicalOperator.values()) {
			LogicalOperator.operators.add(logicalOperator.getOperatorKey());
			LogicalOperator.operatorByKey.put(logicalOperator.getOperatorKey(), logicalOperator);
		}
	}
	
	public static Set<String> getOperators() {
		return LogicalOperator.operators;
	}
	
	public static LogicalOperator getOperator(String key) {
		return operatorByKey.get(key);
	}
}
