package org.gcube.informationsystem.resourceregistry.queries.json.base.entities;

import java.util.List;

import org.gcube.com.fasterxml.jackson.databind.JsonNode;
import org.gcube.com.fasterxml.jackson.databind.node.ArrayNode;
import org.gcube.informationsystem.base.reference.AccessType;
import org.gcube.informationsystem.base.reference.Direction;
import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.types.SchemaException;
import org.gcube.informationsystem.resourceregistry.queries.json.base.JsonQueryERElement;
import org.gcube.informationsystem.resourceregistry.queries.json.base.relations.JsonQueryConsistsOf;
import org.gcube.informationsystem.resourceregistry.queries.json.base.relations.JsonQueryIsRelatedTo;
import org.gcube.informationsystem.resourceregistry.utils.OrientDBUtility;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class JsonQueryResource extends JsonQueryEntity {
	
	public JsonQueryResource(JsonNode jsonQuery) throws SchemaException, ResourceRegistryException {
		super(jsonQuery, AccessType.RESOURCE);
		this.fieldNamesToRemove.add(Resource.CONSISTS_OF_PROPERTY);
		this.fieldNamesToRemove.add(Resource.IS_RELATED_TO_PROPERTY);
	}
	
	public StringBuffer createSelect(StringBuffer stringBuffer, boolean wrapInnerQuery)  throws SchemaException, ResourceRegistryException {
		StringBuffer buffer = new StringBuffer();
		buffer.append("SELECT FROM ");
		
		if(wrapInnerQuery) {
			buffer.append("( ");
			buffer.append(stringBuffer);
			buffer.append(")");
		}else {
			buffer.append(type);
		}
		
		if(entryPoint || size>1) {
			buffer.append(" WHERE ");
		}
		
		if(size > 1) {
			buffer.append(addConstraints(jsonNode, null, null));
			if(entryPoint) {
				buffer.append(" AND ");
			}	
		}
		
		if(entryPoint) {
			buffer.append(OrientDBUtility.ORIENTDB_CLASS_PROPERTY);
			buffer.append(" INSTANCEOF \"");
			buffer.append(type);
			buffer.append("\"");
		}
		
		return buffer;
	}
	
	@Override
	public StringBuffer createTraversalQuery(StringBuffer stringBuffer) throws SchemaException, ResourceRegistryException {
		
		boolean wrapInnerQuery = false;
		
		if(traverseBack) {
			StringBuffer buffer = new StringBuffer();
			buffer.append("TRAVERSE ");
			buffer.append(direction.name().toLowerCase());
			buffer.append("V(\"");
			buffer.append(type);
			buffer.append("\") FROM ( ");
			buffer.append(stringBuffer);
			buffer.append(")");
			stringBuffer = buffer;
			
			wrapInnerQuery = true;
		}
		
		ArrayNode isRelatedToArray = (ArrayNode) jsonNode.get(Resource.IS_RELATED_TO_PROPERTY);
		if(isRelatedToArray!=null && isRelatedToArray.size()>0) {
			--size;
			for(int i=0; i<isRelatedToArray.size(); i++) {
				JsonNode isRelatedToJsonNode = isRelatedToArray.get(i);
				JsonQueryIsRelatedTo jsonQueryIsRelatedTo = new JsonQueryIsRelatedTo(isRelatedToJsonNode);
				jsonQueryIsRelatedTo.setRequestedResourceType(type);
				jsonQueryIsRelatedTo.setDirectionByJson();
				jsonQueryIsRelatedTo.setTraverseBack( (!(!traverseBack) && i==0) );
				stringBuffer = jsonQueryIsRelatedTo.createTraversalQuery(stringBuffer);
			}
			wrapInnerQuery = true;
		}
		
		ArrayNode consistsOfArray = (ArrayNode) jsonNode.get(Resource.CONSISTS_OF_PROPERTY);
		if(consistsOfArray!=null && consistsOfArray.size()>0) {
			--size;
			for(int i=0; i<consistsOfArray.size(); i++) {
				JsonNode consistsOfJsonNode = consistsOfArray.get(i);
				JsonQueryConsistsOf jsonQueryConsistsOf = new JsonQueryConsistsOf(consistsOfJsonNode);
				jsonQueryConsistsOf.setRequestedResourceType(type);
				jsonQueryConsistsOf.setDirection(Direction.IN);
				jsonQueryConsistsOf.setTraverseBack(!((!traverseBack) && !wrapInnerQuery && i==0));
				stringBuffer = jsonQueryConsistsOf.createTraversalQuery(stringBuffer);
			}
			wrapInnerQuery = true; // Must be set after the cycle and not before
		}
		
		// The Resource has no other referenced ER inside
		if(!wrapInnerQuery) {
			return createSelect(stringBuffer, wrapInnerQuery);
		}
		
		if(entryPoint || size>1) {
			return createSelect(stringBuffer, wrapInnerQuery);
		}
		
		return stringBuffer;
	}
	
	@Override
	protected StringBuffer getSpecificMatchQuery(List<JsonQueryERElement> childrenBreadcrumb) throws SchemaException, ResourceRegistryException {
		
		StringBuffer newBuffer = new StringBuffer();
		
		int isRelatedToSize = 0;
		
		ArrayNode consistsOfArray = (ArrayNode) jsonNode.get(Resource.CONSISTS_OF_PROPERTY);
		int consistsOfSize = 0;
		if(consistsOfArray!=null) {
			consistsOfSize = consistsOfArray.size();
		}
		
		int total = consistsOfSize;
		
		ArrayNode isRelatedToArray = (ArrayNode) jsonNode.get(Resource.IS_RELATED_TO_PROPERTY);
		if(isRelatedToArray!=null && isRelatedToArray.size()>0) {
			--size;
			isRelatedToSize = isRelatedToArray.size();
			total += isRelatedToSize;
			
			for(int i=0; i<isRelatedToSize; i++) {
				JsonNode isRelatedToJsonNode = isRelatedToArray.get(i);
				JsonQueryIsRelatedTo jsonQueryIsRelatedTo = new JsonQueryIsRelatedTo(isRelatedToJsonNode);
				jsonQueryIsRelatedTo.setRequestedResourceType(type);
				jsonQueryIsRelatedTo.setDirectionByJson(true);
				jsonQueryIsRelatedTo.setBreadcrumb(childrenBreadcrumb);
				jsonQueryIsRelatedTo.setPosition(i);
				
				boolean traverseBack = true;
//				boolean traverseBack = false;
//				if(i<(isRelatedToSize-1) || consistsOfSize>0) {
//					traverseBack = true;
//				}
				jsonQueryIsRelatedTo.setTraverseBack(traverseBack);
				
				newBuffer = jsonQueryIsRelatedTo.createMatchQuery(newBuffer);

				if(traverseBack) {
					newBuffer.append("\n\t.");
					newBuffer.append(jsonQueryIsRelatedTo.getDirection().name().toLowerCase());
					newBuffer.append("V('");
					newBuffer.append(type);
					newBuffer.append("') ");
					newBuffer.append("{ where: ($matched.");
					newBuffer.append(getAlias(true));
					newBuffer.append(" == $currentMatch)}");
					
					if(entryPoint && i<(total-1)) {
						newBuffer.append("\n");
					}
				}
			}
		}
		
		if(consistsOfSize>0) {
			--size;
			for(int i=0; i<consistsOfArray.size(); i++) {
				JsonNode consistsOfJsonNode = consistsOfArray.get(i);
				JsonQueryConsistsOf jsonQueryConsistsOf = new JsonQueryConsistsOf(consistsOfJsonNode);
				jsonQueryConsistsOf.setRequestedResourceType(type);
				jsonQueryConsistsOf.setDirection(Direction.OUT);
				jsonQueryConsistsOf.setBreadcrumb(childrenBreadcrumb);
				jsonQueryConsistsOf.setPosition(isRelatedToSize+i);
				
				boolean traverseBack = true;
				jsonQueryConsistsOf.setTraverseBack(traverseBack);
				
				newBuffer = jsonQueryConsistsOf.createMatchQuery(newBuffer);
				
				if(traverseBack) {
					newBuffer.append("\n\t.");
					newBuffer.append(jsonQueryConsistsOf.getDirection().name().toLowerCase());
					newBuffer.append("V('");
					newBuffer.append(type);
					newBuffer.append("')");
					newBuffer.append(" { where: ($matched.");
					newBuffer.append(getAlias(true));
					newBuffer.append(" == $currentMatch)}");
					if(entryPoint && i<(consistsOfSize-1)) {
						newBuffer.append("\n");
					}
				}
			}
		}
		
		StringBuffer buffer = new StringBuffer();
		
		if(!entryPoint) {
			buffer.append("\n\t");
			buffer.append(".");
			buffer.append(direction.name().toLowerCase());
			buffer.append("V('");
			buffer.append(type);
			buffer.append("')");
			
			alias = getAlias(true);
			StringBuffer sb = null;
			if(size > 0) {
				sb = addConstraints(jsonNode, null, null);
			}
			
			buffer.append(" {");
			buffer.append(" as: ");
			buffer.append(alias);
			buffer.append(",");
			buffer.append(" where: ");
			if(sb!=null && sb.length()>0) {
				buffer.append("(");
			}
			buffer.append("($currentMatch['@class'] INSTANCEOF '");
			buffer.append(type);
			buffer.append("')");
			
			if(sb!=null && sb.length()>0) {
				buffer.append(" AND (");
				buffer.append(sb);
				buffer.append(")");
				buffer.append(")");
			}
			
			buffer.append("}");
		}
		
		buffer.append(newBuffer);
		if(entryPoint) {
			buffer.append("\n");
		}
		
		return buffer;
	}
	
}
