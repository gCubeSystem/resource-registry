package org.gcube.informationsystem.resourceregistry.queries.json.base.entities;

import java.util.List;

import org.gcube.com.fasterxml.jackson.databind.JsonNode;
import org.gcube.informationsystem.base.reference.AccessType;
import org.gcube.informationsystem.base.reference.Direction;
import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.queries.InvalidQueryException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.types.SchemaException;
import org.gcube.informationsystem.resourceregistry.queries.json.base.JsonQueryERElement;
import org.gcube.informationsystem.resourceregistry.queries.json.base.relations.JsonQueryConsistsOf;
import org.gcube.informationsystem.resourceregistry.utils.OrientDBUtility;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class JsonQueryFacet extends JsonQueryEntity {

	public final static String _SOURCE = "_source";
	
	public JsonQueryFacet(JsonNode jsonQuery) throws SchemaException, ResourceRegistryException {
		super(jsonQuery, AccessType.FACET);
		fieldNamesToRemove.add(JsonQueryFacet._SOURCE);
	}
	
	@Override
	public StringBuffer createTraversalQuery(StringBuffer stringBuffer) throws SchemaException, ResourceRegistryException {
		StringBuffer newBuffer = new StringBuffer();
		
		int size = jsonNode.size();
		
		boolean traverseBackLocal = traverseBack;
		if(jsonNode.has(_SOURCE)) {
			if(!entryPoint) {
				throw new InvalidQueryException(_SOURCE + " property cannot be used in a facet if it is not the entry object");
			}
			JsonNode consistsOfNode = jsonNode.get(_SOURCE);
			JsonQueryConsistsOf jsonQueryConsistsOf = new JsonQueryConsistsOf(consistsOfNode);
			jsonQueryConsistsOf.setTraverseBack(traverseBackLocal);
			jsonQueryConsistsOf.setDirection(Direction.OUT);
			stringBuffer = jsonQueryConsistsOf.createTraversalQuery(stringBuffer);
			traverseBackLocal = true;
			
			/* Need to substract 1 from size otherwise
			 * it add WHERE at the end because _in 
			 * is not a property to be used for a WHERE compare
			 */
			--size;
		}
		
		newBuffer.append("SELECT FROM ");
		
		if(traverseBackLocal) {
			newBuffer.append("( ");
			newBuffer.append("TRAVERSE inV(\"");
		}
		
		newBuffer.append(type);
		
		if(traverseBackLocal) {
			newBuffer.append("\") FROM ( ");
			newBuffer.append(stringBuffer);
			newBuffer.append(")");
			newBuffer.append(")");
		}
		
		/*
		 * If size >1 I have to add constraints.
		 * If is an entry point I have to add the INSTANCEOF to properly support polymorphism
		 */
		if(size > 1 || entryPoint) {
			newBuffer.append(" WHERE ");
		}
		
		// Size 1 means that only 'type' property is present
		if(size > 1) {
			newBuffer.append(addConstraints(jsonNode, null, null));
			if(entryPoint) {
				newBuffer.append(" AND ");
			}
		}
		
		if(entryPoint) {
			newBuffer.append(OrientDBUtility.ORIENTDB_CLASS_PROPERTY);
			newBuffer.append(" INSTANCEOF \"");
			newBuffer.append(type);
			newBuffer.append("\"");
		}
		
		return newBuffer;
	}

	@Override
	protected StringBuffer getSpecificMatchQuery(List<JsonQueryERElement> childrenBreadcrumb)
			throws SchemaException, ResourceRegistryException {
		StringBuffer newBuffer = new StringBuffer();
		
		boolean traverseBack = this.traverseBack;
		if(jsonNode.has(_SOURCE)) {
			if(!entryPoint) {
				throw new InvalidQueryException(_SOURCE + " property cannot be used in a facet if it is not the entry object");
			}
			traverseBack = true;
			
			JsonNode consistsOfNode = jsonNode.get(_SOURCE);
			JsonQueryConsistsOf jsonQueryConsistsOf = new JsonQueryConsistsOf(consistsOfNode);
			jsonQueryConsistsOf.setTraverseBack(traverseBack);
			jsonQueryConsistsOf.setDirection(Direction.IN);
			jsonQueryConsistsOf.setBreadcrumb(childrenBreadcrumb);
			newBuffer = jsonQueryConsistsOf.createMatchQuery(newBuffer);
			
			newBuffer.append("\n\t");
			newBuffer.append(".inV('");
			newBuffer.append(type);
			newBuffer.append("')");
			newBuffer.append(" { where: ($matched.");
			newBuffer.append(alias);
			newBuffer.append(" == $currentMatch)}");
			
			traverseBack = false;
			
			/* Need to substract 1 from size otherwise
			 * it add WHERE at the end because _in 
			 * is not a property to be used for a WHERE compare
			 */
			--size;
		}
		
		StringBuffer buffer = new StringBuffer();
		
		if(!entryPoint) {
			buffer.append("\n\t");		
			buffer.append(".inV('");
			buffer.append(type);
			buffer.append("')");
			
			
			alias = getAlias(true);
			StringBuffer sb = null;
			if(size > 0) {
				sb = addConstraints(jsonNode, null, null);
			}
			
			buffer.append(" {");
			buffer.append(" as: ");
			buffer.append(alias);
			buffer.append(",");
			buffer.append(" where: ");
			if(sb!=null && sb.length()>0) {
				buffer.append("(");
			}
			buffer.append("($currentMatch['@class'] INSTANCEOF '");
			buffer.append(type);
			buffer.append("')");
			
			if(sb!=null && sb.length()>0) {
				buffer.append(" AND (");
				buffer.append(sb);
				buffer.append(")");
				buffer.append(")");
			}
			
			buffer.append("}");
				
		}
		
		buffer.append(newBuffer);
		
//		if(traverseBack) {
//			buffer.append("\n\t");
//			buffer.append(".inV('");
//			buffer.append(type);
//			buffer.append("')");
//			buffer.append(" { where: ($matched.");
//			buffer.append(alias);
//			buffer.append(" == $currentMatch)}");
//		}
		
		return buffer;
		
	}
	
}
