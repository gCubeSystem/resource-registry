package org.gcube.informationsystem.resourceregistry.queries.templates;

import java.util.HashMap;
import java.util.UUID;

import org.gcube.com.fasterxml.jackson.core.JsonProcessingException;
import org.gcube.com.fasterxml.jackson.databind.JsonNode;
import org.gcube.com.fasterxml.jackson.databind.ObjectMapper;
import org.gcube.com.fasterxml.jackson.databind.node.ArrayNode;
import org.gcube.com.fasterxml.jackson.databind.node.ObjectNode;
import org.gcube.informationsystem.base.reference.AccessType;
import org.gcube.informationsystem.base.reference.IdentifiableElement;
import org.gcube.informationsystem.queries.templates.reference.entities.QueryTemplate;
import org.gcube.informationsystem.resourceregistry.api.exceptions.AlreadyPresentException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.AvailableInAnotherContextException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.NotFoundException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.queries.InvalidQueryException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.queries.templates.QueryTemplateAlreadyPresentException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.queries.templates.QueryTemplateNotFoundException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.types.SchemaViolationException;
import org.gcube.informationsystem.resourceregistry.base.ElementManagementUtility;
import org.gcube.informationsystem.resourceregistry.base.entities.EntityElementManagement;
import org.gcube.informationsystem.resourceregistry.contexts.ContextUtility;
import org.gcube.informationsystem.resourceregistry.environments.Environment;
import org.gcube.informationsystem.resourceregistry.environments.Environment.PermissionMode;
import org.gcube.informationsystem.resourceregistry.environments.queries.templates.QueryTemplateEnvironment;
import org.gcube.informationsystem.resourceregistry.instances.model.Operation;
import org.gcube.informationsystem.resourceregistry.queries.json.JsonQuery;
import org.gcube.informationsystem.resourceregistry.rest.requests.RequestUtility;
import org.gcube.informationsystem.resourceregistry.rest.requests.ServerRequestInfo;
import org.gcube.informationsystem.resourceregistry.utils.OrientDBUtility;
import org.gcube.informationsystem.serialization.ElementMapper;
import org.gcube.informationsystem.types.reference.entities.EntityType;
import org.gcube.smartgears.utils.InnerMethodName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.orientechnologies.orient.core.db.document.ODatabaseDocument;
import com.orientechnologies.orient.core.record.OVertex;
import com.orientechnologies.orient.core.record.impl.ODocument;
import com.orientechnologies.orient.core.sql.executor.OResult;
import com.orientechnologies.orient.core.sql.executor.OResultSet;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class QueryTemplateManagement extends EntityElementManagement<QueryTemplate, EntityType> {

	private static Logger logger = LoggerFactory.getLogger(QueryTemplateManagement.class);

	protected String name;
	protected JsonNode params;

	public QueryTemplateManagement() {
		super(AccessType.QUERY_TEMPLATE);
		this.typeName = QueryTemplate.NAME;
	}

	public QueryTemplateManagement(ODatabaseDocument oDatabaseDocument) throws ResourceRegistryException {
		this();
		this.oDatabaseDocument = oDatabaseDocument;
		getWorkingEnvironment();
	}

	protected void checkERMatch() throws ResourceRegistryException {
		getOClass();
	}
	
	protected void checkNameMatch() throws ResourceRegistryException {
		if(jsonNode!=null && name!=null) {
			String jsonName = jsonNode.get(QueryTemplate.NAME_PROPERTY).asText();
			if(name.compareTo(jsonName)!=0) {
				String error = "Name provided in json (%s) differs from the one (%s) used to identify the %s instance".formatted(
						jsonName, name, typeName);
				throw new ResourceRegistryException(error);
			}
		}
	}
	
	public void setName(String name) throws ResourceRegistryException {
		this.name = name;
		checkNameMatch();
	}
	
	protected void checkJsonNode() throws ResourceRegistryException {
		super.checkJsonNode();
		checkNameMatch();
	}
	
	public String getName() {
		if (name == null) {
			if (element == null) {
				if (jsonNode != null) {
					name = jsonNode.get(QueryTemplate.NAME_PROPERTY).asText();
				}
			} else {
				name = element.getProperty(QueryTemplate.NAME_PROPERTY);
			}
		}
		return name;
	}
	
	@Override
	protected Environment getWorkingEnvironment() throws ResourceRegistryException {
		if (workingEnvironment == null) {
			workingEnvironment = QueryTemplateEnvironment.getInstance();
		}
		return workingEnvironment;
	}
	
	@Override
	protected JsonNode createCompleteJsonNode() throws ResourceRegistryException {
		try {
			JsonNode queryTemplate = serializeSelfAsJsonNode();
			ObjectMapper objectMapper = new ObjectMapper();
			String templateString = element.getProperty(QueryTemplate.TEMPLATE_PROPERTY);
			JsonNode templateJsonNode = objectMapper.readTree(templateString);
			((ObjectNode) queryTemplate).replace(QueryTemplate.TEMPLATE_PROPERTY, templateJsonNode);
			return queryTemplate;
		}catch (ResourceRegistryException e) {
			throw e;
		}catch (Exception e) {
			throw new ResourceRegistryException(e);
		}
		
	}
	
	protected StringBuffer getSelectQuery() {
		StringBuffer select = new StringBuffer();
		select.append("SELECT FROM ");
		select.append(QueryTemplate.NAME);
		select.append(" WHERE ");
		select.append(QueryTemplate.NAME_PROPERTY);
		select.append(" = ");
		select.append("\"");
		select.append(getName());
		select.append("\"");
		return select;
	}
	
	protected void checkIfNameAlreadyExists() throws QueryTemplateAlreadyPresentException {
		StringBuffer select = getSelectQuery();
		
		StringBuffer errorMessage = new StringBuffer();
		errorMessage.append("A ");
		errorMessage.append(QueryTemplate.NAME);
		errorMessage.append(" with ");
		errorMessage.append(this.getName());
		errorMessage.append(" already exists");
		
		logger.trace("Checking if {} -> {}", errorMessage, select);
		
		OResultSet resultSet = oDatabaseDocument.command(select.toString(), new HashMap<>());

		if (resultSet != null) {
			try {
				if(resultSet.hasNext()) {
					OResult oResult = resultSet.next();
					try {
						element = ElementManagementUtility.getElementFromOptional(oResult.getVertex());
					} catch (ResourceRegistryException e) {
						
					}
					throw new QueryTemplateAlreadyPresentException(errorMessage.toString());
				}
			}finally {
				resultSet.close();
			}
		}
		
	}

	protected void tryTemplate() throws Exception {
		QueryTemplate queryTemplate = ElementMapper.unmarshal(QueryTemplate.class, jsonNode.toString());
		JsonNode jsonQueryNode = queryTemplate.getJsonQuery();
		JsonQuery jsonQuery = new JsonQuery();
		jsonQuery.setJsonQuery(jsonQueryNode);
		try {
			jsonQuery.query();
		}catch (ResourceRegistryException e) {
			throw e;
		}catch (Exception e) {
			throw new ResourceRegistryException(e);
		}
	}
	
	@Override
	public OVertex retrieveElement() throws NotFoundException, ResourceRegistryException {
		try {
			StringBuffer select = getSelectQuery();
			OResultSet resultSet = oDatabaseDocument.query(select.toString(), new HashMap<>());
			
			if(resultSet == null || !resultSet.hasNext()) {
				if(resultSet!=null) {
					resultSet.close();
				}
				throw new QueryTemplateNotFoundException("Error retrieving " + QueryTemplate.NAME + " with name " + getName());
			}
			
			OResult oResult = resultSet.next();
			OVertex queryTemplate = ElementManagementUtility.getElementFromOptional(oResult.getVertex());
			
			logger.trace("{} representing vertex is {}", QueryTemplate.NAME, OrientDBUtility.getAsStringForLogging(queryTemplate));
			
			if(resultSet.hasNext()) {
				resultSet.close();
				throw new NotFoundException("Found more than one " + QueryTemplate.NAME + " with name " + name + ". This should not occur, please contact the administrator");
			}
			
			resultSet.close();
			return queryTemplate;
		} catch(NotFoundException e) {
			throw getSpecificNotFoundException(e);
		} catch(ResourceRegistryException e) {
			throw e;
		} catch(Exception e) {
			throw new ResourceRegistryException(e);
		}
	}
	
	@Override
	public String createOrUpdate()
			throws NotFoundException, AvailableInAnotherContextException, ResourceRegistryException {
		ODatabaseDocument current = ContextUtility.getCurrentODatabaseDocumentFromThreadLocal();
		try {
			oDatabaseDocument = getWorkingEnvironment().getDatabaseDocument(PermissionMode.WRITER);
			oDatabaseDocument.begin();
			boolean update = false;
			setAsEntryPoint();
			try {
				checkIfNameAlreadyExists();
				setOperation(Operation.CREATE);
				String calledMethod = InnerMethodName.get();
				calledMethod = calledMethod.replace("update", "create");
				InnerMethodName.set(calledMethod);
				internalCreate();				
			} catch(QueryTemplateAlreadyPresentException e) {
				String uuidVertexString = element.getProperty(QueryTemplate.ID_PROPERTY).toString();
				this.uuid = UUID.fromString(uuidVertexString);
				/*
				 * The service accepts the update if:
				 * - the JSON does NOT contains the field 'id': because the name is an id too;
				 * - the JSON contains the field 'id' and the value is null: because some serializators could set to null a missing value;
				 * - the JSON contains the field 'id' and the value is the same value contained in the vertex in the DB.
				 * 
				 * In other words, the service refuse the update with Bad Request if 
				 * the JSON contains the field 'id' and the value differs from the value contained in the vertex in the DB.
				 */
				JsonNode idNode = jsonNode.get(QueryTemplate.ID_PROPERTY);
				if(idNode != null && !idNode.isNull() && idNode.isTextual()) {
					String jsonID = idNode.asText();
					if(uuidVertexString.compareTo(jsonID)!=0) {
						throw new ResourceRegistryException("If you provide the id of the " + QueryTemplate.NAME + " it must has the same value of the id contained in the IS (i.e. " + uuidVertexString + ")");
					}
				}
				
				setOperation(Operation.UPDATE);
				update = true;
				internalUpdate();
			}
			
			oDatabaseDocument.commit();
			
			if(update) {
				setReload(true);
			}
			
			// TODO Notify to subscriptionNotification
			
			return serializeAsJsonNode().toString();
			
		} catch(ResourceRegistryException e) {
			logger.error("Unable to update {} with UUID {}", accessType.getName(), uuid);
			if(oDatabaseDocument != null) {
				oDatabaseDocument.rollback();
			}
			throw e;
		} catch(Exception e) {
			logger.error("Unable to update {} with UUID {}", accessType.getName(), uuid, e);
			if(oDatabaseDocument != null) {
				oDatabaseDocument.rollback();
			}
			throw new ResourceRegistryException(e);
		} finally {
			if(oDatabaseDocument != null) {
				oDatabaseDocument.close();
			}
			
			if(current!=null) {
				current.activateOnCurrentThread();
			}
		}
	}
	
	@Override
	protected OVertex reallyCreate() throws AlreadyPresentException, InvalidQueryException, ResourceRegistryException {
		try {
			checkIfNameAlreadyExists();
			tryTemplate();
			createVertex();
			return getElement();
		} catch(ResourceRegistryException e) {
			throw e;
		} catch(Exception e) {
			logger.trace("Error while creating {} for {} ({}) using {}", OVertex.class.getSimpleName(),
					accessType.getName(), typeName, jsonNode, e);
			throw new ResourceRegistryException("Error Creating " + typeName + " with " + jsonNode, e.getCause());
		}
	}
	
	@Override
	protected OVertex reallyUpdate() throws NotFoundException, ResourceRegistryException {
		try {
			tryTemplate();
			OVertex queryTemplate = getElement();
			queryTemplate = (OVertex) updateProperties(oClass, queryTemplate, jsonNode, ignoreKeys, ignoreStartWithKeys);
			return getElement();
		} catch(ResourceRegistryException e) {
			throw e;
		} catch(Exception e) {
			logger.trace("Error while creating {} for {} ({}) using {}", OVertex.class.getSimpleName(),
					accessType.getName(), typeName, jsonNode, e);
			throw new ResourceRegistryException("Error Creating " + typeName + " with " + jsonNode, e.getCause());
		}
	}

	@Override
	protected void reallyDelete() throws NotFoundException, ResourceRegistryException {
		logger.debug("Going to delete {} with name {}", accessType.getName(), name);
		OVertex oVertex = getElement();
		uuid = UUID.fromString((String) oVertex.getProperty(IdentifiableElement.ID_PROPERTY));
		oVertex.delete();
	}

	@Override
	protected NotFoundException getSpecificNotFoundException(NotFoundException e) {
		return new NotFoundException(e);
	}

	@Override
	protected AlreadyPresentException getSpecificAlreadyPresentException(String message) {
		return new AlreadyPresentException(message);
	}

	@Override
	public String reallyGetAll(boolean polymorphic) throws ResourceRegistryException {
		ObjectMapper objectMapper = new ObjectMapper();
		ArrayNode arrayNode = objectMapper.createArrayNode();
		
		ServerRequestInfo requestInfo = RequestUtility.getRequestInfo().get();
		int limit = requestInfo.getLimit();
		int offset = requestInfo.getOffset();
		
		int position = -1;
		int count = 0;
		
		Iterable<ODocument> iterable = oDatabaseDocument.browseClass(typeName, polymorphic);
		for (ODocument vertex : iterable) {
			if(++position < offset) {
				continue;
			}
			
			QueryTemplateManagement queryTemplateManagement = new QueryTemplateManagement();
			queryTemplateManagement.setElement((OVertex) vertex);
			try {
				JsonNode jsonObject = queryTemplateManagement.serializeAsJsonNode();
				arrayNode.add(jsonObject);
				if(limit > 0 && ++count >= limit) {
					break;
				}
			} catch (ResourceRegistryException e) {
				logger.error("Unable to correctly serialize {}. It will be excluded from results. {}",
						vertex.toString(), OrientDBUtility.SHOULD_NOT_OCCUR_ERROR_MESSAGE);
			}
		}
		try {
			return objectMapper.writeValueAsString(arrayNode);
		} catch (JsonProcessingException e) {
			throw new ResourceRegistryException(e);
		}
	}

	public void setParams(String params) throws ResourceRegistryException {
		try {
			if(params!=null && params.compareTo("")!=0) {
				ObjectMapper objectMapper = new ObjectMapper();
				JsonNode jsonNode = objectMapper.readTree(params);
				setParams(jsonNode);
			}
		} catch (ResourceRegistryException e) {
			throw e;
		} catch (Exception e) {
			throw new ResourceRegistryException(e);
		}
		
	}
	
	public void setParams(JsonNode params) throws ResourceRegistryException {
		this.params = params;
	}

	/**
	 * Run the query after having replaced query parameter with the provided values
	 * @return the result as JSON string
	 * @throws Exception 
	 */
	public String run() throws ResourceRegistryException {
		try {
			String read = read();
			QueryTemplate queryTemplate = ElementMapper.unmarshal(QueryTemplate.class, read);
			JsonNode query = null; 
			if(params!=null) {
				query = queryTemplate.getJsonQuery(params);
			}else {
				query = queryTemplate.getJsonQuery();
			}
			JsonQuery jsonQuery = new JsonQuery();
			jsonQuery.setJsonQuery(query);
			return jsonQuery.query();
		} catch (ResourceRegistryException e) {
			throw e;
		} catch (Exception e) {
			throw new ResourceRegistryException(e);
		}
	}
	
	public void sanityCheck() throws SchemaViolationException, ResourceRegistryException {
		// No sanity check required
	}
	
}
