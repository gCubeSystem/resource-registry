package org.gcube.informationsystem.resourceregistry.instances;

import java.util.Map;
import java.util.UUID;

import org.gcube.informationsystem.base.reference.Element;
import org.gcube.informationsystem.model.impl.relations.ConsistsOfImpl;
import org.gcube.informationsystem.model.reference.entities.Facet;
import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.model.reference.relations.ConsistsOf;
import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.entities.resource.ResourceAlreadyPresentException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.types.SchemaViolationException;
import org.gcube.informationsystem.resourceregistry.instances.model.entities.FacetManagement;
import org.gcube.informationsystem.resourceregistry.instances.model.entities.ResourceManagement;
import org.gcube.informationsystem.resourceregistry.instances.model.relations.ConsistsOfManagement;
import org.gcube.informationsystem.resourceregistry.instances.model.relations.IsRelatedToManagement;
import org.gcube.informationsystem.serialization.ElementMapper;
import org.gcube.resourcemanagement.model.impl.entities.facets.CPUFacetImpl;
import org.gcube.resourcemanagement.model.impl.entities.facets.SimpleFacetImpl;
import org.gcube.resourcemanagement.model.impl.entities.facets.SoftwareFacetImpl;
import org.gcube.resourcemanagement.model.impl.entities.resources.HostingNodeImpl;
import org.gcube.resourcemanagement.model.impl.entities.resources.RunningPluginImpl;
import org.gcube.resourcemanagement.model.impl.relations.consistsof.IsIdentifiedByImpl;
import org.gcube.resourcemanagement.model.reference.entities.facets.CPUFacet;
import org.gcube.resourcemanagement.model.reference.entities.facets.SimpleFacet;
import org.gcube.resourcemanagement.model.reference.entities.facets.SoftwareFacet;
import org.gcube.resourcemanagement.model.reference.entities.resources.Actor;
import org.gcube.resourcemanagement.model.reference.entities.resources.Configuration;
import org.gcube.resourcemanagement.model.reference.entities.resources.EService;
import org.gcube.resourcemanagement.model.reference.entities.resources.HostingNode;
import org.gcube.resourcemanagement.model.reference.entities.resources.RunningPlugin;
import org.gcube.resourcemanagement.model.reference.entities.resources.Service;
import org.gcube.resourcemanagement.model.reference.relations.consistsof.IsIdentifiedBy;
import org.gcube.resourcemanagement.model.reference.relations.isrelatedto.Activates;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class InvalidOperationTest extends ERManagementTest {
	
	private static Logger logger = LoggerFactory.getLogger(InvalidOperationTest.class);
	
	public static final String ACTIVATES = "{\"propagationConstraint\":{\"" + Element.TYPE_PROPERTY + "\":\"PropagationConstraint\",\"add\":\"propagate\",\"remove\":\"cascade\",\"delete\":\"cascade\"},\"" + Element.TYPE_PROPERTY + "\":\"Activates\",\"source\":{\"" + Element.TYPE_PROPERTY + "\":\"Configuration\",\"uuid\":\"CONFIGURATION_UUID\"},\"target\":{\"uuid\":\"ESERVICE_UUID\",\"" + Element.TYPE_PROPERTY + "\":\"EService\"}}";
	public static final String ACTOR = "{\"" + Element.TYPE_PROPERTY + "\":\"Actor\",\"metadata\":null,\"consistsOf\":[{\"" + Element.TYPE_PROPERTY + "\":\"IsIdentifiedBy\",\"metadata\":null,\"propagationConstraint\":{\"" + Element.TYPE_PROPERTY + "\":\"PropagationConstraint\",\"remove\":\"cascade\",\"delete\":\"cascade\",\"add\":\"propagate\"},\"source\":{\"" + Element.TYPE_PROPERTY + "\":\"Actor\",\"metadata\":null},\"target\":{\"" + Element.TYPE_PROPERTY + "\":\"ContactFacet\",\"metadata\":null,\"title\":\"Dr.\",\"name\":\"Frosini\",\"middleName\":null,\"surname\":null,\"eMail\":\"luca.frosini@isti.cnr.it\"}}],\"isRelatedTo\":[]}";
	
	@Test(expected = SchemaViolationException.class)
	public void createInvalidIsRealtedTo() throws Exception {
		Configuration configuration = createConfiguration();
		EService eService = createEService();
		try {
			/*
			 * Trying to create a relation activates between a Configuration and EService
			 * The creation MUST fails raising SchemaViolationException because the
			 * Activates relation is between two Service isntaces
			 *
			 * The only way to try to create it is using static string because Java classes
			 * already deny to create and instance of Activates
			 * 
			 * Here we want to test how the service behave if a client does not behave properly
			 */
			IsRelatedToManagement isRelatedToManagement = new IsRelatedToManagement();
			isRelatedToManagement.setElementType(Activates.NAME);
			String json = ACTIVATES.replace("CONFIGURATION_UUID", configuration.getID().toString());
			json = json.replace("ESERVICE_UUID", eService.getID().toString());
			isRelatedToManagement.setJson(json);
			isRelatedToManagement.create();
		}finally {
			deleteResource(configuration);
			deleteResource(eService);
		}
	}
	
	@Test(expected = ResourceAlreadyPresentException.class)
	public void testRecreate() throws Exception {
		EService eService = createEService();
		try {
			createResource(eService);
		}finally {
			deleteResource(eService);
		}
	}
	
	
	@Test(expected = SchemaViolationException.class)
	public void testCreateStandAloneFacet() throws Exception {
		CPUFacet cpuFacet = new CPUFacetImpl();
		cpuFacet.setClockSpeed("1 GHz");
		cpuFacet.setModel("Opteron");
		cpuFacet.setVendor("AMD");

		FacetManagement facetManagement = new FacetManagement();
		facetManagement.setElementType(CPUFacet.NAME);
		String json = ElementMapper.marshal(cpuFacet);
		logger.debug("{}", json);
		facetManagement.setJson(json);

		/* A facet cannot be created per se */
		facetManagement.create();
	}
	
	@Test(expected = SchemaViolationException.class)
	public void testCreateInvalidRunningPlugin() throws Exception {
		RunningPlugin runningPlugin = new RunningPluginImpl();

		SoftwareFacet softwareFacet = new SoftwareFacetImpl();
		softwareFacet.setGroup("information-system");
		softwareFacet.setName("is-exporter-se-plugin");
		softwareFacet.setVersion("1.0.0");
		
		IsIdentifiedBy<RunningPlugin, SoftwareFacet> isIdentifiedBy = new IsIdentifiedByImpl<>(runningPlugin, softwareFacet);
		runningPlugin.addFacet(isIdentifiedBy);

		createResource(runningPlugin);
	}
	
	@Test(expected = ResourceRegistryException.class)
	public void testCreateAnEntityDifferentFromDeclared() throws Exception {
		EService eService = instantiateValidEService();
		ResourceManagement resourceManagement = new ResourceManagement();
		resourceManagement.setElementType(Service.NAME);
		resourceManagement.setJson(ElementMapper.marshal(eService));
		resourceManagement.create();
	}
	
	@Test(expected = SchemaViolationException.class)
	public void testCreateAbstractEntity() throws Exception {
		ResourceManagement resourceManagement = new ResourceManagement();
		resourceManagement.setElementType(Actor.NAME);
		resourceManagement.setJson(ACTOR);
		resourceManagement.create();
	}
	
	@Test(expected = SchemaViolationException.class)
	public void testCreateHostingNodeAndEServiceWithSharedFacet() throws Exception {
		Map<String, Resource> map = createHostingNodeAndEService();
		EService eService = (EService) map.get(EService.NAME);
		HostingNode hostingNode = (HostingNode) map.get(HostingNode.NAME);
		try {
			Facet shared = hostingNode.getConsistsOf().get(0).getTarget();
	
			ConsistsOfManagement consistsOfManagement = new ConsistsOfManagement();
			consistsOfManagement.setElementType(ConsistsOf.NAME);
			consistsOfManagement.setJson("{}");
			ConsistsOf<EService, Facet> consistsOf = new ConsistsOfImpl<>(eService, shared);
			String consistsOfJsonString = ElementMapper.marshal(consistsOf);
			consistsOfManagement.setJson(consistsOfJsonString);
	
			String json = consistsOfManagement.create();
			logger.debug("Created : {}", json);
			
		} finally {
			deleteResource(eService);
			deleteResource(hostingNode);
		}

	}
	
	@Test(expected = SchemaViolationException.class)
	public void testCreateEServiceAndDeleteRequiredConsistsOf() throws Exception {
		EService eService = null;
		try {
			eService = createEService();
			
			@SuppressWarnings("unchecked")
			IsIdentifiedBy<EService, SoftwareFacet> isIdentifiedBy = (IsIdentifiedBy<EService, SoftwareFacet>) eService.getConsistsOf(IsIdentifiedBy.class).get(0);
			
			ConsistsOfManagement consistsOfManagement = new ConsistsOfManagement();
			consistsOfManagement.setElementType(isIdentifiedBy.getTypeName());
			consistsOfManagement.setUUID(isIdentifiedBy.getID());
			consistsOfManagement.delete();
		}finally {
			deleteResource(eService);
		}
	}
	
	@Test(expected = SchemaViolationException.class)
	public void testCreateEServiceAndDeleteRequiredFacet() throws Exception {
		EService eService = createEService();
		
		@SuppressWarnings("unchecked")
		IsIdentifiedBy<EService, SoftwareFacet> isIdentifiedBy = (IsIdentifiedBy<EService, SoftwareFacet>) eService.getConsistsOf(IsIdentifiedBy.class).get(0);
		ConsistsOfManagement consistsOfManagement = new ConsistsOfManagement();
		consistsOfManagement.setElementType(IsIdentifiedBy.NAME);
		consistsOfManagement.setUUID(isIdentifiedBy.getID());
		
		SoftwareFacet softwareFacet = isIdentifiedBy.getTarget();
		FacetManagement facetManagement = new FacetManagement();
		facetManagement.setElementType(SoftwareFacet.NAME);
		facetManagement.setUUID(softwareFacet.getID());
		
		try {
			facetManagement.delete();
		}finally {
			deleteResource(eService);
		}
	
	}
	
	@Test(expected = SchemaViolationException.class)
	public void testCreateConsistsOfBeetweenResources() throws Exception {
		Map<String, Resource> map = createHostingNodeAndEService();

		UUID hostingNodeUUID = map.get(HostingNode.NAME).getID();
		UUID eServiceUUID = map.get(EService.NAME).getID();

		HostingNode hostingNode = new HostingNodeImpl();
		hostingNode.setID(hostingNodeUUID);

		SimpleFacet fakeEServiceAsSimpleFacet = new SimpleFacetImpl();
		fakeEServiceAsSimpleFacet.setID(eServiceUUID);

		ConsistsOf<HostingNode, SimpleFacet> consistsOf = new ConsistsOfImpl<HostingNode, SimpleFacet>(hostingNode, fakeEServiceAsSimpleFacet);

		try {
			ConsistsOfManagement consistsOfManagement = new ConsistsOfManagement();
			consistsOfManagement.setElementType(ConsistsOf.NAME);
			
			String json = ElementMapper.marshal(consistsOf);
			json = json.replaceAll(SimpleFacet.NAME, EService.NAME);
			consistsOfManagement.setJson(json);

			consistsOfManagement.create();
			throw new Exception("A ConsistsOf has been created between two resoures. This should not happen");
		} finally {
			deleteResource(map.get(EService.NAME));
			deleteResource(map.get(HostingNode.NAME));
		}

	}
}
