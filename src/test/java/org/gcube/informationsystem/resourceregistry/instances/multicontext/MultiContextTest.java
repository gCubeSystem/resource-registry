package org.gcube.informationsystem.resourceregistry.instances.multicontext;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.UUID;

import org.gcube.com.fasterxml.jackson.databind.JsonNode;
import org.gcube.common.security.providers.SecretManagerProvider;
import org.gcube.informationsystem.base.reference.Element;
import org.gcube.informationsystem.model.reference.entities.Facet;
import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint.AddConstraint;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint.RemoveConstraint;
import org.gcube.informationsystem.model.reference.relations.ConsistsOf;
import org.gcube.informationsystem.model.reference.relations.IsRelatedTo;
import org.gcube.informationsystem.model.reference.relations.Relation;
import org.gcube.informationsystem.resourceregistry.ContextTest;
import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;
import org.gcube.informationsystem.resourceregistry.contexts.ContextUtility;
import org.gcube.informationsystem.resourceregistry.instances.ERManagementTest;
import org.gcube.informationsystem.resourceregistry.instances.model.entities.ResourceManagement;
import org.gcube.informationsystem.resourceregistry.instances.model.relations.IsRelatedToManagement;
import org.gcube.informationsystem.serialization.ElementMapper;
import org.gcube.informationsystem.types.TypeMapper;
import org.gcube.informationsystem.utils.TypeUtility;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.orientechnologies.orient.core.record.ODirection;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class MultiContextTest extends ERManagementTest {

	private static Logger logger = LoggerFactory.getLogger(MultiContextTest.class);

	@SuppressWarnings({ "unchecked", "unused" })
	private Map<UUID, Element> getRemovedExpectedInstances(Resource r) throws Exception {
		Map<UUID, Element> expected = new HashMap<>();
		expected.put(r.getID(), r);
		
		@SuppressWarnings("rawtypes")
		List<IsRelatedTo> list = getOutcomingIsRelatedTo(r);
		

		for (IsRelatedTo<? extends Resource, ? extends Resource> isRelatedTo : list) {
			if (isRelatedTo.getPropagationConstraint().getRemoveConstraint() == RemoveConstraint.cascade) {
				expected.put(isRelatedTo.getID(), isRelatedTo);
				expected.putAll(getRemovedExpectedInstances(isRelatedTo.getTarget()));
			}

			if (isRelatedTo.getPropagationConstraint().getRemoveConstraint() == RemoveConstraint.cascadeWhenOrphan) {
				expected.put(isRelatedTo.getID(), isRelatedTo);
				@SuppressWarnings("rawtypes")
				List<IsRelatedTo> incoming = getIncomingIsRelatedTo(r);
				if(incoming.size()==1) {
					Assert.assertTrue(incoming.get(0).getID().compareTo(isRelatedTo.getID())==0);
					expected.putAll(getRemovedExpectedInstances(isRelatedTo.getTarget()));
				}
				
			}
		}
		
		for (ConsistsOf<? extends Resource, ? extends Facet> consistsOf : r.getConsistsOf()) {
			if (consistsOf.getPropagationConstraint().getRemoveConstraint() == RemoveConstraint.cascade || consistsOf
					.getPropagationConstraint().getRemoveConstraint() == RemoveConstraint.cascadeWhenOrphan) {
				expected.put(consistsOf.getID(), consistsOf);
				Facet facet = consistsOf.getTarget();
				expected.put(facet.getID(), facet);
			}
		}
		
		return expected;
	}

	private void removeFromContextThenTestIfBehaveProperly(Resource r, boolean dryRun) throws Exception {
// 		Must be investigated cause infinite recursion to Jackson
//		Map<UUID, Element> expectedInstances = getRemovedExpectedInstances(r);

		ResourceManagement resourceManagement = getResourceManagement(r);
		resourceManagement.setDryRun(dryRun);
		UUID contextUUID = ContextUtility.getCurrentRequestEnvironment().getUUID();
		resourceManagement.removeFromContext(contextUUID);
//		Map<UUID, JsonNode> affectedInstances = resourceManagement.getAffectedInstances();
		
//		SortedSet<UUID> expectedInstancesUUID = new TreeSet<>(expectedInstances.keySet());
//		SortedSet<UUID> affectedInstancesUUID = new TreeSet<>(affectedInstances.keySet());
//
//		logger.trace("Expected Instances are {} : {}", expectedInstancesUUID.size(), expectedInstancesUUID);
//		logger.trace("Affected Instances are {} : {}", affectedInstancesUUID.size(), affectedInstancesUUID);
//
//		Assert.assertTrue(expectedInstancesUUID.size() == affectedInstancesUUID.size());
//
//		Assert.assertTrue(expectedInstancesUUID.containsAll(affectedInstancesUUID));
//		Assert.assertTrue(affectedInstancesUUID.containsAll(expectedInstancesUUID));
//
//		for (UUID uuid : affectedInstances.keySet()) {
//
//			Element element = expectedInstances.get(uuid);
//			String expectedType = TypeMapper.getType(element.getClass());
//
//			JsonNode affectedJsonNode = affectedInstances.get(uuid);
//			String affectedType = affectedJsonNode.get(Element.TYPE_PROPERTY).asText();
//
//			Assert.assertTrue(affectedType.compareTo(expectedType) == 0);
//		}
		
//		if(!dryRun) {
//			resourceManagement = ERManagementTest.getResourceManagement(r);
//			try {
//				resourceManagement.read();
//				String error = String.format("{} with UUID {} should not be visible.", Utility.getTypeName(r), r.getUUID());
//				logger.trace(error);
//				throw new Exception(error);
//			}catch (ResourceAvailableInAnotherContextException e) {
//				// OK
//			}
//		}

	}

	protected void removeFromContextThenTestIfBehaveProperly(Resource r) throws Exception {
		removeFromContextThenTestIfBehaveProperly(r, true);
		removeFromContextThenTestIfBehaveProperly(r, false);
	}
	
	private interface RelationConstraintEvaluator {

		public boolean mustBeAdded(Relation<Resource, Resource> r);

		public boolean mustBeRemoved(Relation<Resource, Resource> r);

	}
	
	@SuppressWarnings("rawtypes")
	protected List<IsRelatedTo> getIncomingIsRelatedTo(Resource r) throws Exception {
		return getIsRelatedTo(r, ODirection.OUT);
	}
	
	@SuppressWarnings("rawtypes")
	protected List<IsRelatedTo> getOutcomingIsRelatedTo(Resource r) throws Exception {
		return getIsRelatedTo(r, ODirection.IN);
	}
	
	@SuppressWarnings("rawtypes")
	protected List<IsRelatedTo> getIsRelatedTo(Resource r, ODirection oDirection) throws Exception {
		ResourceManagement resourceManagement = new ResourceManagement();
		resourceManagement.setElementType(Resource.NAME);

		String resourceType = TypeMapper.getType(r);
		UUID resourceUUID = r.getID();

		// resourceManagement.setUUID(resourceUUID);
		String ret = resourceManagement.query(IsRelatedTo.NAME, resourceType, resourceUUID, oDirection, true,
				new HashMap<>());
		List<IsRelatedTo> isRelatedToList = ElementMapper.unmarshalList(IsRelatedTo.class, ret);
		return isRelatedToList;
	}

	@SuppressWarnings("unchecked")
	protected Resource getAndAddIsRelatedTo(Resource r, RelationConstraintEvaluator relationConstraint)
			throws ResourceRegistryException, Exception {
		@SuppressWarnings("rawtypes")
		List<IsRelatedTo> isRelatedToList = getOutcomingIsRelatedTo(r);
		for (IsRelatedTo<Resource, Resource> isRelatedTo : isRelatedToList) {
			boolean mustBeAdded = relationConstraint.mustBeAdded(isRelatedTo);
			if (mustBeAdded) {
				r.attachResource(isRelatedTo);
			}
		}
		return r;
	}

	protected Map<UUID, Element> getExpectedInstancesAddToContext(Resource resource) throws ResourceRegistryException, Exception {
		
		String json = getResourceManagement(resource).read();
		Resource r = ElementMapper.unmarshal(resource.getClass(), json);
		
		Map<UUID, Element> expected = new HashMap<>();
		expected.put(r.getID(), r);

		RelationConstraintEvaluator relationConstraintEvaluator = new RelationConstraintEvaluator() {

			@Override
			public boolean mustBeAdded(Relation<Resource, Resource> r) {
				return r.getPropagationConstraint().getAddConstraint() == AddConstraint.propagate;
			}

			@Override
			public boolean mustBeRemoved(Relation<Resource, Resource> r) {
				throw new UnsupportedOperationException();
			}

		};

		r = getAndAddIsRelatedTo(r, relationConstraintEvaluator);

		for (IsRelatedTo<? extends Resource, ? extends Resource> isRelatedTo : r.getIsRelatedTo()) {
			if (isRelatedTo.getPropagationConstraint().getAddConstraint() == AddConstraint.propagate) {
				expected.put(isRelatedTo.getID(), isRelatedTo);
				expected.putAll(getExpectedInstancesAddToContext(isRelatedTo.getTarget()));
			}
		}
		for (ConsistsOf<? extends Resource, ? extends Facet> consistsOf : r.getConsistsOf()) {
			if (consistsOf.getPropagationConstraint().getAddConstraint() == AddConstraint.propagate) {
				expected.put(consistsOf.getID(), consistsOf);
				Facet facet = consistsOf.getTarget();
				expected.put(facet.getID(), facet);
			}
		}
		return expected;
	}

	protected void addToContextThenTestIfBehaveProperly(Resource r, boolean dryRun, String targetContextFullName)
			throws ResourceRegistryException, Exception {

//		Map<UUID, Element> expectedInstances = getExpectedInstancesAddToContext(r);

		ResourceManagement resourceManagement = getResourceManagement(r);
		resourceManagement.setDryRun(dryRun);
		UUID contextUUID = ContextUtility.getInstance().getRequestEnvironmentByContextFullName(targetContextFullName).getUUID();
		resourceManagement.addToContext(contextUUID);
//		Map<UUID, JsonNode> affectedInstances = resourceManagement.getAffectedInstances();
//		
//		checkAffectedInstances(expectedInstances, affectedInstances);
//		
		if(!dryRun) {
			String currentContext = SecretManagerProvider.get().getContext();
			ContextTest.setContextByName(targetContextFullName);
			resourceManagement = getResourceManagement(r);
			String json = resourceManagement.read();
			Resource resource = ElementMapper.unmarshal(r.getClass(), json);
			Assert.assertTrue(resource.getClass() == r.getClass());
			Assert.assertTrue(resource.getID().compareTo(r.getID())==0);
			ContextTest.setContextByName(currentContext);
		}
	}
	
	protected void checkAffectedInstances(Map<UUID, Element> expectedInstances, Map<UUID, JsonNode> affectedInstances) {
		
		SortedSet<UUID> expectedInstancesUUID = new TreeSet<>(expectedInstances.keySet());
		SortedSet<UUID> affectedInstancesUUID = new TreeSet<>(affectedInstances.keySet());

		logger.trace("Expected Instances are {} : {}", expectedInstancesUUID.size(), expectedInstancesUUID);
		logger.trace("Affected Instances are {} : {}", affectedInstancesUUID.size(), affectedInstancesUUID);

		Assert.assertTrue(expectedInstancesUUID.size() == affectedInstancesUUID.size());

		Assert.assertTrue(expectedInstancesUUID.containsAll(affectedInstancesUUID));
		Assert.assertTrue(affectedInstancesUUID.containsAll(expectedInstancesUUID));

		for (UUID uuid : affectedInstances.keySet()) {

			Element element = expectedInstances.get(uuid);
			String expectedType = TypeMapper.getType(element.getClass());

			JsonNode affectedJsonNode = affectedInstances.get(uuid);
			String affectedType = TypeUtility.getTypeName(affectedJsonNode);

			Assert.assertTrue(affectedType.compareTo(expectedType) == 0);

		}
		
	}
	
	protected void addToContextThenTestIfBehaveProperly(Resource r, String targetContextFullName) throws Exception {
		addToContextThenTestIfBehaveProperly(r, true, targetContextFullName);
		addToContextThenTestIfBehaveProperly(r, false, targetContextFullName);
	}
	
	protected void addToContextThenTestIfBehaveProperly(IsRelatedTo<? extends Resource, ? extends Resource> isRelatedTo, boolean dryRun, String targetContextFullName)
			throws ResourceRegistryException, Exception {
// 		Must be investigated cause infinite recursion to Jackson
//		Map<UUID, Element> expectedInstances = getExpectedInstancesAddToContext(isRelatedTo.getSource());
//		expectedInstances.putAll(getExpectedInstancesAddToContext(isRelatedTo.getTarget()));
//		expectedInstances.put(isRelatedTo.getUUID(), isRelatedTo);
		
		IsRelatedToManagement isRelatedToManagement = getIsRelatedToManagement(isRelatedTo);
		isRelatedToManagement.setDryRun(dryRun);
		UUID contextUUID = ContextUtility.getInstance().getRequestEnvironmentByContextFullName(targetContextFullName).getUUID();
		isRelatedToManagement.addToContext(contextUUID);
//		Map<UUID, JsonNode> affectedInstances = isRelatedToManagement.getAffectedInstances();
		
		
//		checkAffectedInstances(expectedInstances, affectedInstances);
		
//		if(!dryRun) {
//			String currentContext = ContextTest.getCurrentContextFullName();
//			ContextTest.setContextByName(targetContextFullName);
//			isRelatedToManagement = ERManagementTest.getIsRelatedToManagement(isRelatedTo);
//			String json = isRelatedToManagement.read();
//			@SuppressWarnings("unchecked")
//			IsRelatedTo<Resource, Resource> irt = ElementMapper.unmarshal(isRelatedTo.getClass(), json);
//			Assert.assertTrue(isRelatedTo.getClass() == irt.getClass());
//			Assert.assertTrue(isRelatedTo.getUUID().compareTo(irt.getUUID())==0);
//			
//			Resource source = irt.getSource();
//			ResourceManagement resourceManagement = ERManagementTest.getResourceManagement(source);
//			json = resourceManagement.read();
//			Resource resource = ElementMapper.unmarshal(source.getClass(), json);
//			Assert.assertTrue(resource.getClass() == source.getClass());
//			Assert.assertTrue(resource.getUUID().compareTo(source.getUUID())==0);
//			
//			Resource target = irt.getTarget();
//			resourceManagement = ERManagementTest.getResourceManagement(target);
//			json = resourceManagement.read();
//			resource = ElementMapper.unmarshal(target.getClass(), json);
//			Assert.assertTrue(resource.getClass() == target.getClass());
//			Assert.assertTrue(resource.getUUID().compareTo(target.getUUID())==0);
//			
//			ContextTest.setContextByName(currentContext);
//		}
	}
	
	protected void addToContextThenTestIfBehaveProperly(IsRelatedTo<? extends Resource, ? extends Resource> isRelatedTo, String targetContextFullName) throws Exception {
		addToContextThenTestIfBehaveProperly(isRelatedTo, true, targetContextFullName);
		addToContextThenTestIfBehaveProperly(isRelatedTo, false, targetContextFullName);
	}
	
}
