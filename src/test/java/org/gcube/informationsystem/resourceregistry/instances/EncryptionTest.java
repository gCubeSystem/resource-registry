package org.gcube.informationsystem.resourceregistry.instances;

import java.security.Key;

import org.gcube.common.encryption.encrypter.StringEncrypter;
import org.gcube.informationsystem.model.impl.properties.EncryptedImpl;
import org.gcube.informationsystem.model.reference.properties.Encrypted;
import org.gcube.informationsystem.resourceregistry.dbinitialization.DatabaseEnvironment;
import org.gcube.informationsystem.resourceregistry.instances.model.entities.ResourceManagement;
import org.gcube.informationsystem.serialization.ElementMapper;
import org.gcube.resourcemanagement.model.impl.entities.facets.CPUFacetImpl;
import org.gcube.resourcemanagement.model.reference.entities.facets.CPUFacet;
import org.gcube.resourcemanagement.model.reference.entities.resources.Configuration;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class EncryptionTest extends ERManagementTest {
	
	public static final String PLAIN_VALUE = "plain-value";
	
	// @Test
//	public void decryptPwd() throws Exception {
//		String pwd = StringEncrypter.getEncrypter().decrypt("");
//		Assert.assertTrue(true);
//	}
	
	@Test
	public void test() throws Exception {
		
		Encrypted encrypted = new EncryptedImpl();
		String contextEncryptedValue = StringEncrypter.getEncrypter().encrypt(PLAIN_VALUE);
		encrypted.setValue(contextEncryptedValue);
		
		String getContextEncryptedValue = encrypted.getValue();
		Assert.assertTrue(getContextEncryptedValue.compareTo(getContextEncryptedValue)==0);
		
		
		// Decrypting with Context Key (default key)
		String decryptedValue = StringEncrypter.getEncrypter().decrypt(contextEncryptedValue); 
		Assert.assertTrue(decryptedValue.compareTo(PLAIN_VALUE)==0);
		
		// Encrypting with DB Key
		Key databaseKey = DatabaseEnvironment.getDatabaseKey();
		String dbEncryptedValue = StringEncrypter.getEncrypter().encrypt(decryptedValue, databaseKey);
		
		// Setting the value encrypted with DB key
		encrypted.setValue(dbEncryptedValue);
		
		String getDBEncryptedValue = encrypted.getValue();
		Assert.assertTrue(getDBEncryptedValue.compareTo(dbEncryptedValue)==0);
		
		decryptedValue = StringEncrypter.getEncrypter().decrypt(getDBEncryptedValue, databaseKey);
		Assert.assertTrue(decryptedValue.compareTo(PLAIN_VALUE)==0);
		
	}
	
	@Test
	public void testCreateFacetWithAdditionlEncryptedField() throws Exception {
		/*
		 * A facet cannot be created per se. Going to create a Configuration which does
		 * not impose any particular constraint except the IdentifierFact
		 */
		Configuration configuration = ERManagementTest.instantiateValidConfiguration();

		CPUFacet cpuFacet = new CPUFacetImpl();
		cpuFacet.setClockSpeed("1 GHz");
		cpuFacet.setModel("Opteron");
		cpuFacet.setVendor("AMD");
		Encrypted encrypted = new EncryptedImpl();
		String encryptedValue = StringEncrypter.getEncrypter().encrypt(PLAIN_VALUE);
		encrypted.setValue(encryptedValue);
		String additionalKey = "test";
		cpuFacet.setAdditionalProperty(additionalKey, encrypted);
		configuration.addFacet(cpuFacet);

		ResourceManagement resourceManagement = new ResourceManagement();
		resourceManagement.setElementType(Configuration.NAME);
		String json = ElementMapper.marshal(configuration);
		resourceManagement.setJson(json);

		String configurationJsonString = resourceManagement.create();

		Configuration createdConfiguration = ElementMapper.unmarshal(Configuration.class, configurationJsonString);
		
		CPUFacet readCpuFacet = createdConfiguration.getFacets(CPUFacet.class).get(0);
		String gotEncryptedValue = ((Encrypted) readCpuFacet.getAdditionalProperty(additionalKey)).getValue();
		Assert.assertTrue(gotEncryptedValue.compareTo(encryptedValue) == 0);
		String gotPlainValue = StringEncrypter.getEncrypter().decrypt(gotEncryptedValue);
		Assert.assertTrue(gotPlainValue.compareTo(PLAIN_VALUE) == 0);

		resourceManagement.delete();
	}
	
}
