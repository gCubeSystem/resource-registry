package org.gcube.informationsystem.resourceregistry.instances.multicontext;

import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.gcube.informationsystem.model.impl.properties.PropagationConstraintImpl;
import org.gcube.informationsystem.model.reference.entities.Facet;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint.AddConstraint;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint.RemoveConstraint;
import org.gcube.informationsystem.resourceregistry.ContextTest;
import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;
import org.gcube.informationsystem.resourceregistry.contexts.ServerContextCache;
import org.gcube.informationsystem.resourceregistry.instances.model.entities.FacetManagement;
import org.gcube.informationsystem.resourceregistry.instances.model.entities.ResourceManagement;
import org.gcube.informationsystem.resourceregistry.instances.model.relations.IsRelatedToManagement;
import org.gcube.informationsystem.serialization.ElementMapper;
import org.gcube.informationsystem.utils.TypeUtility;
import org.gcube.resourcemanagement.model.impl.relations.isrelatedto.ActivatesImpl;
import org.gcube.resourcemanagement.model.reference.entities.resources.EService;
import org.gcube.resourcemanagement.model.reference.entities.resources.HostingNode;
import org.gcube.resourcemanagement.model.reference.relations.isrelatedto.Activates;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class ComplexTest extends MultiContextTest {

	private static Logger logger = LoggerFactory.getLogger(ComplexTest.class);
	
	@Test
	public void testGetInstancesContexts() throws ResourceRegistryException, Exception {
		ContextTest.setContextByName(DEVNEXT);
		
		/* Creating HostingNode */
		HostingNode hostingNode = createHostingNode();

		/* Creating EService */
		EService eService = createEService();
		
		
		/* Creating Activates Relation */
		PropagationConstraint propagationConstraint = new PropagationConstraintImpl();
		propagationConstraint.setRemoveConstraint(RemoveConstraint.cascade);
		propagationConstraint.setAddConstraint(AddConstraint.unpropagate);

		
		Activates<HostingNode, EService> activates = new ActivatesImpl<>(hostingNode, eService,
				propagationConstraint);
		
		IsRelatedToManagement activatesManagement = new IsRelatedToManagement();
		activatesManagement.setElementType(Activates.NAME);
		String activatesJson = ElementMapper.marshal(activates);
		activatesManagement.setJson(activatesJson);
		String createdActivatesJson = activatesManagement.create();
		logger.debug("Created : {}", createdActivatesJson);
		
		@SuppressWarnings("unchecked")
		Activates<HostingNode, EService> createdActivates =  ElementMapper.unmarshal(Activates.class, createdActivatesJson);
		logger.debug("UnMarshalled {}", createdActivates);
		
		/* ------------------------------------------------------------------ */
		
		addToContextThenTestIfBehaveProperly(hostingNode, false, NEXTNEXT);
		
		logger.debug("Switching to alternative scope");
		ContextTest.setContextByName(NEXTNEXT);
		
		ResourceManagement hostingNodeManagement = getResourceManagement(hostingNode);
		
		String hostingNodeContexts = hostingNodeManagement.getContexts();
		logger.debug("Contexts of {} with UUID {} have the following UUID {}", HostingNode.NAME, hostingNodeManagement.getUUID(), hostingNodeContexts);
		Map<UUID, String> hostingNodeContextMap = org.gcube.informationsystem.resourceregistry.api.contexts.ContextUtility.getContextMap(hostingNodeContexts);
		Set<UUID> hostingNodeContextUUID = hostingNodeContextMap.keySet();
		Assert.assertTrue(hostingNodeContextMap.size()==2);
		logger.debug("Contexts of {} with UUID {} are {}", HostingNode.NAME, hostingNodeManagement.getUUID(), hostingNodeContextMap.values());
		
		for(Facet facet : hostingNode.getFacets()) {
			FacetManagement facetManagement = new FacetManagement();
			facetManagement.setUUID(facet.getID());
			String typeName = TypeUtility.getTypeName(facet.getClass());
			facetManagement.setElementType(typeName);
			String facetContexts = facetManagement.getContexts();
			logger.debug("Contexts of {} with UUID {} have the following UUID {}", typeName, facetManagement.getUUID(), facetContexts);
			Map<UUID, String> facetContextMap =  org.gcube.informationsystem.resourceregistry.api.contexts.ContextUtility.getContextMap(facetContexts);
			Assert.assertTrue(facetContextMap.size()==2);
			Set<UUID> facetContextSet = facetContextMap.keySet();
			Assert.assertTrue(facetContextSet.containsAll(hostingNodeContextUUID));
			Assert.assertTrue(hostingNodeContextUUID.containsAll(facetContextSet));
			logger.debug("Contexts of {} with UUID {} are {}", typeName, facetManagement.getUUID(), facetContextMap.values());
		}
		
		activatesManagement = new IsRelatedToManagement();
		activatesManagement.setUUID(createdActivates.getID());
		activatesManagement.setElementType(TypeUtility.getTypeName(createdActivates.getClass()));
		String isRelatedToContexts = activatesManagement.getContexts();
		logger.debug("Contexts of {} with UUID {} have the following UUID {}", Activates.NAME, activatesManagement.getUUID(), isRelatedToContexts);
		Map<UUID, String> activatesContextMap = org.gcube.informationsystem.resourceregistry.api.contexts.ContextUtility.getContextMap(isRelatedToContexts);
		Set<UUID> activatesContextsUUID = activatesContextMap.keySet();  
		Assert.assertTrue(activatesContextsUUID.size()==1);
		Assert.assertFalse(activatesContextsUUID.containsAll(hostingNodeContextUUID));
		Assert.assertTrue(hostingNodeContextUUID.containsAll(activatesContextsUUID));
		logger.debug("Contexts of {} with UUID {} are {}", Activates.NAME, activatesManagement.getUUID(), activatesContextMap.values());
		
		
		ResourceManagement eServiceManagement = getResourceManagement(eService);
		String eServiceContexts = eServiceManagement.getContexts();
		logger.debug("Contexts of {} with UUID {} have the following UUID {}", EService.NAME, eServiceManagement.getUUID(), eServiceContexts);
		Map<UUID,String> eServiceContextMap = org.gcube.informationsystem.resourceregistry.api.contexts.ContextUtility.getContextMap(eServiceContexts);
		Set<UUID> eServiceContextsUUID = eServiceContextMap.keySet();
		Assert.assertTrue(eServiceContextsUUID.size()==1);
		Assert.assertFalse(eServiceContextsUUID.containsAll(hostingNodeContextUUID));
		Assert.assertTrue(hostingNodeContextUUID.containsAll(eServiceContextsUUID));
		Set<String> eServiceContextFullNames =  org.gcube.informationsystem.resourceregistry.api.contexts.ContextUtility.getContextFullNameSet(ServerContextCache.getInstance(), eServiceContextsUUID);
		Assert.assertTrue(eServiceContextFullNames.size()==1);
		logger.debug("Contexts of {} with UUID {} are {}", EService.NAME, eServiceManagement.getUUID(), eServiceContextFullNames);
		
		deleteResource(hostingNode);
		
		try {
			ContextTest.setContextByName(DEVNEXT);
			deleteResource(eService);
		} catch (Exception e) {
			logger.error("",e);
		}
	}
	
}
