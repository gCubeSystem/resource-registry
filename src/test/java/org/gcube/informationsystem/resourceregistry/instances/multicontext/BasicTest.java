/**
 * 
 */
package org.gcube.informationsystem.resourceregistry.instances.multicontext;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.gcube.informationsystem.base.reference.IdentifiableElement;
import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint.DeleteConstraint;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint.RemoveConstraint;
import org.gcube.informationsystem.model.reference.relations.ConsistsOf;
import org.gcube.informationsystem.resourceregistry.ContextTest;
import org.gcube.informationsystem.resourceregistry.api.exceptions.entities.resource.ResourceAvailableInAnotherContextException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.entities.resource.ResourceNotFoundException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.relations.RelationAvailableInAnotherContextException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.relations.isrelatedto.IsRelatedToNotFoundException;
import org.gcube.informationsystem.resourceregistry.instances.model.ERManagementUtility;
import org.gcube.informationsystem.resourceregistry.instances.model.entities.ResourceManagement;
import org.gcube.informationsystem.resourceregistry.instances.model.relations.IsRelatedToManagement;
import org.gcube.informationsystem.serialization.ElementMapper;
import org.gcube.resourcemanagement.model.reference.entities.resources.EService;
import org.gcube.resourcemanagement.model.reference.entities.resources.HostingNode;
import org.gcube.resourcemanagement.model.reference.relations.isrelatedto.Activates;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class BasicTest extends MultiContextTest {

	private static Logger logger = LoggerFactory
			.getLogger(BasicTest.class);

	/*
	@Test
	public void testDifferentScopes() throws Exception {
		ContextTest.setContextByName(DEFAULT_TEST_SCOPE);
		
		CPUFacet cpuFacet = new CPUFacetImpl();
		cpuFacet.setClockSpeed("1 GHz");
		cpuFacet.setModel("Opteron");
		cpuFacet.setVendor("AMD");

		FacetManagement facetManagement = new FacetManagement();
		facetManagement.setJson(ElementMapper.marshal(cpuFacet));
		facetManagement.setElementType(CPUFacet.NAME);
		
		String json = facetManagement.create();
		logger.debug("Created : {}", json);

		CPUFacet createdCPUFacet = ElementMapper.unmarshal(CPUFacet.class, json);
		UUID uuid = createdCPUFacet.getUUID();
		
		facetManagement = new FacetManagement();
		facetManagement.setUUID(uuid);
		
		String readJson = facetManagement.read().toString();
		logger.debug("Read : {}", readJson);

		/* ------------------------------------------------------------------ * /

		logger.debug("Switching to another scope");
		ContextTest.setContextByName(PARENT_DEFAULT_TEST_SCOPE);
		try {
			facetManagement = new FacetManagement();
			facetManagement.setUUID(uuid);
			readJson = facetManagement.read().toString();
			logger.debug("You should not be able to read Facet with UUID {}",
					uuid);
			throw new Exception(
					"You should not be able to read Facet with UUID " + uuid);
		} catch (FacetAvailableInAnotherContextException e) {
			logger.debug("Good the facet created in the default context is not visible in an alternative context");
		}

		/* ---------------- entering hierarchic mode * /
		
		ContextUtility.getHierarchicalMode().set(true);
		
		facetManagement = new FacetManagement();
		facetManagement.setUUID(uuid);
		readJson = facetManagement.read().toString();
		logger.debug("You should be able to read it {}", readJson);
		
		ContextUtility.getHierarchicalMode().set(false);
		
		/* ---------------- leaving hierarchic mode * /
		
		cpuFacet.setAdditionalProperty("My", "Test");

		try {
			facetManagement = new FacetManagement();
			facetManagement.setUUID(uuid);
			facetManagement.setJson(ElementMapper.marshal(cpuFacet));
			readJson = facetManagement.update();
			logger.debug("You should not be able to update Facet with UUID {}",
					uuid);
			throw new Exception(
					"You should not be able to read Facet with UUID " + uuid);
		} catch (FacetAvailableInAnotherContextException e) {
			logger.debug("Good the Facet created in the default context cannot be updated in an alternative context");
		}

		/* ------------------------------------------------------------------ * /

		logger.debug("Setting back default scope");
		ContextTest.setContextByName(DEFAULT_TEST_SCOPE);

		facetManagement = new FacetManagement();
		facetManagement.setUUID(uuid);
		facetManagement.setJson(ElementMapper.marshal(cpuFacet));
		readJson = facetManagement.update();
		logger.debug("Updated : {}", readJson);

		facetManagement = new FacetManagement();
		facetManagement.setUUID(uuid);
		readJson = facetManagement.read().toString();
		logger.debug("Read Updated : {}", readJson);

		facetManagement = new FacetManagement();
		facetManagement.setUUID(uuid);
		
		facetManagement.delete();
	}
	*/
	
	@Test
	public void testAddResourceToContext() throws Exception {
		EService eService = createEService();
		try {
			addToContextThenTestIfBehaveProperly(eService, ALTERNATIVE_TEST_SCOPE);
		} catch (Exception e) {
			throw e;
		} finally {
			deleteResource(eService);
		}
	}
	
	
	
	@Test
	public void testCreateEServiceHostingNode() throws Exception {
		EService eService = createEService();
		
		Map<UUID, IdentifiableElement> eServiceInstances = new HashMap<>();
		eServiceInstances.put(eService.getID(), eService);
		for(ConsistsOf<?,?> consistsOf : eService.getConsistsOf()) {
			eServiceInstances.put(consistsOf.getID(), consistsOf);
			eServiceInstances.put(consistsOf.getTarget().getID(), consistsOf.getTarget());
		}
		
		HostingNode hostingNode = createHostingNode(eService);
		UUID hostingNodeUUID = hostingNode.getID();
		
		Map<UUID, IdentifiableElement> hostingNodeInstances = new HashMap<>();
		hostingNodeInstances.put(hostingNodeUUID, hostingNode);
		
		for(ConsistsOf<?,?> consistsOf : hostingNode.getConsistsOf()) {
			hostingNodeInstances.put(consistsOf.getID(), consistsOf);
			hostingNodeInstances.put(consistsOf.getTarget().getID(), consistsOf.getTarget());
		}
		
		/* ------------------------------------------------------------------ */
		
		String targetContextFullName = ALTERNATIVE_TEST_SCOPE;
		
		ResourceManagement rm = (ResourceManagement) ERManagementUtility.getERManagement(Resource.NAME);
		String all = rm.all(true);
		List<Resource> allResources = ElementMapper.unmarshalList(Resource.class, all);
		Assert.assertTrue(allResources.size()==2);
		
		addToContextThenTestIfBehaveProperly(hostingNode, targetContextFullName);
		
		deleteResource(hostingNode);
		
		rm = (ResourceManagement) ERManagementUtility.getERManagement(Resource.NAME);
		all = rm.all(true);
		allResources = ElementMapper.unmarshalList(Resource.class, all);
		Assert.assertTrue(allResources.size()==0);
	}
	
	@Test
	public void testAddAndRemoveFromContext() throws Exception {
		// testAddAndRemoveFromContext(RemoveConstraint.cascade);
		
		//testAddAndRemoveFromContext(RemoveConstraint.cascadeWhenOrphan);
	}
	
	/*
	 * The code does not support RemoveConstraint.keep
	 */
	protected void testAddAndRemoveFromContext(RemoveConstraint removeConstraint) throws Exception {
		if(removeConstraint == RemoveConstraint.keep) {
			throw new Exception("This test is not designed to test a RemoveConstraint.keep");
		}
		
		/* Creating EService */
		EService eService = createEService();
		
		/* Creating HostingNode */
		HostingNode hostingNode = createHostingNode(eService, removeConstraint, DeleteConstraint.cascade);

		@SuppressWarnings("unchecked")
		Activates<HostingNode, EService> activates = (Activates<HostingNode, EService>) getOutcomingIsRelatedTo(hostingNode).get(0);
		
		try {
		
			/* ------------------------------------------------------------------ */
			
			// Adding Activated to ALTERNATIVE_TEST_SCOPE
			
			IsRelatedToManagement isRelatedToManagement = getIsRelatedToManagement(activates);
			addToContextThenTestIfBehaveProperly(activates, ALTERNATIVE_TEST_SCOPE);
			
			/* ------------------------------------------------------------------ */
			
			
			// Removing HostingNode from initial context
			ContextTest.setContextByName(DEFAULT_TEST_SCOPE);
			
			removeFromContextThenTestIfBehaveProperly(hostingNode);
			
			/* 
			 * The cascading MUST remove the relation and the target so that
			 * I MUST not be able to read Activates relation and EService
			 */
			
			ResourceManagement resourceManagement = getResourceManagement(hostingNode);
			try {
				resourceManagement.read();
				String error = "{} with UUID {} should not be visible.".formatted(HostingNode.NAME, hostingNode.getID());
				logger.trace(error);
				throw new Exception(error);
			}catch (ResourceAvailableInAnotherContextException e) {
				// OK
			}
			
			isRelatedToManagement = getIsRelatedToManagement(activates);
			try {
				isRelatedToManagement.read();
				String error = "{} with UUID {} should not be visible.".formatted(Activates.NAME, activates.getID());
				logger.trace(error);
				throw new Exception(error);
			}catch (RelationAvailableInAnotherContextException e) {
				// OK
			}
			
			resourceManagement = getResourceManagement(eService);
			try {
				resourceManagement.read();
				String error = "{} with UUID {} should not be visible.".formatted(EService.NAME, eService.getID());
				logger.trace(error);
				throw new Exception(error);
			}catch (ResourceAvailableInAnotherContextException e) {
				// OK
			}
			
			/* ------------------------------------------------------------------ */
			
			// The Instances MUST be still available in ALTERNATIVE_TEST_SCOPE
			ContextTest.setContextByName(ALTERNATIVE_TEST_SCOPE);
			
			resourceManagement = getResourceManagement(hostingNode);
			resourceManagement.read();
			
			isRelatedToManagement = getIsRelatedToManagement(activates);
			isRelatedToManagement.read();
			
			resourceManagement = getResourceManagement(eService);
			resourceManagement.read();
		
		} finally {
			
			// Removing HostingNode MUST delete Activates and EService due to propagationConstraint
			ResourceManagement resourceManagement = getResourceManagement(hostingNode);
			resourceManagement.delete();
			
			resourceManagement = getResourceManagement(hostingNode);
			
			try {
				resourceManagement.read();
				String error = "{} with UUID {} should not be found.".formatted(HostingNode.NAME, hostingNode.getID());
				logger.trace(error);
				throw new Exception(error);
			}catch (ResourceNotFoundException e) {
				// OK
			}
			
			IsRelatedToManagement isRelatedToManagement = getIsRelatedToManagement(activates);
			try {
				isRelatedToManagement.read();
				String error = "{} with UUID {} should not be found.".formatted(Activates.NAME, activates.getID());
				logger.trace(error);
				throw new Exception(error);
			}catch (IsRelatedToNotFoundException e) {
				// OK
			}
			
			resourceManagement = getResourceManagement(eService);
			try {
				resourceManagement.read();
				String error = "{} with UUID {} should not be found.".formatted(EService.NAME, eService.getID());
				logger.trace(error);
				throw new Exception(error);
			}catch (ResourceNotFoundException e) {
				// OK
			}
		}
	}
	
}
