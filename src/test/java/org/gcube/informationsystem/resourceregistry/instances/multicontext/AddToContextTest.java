package org.gcube.informationsystem.resourceregistry.instances.multicontext;

import java.net.URI;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.gcube.informationsystem.model.impl.properties.PropagationConstraintImpl;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint.AddConstraint;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint.RemoveConstraint;
import org.gcube.informationsystem.resourceregistry.api.exceptions.AvailableInAnotherContextException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.NotFoundException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.types.SchemaViolationException;
import org.gcube.informationsystem.resourceregistry.contexts.ContextUtility;
import org.gcube.informationsystem.resourceregistry.contexts.entities.ContextManagement;
import org.gcube.informationsystem.resourceregistry.instances.model.entities.FacetManagement;
import org.gcube.informationsystem.resourceregistry.instances.model.relations.ConsistsOfManagement;
import org.gcube.informationsystem.resourceregistry.instances.model.relations.IsRelatedToManagement;
import org.gcube.informationsystem.resourceregistry.rest.requests.RequestUtility;
import org.gcube.informationsystem.serialization.ElementMapper;
import org.gcube.resourcemanagement.model.impl.entities.facets.ContactFacetImpl;
import org.gcube.resourcemanagement.model.impl.entities.facets.CoverageFacetImpl;
import org.gcube.resourcemanagement.model.impl.entities.facets.IdentifierFacetImpl;
import org.gcube.resourcemanagement.model.impl.entities.resources.DatasetImpl;
import org.gcube.resourcemanagement.model.impl.properties.ValueSchemaImpl;
import org.gcube.resourcemanagement.model.impl.relations.consistsof.HasOwnerImpl;
import org.gcube.resourcemanagement.model.impl.relations.consistsof.HasSpatialCoverageImpl;
import org.gcube.resourcemanagement.model.impl.relations.consistsof.IsIdentifiedByImpl;
import org.gcube.resourcemanagement.model.impl.relations.isrelatedto.IsCorrelatedToImpl;
import org.gcube.resourcemanagement.model.reference.entities.facets.ContactFacet;
import org.gcube.resourcemanagement.model.reference.entities.facets.CoverageFacet;
import org.gcube.resourcemanagement.model.reference.entities.facets.IdentifierFacet;
import org.gcube.resourcemanagement.model.reference.entities.facets.IdentifierFacet.IdentificationType;
import org.gcube.resourcemanagement.model.reference.entities.facets.SoftwareFacet;
import org.gcube.resourcemanagement.model.reference.entities.resources.Dataset;
import org.gcube.resourcemanagement.model.reference.entities.resources.EService;
import org.gcube.resourcemanagement.model.reference.properties.ValueSchema;
import org.gcube.resourcemanagement.model.reference.relations.consistsof.HasCoverage;
import org.gcube.resourcemanagement.model.reference.relations.consistsof.HasOwner;
import org.gcube.resourcemanagement.model.reference.relations.consistsof.IsIdentifiedBy;
import org.gcube.resourcemanagement.model.reference.relations.isrelatedto.IsCorrelatedTo;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class AddToContextTest extends MultiContextTest {
	
	private static Logger logger = LoggerFactory.getLogger(AddToContextTest.class);
	
	@Test
	public void testCreateEServiceAndRemoveFromContextRequiredFacet() throws Exception {
		EService eService = createEService();
		
		@SuppressWarnings("unchecked")
		IsIdentifiedBy<EService, SoftwareFacet> isIdentifiedBy = (IsIdentifiedBy<EService, SoftwareFacet>) eService.getConsistsOf(IsIdentifiedBy.class).get(0);
		ConsistsOfManagement consistsOfManagement = new ConsistsOfManagement();
		consistsOfManagement.setElementType(IsIdentifiedBy.NAME);
		consistsOfManagement.setUUID(isIdentifiedBy.getID());
		
		try {
			consistsOfManagement.removeFromContext(ContextUtility.getCurrentRequestEnvironment().getUUID());
			throw new Exception("You should not be able to delete a mandatory ConsistsOf");
		}catch (SchemaViolationException e) {
			// As expected
		}catch (Exception e) {
			deleteResource(eService);
			throw e;
		}
		
		SoftwareFacet softwareFacet = isIdentifiedBy.getTarget();
		FacetManagement facetManagement = new FacetManagement();
		facetManagement.setElementType(SoftwareFacet.NAME);
		facetManagement.setUUID(softwareFacet.getID());
		
		try {
			facetManagement.removeFromContext(ContextUtility.getCurrentRequestEnvironment().getUUID());
			throw new Exception("You should not be able to delete a mandatory Facet");
		}catch (SchemaViolationException e) {
			// As expected
		}catch (Exception e) {
			deleteResource(eService);
			throw e;
		}
		
		deleteResource(eService);
		
	}
	
	protected Dataset createDataset(String id, String uuidString) throws Exception {
		Dataset dataset = new DatasetImpl();
		dataset.setID(UUID.fromString(uuidString));
		
		IdentifierFacet identifierFacet = new IdentifierFacetImpl();
		identifierFacet.setValue(id);
		identifierFacet.setIdentificationType(IdentificationType.STRING);
		identifierFacet.setPersistent(false);
		IsIdentifiedBy<Dataset, IdentifierFacet> isIdentifiedBy = new IsIdentifiedByImpl<>(dataset, identifierFacet);
		dataset.addFacet(isIdentifiedBy);
		
		ContactFacet contactFacet = new ContactFacetImpl();
		contactFacet.setTitle("Dr.");
		contactFacet.setName("Luca");
		contactFacet.setSurname("Frosini");
		contactFacet.setEMail("luca.frosini@d4science.org");
		HasOwner<Dataset, ContactFacet> hasOwner = new HasOwnerImpl<>(dataset, contactFacet);
		dataset.addFacet(hasOwner);
		
		CoverageFacet coverageFacet = new CoverageFacetImpl();
		ValueSchema coverage = new ValueSchemaImpl();
		coverage.setValue("Test");
		coverage.setSchema(new URI("String"));
		coverageFacet.setCoverage(coverage);
		HasCoverage<Dataset, CoverageFacet> hasCoverage = new HasSpatialCoverageImpl<Dataset, CoverageFacet>(dataset, coverageFacet);
		dataset.addFacet(hasCoverage);
		
		try {
			deleteResource(dataset);
		}catch (NotFoundException e) {
			// OK
			logger.trace("OK");
		}
		
		return createResource(dataset);
	}
	
	protected PropagationConstraint getPropagationConstraint() {
		PropagationConstraint propagationConstraint = new PropagationConstraintImpl();
		propagationConstraint.setAddConstraint(AddConstraint.propagate);
		propagationConstraint.setRemoveConstraint(RemoveConstraint.cascade);
		return propagationConstraint;
	}
	
	@SuppressWarnings("unchecked")
	protected IsCorrelatedTo<Dataset, Dataset> createIsCorrelatedTo(Dataset source, Dataset target, String uuid) throws Exception {
		PropagationConstraint propagationConstraint = getPropagationConstraint();
		IsCorrelatedTo<Dataset, Dataset> isCorrelatedTo = new IsCorrelatedToImpl<Dataset, Dataset>(source, target, propagationConstraint);
		isCorrelatedTo.setID(UUID.fromString(uuid));
		
		IsRelatedToManagement isRelatedToManagement = getIsRelatedToManagement(isCorrelatedTo);
		String json = isRelatedToManagement.create();
		
		isCorrelatedTo = ElementMapper.unmarshal(isCorrelatedTo.getClass(), json);
		
		return isCorrelatedTo;
	}
	
		
	protected void checkNumerOfContext(Dataset dataset, int expectedContextSize) throws Exception {
		setContextByName(GCUBE);
		
		RequestUtility.getRequestInfo().get().setHierarchicalMode(true);
		RequestUtility.getRequestInfo().get().setIncludeContexts(true);
		
		String json = getResourceManagement(dataset).read();
		logger.trace("Resource with contexts in HierarchicalMode from server is {}", json);
		Dataset r1 = ElementMapper.unmarshal(Dataset.class, json);
		logger.trace("Resource with contexts in HierarchicalMode with Jackson is {}", r1.toString());
		
		Map<UUID,String> contextsR1 = r1.getContexts();
		Set<String> contextsR1Fullname = new HashSet<>(contextsR1.values());
		logger.info("Contexts of {} with UUID {} and Id {} are {}", Dataset.NAME, r1.getID(), ((IdentifierFacet)r1.getIdentificationFacets().get(0)).getValue(), contextsR1Fullname);
		Assert.assertTrue(contextsR1.size()==expectedContextSize);
		
		RequestUtility.getRequestInfo().get().setHierarchicalMode(false);
		RequestUtility.getRequestInfo().get().setIncludeContexts(false);

	}
	
	@Test
	public void testAddToContextFromDifferentSourceContext() throws Exception {
		setContextByName(GCUBE);
		
		/* Needed to init context cache */
		ContextManagement contextManagement = new ContextManagement();
		contextManagement.all(false);
		
		Dataset datasetR1 = createDataset("R1", "da111111-dada-1111-1111-111111111111");
		Dataset datasetR2 = null;
		Dataset datasetR3 = null;
		try {
			addToContextThenTestIfBehaveProperly(datasetR1, false, DEVNEXT);
			addToContextThenTestIfBehaveProperly(datasetR1, false, DEVSEC);
			
			setContextByName(DEVNEXT);
			datasetR2 = createDataset("R2", "da222222-dada-2222-2222-222222222222");
			IsCorrelatedTo<Dataset, Dataset> isCorrelatedToR1R2 = createIsCorrelatedTo(datasetR1, datasetR2, "da111111-1111-aaaa-2222-222222222222");
			
			getResourceManagement(datasetR1).exists();
			getIsRelatedToManagement(isCorrelatedToR1R2).exists();
			getResourceManagement(datasetR2).exists();
			
			
			setContextByName(DEVSEC);
			datasetR3 = createDataset("R3", "da333333-dada-3333-3333-333333333333");
			IsCorrelatedTo<Dataset, Dataset> isCorrelatedToR1R3 = createIsCorrelatedTo(datasetR1, datasetR3, "da111111-1111-aaaa-3333-333333333333");
			
			getResourceManagement(datasetR1).exists();
			getIsRelatedToManagement(isCorrelatedToR1R3).exists();
			getResourceManagement(datasetR3).exists();
			
			
			try {
				getIsRelatedToManagement(isCorrelatedToR1R2).exists();
			}catch (AvailableInAnotherContextException e) {
				// OK
				logger.trace("As expected");
			}
			
			try {
				getResourceManagement(datasetR2).exists();
			}catch (AvailableInAnotherContextException e) {
				// OK
				logger.trace("As expected");
			}
			
			
			
			setContextByName(DEVNEXT);
			try {
				getIsRelatedToManagement(isCorrelatedToR1R3).exists();
			}catch (AvailableInAnotherContextException e) {
				// OK
				logger.trace("As expected");
			}
			
			try {
				getResourceManagement(datasetR3).exists();
			}catch (AvailableInAnotherContextException e) {
				// OK
				logger.trace("As expected");
			}
			
			
			
			setContextByName(GCUBE);
			getResourceManagement(datasetR1).exists();
			try {
				getIsRelatedToManagement(isCorrelatedToR1R2).exists();
			}catch (AvailableInAnotherContextException e) {
				// OK
				logger.trace("As expected");
			}
			
			try {
				getResourceManagement(datasetR2).exists();
			}catch (AvailableInAnotherContextException e) {
				// OK
				logger.trace("As expected");
			}
			
			try {
				getIsRelatedToManagement(isCorrelatedToR1R3).exists();
			}catch (AvailableInAnotherContextException e) {
				// OK
				logger.trace("As expected");
			}
			
			try {
				getResourceManagement(datasetR3).exists();
			}catch (AvailableInAnotherContextException e) {
				// OK
				logger.trace("As expected");
			}
			
			checkNumerOfContext(datasetR1, 3);
			checkNumerOfContext(datasetR2, 1); 
			checkNumerOfContext(datasetR3, 1);
			
			setContextByName(DEVNEXT);
			addToContextThenTestIfBehaveProperly(datasetR1, false, NEXTNEXT);
			checkNumerOfContext(datasetR1, 4);
			checkNumerOfContext(datasetR2, 2); 
			checkNumerOfContext(datasetR3, 1);
			
			setContextByName(DEVSEC);
			addToContextThenTestIfBehaveProperly(datasetR1, false, DEVVRE);
			checkNumerOfContext(datasetR1, 5);
			checkNumerOfContext(datasetR2, 2); 
			checkNumerOfContext(datasetR3, 2);
			
		} catch (Exception e) {
			logger.error("", e);
			throw e;
		}finally {
			
			RequestUtility.getRequestInfo().get().setHierarchicalMode(false);
			RequestUtility.getRequestInfo().get().setIncludeContexts(false);
			
			setContextByName(GCUBE);
			deleteResource(datasetR1);
				
			if(datasetR2!=null) {
				setContextByName(DEVNEXT);
				deleteResource(datasetR2);
			}
				
			if(datasetR3!=null) {
				setContextByName(DEVSEC);
				deleteResource(datasetR3);
			}
			
		}
	}
	
//	@Test
//	public void testAddToContext() throws Exception {
//		ContextCache contextCache = ContextCache.getInstance();
//		HostingNode hostingNode = new HostingNodeImpl();
//		hostingNode.setUUID(UUID.fromString("a87bb07e-5320-4fd8-a48d-bf3cc55756c4"));
//		ResourceManagement resourceManagement = getResourceManagement(hostingNode);
//		resourceManagement.setDryRun(false);
//		UUID contextUUID = ContextUtility.getInstance().getRequestEnvironmentByContextFullName("/gcube/devsec").getUUID();
//		resourceManagement.addToContext(contextUUID);
//	}
	
}
