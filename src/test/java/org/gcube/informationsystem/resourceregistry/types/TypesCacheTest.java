package org.gcube.informationsystem.resourceregistry.types;

import java.util.List;

import org.gcube.informationsystem.base.reference.AccessType;
import org.gcube.informationsystem.types.TypeMapper;
import org.gcube.informationsystem.types.reference.Type;
import org.gcube.resourcemanagement.model.reference.entities.resources.RunningPlugin;
import org.gcube.resourcemanagement.model.reference.entities.resources.Service;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.orientechnologies.orient.core.metadata.schema.OClass;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class TypesCacheTest {
	
	private static Logger logger = LoggerFactory.getLogger(TypesCacheTest.class);
	
	protected CachedType<?> getCachedType(TypesCache typesCache, String typeName) throws Exception {
		CachedType<?> cachedType = typesCache.getCachedType(typeName);
		OClass oClass = cachedType.getOClass();
		AccessType accessType = cachedType.getAccessType();
		Type type = cachedType.getType();
		logger.debug("{} ({}) : {}", oClass.toString(), accessType.toString(), TypeMapper.serializeTypeDefinition(type));
		return cachedType;
	}
	
	public void testCacheType(String typeName) throws Exception {
		
		TypesCache typesCache = TypesCache.getInstance();
		CachedType<?> cachedType = getCachedType(typesCache, typeName);
		
		List<String> superTypes = cachedType.getSuperTypes();
		logger.debug("{} typeSuperTypes are {}", typeName, superTypes);
		for(String superType : superTypes) {
			getCachedType(typesCache, superType);
		}
		
		List<String> subTypes = cachedType.getSubTypes();
		logger.debug("{} subTypes are {}", typeName, subTypes);
		for(String subType : subTypes) {
			getCachedType(typesCache, subType);
		}
		
	}
	
	@Test
	public void test() throws Exception {
		testCacheType(Service.NAME);
		testCacheType(RunningPlugin.NAME);
	}
	
}
