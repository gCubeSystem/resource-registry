/**
 * 
 */
package org.gcube.informationsystem.resourceregistry.types;

import java.util.List;
import java.util.Map;

import org.gcube.informationsystem.base.reference.AccessType;
import org.gcube.informationsystem.base.reference.Element;
import org.gcube.informationsystem.base.reference.properties.PropertyElement;
import org.gcube.informationsystem.contexts.reference.entities.Context;
import org.gcube.informationsystem.model.reference.entities.Entity;
import org.gcube.informationsystem.model.reference.entities.Facet;
import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.model.reference.properties.Encrypted;
import org.gcube.informationsystem.model.reference.properties.Metadata;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint;
import org.gcube.informationsystem.model.reference.properties.Property;
import org.gcube.informationsystem.model.reference.relations.ConsistsOf;
import org.gcube.informationsystem.model.reference.relations.IsRelatedTo;
import org.gcube.informationsystem.model.reference.relations.Relation;
import org.gcube.informationsystem.resourceregistry.ContextTest;
import org.gcube.informationsystem.resourceregistry.api.exceptions.types.SchemaAlreadyPresentException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.types.SchemaException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.types.SchemaNotFoundException;
import org.gcube.informationsystem.types.TypeMapper;
import org.gcube.informationsystem.types.reference.Type;
import org.gcube.informationsystem.types.reference.relations.RelationType;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.reference.entities.facets.AccessPointFacet;
import org.gcube.resourcemanagement.model.reference.entities.facets.ContactFacet;
import org.gcube.resourcemanagement.model.reference.entities.resources.Actor;
import org.gcube.resourcemanagement.model.reference.entities.resources.EService;
import org.gcube.resourcemanagement.model.reference.entities.resources.RunningPlugin;
import org.gcube.resourcemanagement.model.reference.entities.resources.Service;
import org.gcube.resourcemanagement.model.reference.properties.ValueSchema;
import org.gcube.resourcemanagement.model.reference.relations.consistsof.HasVolatileMemory;
import org.gcube.resourcemanagement.model.reference.relations.isrelatedto.Hosts;
import org.gcube.resourcemanagement.model.reference.relations.isrelatedto.Uses;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class SchemaManagementImplTest extends ContextTest {

	private static Logger logger = LoggerFactory
			.getLogger(SchemaManagementImplTest.class);

	@Test
	public void registerPropertyTypeSchema() throws Exception {
		Class<? extends PropertyElement> clz = Metadata.class;
		String json = TypeMapper.serializeType(clz);
		logger.debug(json);
		//new SchemaManagementImpl().create(json, AccessType.PROPERTY);
	}

	@Test
	public void getPropertyTypeSchema() throws Exception {
		TypeManagement typeManagement = new TypeManagement();
		typeManagement.setTypeName(Property.NAME);
		List<Type> types = typeManagement.read(false);
		String json = TypeMapper.serializeTypeDefinitions(types);
		logger.debug(json);
	}

	@Test
	public void serializeTypeDefinition() throws Exception {
		Class<? extends Resource> clz = Service.class;
		String json = TypeMapper.serializeType(clz);
		logger.debug(json);
		Type t = TypeMapper.deserializeTypeDefinition(json);
		String typeJson = TypeMapper.serializeTypeDefinition(t);
		logger.debug(typeJson);
	}
	
	@Test
	public void registerFacetSchema() throws Exception {
		Class<? extends Facet> clz = ContactFacet.class;
		String json = TypeMapper.serializeType(clz);
		TypeMapper.deserializeTypeDefinition(json);
	}

	@Test
	public void getFacetSchema() throws Exception {
		TypeManagement typeManagement = new TypeManagement();
		typeManagement.setTypeName(ContactFacet.NAME);
		List<Type> types = typeManagement.read(false);
		String json = TypeMapper.serializeTypeDefinitions(types);
		logger.info(json);
		List<Type> typeDefinitions = TypeMapper.deserializeTypeDefinitions(json);
		logger.info("{}", typeDefinitions);
		
	}

	@Test
	public void registerEntityTypeSchema() throws Exception {
		Class<? extends Entity> clz = Entity.class;
		String json = TypeMapper.serializeType(clz);
		logger.trace(json);
		// new SchemaManagementImpl().registerEntitySchema(json);
	}

	@Test
	public void registerResourceSchema() throws Exception {
		Class<? extends Resource> clz = Resource.class;
		String json = TypeMapper.serializeType(clz);
		logger.info(json);
	}

	@Test
	public void getResourceSchema() throws Exception {
		TypeManagement typeManagement = new TypeManagement();
		typeManagement.setTypeName(Actor.NAME);
		List<Type> types = typeManagement.read(false);
		String json = TypeMapper.serializeTypeDefinitions(types);
		logger.trace(json);
	}

	@Test
	public void registerRelation() throws Exception {
		Class<? extends Element> clz = Hosts.class;
		String json = TypeMapper.serializeType(clz);
		logger.trace(json);
	}
	
	@Test
	public void getResourceTypes() throws Exception {
		logger.debug("\n\n\n");

		boolean includeSubTypes = true;

		TypeManagement typeManagement = new TypeManagement();
		typeManagement.setTypeName(Resource.NAME);
		List<Type> types = typeManagement.read(includeSubTypes);
		String list = TypeMapper.serializeTypeDefinitions(types);
		logger.info("{} list : {}", Resource.NAME, list);
	}
	
	@Test
	public void getFacetTypes() throws Exception {
		logger.debug("\n\n\n");

		boolean includeSubTypes = true;

		TypeManagement typeManagement = new TypeManagement();
		typeManagement.setTypeName(Facet.NAME);
		List<Type> types = typeManagement.read(includeSubTypes);
		String list = TypeMapper.serializeTypeDefinitions(types);
		logger.info("{} list : {}", Facet.NAME, list);
	}
		
	@Test
	public void getConsistsOfTypes() throws Exception {
		logger.debug("\n\n\n");

		boolean includeSubTypes = true;

		TypeManagement typeManagement = new TypeManagement();
		typeManagement.setTypeName(ConsistsOf.NAME);
		List<Type> types = typeManagement.read(includeSubTypes);
		String list = TypeMapper.serializeTypeDefinitions(types);
		logger.info("{} list : {}", ConsistsOf.NAME, list);
	}
	
	@Test
	public void getIsRelatedToTypes() throws Exception {
		logger.debug("\n\n\n");

		boolean includeSubTypes = true;

		TypeManagement typeManagement = new TypeManagement();
		typeManagement.setTypeName(IsRelatedTo.NAME);
		List<Type> types = typeManagement.read(includeSubTypes);
		String list = TypeMapper.serializeTypeDefinitions(types);
		logger.info("{} list : {}", IsRelatedTo.NAME, list);

	}
	
	
	@Test
	public void getInvalidTypeDefinition() throws Exception {
		logger.debug("\n\n\n");

		boolean includeSubTypes = true;

		TypeManagement typeManagement = new TypeManagement();
		try {
			typeManagement.setTypeName(PropertyElement.NAME);
			typeManagement.read(includeSubTypes);
			throw new Exception("Should not be allowed to get " + PropertyElement.NAME + " schema");
		} catch (SchemaException e) {
			
		}
		
		try {
			typeManagement = new TypeManagement();
			typeManagement.setTypeName(Entity.NAME);
			typeManagement.read(includeSubTypes);
			throw new Exception("Should not be allowed to get " + Entity.NAME + " schema");
		} catch (SchemaException e) {
			
		}
		
		try {
			typeManagement = new TypeManagement();
			typeManagement.setTypeName(Relation.NAME);
			typeManagement.read(includeSubTypes);
			throw new Exception("Should not be allowed to get " + Relation.NAME + " schema");
		} catch (SchemaException e) {
			
		}

	}
	
	
	protected String create(Class<? extends Element> clz)
			throws SchemaAlreadyPresentException, SchemaException, Exception {
		TypeManagement typeManagement = new TypeManagement();
		typeManagement.setTypeAndTypeName(clz);
		String ret = typeManagement.create();
		logger.debug(ret);
		return ret;
	}
	
	protected String update(Class<? extends Element> clz, boolean skipVersionCheckOnUpdate)
			throws SchemaAlreadyPresentException, SchemaException, SchemaNotFoundException, Exception {
		TypeManagement typeManagement = new TypeManagement();
		typeManagement.setTypeAndTypeName(clz);
		typeManagement.setSkipVersionCheckOnUpdate(skipVersionCheckOnUpdate);
		String ret = typeManagement.update();
		logger.debug(ret);
		return ret;
	}
	
	@Test(expected=SchemaAlreadyPresentException.class)
	public void createPropertyType() throws Exception {
		create(ValueSchema.class);
	}
	
	@Test(expected=SchemaAlreadyPresentException.class)
	public void createEncryptedType() throws Exception {
		create(Encrypted.class);
	}
	
	@Test(expected=SchemaAlreadyPresentException.class)
	public void createContextType() throws Exception {
		create(Context.class);
	}
	
	@Test(expected=SchemaAlreadyPresentException.class)
	public void createFacetType() throws Exception {
		create(AccessPointFacet.class);
	}
	
	@Test(expected=SchemaAlreadyPresentException.class)
	public void createResourceType() throws Exception {
		create(EService.class);
		create(RunningPlugin.class);
	}
	
	// @Test(expected=SchemaAlreadyPresentException.class)
	public void createRelationTypeDefinitionType() throws Exception {
		create(RelationType.class);
	}
	
	@Test(expected=SchemaAlreadyPresentException.class)
	public void createIsRelatedToType() throws Exception {
		create(Uses.class);
	}
	
	@Test(expected=SchemaAlreadyPresentException.class)
	public void createConsistsOfType() throws Exception {
		create(HasVolatileMemory.class);
	}
	
	private void compareTypes(Type type, Type typeManaged) {
		Assert.assertTrue(type.getName().compareTo(typeManaged.getName())==0);
		Assert.assertTrue(type.getVersion().compareTo(typeManaged.getVersion())==0);
		Assert.assertTrue(type.getDescription().compareTo(typeManaged.getDescription())==0);
		Assert.assertTrue(type.isAbstract() == typeManaged.isAbstract());
		
		Assert.assertTrue(type.getExtendedTypes().containsAll(typeManaged.getExtendedTypes()));
		Assert.assertTrue(typeManaged.getExtendedTypes().containsAll(type.getExtendedTypes()));
		
		
		Map<Version, String> typeChangelog = type.getChangelog();
		Map<Version, String> typeManagedChangelog = typeManaged.getChangelog();
		
		Assert.assertTrue(typeChangelog.keySet().containsAll(typeManagedChangelog.keySet()));
		Assert.assertTrue(typeManagedChangelog.keySet().containsAll(typeChangelog.keySet()));
		
		for(Version typeVersion : typeChangelog.keySet()) {
			Assert.assertTrue(typeChangelog.get(typeVersion).compareTo(typeManagedChangelog.get(typeVersion))==0);
		}
		
		Assert.assertTrue(type.getProperties().containsAll(typeManaged.getProperties()));
		Assert.assertTrue(typeManaged.getProperties().containsAll(type.getProperties()));
	}
	
	@Test
	public void deleteTestFacetType() throws Exception {
		TypeManagement typeManagement = new TypeManagement();
		String typeName = TestFacet.NAME;
		typeManagement.setTypeName(typeName);
		try {
			logger.info("Going to delete {}", typeName);
			typeManagement.delete(AccessType.FACET);
			logger.info("Deleted {}", typeName);
		} catch (SchemaNotFoundException e) {
			// The test has been failed before creating the type
		} catch (Exception e) {
			throw e;
		}
	}
	
	@Test
	public void createUpdateDeleteFacetType() throws Exception {
		
		TypeManagement typeManagement = new TypeManagement();
		String typeName = TestFacet.NAME;
		typeManagement.setTypeName(typeName);
		try {
			logger.info("Going to delete {}", typeName);
			typeManagement.delete(AccessType.FACET);
			logger.info("Deleted {}", typeName);
		} catch (SchemaNotFoundException e) {
			// Removing the test type in case of a previous failure on test occurred
			// In case no failure occurred the type was not found
		} catch (Exception e) {
			throw e;
		}
		
		
		try {
			@SuppressWarnings("unchecked")
			Class<? extends Facet>[] classes = new Class[]{TestFacet.class, TestFacet1_0_1.class, TestFacet1_0_2.class};
			for(Class<? extends Facet> c : classes) {
				Type type = TypeMapper.createTypeDefinition(c); 
				String ret = null;
				if(c == TestFacet.class) {
					logger.info("Going to create {} : {}", type.getName(), TypeMapper.serializeTypeDefinition(type));
					ret = create(c);
					logger.info("Created {} : {}", type.getName(), ret);
				} else {
					logger.info("Going to update {} : {}", type.getName(), TypeMapper.serializeTypeDefinition(type));
					ret = update(c, false);
					logger.info("Updated {} : {}", type.getName(), ret);
				}
				
				
				Type typeManaged = TypeMapper.deserializeTypeDefinition(ret);
				compareTypes(type, typeManaged);
			}
			
			Type type = TypeMapper.createTypeDefinition(TestFacet.class); 
			logger.info("Going to update {} : {}", type.getName(), TypeMapper.serializeTypeDefinition(type));
			String ret = update(TestFacet.class, true);
			logger.info("Updated {} : {}", type.getName(), ret);
			Type typeManaged = TypeMapper.deserializeTypeDefinition(ret);
			compareTypes(type, typeManaged);
		
		} catch (Exception e) {
			throw e;
		} finally {
			typeManagement = new TypeManagement();
			typeName = TestFacet.NAME;
			typeManagement.setTypeName(typeName);
			try {
				logger.info("Going to delete {}", typeName);
				typeManagement.delete(AccessType.FACET);
				logger.info("Deleted {}", typeName);
			} catch (SchemaNotFoundException e) {
				// The test has been failed before creating the type
			} catch (Exception e) {
				throw e;
			}
		}
		
	}
	
	@Test(expected=SchemaNotFoundException.class)
	public void deleteFacetType() throws Exception {
		TypeManagement typeManagement = new TypeManagement();
		typeManagement.setTypeName(TestFacet.NAME);
		typeManagement.delete(AccessType.FACET);
	}
	
	// @Test
	public void updateType() throws Exception {
		Type type = TypeMapper.createTypeDefinition(PropagationConstraint.class); 
		logger.info("Going to update {} : {}", type.getName(), TypeMapper.serializeTypeDefinition(type));
		String ret = update(PropagationConstraint.class, true);
		logger.info("Updated {} : {}", type.getName(), ret);
		Type typeManaged = TypeMapper.deserializeTypeDefinition(ret);
		compareTypes(type, typeManaged);
	}
	
	
	@Test
	public void update_to_IS_Model_7_1_0_and_GCube_Model_5_1_0() throws Exception {
//		TypeManagement typeManagement = new TypeManagement();
//		String typeName = PropertyDefinition.NAME;
//		typeManagement.setTypeName(typeName);
//		logger.info("Going to delete {}", typeName);
//		typeManagement.delete(AccessType.PROPERTY_ELEMENT);
//		
//			
//		Type type = TypeMapper.createTypeDefinition(PropertyDefinition.class); 
//		logger.info("Going to create {} : {}", type.getName(), TypeMapper.serializeTypeDefinition(type));
//		String ret = create(PropertyDefinition.class);
//		logger.info("Created {} : {}", type.getName(), ret);
//		Type typeManaged = TypeMapper.deserializeTypeDefinition(ret);
//		compareTypes(type, typeManaged);
		
		
//		TypeManagement typeManagement = new TypeManagement();
//		String typeName = TemplateVariable.NAME;
//		typeManagement.setTypeName(typeName);
//		logger.info("Going to delete {}", typeName);
//		typeManagement.delete(AccessType.PROPERTY_ELEMENT);
		
			
//		Type type = TypeMapper.createTypeDefinition(TemplateVariable.class); 
//		logger.info("Going to create {} : {}", type.getName(), TypeMapper.serializeTypeDefinition(type));
//		String ret = create(TemplateVariable.class);
//		logger.info("Created {} : {}", type.getName(), ret);
//		Type typeManaged = TypeMapper.deserializeTypeDefinition(ret);
//		compareTypes(type, typeManaged);
		
//		create(AttributeProperty.class);
		
//		create(TemplateFacet.class);
		
//		update(ConfigurationTemplate.class, true);
		
//		create(Event.class);
		
//		update(EventFacet.class, true);

	}
}
