/**
 * 
 */
package org.gcube.informationsystem.resourceregistry.types;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.model.reference.entities.Facet;
import org.gcube.informationsystem.types.annotations.ISProperty;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.entities.facets.SimpleFacetImpl;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as=SimpleFacetImpl.class)
@TypeMetadata(
	name = TestFacet.NAME, 
	description = "TestFacet 1.0.2",
	version = "1.0.2"
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
@Change(version = "1.0.1", description = "Removed 'name' property, added 'title' property.")
@Change(version = "1.0.2", description = "Restored 'name' property. Set 'title' property as not mandatory and nullable.")
public interface TestFacet1_0_2 extends Facet {
	
	public static final String NAME = TestFacet.NAME; // SimpleFacet.class.getSimpleName();
	
	@ISProperty(description = "Description of name for TestFacet 1.0.2", mandatory=true, nullable=false)
	public String getName();
	
	public void setName(String name);
	
	@ISProperty(description = "Description of title for TestFacet 1.0.2", mandatory=false, nullable=true)
	public String getTitle();
	
	public void setTitle(String title);
		
}