package org.gcube.informationsystem.resourceregistry.types;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ServiceLoader;

import org.gcube.informationsystem.discovery.Discovery;
import org.gcube.informationsystem.discovery.RegistrationProvider;
import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.types.TypeMapper;
import org.gcube.resourcemanagement.model.reference.entities.resources.Actor;
import org.gcube.resourcemanagement.model.reference.entities.resources.ConcreteDataset;
import org.gcube.resourcemanagement.model.reference.entities.resources.Configuration;
import org.gcube.resourcemanagement.model.reference.entities.resources.ConfigurationTemplate;
import org.gcube.resourcemanagement.model.reference.entities.resources.Dataset;
import org.gcube.resourcemanagement.model.reference.entities.resources.EService;
import org.gcube.resourcemanagement.model.reference.entities.resources.GCubeResource;
import org.gcube.resourcemanagement.model.reference.entities.resources.HostingNode;
import org.gcube.resourcemanagement.model.reference.entities.resources.LegalBody;
import org.gcube.resourcemanagement.model.reference.entities.resources.Person;
import org.gcube.resourcemanagement.model.reference.entities.resources.Plugin;
import org.gcube.resourcemanagement.model.reference.entities.resources.RunningPlugin;
import org.gcube.resourcemanagement.model.reference.entities.resources.Schema;
import org.gcube.resourcemanagement.model.reference.entities.resources.Service;
import org.gcube.resourcemanagement.model.reference.entities.resources.Site;
import org.gcube.resourcemanagement.model.reference.entities.resources.Software;
import org.gcube.resourcemanagement.model.reference.entities.resources.VirtualMachine;
import org.gcube.resourcemanagement.model.reference.entities.resources.VirtualService;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
/**
 * This class contains a test for the discovery of resources using the Discovery class.
 * It uses the ServiceLoader to load RegistrationProvider implementations and retrieves
 * the packages to register from them. These packages are then added to the Discovery
 * instance, which discovers the resources within those packages.
 * 
 * The test verifies that the discovered resources match the expected list of resource
 * classes. It also logs the type definitions of the discovered resources for debugging
 * purposes.
 * 
 * The expected resources include:
 * - Resource
 * - Actor
 * - ConcreteDataset
 * - Configuration
 * - ConfigurationTemplate
 * - Dataset
 * - EService
 * - GCubeResource
 * - HostingNode
 * - LegalBody
 * - Person
 * - Plugin
 * - RunningPlugin
 * - Schema
 * - Service
 * - Site
 * - Software
 * - VirtualMachine
 * - VirtualService
 * 
 * The test asserts that all expected resources are found and that all found resources
 * are expected.
 * 
 * @throws Exception if any error occurs during the discovery process
 */
public class DiscoveryTest {
	
	private static Logger logger = LoggerFactory.getLogger(DiscoveryTest.class);
	
	@Test
	public void discover() throws Exception {
		ServiceLoader<? extends RegistrationProvider> regsitrationProviders = ServiceLoader
				.load(RegistrationProvider.class);
		List<Package> packages = new ArrayList<>();
		for(RegistrationProvider registrationProvider : regsitrationProviders) {
			packages.addAll(registrationProvider.getPackagesToRegister());
		}
		Package[] packagesArray = packages.stream().toArray(Package[]::new);
		
		Discovery<Resource> resourceDiscovery = new Discovery<>(Resource.class);
		Arrays.stream(packagesArray).forEach(p -> resourceDiscovery.addPackage(p));
		resourceDiscovery.discover();

		List<Class<? extends Resource>> expected = new ArrayList<>();
		expected.add(Resource.class);
		expected.add(Actor.class);
		expected.add(ConcreteDataset.class);
		expected.add(Configuration.class);
		expected.add(ConfigurationTemplate.class);
		expected.add(Dataset.class);
		expected.add(EService.class);
		expected.add(GCubeResource.class);
		expected.add(HostingNode.class);
		expected.add(LegalBody.class);
		expected.add(Person.class);
		expected.add(Plugin.class);
		expected.add(RunningPlugin.class);
		expected.add(Schema.class);
		expected.add(Service.class);
		expected.add(Site.class);
		expected.add(Software.class);
		expected.add(VirtualMachine.class);
		expected.add(VirtualService.class);
		
		List<Class<Resource>> found = resourceDiscovery.getDiscoveredElements();
		
		for (Class<Resource> resourceClass : found) {
			String typeDefinition = TypeMapper.serializeType(resourceClass);
			logger.debug(typeDefinition);
		}
		
		Assert.assertTrue(expected.containsAll(found));
		Assert.assertTrue(found.containsAll(expected));
	}
	
}
