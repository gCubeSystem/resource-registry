/**
 * 
 */
package org.gcube.informationsystem.resourceregistry.types;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.model.reference.entities.Facet;
import org.gcube.informationsystem.types.annotations.ISProperty;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.entities.facets.SimpleFacetImpl;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as=SimpleFacetImpl.class)
@TypeMetadata(
	name = TestFacet.NAME, 
	description = "TestFacet 1.0.1",
	version = "1.0.1"
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
@Change(version = "1.0.1", description = "Removed 'name' property, added 'title' property.")
public interface TestFacet1_0_1 extends Facet {
	
	public static final String NAME = TestFacet.NAME; // SimpleFacet.class.getSimpleName();
	
	@ISProperty(description = "Description of title for TestFacet 1.0.1", mandatory=true, nullable=false)
	public String getTitle();
	
	public void setTitle(String title);
		
}