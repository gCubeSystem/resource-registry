/**
 * 
 */
package org.gcube.informationsystem.resourceregistry.types;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.model.reference.entities.Facet;
import org.gcube.informationsystem.types.annotations.ISProperty;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.entities.facets.SimpleFacetImpl;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as=SimpleFacetImpl.class)
@TypeMetadata(
	name = TestFacet.NAME, 
	description = "TestFacet 1.0.0",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface TestFacet extends Facet {
	
	public static final String NAME = "TestFacet"; // SimpleFacet.class.getSimpleName();
	
	@ISProperty(description = "Description of name for TestFacet 1.0.0", mandatory=true, nullable=false)
	public String getName();
	
	public void setName(String name);
		
}