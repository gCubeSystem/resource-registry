package org.gcube.informationsystem.resourceregistry.environments;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;
import org.gcube.informationsystem.resourceregistry.environments.administration.AdminEnvironment;
import org.gcube.informationsystem.resourceregistry.environments.contexts.ContextEnvironment;
import org.gcube.informationsystem.resourceregistry.environments.contexts.ShadowContextEnvironment;
import org.gcube.informationsystem.resourceregistry.environments.queries.templates.QueryTemplateEnvironment;
import org.gcube.informationsystem.resourceregistry.environments.types.TypeEnvironment;
import org.gcube.informationsystem.utils.ReflectionUtility;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class EnvironmentsTest {
	
	private static Logger logger = LoggerFactory.getLogger(EnvironmentsTest.class);
	
	@Test
	public void testExistingSystemEnvironments() throws ResourceRegistryException {
		Map<UUID,SystemEnvironment> contexts = new HashMap<>();
		try {
			Class<?> clz = SystemEnvironment.class;
			List<Class<?>> classes = ReflectionUtility.getClassesForPackage(clz.getPackage());
			for(Class<?> c : classes) {
				if(c!=clz && clz.isAssignableFrom(c)) {
					Method m = c.getMethod("getInstance");
					SystemEnvironment instance = (SystemEnvironment) m.invoke(null);
					contexts.put(instance.getUUID(), instance);
				}
			}
		}catch (Exception e) {
			logger.error(":(",e);
		}
		
		Assert.assertTrue(contexts.size()==5);
		
		SystemEnvironment adminEnvironment = contexts.get(AdminEnvironment.getInstance().getUUID());
		Assert.assertTrue(adminEnvironment instanceof AdminEnvironment);
		
		SystemEnvironment contextEnvironment = contexts.get(ContextEnvironment.getInstance().getUUID());
		Assert.assertTrue(contextEnvironment instanceof ContextEnvironment);
		
		SystemEnvironment shadowContextEnvironment = contexts.get(ShadowContextEnvironment.getInstance().getUUID());
		Assert.assertTrue(shadowContextEnvironment instanceof ShadowContextEnvironment);
		
		SystemEnvironment queryTemplateEnvironment = contexts.get(QueryTemplateEnvironment.getInstance().getUUID());
		Assert.assertTrue(queryTemplateEnvironment instanceof QueryTemplateEnvironment);
		
		SystemEnvironment typeEnvironment = contexts.get(TypeEnvironment.getInstance().getUUID());
		Assert.assertTrue(typeEnvironment instanceof TypeEnvironment);
		
	}
	
}
