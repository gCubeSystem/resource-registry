package org.gcube.informationsystem.resourceregistry.utils;

import org.gcube.informationsystem.resourceregistry.ContextTest;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class MetadataUtilityTest extends ContextTest {
	
	private static final Logger logger = LoggerFactory.getLogger(MetadataUtilityTest.class);
	
	@Test
	public void testGetUsername() {
		String username = MetadataUtility.getUser();
		logger.info("Current User username is {}", username);
	}

}
