package org.gcube.informationsystem.resourceregistry.dbinitialization;

import org.gcube.informationsystem.base.reference.Element;
import org.gcube.informationsystem.resourceregistry.ContextTest;
import org.gcube.informationsystem.resourceregistry.environments.Environment.PermissionMode;
import org.gcube.informationsystem.resourceregistry.environments.administration.AdminEnvironment;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.orientechnologies.orient.core.db.ODatabase.ATTRIBUTES;
import com.orientechnologies.orient.core.db.document.ODatabaseDocument;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class DatabaseEnvironmentTest {
	
	private static Logger logger = LoggerFactory.getLogger(DatabaseEnvironmentTest.class);
	
	@Test
	public void createDB() throws Exception {
		ContextTest.setContextByName(ContextTest.GCUBE);
		String db = DatabaseEnvironment.DB_URI;
		logger.trace("Created DB is {}", db);
	}
	
	@Test
	public void testDateTimeFormat() throws Exception {
		ContextTest.setContextByName(ContextTest.GCUBE);
		ODatabaseDocument oDatabaseDocument = AdminEnvironment.getInstance().getDatabaseDocument(PermissionMode.WRITER);
		String dateTime = oDatabaseDocument.get(ATTRIBUTES.DATETIMEFORMAT).toString();
		Assert.assertTrue(dateTime.compareTo(Element.DATETIME_PATTERN)==0);
	}
	
	/*
	@Test
	public void generateDBKey() throws Exception {
		Properties properties = new Properties();
		InputStream input = DatabaseEnvironmentTest.class.getClassLoader().getResourceAsStream("config.properties.prod");
			
		// load a properties file
		properties.load(input);
		
		String keyFileName = properties.getProperty("DB_KEY_FILENAME");
		String keyAlgorithm = properties.getProperty("DB_KEY_ALGORITHM_VARNAME");
		
		URL url = SymmetricKey.class.getResource(keyFileName);
		File keyFile = new File("src/test/resources", keyFileName); 
		Key key = null;
		if(url!=null) {
			try {
				key = SymmetricKey.loadKeyFromFile(keyFile, keyAlgorithm);
			} catch(Exception e) {
			
			}
		}
		
		if(key==null){
			key = KeyFactory.newAESKey();
			KeySerialization.store(key, keyFile);
		}
	}
	*/
	
}
