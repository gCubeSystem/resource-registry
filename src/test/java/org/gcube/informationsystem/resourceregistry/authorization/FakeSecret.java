package org.gcube.informationsystem.resourceregistry.authorization;

import java.util.HashMap;
import java.util.Map;

import org.gcube.com.fasterxml.jackson.databind.ObjectMapper;
import org.gcube.common.security.Owner;
import org.gcube.common.security.secrets.Secret;

/**
 * The FakeSecret class is a mock implementation of the Secret class used for testing purposes.
 * It provides methods to handle fake secrets, including generating tokens and managing context and owner information.
 * 
 * <p>This class includes two constructors for creating FakeSecret instances either from a token or a FakeUser object.
 * It also overrides methods from the Secret class to provide specific behaviors for fake secrets.</p>
 * 
 * <p>Methods:</p>
 * <ul>
 *   <li>{@link #FakeSecret(String, String)} - Constructs a FakeSecret from a token and context.</li>
 *   <li>{@link #FakeSecret(FakeUser, String)} - Constructs a FakeSecret from a FakeUser and context.</li>
 *   <li>{@link #getOwner()} - Returns the owner of the secret.</li>
 *   <li>{@link #getContext()} - Returns the context of the secret.</li>
 *   <li>{@link #getHTTPAuthorizationHeaders()} - Returns HTTP authorization headers containing the fake secret token.</li>
 *   <li>{@link #isValid()} - Always returns true, indicating the secret is valid.</li>
 *   <li>{@link #isExpired()} - Always returns false, indicating the secret is not expired.</li>
 * </ul>
 * 
 * <p>Exceptions:</p>
 * <ul>
 *   <li>{@link RuntimeException} - Thrown if there is an error during the creation of the FakeSecret.</li>
 * </ul>
 * 
 * <p>Dependencies:</p>
 * <ul>
 *   <li>com.fasterxml.jackson.databind.ObjectMapper - Used for JSON serialization and deserialization.</li>
 * </ul>
 * 
 * <p>Note: This class is intended for testing purposes only and should not be used in production environments.</p>
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
public class FakeSecret extends Secret {

	protected String token;
	protected String context;
	protected Owner owner;
	
	public FakeSecret(String token, String context) {
		try {
			this.token = token;
			this.context = context;
			ObjectMapper mapper = new ObjectMapper();
			FakeUser user = mapper.readValue(token, FakeUser.class);
			this.owner = new FakeOwner(user);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	public FakeSecret(FakeUser user, String context) {
		try {
			this.context = context;
			this.owner = new FakeOwner(user);
			ObjectMapper mapper = new ObjectMapper();
			this.token = mapper.writeValueAsString(user);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Owner getOwner() {
		return owner;
	}

	@Override
	public String getContext() {
		return context;
	}

	@Override
	public Map<String, String> getHTTPAuthorizationHeaders() {
		Map<String, String> authorizationHeaders = new HashMap<>();
		authorizationHeaders.put("x-fake-secret", token);
		return authorizationHeaders;
	}

	@Override
	public boolean isValid() {
		return true;
	}

	@Override
	public boolean isExpired() {
		return false;
	}

}
