package org.gcube.informationsystem.resourceregistry.authorization;

import java.io.File;

import org.gcube.common.security.Owner;
import org.gcube.common.security.providers.SecretManagerProvider;
import org.gcube.common.security.secrets.Secret;
import org.gcube.informationsystem.resourceregistry.ContextTest;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
/**
 * FakeSecretTest is a test class that extends ContextTest to validate the functionality of processing fake user data
 * in different contexts.
 * 
 * <p>The main test method iterates over a predefined set of contexts and for each context, it retrieves the directory
 * containing fake user files. It then processes each file in the directory that ends with a ".json" extension by 
 * invoking the makeTheTest method with the current context and file name.</p>
 * 
 * <p>The makeTheTest method performs the following steps:
 * <ul>
 *   <li>Retrieves a FakeUser object from the given filename using FakeSecretUtility.</li>
 *   <li>Sets a fake secret for the user in the given context using FakeSecretUtility.</li>
 *   <li>Retrieves the Secret object from SecretManagerProvider.</li>
 *   <li>Asserts that the context of the Secret matches the given context.</li>
 *   <li>Asserts that the owner of the Secret is not null and matches the FakeUser's details (ID, first name, last name, email, and roles).</li>
 * </ul>
 * </p>
 * 
 * @throws Exception if any error occurs during the test execution.
 */
public class FakeSecretTest extends ContextTest {

    /**
     * Test method to validate the functionality of processing fake user data in different contexts.
     * 
     * @throws Exception if any error occurs during the test execution.
     * 
     * The test iterates over a predefined set of contexts and for each context, it retrieves the directory
     * containing fake user files. It then processes each file in the directory that ends with a ".json" extension
     * by invoking the makeTheTest method with the current context and file name.
     */
    @Test 
    public void test() throws Exception{
        String[] contexts = new String[] {GCUBE, DEVSEC, DEVVRE, DEVNEXT, NEXTNEXT};

        for(String context : contexts){
            File dir = FakeSecretUtility.getFakeUsersDirectory();
            File[] files = dir.listFiles();
            for (File file : files) {
                if (file.getName().endsWith(".json")) {
                    makeTheTest(context, file.getName());
                }
            }
        }
    }

    
    /**
     * This method tests the creation and validation of a secret using a fake user and context.
     * It performs the following steps:
     * 1. Retrieves a fake user from the specified filename.
     * 2. Sets a fake secret for the user with the given context.
     * 3. Retrieves the secret from the SecretManagerProvider.
     * 4. Asserts that the context of the secret matches the provided context.
     * 5. Asserts that the owner of the secret is not null.
     * 6. Asserts that the owner's ID matches the fake user's ID.
     * 7. Asserts that the owner's first name matches the fake user's first name.
     * 8. Asserts that the owner's last name matches the fake user's last name.
     * 9. Asserts that the owner's email matches the fake user's email.
     * 10. Asserts that the owner's roles contain all the roles of the fake user.
     * 11. Asserts that the fake user's roles contain all the roles of the owner.
     *
     * @param context  the context to be used for the secret
     * @param filename the filename from which to retrieve the fake user
     * @throws Exception if any error occurs during the test
     */
    protected void makeTheTest(String context, String filename) throws Exception{
        FakeUser user = FakeSecretUtility.getFakeUser(filename);
        FakeSecretUtility.setFakeSecret(user, context);
        Secret secret = SecretManagerProvider.get();
        Assert.assertTrue(secret.getContext().compareTo(context)==0);
        Owner owner = secret.getOwner();
        Assert.assertTrue(owner!=null);
        Assert.assertTrue(owner.getId().compareTo(user.getId())==0);
        Assert.assertTrue(owner.getFirstName().compareTo(user.getFirstName())==0);
        Assert.assertTrue(owner.getLastName().compareTo(user.getLastName())==0);
        Assert.assertTrue(owner.getEmail().compareTo(user.getEMail())==0);
        Assert.assertTrue(owner.getRoles().containsAll(user.getRoles()));
        Assert.assertTrue(user.getRoles().containsAll(owner.getRoles()));
    }

}
