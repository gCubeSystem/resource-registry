package org.gcube.informationsystem.resourceregistry.authorization;

import java.io.File;
import java.net.URL;

import org.gcube.com.fasterxml.jackson.databind.ObjectMapper;
import org.gcube.common.security.secrets.Secret;
import org.gcube.informationsystem.resourceregistry.ContextTest;
import org.gcube.informationsystem.resourceregistry.queries.JsonQueryTest;

/**
 * Utility class for handling fake user data and secrets for testing purposes.
 *
 * This class provides methods to retrieve directories, read fake user data from JSON files,
 * generate secrets, and set fake secrets for testing contexts.
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
public class FakeSecretUtility {

	/**
	 * Retrieves the directory containing fake user data.
	 *
	 * This method locates the "logback-test.xml" file in the classpath, 
	 * determines its parent directory, and returns a new File object 
	 * representing the "fake-users" directory within that parent directory.
	 *
	 * @return a File object representing the "fake-users" directory.
	 * @throws Exception if an error occurs while locating or accessing the file.
	 */
	public static File getFakeUsersDirectory() throws Exception {
		URL logbackFileURL = JsonQueryTest.class.getClassLoader().getResource("logback-test.xml");
		File logbackFile = new File(logbackFileURL.toURI());
		File resourcesDirectory = logbackFile.getParentFile();
		return new File(resourcesDirectory, "fake-users");
	}

	/**
	 * Retrieves a FakeUser object from a JSON file.
	 *
	 * @param filename the name of the file containing the FakeUser data
	 * @return a FakeUser object populated with data from the specified file
	 * @throws Exception if an error occurs while reading the file or parsing the JSON data
	 */
	public static FakeUser getFakeUser(String filename) throws Exception {
		ObjectMapper objectMapper = new ObjectMapper();
		File file = new File(getFakeUsersDirectory(), filename);
		FakeUser user = objectMapper.readValue(file, FakeUser.class);
		return user;
	}
	
	/**
	 * Retrieves a secret based on the provided filename and context.
	 *
	 * @param filename the name of the file containing user information
	 * @param context the context for which the secret is being retrieved
	 * @return a Secret object containing the generated secret
	 * @throws Exception if an error occurs while retrieving the secret
	 */
	public static Secret getSecret(String filename, String context) throws Exception {
		FakeUser user = getFakeUser(filename);
		FakeSecret secret = new FakeSecret(user, context);
		return secret;
	}
	
	/**
	 * Generates a secret for the given user and context.
	 *
	 * @param user the user for whom the secret is generated
	 * @param context the context in which the secret is used
	 * @return the generated secret
	 * @throws Exception if an error occurs during secret generation
	 */
	public static Secret getSecret(FakeUser user, String context) throws Exception {
		FakeSecret secret = new FakeSecret(user, context);
		return secret;
	}

	/**
	 * Sets a fake secret for testing purposes.
	 *
	 * @param filename the name of the file containing the secret
	 * @param context the context in which the secret is used
	 * @throws Exception if an error occurs while retrieving or setting the secret
	 */
	public static void setFakeSecret(String filename, String context) throws Exception {
		Secret secret = getSecret(filename, context);
		ContextTest.set(secret);
	}
	
	/**
	 * Sets a fake secret for the given user and context.
	 *
	 * @param user the fake user for whom the secret is to be set
	 * @param context the context in which the secret is to be set
	 * @throws Exception if an error occurs while setting the fake secret
	 */
	public static void setFakeSecret(FakeUser user, String context) throws Exception {
		Secret secret = getSecret(user, context);
		ContextTest.set(secret);
	}

}
