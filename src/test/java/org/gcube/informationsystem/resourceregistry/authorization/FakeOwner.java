package org.gcube.informationsystem.resourceregistry.authorization;

import java.util.ArrayList;
import org.gcube.common.security.Owner;

/**
 * The FakeOwner class is a subclass of the Owner class.
 * It is used to create an instance of an owner with the details of a FakeUser.
 * 
 * <p>This class is primarily used for testing purposes.</p>
 * 
 * @see Owner
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
public class FakeOwner extends Owner {
    
    /**
     * Constructs a new FakeOwner instance.
     *
     * @param user the FakeUser instance from which to initialize the FakeOwner.
     */
    public FakeOwner(FakeUser user) {
        super(user.getId(), user.getRoles(), 
            new ArrayList<>(), user.getEMail(),
            user.getFirstName(), user.getLastName(), 
            false, false);
    }

    
}
