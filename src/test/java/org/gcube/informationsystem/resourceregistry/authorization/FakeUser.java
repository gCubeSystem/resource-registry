package org.gcube.informationsystem.resourceregistry.authorization;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.gcube.com.fasterxml.jackson.annotation.JsonAnyGetter;
import org.gcube.com.fasterxml.jackson.annotation.JsonAnySetter;
import org.gcube.com.fasterxml.jackson.annotation.JsonIgnore;
import org.gcube.com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
/**
 * Represents a fake user with basic user information and additional properties.
 * 
 * <p>This class is used for testing purposes and includes fields for user ID, roles,
 * first name, last name, email, and any additional properties that may be needed.</p>
 * 
 * <p>Fields annotated with {@code @JsonProperty} will be included in JSON serialization
 * and deserialization, while the {@code additionalProperties} field is ignored by default
 * but can store any extra properties using {@code @JsonAnyGetter} and {@code @JsonAnySetter}.</p>
 * 
 * <p>Methods are provided to get and set each field, as well as to manage the additional
 * properties map.</p>
 * 
 * <p>Example usage:</p>
 * <pre>
 * {@code
 * FakeUser user = new FakeUser();
 * user.setId("12345");
 * user.setFirstName("John");
 * user.setLastName("Doe");
 * user.setEMail("john.doe@example.com");
 * user.setRoles(Arrays.asList("admin", "user"));
 * user.setAdditionalProperty("customField", "customValue");
 * }
 * </pre>
 * 
 * @see com.fasterxml.jackson.annotation.JsonProperty
 * @see com.fasterxml.jackson.annotation.JsonIgnore
 * @see com.fasterxml.jackson.annotation.JsonAnyGetter
 * @see com.fasterxml.jackson.annotation.JsonAnySetter
 */
public class FakeUser {
	
	@JsonProperty
	protected String id;

	@JsonProperty
	protected List<String> roles;
	
	@JsonProperty
	protected String firstName;

	@JsonProperty
	protected String lastName;

	@JsonProperty
	protected String eMail;
	
	/**
	 * Used to allow to have any additional properties
	 */
	@JsonIgnore
	protected Map<String, Object> additionalProperties;
	
	public FakeUser() {
		this.additionalProperties = new HashMap<>();
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<String> getRoles() {
		return roles;
	}

	public void setRoles(List<String> roles) {
		this.roles = roles;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEMail() {
		return eMail;
	}

	public void setEMail(String eMail) {
		this.eMail = eMail;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return additionalProperties;
	}

	public void setAdditionalProperties(Map<String, Object> additionalProperties) {
		this.additionalProperties = additionalProperties;
	}

	public Object getAdditionalProperty(String key) {
		return additionalProperties.get(key);
	}

	@JsonAnySetter
	public void setAdditionalProperty(String key, Object value) {
		this.additionalProperties.put(key, value);
	}

}
