package org.gcube.informationsystem.resourceregistry.tobereviewed;

import java.util.UUID;

import org.gcube.informationsystem.base.reference.IdentifiableElement;
import org.gcube.informationsystem.model.reference.entities.Facet;
import org.gcube.informationsystem.resourceregistry.instances.multicontext.MultiContextTest;
import org.gcube.informationsystem.resourceregistry.utils.MetadataUtility;
import org.gcube.resourcemanagement.model.impl.entities.facets.SoftwareFacetImpl;
import org.gcube.resourcemanagement.model.reference.entities.facets.SoftwareFacet;
import org.junit.Assert;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class FacetManagementTest extends MultiContextTest {
	
//	private static Logger logger = LoggerFactory.getLogger(ERManagementTest.class);
	
	public static final String GROUP = "InformationSystem";
	public static final String NAME = "resource-registry";
	public static final String VERSION = "1.0.0";
	public static final String NEW_VERSION = "2.0.0";
	
	public static SoftwareFacet getSoftwareFacet() {
		SoftwareFacet softwareFacet = new SoftwareFacetImpl();
		softwareFacet.setGroup(GROUP);
		softwareFacet.setName(NAME);
		softwareFacet.setVersion(VERSION);
		return softwareFacet;
	}
	
	public static void checkSoftwareFacetAssertion(SoftwareFacet softwareFacet, String version) {
		Assert.assertTrue(softwareFacet.getGroup().compareTo(GROUP) == 0);
		Assert.assertTrue(softwareFacet.getName().compareTo(NAME) == 0);
		Assert.assertTrue(softwareFacet.getVersion().compareTo(version) == 0);
	}
	
	public static void checkUUIDAndMetadata(IdentifiableElement er, UUID uuid, boolean create) {
		Assert.assertTrue(er.getID() != null);
		
		if(uuid != null) {
			Assert.assertTrue(er.getID().compareTo(uuid) == 0);
		}
		
		Assert.assertTrue(er.getMetadata() != null);
		
		String user = MetadataUtility.getUser();
		Assert.assertTrue(er.getMetadata().getLastUpdateBy().compareTo(user) == 0);
		
		if(create) {
			Assert.assertTrue(er.getMetadata().getCreatedBy().compareTo(user) == 0);
			Assert.assertTrue(er.getMetadata().getCreationTime().compareTo(er.getMetadata().getLastUpdateTime()) == 0);
		} else {
			Assert.assertTrue(er.getMetadata().getCreationTime().before(er.getMetadata().getLastUpdateTime()));
		}
	}
	
	public static void checkAssertion(Facet facet, UUID uuid, boolean create) {
		checkUUIDAndMetadata(facet, uuid, create);
	}
//	
//	protected <F extends Facet> F create(F facet) throws Exception {
//		FacetManagement facetManagement = new FacetManagement();
//		String facetType = Utility.getTypeName(facet);
//		facetManagement.setElementType(facetType);
//		facetManagement.setJson(ElementMapper.marshal(facet));
//		
//		String json = facetManagement.create();
//		logger.debug("Created : {}", json);
//		@SuppressWarnings("unchecked")
//		F createdFacet = (F) ElementMapper.unmarshal(facet.getClass(), json);
//		logger.debug("Unmarshalled {}", createdFacet);
//		
//		UUID uuid = facet.getUUID();
//		checkAssertion(createdFacet, uuid, true);
//		return createdFacet;
//	}
//	
//	protected <F extends Facet> F update(F facet) throws Exception {
//		FacetManagement facetManagement = new FacetManagement();
//		String facetType = Utility.getTypeName(facet);
//		facetManagement.setElementType(facetType);
//		facetManagement.setJson(ElementMapper.marshal(facet));
//		
//		String json = facetManagement.update();
//		logger.debug("Updated : {}", json);
//		@SuppressWarnings("unchecked")
//		F updatedFacet = (F) ElementMapper.unmarshal(facet.getClass(), json);
//		logger.debug("Unmarshalled {}", updatedFacet);
//		
//		UUID uuid = facet.getUUID();
//		checkAssertion(updatedFacet, uuid, false);
//		
//		return updatedFacet;
//	}
//	
//	protected <F extends Facet> F read(F facet) throws Exception {
//		UUID uuid = facet.getUUID();
//		
//		FacetManagement facetManagement = new FacetManagement();
//		String facetType = Utility.getTypeName(facet);
//		facetManagement.setElementType(facetType);
//		facetManagement.setUUID(uuid);
//		
//		String json = facetManagement.read().toString();
//		logger.debug("Read : {}", json);
//		@SuppressWarnings("unchecked")
//		F readFacet = (F) ElementMapper.unmarshal(facet.getClass(), json);
//		logger.debug("Unmarshalled {}", readFacet);
//		
//		checkAssertion(readFacet, uuid, false);
//		
//		return readFacet;
//	}
//	
//	protected <F extends Facet> void delete(F facet) throws Exception {
//		FacetManagement facetManagement = new FacetManagement();
//		String facetType = Utility.getTypeName(facet);
//		facetManagement.setElementType(facetType);
//		facetManagement.setUUID(facet.getUUID());
//		
//		facetManagement.delete();
//		
//		try {
//			read(facet);
//		} catch(FacetNotFoundException e) {
//			logger.info("Facet not found as expected");
//		}
//	}
//	
//	protected <F extends Facet> Map<UUID,JsonNode> addToContext(F facet) throws Exception {
//		FacetManagement facetManagement = new FacetManagement();
//		String facetType = Utility.getTypeName(facet);
//		facetManagement.setElementType(facetType);
//		facetManagement.setUUID(facet.getUUID());
//		
//		facetManagement.addToContext(ContextUtility.getCurrentRequestEnvironment().getUUID());
//		Map<UUID,JsonNode> affectedInstances = facetManagement.getAffectedInstances();
//		Assert.assertTrue(affectedInstances.containsKey(facet.getUUID()));
//		Assert.assertTrue(affectedInstances.size()==1);
//		
//		return affectedInstances;
//	}
//	
//	protected <F extends Facet> Map<UUID,JsonNode> removeFromContext(F facet) throws Exception {
//		FacetManagement facetManagement = new FacetManagement();
//		String facetType = Utility.getTypeName(facet);
//		facetManagement.setElementType(facetType);
//		facetManagement.setUUID(facet.getUUID());
//		
//		facetManagement.removeFromContext(ContextUtility.getCurrentRequestEnvironment().getUUID());
//		Map<UUID,JsonNode> affectedInstances = facetManagement.getAffectedInstances();
//		Assert.assertTrue(affectedInstances.containsKey(facet.getUUID()));
//		Assert.assertTrue(affectedInstances.size()==1);
//		
//		return affectedInstances;
//	}
//	
//	interface ActionFunction<F extends Facet> {
//		void call(F facet) throws Exception;
//	}
//	
//	protected <F extends Facet, C extends Exception, E extends Exception> void assertThrow(F facet, Class<C> c,
//			ActionFunction<F> action) throws Exception {
//		try {
//			action.call(facet);
//			throw new RuntimeException("Expected " + c.getName());
//		} catch(Exception e) {
//			if(c.isAssignableFrom(e.getClass())) {
//				logger.debug("As expected {} has been thrown", c.getName());
//				return;
//			}
//			throw e;
//		}
//	}
//	
//	@Test
//	public void createUpdateReadDelete() throws Exception {
//		SoftwareFacet softwareFacet = getSoftwareFacet();
//		
//		/* Testing Create */
//		softwareFacet = create(softwareFacet);
//		checkSoftwareFacetAssertion(softwareFacet, VERSION);
//		
//		/* Testing Update */
//		softwareFacet.setVersion(NEW_VERSION);
//		softwareFacet = update(softwareFacet);
//		checkSoftwareFacetAssertion(softwareFacet, NEW_VERSION);
//		
//		/* Testing Read */
//		softwareFacet = read(softwareFacet);
//		checkSoftwareFacetAssertion(softwareFacet, NEW_VERSION);
//		
//		assertThrow(softwareFacet, FacetAlreadyPresentException.class, (SoftwareFacet s) -> {
//			create(s);
//		});
//		
//		/* Testing Delete */
//		delete(softwareFacet);
//		
//		/* Testing new Create to check creation with provided UUID */
//		softwareFacet = create(softwareFacet);
//		checkSoftwareFacetAssertion(softwareFacet, NEW_VERSION);
//		
//		delete(softwareFacet);
//		
//	}
//	
//	@Test
//	public void testHierarchy() throws Exception {
//		/* Setting scope /gcube/devNext/NextNext */
//		ContextTest.setContextByName(ALTERNATIVE_TEST_SCOPE);
//		
//		SoftwareFacet softwareFacet = getSoftwareFacet();
//		
//		/* Testing Create */
//		softwareFacet = create(softwareFacet);
//		checkSoftwareFacetAssertion(softwareFacet, VERSION);
//		
//		softwareFacet = update(softwareFacet);
//		checkSoftwareFacetAssertion(softwareFacet, VERSION);
//		
//		/* Testing Read */
//		softwareFacet = read(softwareFacet);
//		checkSoftwareFacetAssertion(softwareFacet, VERSION);
//		
//		/* Setting (parent) scope /gcube/devNext */
//		ContextTest.setContextByName(DEFAULT_TEST_SCOPE);
//		
//		assertThrow(softwareFacet, FacetAvailableInAnotherContextException.class, (SoftwareFacet s) -> {
//			read(s);
//		});
//		
//		/* Entering hierarchical mode */
//		ContextUtility.getHierarchicalMode().set(true);
//		
//		softwareFacet = read(softwareFacet);
//		checkSoftwareFacetAssertion(softwareFacet, VERSION);
//		
//		/* Setting (parent of parent) scope /gcube */
//		ContextTest.setContextByName(PARENT_DEFAULT_TEST_SCOPE);
//		
//		softwareFacet = read(softwareFacet);
//		checkSoftwareFacetAssertion(softwareFacet, VERSION);
//		
//		/* Leaving hierarchical mode */
//		ContextUtility.getHierarchicalMode().set(false);
//		
//		assertThrow(softwareFacet, FacetAvailableInAnotherContextException.class, (SoftwareFacet s) -> {
//			read(s);
//		});
//		
//		/* Adding to /gcube. The context are now /gcube and /gcube/devNext/NextNext */
//		addToContext(softwareFacet);
//		softwareFacet = read(softwareFacet);
//		checkSoftwareFacetAssertion(softwareFacet, VERSION);
//		
//		softwareFacet.setVersion(NEW_VERSION);
//		softwareFacet = update(softwareFacet);
//		checkSoftwareFacetAssertion(softwareFacet, NEW_VERSION);
//		
//		/* Restoring scope /gcube/devNext/NextNext */
//		ContextTest.setContextByName(ALTERNATIVE_TEST_SCOPE);
//		read(softwareFacet);
//		checkSoftwareFacetAssertion(softwareFacet, NEW_VERSION);
//		
//		/* Removing from /gcube/devNext/NextNext. The context is now /gcube */
//		removeFromContext(softwareFacet);
//		
//		assertThrow(softwareFacet, FacetAvailableInAnotherContextException.class, (SoftwareFacet s) -> {
//			read(s);
//		});
//		
//		/* Setting (parent) scope /gcube/devNext */
//		ContextTest.setContextByName(DEFAULT_TEST_SCOPE);
//		assertThrow(softwareFacet, FacetAvailableInAnotherContextException.class, (SoftwareFacet s) -> {
//			read(s);
//		});
//		
//		/* Entering hierarchical mode */
//		ContextUtility.getHierarchicalMode().set(true);
//		
//		assertThrow(softwareFacet, FacetAvailableInAnotherContextException.class, (SoftwareFacet s) -> {
//			read(s);
//		});
//		
//		/* Setting (parent of parent) scope /gcube */
//		ContextTest.setContextByName(PARENT_DEFAULT_TEST_SCOPE);
//		// The facet must be readable in hierarchic mode in /gcube because the context
//		// has been explicitly added
//		read(softwareFacet);
//		checkSoftwareFacetAssertion(softwareFacet, NEW_VERSION);
//		
//		/* Leaving hierarchical mode */
//		ContextUtility.getHierarchicalMode().set(false);
//		
//		read(softwareFacet);
//		checkSoftwareFacetAssertion(softwareFacet, NEW_VERSION);
//		
//		delete(softwareFacet);
//	}
	
}
