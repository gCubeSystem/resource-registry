/**
 * 
 */
package org.gcube.informationsystem.resourceregistry.tobereviewed;

import org.gcube.informationsystem.resourceregistry.instances.multicontext.MultiContextTest;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class AnotherEnhancedTest extends MultiContextTest {

//	private static Logger logger = LoggerFactory
//			.getLogger(AnotherEnhancedTest.class);
//
//	@Test
//	public void testNoFollows() throws Exception {
//		ContextTest.setContextByName(DEFAULT_TEST_SCOPE);
//		
//		EService eService = ERManagementTest.instantiateValidEService();
//		
//		ResourceManagement resourceManagement = new ResourceManagement();
//		resourceManagement.setElementType(EService.NAME);
//		resourceManagement.setJson(ElementMapper.marshal(eService));
//
//		String json = resourceManagement.create();
//		logger.debug("Created : {}", json);
//		eService = ElementMapper.unmarshal(EService.class, json);
//		logger.debug("Unmarshalled {} {}", EService.NAME, eService);
//		
//		JsonNode jsonNode = ElementMapper.getObjectMapper().readTree(json);
//		
//		Map<UUID, JsonNode> expectedInstances = new HashMap<>();
//		expectedInstances.put(eService.getUUID(), jsonNode);
//		
//		UUID contextUUID = ContextUtility.getInstance().getRequestEnvironmentByContextFullName(ALTERNATIVE_TEST_SCOPE).getUUID();
//		
//		try {
//			ERManagementUtility.addToContextNoPropagationConstraint(expectedInstances, contextUUID, true);
//		}catch (ResourceRegistryException e) {
//			logger.debug("As expected {}", e.getMessage());
//		}
//		
//		@SuppressWarnings("unchecked")
//		IsIdentifiedBy<EService, Facet> gotIsIdentifiedBy = (IsIdentifiedBy<EService, Facet>) eService.getConsistsOf(IsIdentifiedBy.class).get(0);
//		
//		JsonNode gotIsIdentifiedByJsonNode = ElementMapper.getObjectMapper().readTree(ElementMapper.marshal(gotIsIdentifiedBy));
//		expectedInstances.put(gotIsIdentifiedBy.getUUID(), gotIsIdentifiedByJsonNode);
//		
//		try {
//			ERManagementUtility.addToContextNoPropagationConstraint(expectedInstances, contextUUID, true);
//		}catch (ResourceRegistryException e) {
//			// as expected
//			logger.debug("As expected {}", e.getMessage());
//		}finally {
//			resourceManagement.delete();
//		}
//		
//	}
	
}
