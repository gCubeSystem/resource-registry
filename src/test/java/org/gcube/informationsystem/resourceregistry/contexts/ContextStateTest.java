package org.gcube.informationsystem.resourceregistry.contexts;

import jakarta.ws.rs.BadRequestException;
import jakarta.ws.rs.ForbiddenException;

import org.gcube.informationsystem.contexts.impl.entities.ContextImpl;
import org.gcube.informationsystem.contexts.reference.ContextState;
import org.gcube.informationsystem.contexts.reference.entities.Context;
import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.resourceregistry.ContextTest;
import org.gcube.informationsystem.resourceregistry.authorization.FakeSecretUtility;
import org.gcube.informationsystem.resourceregistry.contexts.entities.ContextManagement;
import org.gcube.informationsystem.resourceregistry.instances.ERManagementTest;
import org.gcube.informationsystem.resourceregistry.instances.model.entities.ResourceManagement;
import org.gcube.informationsystem.serialization.ElementMapper;
import org.gcube.informationsystem.tree.Tree;
import org.gcube.resourcemanagement.model.reference.entities.resources.EService;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ContextStateTest extends ContextManagementTest {

	private static Logger logger = LoggerFactory.getLogger(ContextManagementTest.class);
	
	public <R extends Resource> ResourceManagement getResourceManagement(R r) throws Exception {
		ResourceManagement resourceManagement = new ResourceManagement();
		resourceManagement.setElementType(r.getTypeName());
		resourceManagement.setJson(ElementMapper.marshal(r));
		if (r.getID() != null) {
			resourceManagement.setUUID(r.getID());
		}
		return resourceManagement;
	}
	
	@Test()
	public void testNonActiveContext() throws Exception {
		Tree<Context> tree = ServerContextCache.getInstance().getContextsTree();
		Context root = tree.getRootNode().getNodeElement();
		String rootContext = "/" + root.getName();
		
		FakeSecretUtility.setFakeSecret("is-manager.json", rootContext);
		
		Context contextA1 = new ContextImpl(CTX_NAME_A);
		contextA1.setParent(root);
		
		try {
			contextA1 = create(contextA1);
		}catch (Exception e) {
			ContextTest.setContextByName(rootContext);
			delete(contextA1);
			return;
		}
		
		String newContext = rootContext+"/"+contextA1.getName();
		
		boolean resourceCreated = false;
		EService createdEservice = null;
		
		try {
			FakeSecretUtility.setFakeSecret("noroles.json", newContext);
			
			EService eService = ERManagementTest.instantiateValidEService(); 
			ResourceManagement resourceManagement = getResourceManagement(eService);
			
			String json = null;
			try {
				json = resourceManagement.create();
			} catch (ForbiddenException e) {
				logger.info("As expected the no role user cannot operate in non active context", e.getMessage());
			} catch (Exception e) {
				throw e;
			} finally {
				if(json!=null) {
					resourceManagement.delete();
				}
				
			}
			
			FakeSecretUtility.setFakeSecret("infrastructure-manager.json", newContext);
			
			try {
				json = null;
				resourceManagement = getResourceManagement(eService);
				json = resourceManagement.create();
				resourceCreated = true;
			} catch (Exception e) {
				if(json != null) {
					resourceManagement.delete();
				}
				throw e;
			} 
			
			createdEservice = ElementMapper.unmarshal(EService.class, json);
			
			try {
				resourceManagement = getResourceManagement(createdEservice);
				json = resourceManagement.read();
			} catch (Exception e) {
				throw e;
			}
			
			try {
				resourceManagement = getResourceManagement(createdEservice);
				json = resourceManagement.update();
			} catch (Exception e) {
				throw e;
			}
			
			FakeSecretUtility.setFakeSecret("noroles.json", newContext);
			
			try {
				resourceManagement = getResourceManagement(createdEservice);
				json = resourceManagement.read();
			} catch (ForbiddenException e) {
				logger.info("As expected the no role user cannot operate in non active context", e.getMessage());
			} catch (Exception e) {
				throw e;
			}
			
			try {
				resourceManagement = getResourceManagement(createdEservice);
				json = resourceManagement.update();
			} catch (ForbiddenException e) {
				logger.info("As expected the no role user cannot operate in non active context", e.getMessage());
			} catch (Exception e) {
				throw e;
			}
			
			try {
				resourceManagement = getResourceManagement(createdEservice);
				resourceManagement.delete();
			} catch (ForbiddenException e) {
				logger.info("As expected the no role user cannot operate in non active context", e.getMessage());
			} catch (Exception e) {
				throw e;
			}
			
			FakeSecretUtility.setFakeSecret("is-manager.json", newContext);
			
			try {
				resourceManagement = getResourceManagement(createdEservice);
				json = resourceManagement.read();
			} catch (Exception e) {
				throw e;
			}
			
			try {
				resourceManagement = getResourceManagement(createdEservice);
				json = resourceManagement.update();
			} catch (Exception e) {
				throw e;
			}
			
			
			FakeSecretUtility.setFakeSecret("noroles.json", newContext);
			
			try {
				resourceManagement = getResourceManagement(createdEservice);
				json = resourceManagement.read();
			} catch (ForbiddenException e) {
				logger.info("As expected the no role user cannot operate in non active context", e.getMessage());
			} catch (Exception e) {
				throw e;
			}
			
			try {
				resourceManagement = getResourceManagement(createdEservice);
				json = resourceManagement.update();
			} catch (ForbiddenException e) {
				logger.info("As expected the no role user cannot operate in non active context", e.getMessage());
			} catch (Exception e) {
				throw e;
			}
			
			try {
				resourceManagement = getResourceManagement(createdEservice);
				resourceManagement.delete();
			} catch (ForbiddenException e) {
				logger.info("As expected the no role user cannot operate in non active context", e.getMessage());
			} catch (Exception e) {
				throw e;
			}
			
			
			FakeSecretUtility.setFakeSecret("is-manager.json", newContext);
			
			try {
				resourceManagement = getResourceManagement(createdEservice);
				resourceManagement.delete();
				resourceCreated = false;
			} catch (ForbiddenException e) {
				logger.info("As expected the no role user cannot operate in non active context", e.getMessage());
			} catch (Exception e) {
				throw e;
			}
			
		} finally {
			FakeSecretUtility.setFakeSecret("is-manager.json", newContext);
			
			if(resourceCreated) {
				ResourceManagement resourceManagement = getResourceManagement(createdEservice);
				try {
					resourceManagement.delete();
				}catch (Exception e) {
					logger.error("Unable to delete resource {}", createdEservice);
				}
			}
			
			FakeSecretUtility.setFakeSecret("is-manager.json", rootContext);
			
			delete(contextA1);
		}
		
	}
	
	
	@Test()
	public void testContextStates() throws Exception {
		Tree<Context> tree = ServerContextCache.getInstance().getContextsTree();
		Context root = tree.getRootNode().getNodeElement();
		String rootContext = "/" + root.getName();
		
		FakeSecretUtility.setFakeSecret("is-manager.json", rootContext);
		
		Context contextA1 = new ContextImpl(CTX_NAME_A);
		contextA1.setParent(root);
		
		String newContext = null;
		
		try {
			contextA1 = create(contextA1);
			ContextState state = ContextState.fromString(contextA1.getState());
			newContext =  rootContext + "/" + contextA1.getName();
			Assert.assertTrue(state == ContextState.CREATED);
			logger.info("Created contexts {} {} state is {}", newContext, contextA1.getID(), contextA1.getState());
		}catch (Exception e) {
			ContextTest.setContextByName(rootContext);
			delete(contextA1);
			throw e;
		}
		
		
		try {
			ContextManagement contextManagement = new ContextManagement();
			ContextState contextState = ContextState.ACTIVE;
			String state = contextState.getState();
			contextA1.setState(state);
			contextManagement.setJson(ElementMapper.marshal(contextA1));
			String contextString = contextManagement.update();
			contextA1 = ElementMapper.unmarshal(Context.class, contextString);
			ContextState gotState = ContextState.fromString(contextA1.getState());
			logger.info("Contexts {} {} expected state {} - got state {}", newContext, contextA1.getID(), contextState.getState(), gotState.getState());
			Assert.assertTrue(contextState == gotState);
			logger.info("Contexts {} {} successfully set as {}", newContext, contextA1.getID(), gotState.getState());
			
			
			contextManagement = new ContextManagement();
			contextState = ContextState.CREATED;
			state = contextState.getState();
			contextA1.setState(state);
			contextManagement.setJson(ElementMapper.marshal(contextA1));
			try {
				contextString = contextManagement.update();
				contextA1 = ElementMapper.unmarshal(Context.class, contextString);
				gotState = ContextState.fromString(contextA1.getState());
				throw new Exception("It should be possibile set explicitly the context " + newContext + " " + contextA1.getID() + " as " + gotState.getState());
			}catch (BadRequestException e) {
				logger.info("As expected a context cannot be explicitly se to {}", state, e.getMessage());
			}
			
			
			contextManagement = new ContextManagement();
			contextState = ContextState.SUSPENDED;
			state = contextState.getState();
			contextA1.setState(state);
			contextManagement.setJson(ElementMapper.marshal(contextA1));
			contextString = contextManagement.update();
			contextA1 = ElementMapper.unmarshal(Context.class, contextString);
			gotState = ContextState.fromString(contextA1.getState());
			logger.info("Contexts {} {} expected state {} - got state {}", newContext, contextA1.getID(), contextState.getState(), gotState.getState());
			Assert.assertTrue(contextState == gotState);
			logger.info("Contexts {} {} successfully set as {}", newContext, contextA1.getID(), gotState.getState());
			
			
			contextManagement = new ContextManagement();
			contextState = ContextState.DELETED;
			state = contextState.getState();
			contextA1.setState(state);
			contextManagement.setJson(ElementMapper.marshal(contextA1));
			try {
				contextString = contextManagement.update();
				contextA1 = ElementMapper.unmarshal(Context.class, contextString);
				gotState = ContextState.fromString(contextA1.getState());
				throw new Exception("It should be possibile set explicitly the context " + newContext + " " + contextA1.getID() + " as " + gotState.getState());
			}catch (BadRequestException e) {
				logger.info("As expected a context cannot be explicitly se to {}", state, e.getMessage());
			}
			
		} finally {
			delete(contextA1);
		}
	}
	
}
