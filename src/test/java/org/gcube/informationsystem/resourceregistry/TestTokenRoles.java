package org.gcube.informationsystem.resourceregistry;

import java.util.Collection;

import org.gcube.common.iam.OIDCBearerAuth;
import org.gcube.common.security.Owner;
import org.gcube.common.security.providers.SecretManagerProvider;
import org.gcube.common.security.secrets.Secret;
import org.gcube.informationsystem.resourceregistry.environments.Environment;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class TestTokenRoles extends ContextTest {

	private static final Logger logger = LoggerFactory.getLogger(ContextTest.class);
	
	@Test
	public void testRoles() throws Exception {
		String[] contexts = new String[] {GCUBE, DEVSEC, DEVVRE, DEVNEXT, NEXTNEXT};
		for(String context : contexts){
			String accessToken = getJWTAccessToken(context);
			OIDCBearerAuth auth = OIDCBearerAuth.fromAccessTokenString(accessToken);
			Collection<String> roles = auth.getRoles();
			logger.debug("{} All roles are {}", context, roles);
			Assert.assertTrue(roles.contains(Environment.IS_MANAGER));
			Collection<String> contextRoles = auth.getContextRoles();
			logger.debug("{} Context roles are {}", context, contextRoles);
			Collection<String> globalRoles = auth.getGlobalRoles();
			logger.debug("{} Global roles are {}", context, globalRoles);
			Assert.assertTrue(globalRoles.contains(Environment.IS_MANAGER));
			Assert.assertTrue(roles.size()==(contextRoles.size() + globalRoles.size()));
			Assert.assertTrue(roles.containsAll(contextRoles));
			Assert.assertTrue(roles.containsAll(globalRoles));
		}
	}

	@Test
	public void testToken() throws Exception {
		ContextTest.setContextByName(DEVNEXT);
		Secret secret = SecretManagerProvider.get();
		Owner owner = secret.getOwner();
		logger.debug("Owner is {} and has roles {} and global roles {}", owner.getId(), owner.getRoles(), owner.getGlobalRoles());
		SecretManagerProvider.reset();
	}

}
