package org.gcube.informationsystem.resourceregistry.queries;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.net.URL;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.gcube.com.fasterxml.jackson.databind.JsonNode;
import org.gcube.com.fasterxml.jackson.databind.ObjectMapper;
import org.gcube.informationsystem.model.reference.entities.Entity;
import org.gcube.informationsystem.resourceregistry.ContextTest;
import org.gcube.informationsystem.resourceregistry.queries.json.JsonQuery;
import org.gcube.informationsystem.serialization.ElementMapper;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class JsonQueryTest extends ContextTest {

	private static Logger logger = LoggerFactory.getLogger(JsonQueryTest.class);
	
	public File getQueriesDirectory() throws Exception {
		URL logbackFileURL = JsonQueryTest.class.getClassLoader().getResource("logback-test.xml");
		File logbackFile = new File(logbackFileURL.toURI());
		File resourcesDirectory = logbackFile.getParentFile();
		return new File(resourcesDirectory, "queries");
	}
	
	public File getProjectionQueriesDirectory() throws Exception {
		URL logbackFileURL = JsonQueryTest.class.getClassLoader().getResource("logback-test.xml");
		File logbackFile = new File(logbackFileURL.toURI());
		File resourcesDirectory = logbackFile.getParentFile();
		return new File(resourcesDirectory, "projection-queries");
	}
	
	protected boolean compareQueries(StringBuffer createdSb, StringBuffer expectedSb) {
		return compareQueries(createdSb.toString(), expectedSb.toString());
	}
	
	protected String normalizeString(String s) {
		return s.replaceAll("\n{1,}", "")
				.replaceAll("\r{1,}", "")
				.replaceAll("\t{1,}", "")
				.replaceAll("\\s{2,}", " ")
				.replaceAll("\\(\\s{1,}", "(")
				.replaceAll("\\s{1,}\\(", "(")
				.replaceAll("\\)\\s{1,}", ")")
				.replaceAll("\\s{1,}\\)", ")");
	}
	
	protected boolean compareQueries(String createdString, String expectedString) {
		String created = normalizeString(createdString);
		String expected = normalizeString(expectedString);
		logger.debug(created);
		logger.debug(expected);
		return created.compareTo(expected)==0 ? true : false;	
	}
	
	@Test
	public void testCompares() throws Exception {
		String a = "))\n\t\r  ) ) ) )";
		String b = "))))))";
		Assert.assertTrue(compareQueries(a, b));
	}
	
	@Test
	public void testQueries() throws Exception {
		ContextTest.setContextByName(DEVVRE);
		
		File queriesDirectory = getQueriesDirectory();
		
		FilenameFilter filenameFilter = new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.endsWith(".json");
			}
		};
		
		for(File jsonQueryFile : queriesDirectory.listFiles(filenameFilter)) {
			logger.info("Going to read JSON query from file {}", jsonQueryFile.getAbsolutePath());
			
			ObjectMapper objectMapper = new ObjectMapper();
			JsonNode jsonNode = objectMapper.readTree(jsonQueryFile);
			logger.info("Going to test the following JSON query {}", jsonNode.toString());
			
			JsonQuery jsonQuery = new JsonQuery();
			jsonQuery.setJsonQuery(jsonNode);
			StringBuffer createdStringBuffer = jsonQuery.createQuery();
			
			logger.info("Created Query from JSON:  {}", createdStringBuffer.toString());
			
			StringBuffer expectedStringBuffer = new StringBuffer();
			File expectedQueryFile = new File(queriesDirectory, jsonQueryFile.getName().replace("json", "match.oquery"));
			try(BufferedReader br = new BufferedReader(new FileReader(expectedQueryFile))) {
			    for(String line; (line = br.readLine()) != null; ) {
			    	expectedStringBuffer.append(line);
			    }
			}
			
			logger.info("Expected Query from JSON: {}", expectedStringBuffer.toString());
			Assert.assertTrue(compareQueries(createdStringBuffer, expectedStringBuffer));
			
			String result = jsonQuery.query();
			logger.info("Result : {}", result);
		}
	}
	
	@Test
	public void testSingleQuery() throws Exception {
		File queriesDirectory = getQueriesDirectory();
		File jsonQueryFile = new File(queriesDirectory, "query10.json");
		ObjectMapper objectMapper = new ObjectMapper();
		JsonNode jsonNode = objectMapper.readTree(jsonQueryFile);
		logger.info("Going to test the following JSON query {}", jsonNode.toString());
		
		JsonQuery jsonQuery = new JsonQuery();
		jsonQuery.setJsonQuery(jsonNode);
		StringBuffer createdStringBuffer = jsonQuery.createQuery();
		
		logger.info("Created Query from JSON:  {}", createdStringBuffer.toString());
		
		StringBuffer expectedStringBuffer = new StringBuffer();
		File expectedQueryFile = new File(queriesDirectory, jsonQueryFile.getName().replace("json", "match.oquery"));
		try(BufferedReader br = new BufferedReader(new FileReader(expectedQueryFile))) {
		    for(String line; (line = br.readLine()) != null; ) {
		    	expectedStringBuffer.append(line);
		    }
		}
		
		logger.info("Expected Query from JSON: {}", expectedStringBuffer.toString());
		Assert.assertTrue(compareQueries(createdStringBuffer, expectedStringBuffer));
		
		String result = jsonQuery.query();
		logger.info("Result : {}", result);
	}
		
	protected List<Entity> testSingleQuery(int offset, int limit) throws Exception {
		ContextTest.setContextByName(DEVVRE);
		File queriesDirectory = getQueriesDirectory();
		File jsonQueryFile = new File(queriesDirectory, "query1.json");
		ObjectMapper objectMapper = new ObjectMapper();
		JsonNode jsonNode = objectMapper.readTree(jsonQueryFile);
		logger.info("Going to test the following JSON query {}", jsonNode.toString());
		
		JsonQuery jsonQuery = new JsonQuery();
		jsonQuery.setJsonQuery(jsonNode);
		String res  = jsonQuery.query();
		logger.info(res);
		
		List<Entity> ret = ElementMapper.unmarshalList(Entity.class, res);
		return ret;
	}
	
	
	
	@Test
	public void testLimitOffset() throws Exception {
		int limit = 2;
		
		List<Entity> entities = testSingleQuery(0, limit);
		if(entities.size()==0) {
			return;
		}
		Assert.assertTrue(entities.size() <= limit);
		
		if(entities.size()< limit) {
			return;
		}
		
		Set<UUID> uuids = new HashSet<>();
		for(Entity entity : entities) {
			UUID uuid = entity.getID();
			uuids.add(uuid);
			logger.info("Found {} with UUID {}", Entity.NAME, uuid);
		}
		
		entities = testSingleQuery(limit, limit);
		
		if(entities.size()>0) {
			Assert.assertTrue(entities.size() <= limit);
			
			for(Entity entity : entities) {
				UUID uuid = entity.getID();
				Assert.assertFalse(uuids.contains(uuid));
				uuids.add(uuid);
				logger.info("Found {} with UUID {}", Entity.NAME, uuid);
			}
			
			if(entities.size()<limit) {
				return;
			}
			
			
			int doubleLimit = limit*2;
			
			entities = testSingleQuery(0, doubleLimit);
			
			Assert.assertTrue(entities.size() <= doubleLimit);
			
			for(Entity entity : entities) {
				UUID uuid = entity.getID();
				logger.info("Checking if {} with UUID {} was contained in the previous queries", Entity.NAME, uuid);
				Assert.assertTrue(uuids.contains(uuid));
				logger.info("As expected got {} with UUID {} and name {}", Entity.NAME, uuid);
			}
		}
		
		entities = testSingleQuery(0, -1);
		
		Assert.assertTrue(entities.size()>=uuids.size());
		
		for(Entity entity : entities) {
			UUID uuid = entity.getID();
			logger.info("No limit listing: Got {} with UUID {}", Entity.NAME, uuid);
		}
	}
	
	@Test
	public void testSingleProjectionQuery() throws Exception {
		File queriesDirectory = getProjectionQueriesDirectory();
		File jsonQueryFile = new File(queriesDirectory, "HostingNode-query.json");
		ObjectMapper objectMapper = new ObjectMapper();
		JsonNode jsonNode = objectMapper.readTree(jsonQueryFile);
		logger.info("Going to test the following JSON query {}", jsonNode.toString());
		
		JsonQuery jsonQuery = new JsonQuery();
		jsonQuery.setJsonQuery(jsonNode);
		StringBuffer createdStringBuffer = jsonQuery.createMatchQuery();
		
		logger.info("Created Query from JSON:\n{}", createdStringBuffer.toString());
		
		StringBuffer expectedStringBuffer = new StringBuffer();
		File expectedQueryFile = new File(queriesDirectory, jsonQueryFile.getName().replace("-query.json", ".match.oquery"));
		try(BufferedReader br = new BufferedReader(new FileReader(expectedQueryFile))) {
		    for(String line; (line = br.readLine()) != null; ) {
		    	expectedStringBuffer.append(line);
		    }
		}
		
		logger.info("Expected Query from JSON: {}", expectedStringBuffer.toString());
		
		Assert.assertTrue(compareQueries(createdStringBuffer, expectedStringBuffer));
		
		String result = jsonQuery.query();
		logger.info("Result : {}", result);
	}
	
	@Test
	public void testProjectionQueries() throws Exception {
		File queriesDirectory = getProjectionQueriesDirectory();

		FilenameFilter filenameFilter = new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.endsWith("-query.json");
			}
		};
		
		for(File jsonQueryFile : queriesDirectory.listFiles(filenameFilter)) {
			ObjectMapper objectMapper = new ObjectMapper();
			JsonNode jsonNode = objectMapper.readTree(jsonQueryFile);
			logger.info("Going to test the following JSON query {}", jsonNode.toString());
			
			JsonQuery jsonQuery = new JsonQuery();
			jsonQuery.setJsonQuery(jsonNode);
			StringBuffer createdStringBuffer = jsonQuery.createMatchQuery();
			
			logger.info("Created Query from JSON:\n{}", createdStringBuffer.toString());
			
			StringBuffer expectedStringBuffer = new StringBuffer();
			File expectedQueryFile = new File(queriesDirectory, jsonQueryFile.getName().replace("-query.json", ".match.oquery"));
			try(BufferedReader br = new BufferedReader(new FileReader(expectedQueryFile))) {
			    for(String line; (line = br.readLine()) != null; ) {
			    	expectedStringBuffer.append(line);
			    }
			}
			
			logger.info("Expected Query from JSON: {}", expectedStringBuffer.toString());
			Assert.assertTrue(compareQueries(createdStringBuffer, expectedStringBuffer));
			
			String result = jsonQuery.query();
			logger.info("Result : {}", result);
		}
	}
}
