package org.gcube.informationsystem.resourceregistry.queries.operators;

import org.gcube.com.fasterxml.jackson.databind.JsonNode;
import org.gcube.com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Test class for the MathematicsOperator.
 * 
 * This class contains a test method to verify the functionality of the 
 * MathematicsOperator's generateFieldToEmit method.
 * 
 * The JSON string used in the test represents a mathematical operation 
 * to be performed on certain fields.
 * 
 * The testGenerateFieldToEmit method:
 * - Parses the JSON string into a JsonNode object.
 * - Calls the generateFieldToEmit method of the MathematicsOperator with the parsed JsonNode and a field name.
 * - Logs the result for debugging purposes.
 * 
 * Dependencies:
 * - com.fasterxml.jackson.databind.ObjectMapper
 * - com.fasterxml.jackson.databind.JsonNode
 * - org.slf4j.Logger
 * - org.slf4j.LoggerFactory
 * - org.junit.Test
 * 
 * Note: Ensure that the MathematicsOperator class and its SUM constant are properly defined and imported.
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
public class MathematicsOperatorTest {

	protected static Logger logger = LoggerFactory.getLogger(MathematicsOperatorTest.class);
	
	public static final String JSON = 
"""
{
	"values" : [
		{
			"_minus" : [
				"size",
				"used"
			]
		},
		"unit"
	],
	"separator" : " ",
	"as" : "HD Space Left"
}
""";
	
	@Test
	public void testGenerateFieldToEmit() throws Exception {
		ObjectMapper om = new ObjectMapper();
		JsonNode jn = om.readTree(JSON);
		String s = MatemathicsOperator.SUM.generateFieldToEmit(jn, "haspersistentmemory20");
		logger.debug(s);
	}
	
}



