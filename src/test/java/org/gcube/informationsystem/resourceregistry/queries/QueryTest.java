package org.gcube.informationsystem.resourceregistry.queries;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.gcube.common.authorization.client.exceptions.ObjectNotFound;
import org.gcube.informationsystem.model.reference.entities.Facet;
import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.model.reference.relations.ConsistsOf;
import org.gcube.informationsystem.model.reference.relations.IsRelatedTo;
import org.gcube.informationsystem.resourceregistry.api.exceptions.queries.InvalidQueryException;
import org.gcube.informationsystem.resourceregistry.base.ElementManagement;
import org.gcube.informationsystem.resourceregistry.instances.ERManagementTest;
import org.gcube.informationsystem.resourceregistry.instances.model.ERManagementUtility;
import org.gcube.informationsystem.resourceregistry.instances.model.entities.ResourceManagement;
import org.gcube.informationsystem.resourceregistry.instances.model.relations.ConsistsOfManagement;
import org.gcube.informationsystem.resourceregistry.instances.model.relations.IsRelatedToManagement;
import org.gcube.informationsystem.serialization.ElementMapper;
import org.gcube.informationsystem.utils.TypeUtility;
import org.gcube.resourcemanagement.model.impl.entities.facets.IdentifierFacetImpl;
import org.gcube.resourcemanagement.model.impl.entities.facets.SimplePropertyFacetImpl;
import org.gcube.resourcemanagement.model.impl.entities.resources.ConfigurationImpl;
import org.gcube.resourcemanagement.model.impl.relations.consistsof.IsIdentifiedByImpl;
import org.gcube.resourcemanagement.model.reference.entities.facets.IdentifierFacet;
import org.gcube.resourcemanagement.model.reference.entities.facets.IdentifierFacet.IdentificationType;
import org.gcube.resourcemanagement.model.reference.entities.facets.SimplePropertyFacet;
import org.gcube.resourcemanagement.model.reference.entities.facets.SoftwareFacet;
import org.gcube.resourcemanagement.model.reference.entities.resources.Configuration;
import org.gcube.resourcemanagement.model.reference.entities.resources.ConfigurationTemplate;
import org.gcube.resourcemanagement.model.reference.entities.resources.EService;
import org.gcube.resourcemanagement.model.reference.entities.resources.GCubeResource;
import org.gcube.resourcemanagement.model.reference.entities.resources.HostingNode;
import org.gcube.resourcemanagement.model.reference.entities.resources.Service;
import org.gcube.resourcemanagement.model.reference.relations.consistsof.IsIdentifiedBy;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.orientechnologies.orient.core.record.ODirection;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class QueryTest extends ERManagementTest {

	private static Logger logger = LoggerFactory.getLogger(QueryTest.class);
	
	private static final String EXTRA = "EXTRA";
	
	@Test
	public void testRawQuery() throws InvalidQueryException {
		QueryImpl queryImpl = new QueryImpl();
		
		String query = "SELECT FROM SoftwareFacet";
		String ret = queryImpl.query(query, false);
		
		logger.debug(ret);
		
		ret = queryImpl.query(query, false);
		
		logger.debug(ret);
	}
	
	@Test
	public void testGetAll() throws Exception {
		Map<String, List<Resource>> resources = new HashMap<>();

		try {
			final int MAX = 5;
			int typeNumber = 0;
	
			for (int i = 0; i < MAX; i++) {
				Map<String, Resource> map = createHostingNodeAndEService();
				if (typeNumber == 0) {
					typeNumber = map.size();
				}
				for (String key : map.keySet()) {
					if (!resources.containsKey(key)) {
						resources.put(key, new ArrayList<Resource>());
					}
					resources.get(key).add(map.get(key));
				}
			}
	
			/* Getting all instances of created specific Resources */
			for (String key : resources.keySet()) {
				ResourceManagement resourceManagement = (ResourceManagement) ERManagementUtility.getERManagement(key);
				String json = resourceManagement.all(false);
	
				List<Resource> list = ElementMapper.unmarshalList(Resource.class, json);
				logger.debug("{} are {} : {} ", key, list.size(), list);
				Assert.assertTrue(list.size() == MAX);
			}
	
			/* Getting all Resources polymorphic and non polymorphic */
	
			ResourceManagement resourceManagement = (ResourceManagement) ERManagementUtility
					.getERManagement(Resource.NAME);
	
			String json = resourceManagement.all(true);
			List<Resource> list = ElementMapper.unmarshalList(Resource.class, json);
			logger.debug("{} are {} : {} ", Resource.NAME, list.size(), list);
			Assert.assertTrue(list.size() == (MAX * typeNumber));
	
			json = resourceManagement.all(false);
			list = ElementMapper.unmarshalList(Resource.class, json);
			Assert.assertTrue(list.size() == 0);
	
			/* Getting all IsRelatedTo polymorphic and non polymorphic */
	
			IsRelatedToManagement isRelatedToManagement = (IsRelatedToManagement) ERManagementUtility
					.getERManagement(IsRelatedTo.NAME);
	
			json = isRelatedToManagement.all(true);
	
			@SuppressWarnings("rawtypes")
			List<IsRelatedTo> resourcesList = ElementMapper.unmarshalList(IsRelatedTo.class, json);
			logger.debug("{} are {} : {} ", IsRelatedTo.NAME, resourcesList.size(), resourcesList);
			Assert.assertTrue(resourcesList.size() == MAX);
	
			json = isRelatedToManagement.all(false);
			resourcesList = ElementMapper.unmarshalList(IsRelatedTo.class, json);
			Assert.assertTrue(resourcesList.size() == 0);
	
			/* Getting all ConsistsOf polymorphic and non polymorphic */
	
			ConsistsOfManagement consistsOfManagement = (ConsistsOfManagement) ERManagementUtility
					.getERManagement(ConsistsOf.NAME);
	
			json = consistsOfManagement.all(true);
			@SuppressWarnings("rawtypes")
			List<ConsistsOf> consistsOfPolimorphicList = ElementMapper.unmarshalList(ConsistsOf.class, json);
			logger.debug("{} are {} : {} ", ConsistsOf.NAME, consistsOfPolimorphicList.size(), consistsOfPolimorphicList);
	
			json = consistsOfManagement.all(false);
			@SuppressWarnings("rawtypes")
			List<ConsistsOf> consistsOfNonPolimorphicList = ElementMapper.unmarshalList(ConsistsOf.class, json);
			logger.debug("{} are {} : {} ", ConsistsOf.NAME, consistsOfNonPolimorphicList.size(),
					consistsOfNonPolimorphicList);
	
			Assert.assertTrue(consistsOfPolimorphicList.size() >= consistsOfNonPolimorphicList.size());
		} finally {
			/* Removing created Entity and Relation to have a clean DB */
	
			List<Resource> resourceList = resources.get(HostingNode.NAME);
			for (Resource r : resourceList) {
				deleteResource(r);
			}
		}
	}

	@Test
	public void testGetAllFrom() throws Exception {
		Map<String, Resource> map = createHostingNodeAndEService();
		EService eService = (EService) map.get(EService.NAME);
		HostingNode hostingNode = (HostingNode) map.get(HostingNode.NAME);
		
		try {
			UUID eServiceUUID = eService.getID();
			UUID hostingNodeUUID = hostingNode.getID();
	
			ResourceManagement resourceManagement = new ResourceManagement();
			resourceManagement.setElementType(Service.NAME);
	
			/* Getting Hosting Node */
			String json = resourceManagement.query(IsRelatedTo.NAME, EService.NAME, eServiceUUID, ODirection.BOTH, true,
					null);
			List<Resource> resourceList = ElementMapper.unmarshalList(Resource.class, json);
			Assert.assertTrue(resourceList.size() == 1);
			Resource resource = resourceList.get(0);
			Assert.assertTrue(resource.getID().compareTo(hostingNodeUUID) == 0);
	
			json = resourceManagement.query(IsRelatedTo.NAME, EService.NAME, eServiceUUID, ODirection.OUT, true, null);
			resourceList = ElementMapper.unmarshalList(Resource.class, json);
			Assert.assertTrue(resourceList.size() == 1);
			resource = resourceList.get(0);
			Assert.assertTrue(resource.getID().compareTo(hostingNodeUUID) == 0);
	
			json = resourceManagement.query(IsRelatedTo.NAME, EService.NAME, eServiceUUID, ODirection.IN, true, null);
			resourceList = ElementMapper.unmarshalList(Resource.class, json);
			Assert.assertTrue(resourceList.size() == 0);
	
			json = resourceManagement.query(IsRelatedTo.NAME, EService.NAME, eServiceUUID, ODirection.BOTH, false, null);
			resourceList = ElementMapper.unmarshalList(Resource.class, json);
			Assert.assertTrue(resourceList.size() == 0);
			json = resourceManagement.query(IsRelatedTo.NAME, EService.NAME, eServiceUUID, ODirection.OUT, false, null);
			resourceList = ElementMapper.unmarshalList(Resource.class, json);
			Assert.assertTrue(resourceList.size() == 0);
			json = resourceManagement.query(IsRelatedTo.NAME, EService.NAME, eServiceUUID, ODirection.IN, false, null);
			resourceList = ElementMapper.unmarshalList(Resource.class, json);
			Assert.assertTrue(resourceList.size() == 0);
			/* END Getting Hosting Node */
	
			/* Getting EService */
			json = resourceManagement.query(IsRelatedTo.NAME, HostingNode.NAME, hostingNodeUUID, ODirection.BOTH, true,
					null);
			resourceList = ElementMapper.unmarshalList(Resource.class, json);
			Assert.assertTrue(resourceList.size() == 1);
			Assert.assertTrue(resourceList.get(0).getID().compareTo(eServiceUUID) == 0);
	
			json = resourceManagement.query(IsRelatedTo.NAME, HostingNode.NAME, hostingNodeUUID, ODirection.OUT, true, null);
			resourceList = ElementMapper.unmarshalList(Resource.class, json);
			Assert.assertTrue(resourceList.size() == 0);
	
			json = resourceManagement.query(IsRelatedTo.NAME, HostingNode.NAME, hostingNodeUUID, ODirection.IN, true, null);
			resourceList = ElementMapper.unmarshalList(Resource.class, json);
			Assert.assertTrue(resourceList.size() == 1);
			Assert.assertTrue(resourceList.get(0).getID().compareTo(eServiceUUID) == 0);
	
			json = resourceManagement.query(IsRelatedTo.NAME, HostingNode.NAME, hostingNodeUUID, ODirection.BOTH, false,
					null);
			resourceList = ElementMapper.unmarshalList(Resource.class, json);
			Assert.assertTrue(resourceList.size() == 0);
	
			json = resourceManagement.query(IsRelatedTo.NAME, HostingNode.NAME, hostingNodeUUID, ODirection.OUT, false,
					null);
			resourceList = ElementMapper.unmarshalList(Resource.class, json);
			Assert.assertTrue(resourceList.size() == 0);
	
			json = resourceManagement.query(IsRelatedTo.NAME, HostingNode.NAME, hostingNodeUUID, ODirection.IN, false, null);
			resourceList = ElementMapper.unmarshalList(Resource.class, json);
			Assert.assertTrue(resourceList.size() == 0);
			/* END Getting HostingNode */
	
			Facet identificationFacet = eService.getIdentificationFacets().get(0);
			UUID identificationFacetUUID = identificationFacet.getID();
	
			/* EService --ConsistsOf--> SoftwareFacet */
			try {
				json = resourceManagement.query(ConsistsOf.NAME, SoftwareFacet.NAME, identificationFacetUUID,
						ODirection.BOTH, true, null);
			} catch (InvalidQueryException e) {
				// Ok expected
			}
	
			json = resourceManagement.query(ConsistsOf.NAME, SoftwareFacet.NAME, identificationFacetUUID, ODirection.OUT,
					true, null);
			resourceList = ElementMapper.unmarshalList(Resource.class, json);
			Assert.assertTrue(resourceList.size() == 1);
			resource = resourceList.get(0);
			Facet targetIdentificationFacet = ((GCubeResource) resource).getIdentificationFacets().get(0);
			Assert.assertTrue(resource.getID().compareTo(eServiceUUID) == 0);
			Assert.assertTrue(targetIdentificationFacet.getID().compareTo(identificationFacetUUID) == 0);
	
			try {
				json = resourceManagement.query(ConsistsOf.NAME, SoftwareFacet.NAME, identificationFacetUUID, ODirection.IN,
						true, null);
				throw new Exception("Expected InvalidQueryException");
			} catch (InvalidQueryException e) {
				// Ok expected
			}
	
			try {
				json = resourceManagement.query(ConsistsOf.NAME, SoftwareFacet.NAME, identificationFacetUUID,
						ODirection.BOTH, false, null);
				throw new Exception("Expected InvalidQueryException");
			} catch (InvalidQueryException e) {
				// Ok expected
			}
	
			json = resourceManagement.query(ConsistsOf.NAME, SoftwareFacet.NAME, identificationFacetUUID, ODirection.OUT,
					false, null);
			resourceList = ElementMapper.unmarshalList(Resource.class, json);
			Assert.assertTrue(resourceList.size() == 0);
	
			try {
				json = resourceManagement.query(ConsistsOf.NAME, SoftwareFacet.NAME, identificationFacetUUID, ODirection.IN,
						false, null);
				throw new Exception("Expected InvalidQueryException");
			} catch (InvalidQueryException e) {
				// Ok expected
			}
	
			/* END EService --ConsistsOf--> SoftwareFacet */
			
		} finally {
			deleteResource(eService);
			deleteResource(hostingNode);
		}
		
	}
	
	@Test
	public void testQuery() throws Exception {
		String relationType = ConsistsOf.NAME;
		String facetType = SoftwareFacet.NAME;
		
		Map<String, String> constraint = new HashMap<>();
		constraint.put(SoftwareFacet.GROUP_PROPERTY, "Gis");
		constraint.put(SoftwareFacet.NAME_PROPERTY, "Thredds");
		
		String type = Service.NAME;
		
		@SuppressWarnings("rawtypes")
		ElementManagement erManagement = ERManagementUtility.getERManagement(type);
		
		if (erManagement instanceof ResourceManagement management) {
			boolean[] booleans = new boolean[] {true, false};
			for(boolean bool : booleans) {
				String ret = management.query(relationType, facetType, null, ODirection.OUT, bool, constraint);
				logger.debug("Result of query for {}polymorphic {} --{}--> {} with constaint {} is {}", bool ? "" : "NOT ", 
						type, relationType, facetType, constraint, ret);
			}
		}
		
		
	}
	
	protected Configuration createConfiguration(String name, Object value) throws Exception {
		Configuration configuration = new ConfigurationImpl();
		
		IdentifierFacet identifierFacet =  new IdentifierFacetImpl();
		identifierFacet.setValue(name);
		identifierFacet.setIdentificationType(IdentificationType.STRING);
		if(value!=null) {
			identifierFacet.setAdditionalProperty(EXTRA, value);
		}
		
		IsIdentifiedBy<Configuration, IdentifierFacet> isIdentifiedBy = new IsIdentifiedByImpl<Configuration, IdentifierFacet>(configuration, identifierFacet);
		configuration.addFacet(isIdentifiedBy);
		
		SimplePropertyFacet simplePropertyFacet = new SimplePropertyFacetImpl();
		simplePropertyFacet.setName("test");
		simplePropertyFacet.setValue("test");
		configuration.addFacet(simplePropertyFacet);
		
		return createResource(configuration);
	}
	
	
	@Test
	public void getAllResourcesHavingFacet() throws ObjectNotFound, Exception {
		String[] names = new String[]{"MyTest", "MyTest", "Aux", "MyID", "MyID"};
		Object[] additionalProperties = new Object[] {5, 6.0, false, "test", null};
		
		List<Configuration> createdConfigurations = new ArrayList<>();
		
		for(int i=0; i<names.length; i++) {
			createdConfigurations.add(createConfiguration(names[i], additionalProperties[i]));
		}
		
		try {
			String parentResourceType = TypeUtility.getTypeName(ConfigurationTemplate.class);
			String resourceType = TypeUtility.getTypeName(Configuration.class);
			String relationType = TypeUtility.getTypeName(IsIdentifiedBy.class);
			String referenceType = TypeUtility.getTypeName(IdentifierFacet.class);
			
			ResourceManagement resourceManagement = new ResourceManagement(); 
			resourceManagement.setElementType(resourceType);
			
			ResourceManagement parentResourceManagement = new ResourceManagement(); 
			parentResourceManagement.setElementType(parentResourceType);
		
			
			ODirection directionEnum = ODirection.OUT;
			UUID refereceUUID = null;
			
			Boolean[] polymorphics = new Boolean[] {true, false};
			
			int expectedSize = 1;
			
			Set<UUID> expectedUUIDFor2Results = new HashSet<>();
			expectedUUIDFor2Results.add(createdConfigurations.get(3).getID());
			expectedUUIDFor2Results.add(createdConfigurations.get(4).getID());
			
			for(int i=0; i<names.length; i++) {
				
				expectedSize = 1;
				if(i==4) {
					expectedSize = 2;
				}
				
				Configuration expected =  createdConfigurations.get(i);
				UUID expectedUUID = expected.getID();
				
				for(Boolean polymorphic : polymorphics) {
					
					
					Map<String,String> constraint = new HashMap<>();
					constraint.put(IdentifierFacet.VALUE_PROPERTY, names[i]);
					if(additionalProperties[i]!=null) {
						constraint.put(EXTRA, additionalProperties[i].toString());
					}
					
					logger.debug("Going to query for {}polymorphic {} --{}--> {} with constaints {}", polymorphic ? "" : "NOT ", 
							resourceType, relationType, referenceType, constraint);
					
					String ret = resourceManagement.query(relationType, referenceType, refereceUUID, directionEnum,
							polymorphic, constraint);
					
					List<Configuration> list = ElementMapper.unmarshalList(Configuration.class, ret);
					Assert.assertTrue(list.size()==expectedSize);
					if(expectedSize==1) {
						Configuration configuration = list.get(0);
						UUID gotUUID = configuration.getID();
						Assert.assertTrue(gotUUID.compareTo(expectedUUID)==0);
					}else {
						Set<UUID> gotUUIDs = new HashSet<>();
						for(Configuration c : list) {
							gotUUIDs.add(c.getID());
						}
						
						Assert.assertTrue(gotUUIDs.containsAll(expectedUUIDFor2Results));
						Assert.assertTrue(expectedUUIDFor2Results.containsAll(gotUUIDs));
						
					}
					
					
					if(polymorphic) {
						
						logger.debug("Going to query for polymorphic {} --{}--> {} with constaints {}",  
								parentResourceType, relationType, referenceType, constraint);
						
						String retPolimorphic = parentResourceManagement.query(relationType, referenceType, refereceUUID, directionEnum,
								polymorphic, constraint);
						List<ConfigurationTemplate> listPolimorphic = ElementMapper.unmarshalList(ConfigurationTemplate.class, retPolimorphic);
						Assert.assertTrue(listPolimorphic.size()==expectedSize);
						
						if(expectedSize==1) {
							ConfigurationTemplate configurationTemplate = listPolimorphic.get(0);
							Assert.assertTrue(configurationTemplate instanceof Configuration);
							UUID gotPolimorphicUUID = configurationTemplate.getID();
							Assert.assertTrue(gotPolimorphicUUID.compareTo(expectedUUID)==0);
						}else {
							Set<UUID> gotUUIDs = new HashSet<>();
							for(ConfigurationTemplate c : listPolimorphic) {
								Assert.assertTrue(c instanceof Configuration);
								gotUUIDs.add(c.getID());
							}
							
							Assert.assertTrue(gotUUIDs.containsAll(expectedUUIDFor2Results));
							Assert.assertTrue(expectedUUIDFor2Results.containsAll(gotUUIDs));
							
							
						}
						
						
						
					}
					
				}
			}
		} finally {
			for(Configuration configuration : createdConfigurations) {
				deleteResource(configuration);
			}
		}

	}

}
