package org.gcube.informationsystem.resourceregistry.queries.templates;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.gcube.com.fasterxml.jackson.databind.JavaType;
import org.gcube.com.fasterxml.jackson.databind.JsonNode;
import org.gcube.com.fasterxml.jackson.databind.ObjectMapper;
import org.gcube.com.fasterxml.jackson.databind.node.ObjectNode;
import org.gcube.informationsystem.base.reference.IdentifiableElement;
import org.gcube.informationsystem.model.reference.properties.Metadata;
import org.gcube.informationsystem.queries.templates.impl.entities.QueryTemplateImpl;
import org.gcube.informationsystem.queries.templates.impl.properties.TemplateVariableImpl;
import org.gcube.informationsystem.queries.templates.reference.entities.QueryTemplate;
import org.gcube.informationsystem.queries.templates.reference.properties.TemplateVariable;
import org.gcube.informationsystem.resourceregistry.ContextTest;
import org.gcube.informationsystem.resourceregistry.api.exceptions.AlreadyPresentException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.queries.templates.QueryTemplateAlreadyPresentException;
import org.gcube.informationsystem.serialization.ElementMapper;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class QueryTemplateManagementTest extends ContextTest {
	
	private static Logger logger = LoggerFactory.getLogger(QueryTemplateManagementTest.class);
	
	public static final String QUERY_TEMPLATE_NAME = "Test";
	public static final String QUERY_TEMPLATE_DESCRIPTION = "A Test Query Template";
	public static final String QUERY_TEMPLATE_DESCRIPTION_UPDATED = "A Test Query Template UPDATED";
	
	public static final String STATE_VARIABLE_NAME = "$state";
	public static final String NAME_VARIABLE_NAME = "$name";
	public static final String GROUP_VARIABLE_NAME = "$group";
	
	public static void checkUUIDAndMetadata(IdentifiableElement previous, IdentifiableElement got) {
		Assert.assertTrue(got.getID()!= null);
		if(previous.getID()!=null) {
			Assert.assertTrue(got.getID().compareTo(previous.getID()) == 0);
		}
		
		Metadata gotMetadata = got.getMetadata();
		Metadata previousMetadata = previous.getMetadata();
		
		Assert.assertTrue(gotMetadata != null);
		
		String user = ContextTest.getUser();
		Assert.assertTrue(gotMetadata.getLastUpdateBy().compareTo(user) == 0);
		
		if(previousMetadata != null) {
			
			Assert.assertTrue(gotMetadata.getCreatedBy().compareTo(user) == 0);
			Assert.assertTrue(gotMetadata.getCreatedBy().compareTo(previousMetadata.getCreatedBy()) == 0);
			Assert.assertTrue(gotMetadata.getCreationTime().compareTo(previousMetadata.getCreationTime()) == 0);
		} 
		
		Assert.assertFalse(gotMetadata.getCreationTime().after(gotMetadata.getLastUpdateTime()));
		
	}
	
	protected void assertions(QueryTemplate expected, QueryTemplate got) {
		checkUUIDAndMetadata(expected, got);
		Assert.assertTrue(expected.getName().compareTo(got.getName()) == 0);
		Assert.assertTrue(expected.getDescription().compareTo(got.getDescription()) == 0);
		// Template and TemplateVariable should be compared
	}
	
	protected QueryTemplate create(QueryTemplate queryTemplate) throws ResourceRegistryException, IOException {
		//list();
		
		QueryTemplateManagement queryTemplateManagement = new QueryTemplateManagement();
		queryTemplateManagement.setName(QUERY_TEMPLATE_NAME);
		queryTemplateManagement.setForceIncludeMeta(true);
		String json = ElementMapper.marshal(queryTemplate);
		queryTemplateManagement.setJson(json);
		String created = queryTemplateManagement.create();
		logger.info("Created {} is {}", QueryTemplate.NAME, created);
		QueryTemplate createdQueryTemplate = ElementMapper.unmarshal(QueryTemplate.class, created);
		
		assertions(queryTemplate, createdQueryTemplate);
		// list(createdQueryTemplate);
		return createdQueryTemplate;
	}

	protected QueryTemplate read(QueryTemplate queryTemplate) throws ResourceRegistryException, IOException {
		list(queryTemplate);
		
		QueryTemplateManagement queryTemplateManagement = new QueryTemplateManagement();
		queryTemplateManagement.setName(QUERY_TEMPLATE_NAME);
		String read = queryTemplateManagement.read();
		logger.info("Read {} is {}", QueryTemplate.NAME, read);
		QueryTemplate readQueryTemplate = ElementMapper.unmarshal(QueryTemplate.class, read);
		
		assertions(queryTemplate, readQueryTemplate);
		return queryTemplate;
	}
	
	protected void run() throws Exception {
		QueryTemplateManagement queryTemplateManagement = new QueryTemplateManagement();
		queryTemplateManagement.setName(QUERY_TEMPLATE_NAME);
		queryTemplateManagement.run();
		
		ObjectMapper objectMapper = new ObjectMapper();
		ObjectNode params = objectMapper.createObjectNode();
		params.put(STATE_VARIABLE_NAME, "running");
		params.put(GROUP_VARIABLE_NAME, "DataAccess");
		params.put(NAME_VARIABLE_NAME, "StorageHub");
		queryTemplateManagement.setParams(params);
		queryTemplateManagement.run();
	}
	
	protected QueryTemplate update(QueryTemplate queryTemplate) throws ResourceRegistryException, IOException {
		list(queryTemplate);
		
		queryTemplate.setDescription(QUERY_TEMPLATE_DESCRIPTION_UPDATED);
		
		QueryTemplateManagement queryTemplateManagement = new QueryTemplateManagement();
		queryTemplateManagement.setName(QUERY_TEMPLATE_NAME);
		String json = ElementMapper.marshal(queryTemplate);
		queryTemplateManagement.setJson(json);
		String updated = queryTemplateManagement.update();
		logger.info("Updated {} is {}", QueryTemplate.NAME, updated);
		QueryTemplate updatedQueryTemplate = ElementMapper.unmarshal(QueryTemplate.class, updated);
		
		assertions(queryTemplate, updatedQueryTemplate);
		list(updatedQueryTemplate);
		
		// Restoring Previous description on queryTemplate
		queryTemplate.setDescription(QUERY_TEMPLATE_DESCRIPTION);
		return updatedQueryTemplate;
	}
	
	
	protected QueryTemplate invalidUpdate(QueryTemplate queryTemplate) throws ResourceRegistryException, Exception {
		list(queryTemplate);
		
		queryTemplate.setName("AnotherName");
		QueryTemplateManagement queryTemplateManagement = new QueryTemplateManagement();
		queryTemplateManagement.setName(QUERY_TEMPLATE_NAME);
		String json = ElementMapper.marshal(queryTemplate);
		queryTemplateManagement.setJson(json);
		try {
			queryTemplateManagement.update();
			throw new Exception("It should not be possibile to update the " + QueryTemplate.NAME + " with json name different from the name used to identify the instance");
		}catch (ResourceRegistryException e) {
			// OK
			logger.debug("As expected {}", e.getMessage());
			// Restoring Previous name on queryTemplate
			queryTemplate.setName(QUERY_TEMPLATE_NAME);
		}
		
		list(queryTemplate);
		return queryTemplate;
	}
	
	protected boolean delete(QueryTemplate queryTemplate) throws ResourceRegistryException {
		list(queryTemplate);
		
		QueryTemplateManagement queryTemplateManagement = new QueryTemplateManagement();
		queryTemplateManagement.setName(QUERY_TEMPLATE_NAME);
		queryTemplateManagement.delete();
		
		logger.debug("Deleted {} with name {}", QueryTemplate.NAME, queryTemplate.getName());
		list();
		return true;
	}
	
	protected void list(QueryTemplate...queryTemplates) throws ResourceRegistryException {
		QueryTemplateManagement queryTemplateManagement = new QueryTemplateManagement();
		String allString = queryTemplateManagement.all(false);
		JavaType type = ElementMapper.getObjectMapper().getTypeFactory().constructCollectionType(ArrayList.class, QueryTemplate.class);
		List<QueryTemplate> all = null;
		try {
			all = ElementMapper.getObjectMapper().readValue(allString, type);
		}catch (Exception e) {
			throw new RuntimeException(e);
		}
		if(queryTemplates==null) {
			Assert.assertTrue(all.size()==0);
			return;
		}
		Assert.assertTrue(all.size()==queryTemplates.length);
		for(QueryTemplate expected : queryTemplates) {
			String expectedName = expected.getName();
			boolean found = false;
			for(QueryTemplate got : all) {
				String gotName = got.getName();
				if(gotName.compareTo(expectedName)==0) {
					found = true;
					assertions(expected, got);
					break;
				}
			}
			Assert.assertTrue(found);
		}
	}
	
	protected void invalidCreate(QueryTemplate queryTemplate) throws ResourceRegistryException, IOException {
		try {
			QueryTemplate recreatedQueryTemplate = create(queryTemplate);
			throw new RuntimeException(ElementMapper.marshal(recreatedQueryTemplate) + " was created successfully. This is not what we expected");
		} catch(AlreadyPresentException e) {
			logger.debug("As expected {} cannot be created.", ElementMapper.marshal(queryTemplate));
		}
	}
	
	public File getQueryTemplatesDirectory() throws Exception {
		URL logbackFileURL = ContextTest.class.getClassLoader().getResource("logback-test.xml");
		File logbackFile = new File(logbackFileURL.toURI());
		File resourcesDirectory = logbackFile.getParentFile();
		return new File(resourcesDirectory, "queryTemplates");
	}
	
	protected QueryTemplate getQueryTemplate(String name, JsonNode jsonNode) {
		QueryTemplate queryTemplate = new QueryTemplateImpl();
		queryTemplate.setName(name);
		queryTemplate.setDescription(QUERY_TEMPLATE_DESCRIPTION);
		queryTemplate.setTemplate(jsonNode);
		
		TemplateVariable stateTemplateVariable = new TemplateVariableImpl();
		stateTemplateVariable.setName(STATE_VARIABLE_NAME);
		stateTemplateVariable.setDescription("StateFacet value");
		stateTemplateVariable.setDefaultValue("running");
		stateTemplateVariable.setPropertyType("String");
		queryTemplate.addTemplateVariable(stateTemplateVariable);
		
		TemplateVariable nameTemplateVariable = new TemplateVariableImpl();
		nameTemplateVariable.setName(NAME_VARIABLE_NAME);
		nameTemplateVariable.setDescription("SoftwareFacet name");
		nameTemplateVariable.setDefaultValue("resource-registry");
		nameTemplateVariable.setPropertyType("String");
		queryTemplate.addTemplateVariable(nameTemplateVariable);
		
		TemplateVariable groupTemplateVariable = new TemplateVariableImpl();
		groupTemplateVariable.setName(GROUP_VARIABLE_NAME);
		groupTemplateVariable.setDescription("SoftwareFacet group");
		groupTemplateVariable.setDefaultValue("information-system");
		groupTemplateVariable.setPropertyType("String");
		queryTemplate.addTemplateVariable(groupTemplateVariable);
		
		return queryTemplate;
	}
	
	protected JsonNode getTemplate() throws Exception {
		File queryTemplatesDirectory = getQueryTemplatesDirectory();
		File jsonQueryTemplateFile = new File(queryTemplatesDirectory, "queryTemplate1.json");
		ObjectMapper objectMapper = new ObjectMapper();
		JsonNode jsonNode = objectMapper.readTree(jsonQueryTemplateFile);
		return jsonNode;
	}
	
	protected QueryTemplate getQueryTemplate() throws Exception {
		JsonNode jsonNode = getTemplate();
		QueryTemplate queryTemplate = getQueryTemplate(QUERY_TEMPLATE_NAME, jsonNode);
		String json = ElementMapper.marshal(queryTemplate);
		logger.info("Marshalled {} - {}", QueryTemplate.NAME, json);
		return queryTemplate;
	}
	
	// @Test
	public void readTest() throws Exception {
		QueryTemplateManagement queryTemplateManagement = new QueryTemplateManagement();
		queryTemplateManagement.setName(QUERY_TEMPLATE_NAME);
		String read = queryTemplateManagement.read();
		logger.info("Read {} is {}", QueryTemplate.NAME, read);
		QueryTemplate readQueryTemplate = ElementMapper.unmarshal(QueryTemplate.class, read);
		String json = ElementMapper.marshal(readQueryTemplate);
		logger.info("Read {}: {}", QueryTemplate.NAME, json);
	}
	
	@Test
	public void completeTest() throws Exception {
		try {
			QueryTemplate queryTemplate = getQueryTemplate();
						
			QueryTemplate createdQueryTemplate = create(queryTemplate);
			
			QueryTemplate readQueryTemplate = read(createdQueryTemplate);
			
			QueryTemplate updatedQueryTemplate = update(readQueryTemplate);
			
			updatedQueryTemplate = invalidUpdate(updatedQueryTemplate);
			
			run();
			
			QueryTemplate readUpdatedQueryTemplate = read(updatedQueryTemplate);
			
			delete(readUpdatedQueryTemplate);
		}catch (Throwable e) {
			QueryTemplateManagement queryTemplateManagement = new QueryTemplateManagement();
			queryTemplateManagement.setName(QUERY_TEMPLATE_NAME);
			queryTemplateManagement.delete();
		}
		logger.debug("The DB should be now clean");
	}
	
	@Test(expected = QueryTemplateAlreadyPresentException.class)
	public void doubleCreateTest() throws Exception {
		QueryTemplate queryTemplate = getQueryTemplate();
		try {
			create(queryTemplate);
			logger.info("Created {} with name '{}'", QueryTemplate.NAME, queryTemplate.getName());
			create(queryTemplate);
			throw new Exception("The second create should fails. You should not be here");
		} catch (QueryTemplateAlreadyPresentException e) {
			logger.info("As expected {} with name '{}' already exists", QueryTemplate.NAME, queryTemplate.getName());
			throw e;
		}finally {
			QueryTemplateManagement queryTemplateManagement = new QueryTemplateManagement();
			queryTemplateManagement.setName(QUERY_TEMPLATE_NAME);
			queryTemplateManagement.delete();
			logger.info("{} with name '{}' successfully deleted", QueryTemplate.NAME, queryTemplate.getName());
			logger.debug("The DB should be now clean");
		}
	}
	
	@Test
	public void createUpdateTest() throws Exception {
		QueryTemplate queryTemplate = getQueryTemplate();
		try {
			QueryTemplate createdQueryTemplate = create(queryTemplate);

			// test no UUID
			logger.info("Created {} with name '{}'", QueryTemplate.NAME, createdQueryTemplate.getName());
			QueryTemplateManagement queryTemplateManagement = new QueryTemplateManagement();
			queryTemplateManagement.setName(QUERY_TEMPLATE_NAME);
			queryTemplateManagement.setForceIncludeMeta(true);
			String updatedDescription = "My 1st udpated description";
			queryTemplate = getQueryTemplate();
			queryTemplate.setDescription(updatedDescription);
			String json = ElementMapper.marshal(queryTemplate);
			queryTemplateManagement.setJson(json);
			String updated = queryTemplateManagement.createOrUpdate();
			logger.info("Updated {} is {}", QueryTemplate.NAME, updated);
			QueryTemplate updatedQueryTemplate = ElementMapper.unmarshal(QueryTemplate.class, updated);
			Assert.assertTrue(updatedQueryTemplate.getDescription().compareTo(updatedDescription)==0);
			
			// test UUID null
			queryTemplateManagement = new QueryTemplateManagement();
			queryTemplateManagement.setName(QUERY_TEMPLATE_NAME);
			queryTemplateManagement.setForceIncludeMeta(true);
			updatedDescription = "My 2nd udpated description";
			queryTemplate = getQueryTemplate();
			queryTemplate.setDescription(updatedDescription);
			queryTemplate.setID(null);
			json = ElementMapper.marshal(queryTemplate);
			queryTemplateManagement.setJson(json);
			updated = queryTemplateManagement.createOrUpdate();
			logger.info("Updated {} is {}", QueryTemplate.NAME, updated);
			updatedQueryTemplate = ElementMapper.unmarshal(QueryTemplate.class, updated);
			Assert.assertTrue(updatedQueryTemplate.getDescription().compareTo(updatedDescription)==0);
			
			// test UUID same
			queryTemplateManagement = new QueryTemplateManagement();
			queryTemplateManagement.setName(QUERY_TEMPLATE_NAME);
			queryTemplateManagement.setForceIncludeMeta(true);
			updatedDescription = "My 3rd udpated description";
			queryTemplate = getQueryTemplate();
			queryTemplate.setDescription(updatedDescription);
			queryTemplate.setID(createdQueryTemplate.getID());
			json = ElementMapper.marshal(queryTemplate);
			queryTemplateManagement.setJson(json);
			updated = queryTemplateManagement.createOrUpdate();
			logger.info("Updated {} is {}", QueryTemplate.NAME, updated);
			updatedQueryTemplate = ElementMapper.unmarshal(QueryTemplate.class, updated);
			Assert.assertTrue(updatedQueryTemplate.getDescription().compareTo(updatedDescription)==0);
			
			
			// test another UUID
			queryTemplateManagement = new QueryTemplateManagement();
			queryTemplateManagement.setName(QUERY_TEMPLATE_NAME);
			queryTemplateManagement.setForceIncludeMeta(true);
			updatedDescription = "My 4th udpated description";
			queryTemplate = getQueryTemplate();
			queryTemplate.setDescription(updatedDescription);
			queryTemplate.setID(UUID.randomUUID());
			json = ElementMapper.marshal(queryTemplate);
			queryTemplateManagement.setJson(json);
			try {
				updated = queryTemplateManagement.createOrUpdate();
				throw new Exception("Trying to udpate the " + QueryTemplate.NAME + " using a different " + QueryTemplate.ID_PROPERTY + " must fails. Please check your code");
			}catch (ResourceRegistryException e) {
				logger.info("Trying to update {} with name '{}' failed as expected : {} ", QueryTemplate.NAME, queryTemplate.getName(), e.getMessage());
			}			
		}finally {
			QueryTemplateManagement queryTemplateManagement = new QueryTemplateManagement();
			queryTemplateManagement.setName(QUERY_TEMPLATE_NAME);
			queryTemplateManagement.delete();
			logger.info("{} with name '{}' successfully deleted", QueryTemplate.NAME, queryTemplate.getName());
			logger.debug("The DB should be now clean");
		}
		
	}
	
}
